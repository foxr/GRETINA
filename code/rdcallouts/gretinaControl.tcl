##
# Package that provides access to Gretina's run control.
#
# 

package provide gretinaRctl 1.0
package require epics

namespace eval gretina {
    variable state "--undefined--"
    variable stateChannel
    variable controlChannel
    variable GEBHeaders
}


## 
# Establish state as a monitor for 
#  Cluster_CV_State
#

epicschannel Cluster_CV_State
Cluster_CV_State link ::gretina::state
set gretina::stateChannel Cluster_CV_State

##
# Establish the channel for run control
#

epicschannel Online_CS_StartStop
set gretina::controlChannel Online_CS_StartStop

epicschannel Trig0_CS_PC2_12;	# Single imperative synch

epicschannel Data_CS_RunDir;	# Run number.
epicschannel Data_CS_Dir;	# Run directory.
epicschannel GEB_CV_Events;;	# GEB Header rate(?)
GEB_CV_Events  link ::gretina::GEBHeaders


##
# Get current cluster state
#
proc gretina::getState {} {
    return $::gretina::state
}

##
# Start a run:
#
proc gretina::start {} {
    $::gretina::controlChannel set 1
}
##
# Stop a run:
#
proc gretina::stop {} {
    $::gretina::controlChannel set 0
}
##
# Send an imperative clock sync.
#
proc gretina::sendImpSynch {} {
    Trig0_CS_PC2_12 set 1
}
##
# Get the run from Gretina:
#
proc gretina::runNumber {} {
    set runarray [Data_CS_RunDir get]
    set runDirString [::gretina::_ListToString $runarray]
    set run  [string trimleft [string range $runDirString 3 end] 0]
    #
    #   Special case;  If the run number is 0, the trimleft will
    #   leave us with an empty string:
    #
    if {$run eq ""} {
	set run 0
    }
    return $run

}
##
# Get the directory in which gretina will store the run file.
#

proc gretina::runDir {} {
    set dirarray [Data_CS_Dir get]
    return [::gretina::_ListToString $dirarray]
}

#----------------------------------------------------------------------------------
##
# Turn a list of ascii character codes into a string
# 
# @param characters - list of characters.
#
# @return string - the converted string.
#
proc gretina::_ListToString characters {
    foreach char $characters {
	if {$char == 0} {
	    break
	}
	#
	# Evidently now we get trash in the top bits.
	#
	set char [expr {$char & 0xff}]
	append result [format %c $char]
    }
    return $result
}
