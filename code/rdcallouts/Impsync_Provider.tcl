#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
    exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#            NSCL
#            Michigan State University

##
# @file Impsync_Provider.tcl
# @brief Data source provider to produce GRETINA impsyncs at begin.
#

package provide Impsync_Provider 1.0
package require gretinaRctl;	# Has impsync commands.

namespace eval Impsync {	#  Create the namespace we'll use
}

proc Impsync::parameters {} {
    return [dict create];	# no parameters
}

##
#  We're going to allow any number of these beasts
# an it's not really important to know which is which.
#
proc Impsync::start parameters {}

##
#  the source is always alive:

proc Impsync::check id { return 1}

##
# begin is when we do the sync:
#
proc Impsync::begin {id run title} {
    gretina::sendImpSynch
}

## all the other state transitions are noops:

proc Impsync::pause id {}
proc Impsync::resume id {}
proc Impsync::end id {}

proc Impsync::stop {} {}

proc Impsync::capabilities {} {
    return [dict create                            \
		canPause false                             \
		runsHaveTitles  true                           \
		runsHaveNumbers true                       \
	       ]
}
