#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


##
# @file gretinaGlomProvider.tcl
# @brief Provider to create/manage a tapcat | glom pipline.
# @author Ron Fox <fox@nscl.msu.edu>
#

package provide GretinaTapGlom_Provider 1.0

lappend auto_path [file dirname [info script]]
package require GretinaTapGlom_Prompter

lappend auto_path [file join $::env(DAQROOT) TclLibs]
package require ReadoutGUIPanel


namespace eval GretinaTapGlom {
    variable instances;                   # Provider instances.
    array set instances [list]
}

##
# Note we're only going to allow a single tap|glom pipeline because I don't
# see a use case (at this moment) for more than 1.  The checks that enforce
# that can be easily removed.


##
# parameters
#    @return dictionary - the parameterization dictionary for provider
#                         instances
#
proc GretinaTapGlom::parameters {} {
    return $GretinaTapGlom::parameterization
}

##
# start
#   Start a provider... This method has the code that ensures a single
#   instance if you want to remove that restriction.
#
# @param params - The data source parameterization.
#

proc GretinaTapGlom::start params {
    if {[array size GretinaTapGlom::instances] > 0} {
        error "GRETINAGlom provider only supports one provider"
    }
    set id [dict get $params sourceid]
    dict set params fd "" eofexpected false;  # For output capture.

    # Create the ring if necessary:

    set ringName [dict get $params ring]
    catch {exec $::env(DAQBIN)/ringbuffer create $ringName}
    
    set GretinaTapGlom::instances($id) $params
    
}
##
# check
#   Since there are no persistent processes (The tap exits when
#   data taking stops), returns true always.
#
#  @return bool always true because we're always alive.
#
proc GretinaTapGlom::check id {
    return 1
}
##
# stop
#  Just removes the provider from instances.
#  This is written as if multiple instances are allowed.
#   @param id - id of the provider to stop.
#
proc GretinaTapGlom::stop id {
    if {[array names GretinaTapGlom::instances $id] eq ""} {
        error "GRETINAGlom provider has no instance $id"
    }
    
    array unset GretinaTapGlom::instances $id
}
##
# begin
#    Begins a run with a data source.
#   - Figures out the node in which the GEB is running (GEBnode).
#   - Creates the command to run the tapcat |glom pipeline
#   - Opens it on a pipe that's readable with a file event to
#     capture output/stderr and ship it off to the ReadoutGui.
#
# @param id - id of the source to start.
# @param run - Run number [ignored].
# @param title -Run title [ignored].
#
proc  GretinaTapGlom::begin {id run title} {
    if {[array names GretinaTapGlom::instances $id] eq ""} {
        error "GRETINAGlom provider has no instance $id"
    }
    
    set params $GretinaTapGlom::instances($id)
    set host  [exec -- [file join  $GretinaTapGlom::gretinabin gebnode ]]
    
    set command [file join $GretinaTapGlom::gretinabin tapcat]
    append command " " [GretinaTapGlom::computeTapcatOptions $params]
    append command " | "
    append command [file join $GretinaTapGlom::gretinabin glom]
    append command " " [GretinaTapGlom::computeGlomOptions $params] " | "
    set stdintoring [list [file join $::env(DAQBIN) stdintoring] [dict get $params ring]]
    append command $stdintoring
    append command " |& cat " ;                  # Captures output and error to pipe.
    puts "Starting tap pipeline: '$command'"
    
    set fd [open "| $command" r]
    
    dict set params fd $fd
    dict set params eofexpected false
    set GretinaTapGlom::instances($id) $params
    
    fconfigure $fd -buffering line
    fileevent $fd readable [list GretinaTapGlom::_onInput $id]


}

##
#  pause/resume are no-ops since its not supported and therefore
#  should never happen.

proc GretinaTapGlom::pause id {}
proc GretinaTapGlom::resume id {}

##
#  end
#   We just need to indicate that it's ok to get an EOF on the
#   pipeline because when the run ends and all data are flushed,
#   the GEB will drop the connection to the tapcat.
#
#  @param id   - id of the data source to end.
#
proc GretinaTapGlom::end id {
    if {[array names GretinaTapGlom::instances $id] eq ""} {
        error "GRETINAGlom provider has no instance $id"
    }
    set params $GretinaTapGlom::instances($id)
    dict set params eofexpected true
    set GretinaTapGlom::instances($id) $params
}

##
# init
#   -  Set the data source so that eof on fd is ok.
#   -  Kill this source's tapcat processes in this system to
#      try to bring any of them down.
#
# @param id - id of the data source to init.
#
proc GretinaTapGlom::init id {
    if {[array names GretinaTapGlom::instances $id] eq ""} {
        error "GRETINAGlom provider has no instance $id"
    }
    set params $GretinaTapGlom::instances($id)
    set fd     [dict get $params fd]
    set pids   [pid $fd]
    
    dict set params eofexpected true
    set GretinaTapGlom::instances($id) $params
    
    foreach p $pids {
        catch {kill -9 $p}
    }
}
##
# capabilities
#    Return the capabilities dict:
#    -   canPause false - gretina does not pause.
#    -   runsHaveTitles true - while gretina does not have titles,
#                              we don't want to disable the NSCLDAQ titles.
#    -   runHaveNumbers true - Gretina runs do have numbers.
#
proc GretinaTapGlom::capabilities {} {
    return [dict create                                \
        canPause   false                              \
        runsHaveTitles true                           \
	runsHaveNumbers true                            \
    ]
}

##
# _onInput
#   Called when a data source's pipe is readable.
#
# @param id - the id of the readable source.
# @note - input is output to the ReadoutGUI's output window.
# @note - an unexpected EOF is flagged with a popup.
#
proc GretinaTapGlom::_onInput id {
    if {[array names GretinaTapGlom::instances $id] eq ""} {
        error "GRETINAGlom provider has no instance $id"
    }
    set params $GretinaTapGlom::instances($id)
    set fd [dict get $params fd]
    
    if {[eof $fd]} {
        set expected [dict get $params eofexpected]
        if {!$expected} {
            tk_messageBox -title {Unexpected EOF on pipe} \
                -icon error -type ok                      \
                -message "tap|glom pipe exited unexpectedly."
        }
        catch {close $fd};             # stderr makes this error too.
        dict set params fd ""
        set GretinaTapGlom::instances($id) $params.
    } else {
        set line [gets $fd]
        set time [clock format [clock seconds]]
        set line "(Gretina Tap pipeline) $time - $line"
        ::ReadoutGUIPanel::outputText $line
    }
}
##
# computeTapcatOptions
#   Given the configuration parameters, compute/return the options to tapcat.
#
# @param params  - dict of configuration.
# @return list   - command line options to pass to tapcat.
#
proc  GretinaTapGlom::computeTapcatOptions params {
    set gebhostpgm [file join $::env(GRBIN) gebnode]
    set gebhost    [exec $gebhostpgm]
    set typemask   [dict get $params type]
    set grouping   [dict get $params grouping]

    return [list --host $gebhost --type $typemask --grouping $grouping]
}

##
# computeGlomOptions
#   Given the configuration parameters, compute/return the options to gretina glom.
#
# @param params - configuration dict.
# @return list - command line options to pass to gretina glom
#
proc  GretinaTapGlom::computeGlomOptions params {
    set dt [dict get $params dt]
    set ctype [dict get $params controltype]
    set stype [dict get $params scalertype]
    set tag   [dict get $params tag]

    return [list --dt $dt --controltype $ctype --scalertype $stype --tag $tag]
}
