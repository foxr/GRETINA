#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Jeromy Tompkins 
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



##
#  @file   Vwait.tcl
#  @brief  vwait with timeout.

package provide timedVwait 1.0

package require snit


##
#  @class VWait
#  
#   class that implements vwaits with timeout.
#
# OPTIONS
#    -variable - Fully namespace scoped path to the variable name.
#    -timeout  -  milliseconds to wait for a change (defaults to 100).
#
#  METHODS:
#     wait
#
#
#  @note  Modifying -variable or -timeout during a wait has no effect until the
#         next wait.


snit::type VWait {
    option   -variable
    option   -timeout

    variable timerId -1                ;#   Id of the after timer used to timeout.
    variable waitvar  0                ;#   Variable we'll vwait on.


    #-----------------------------------------------------------------------
    # Public methods
    #
    constructor args {

	$self configurelist $args
    }

    destructor {
	#
	#   If the vwait is active, we need to kill the timer and cancel the trace.
	#
	if {$timerId > 0} {
	    after cancel $timerId
	    trace remove variable $options(-variable) [list write] [mymethod _traceHandler]
	    incr waitvar            ;#  Release the wait.
	}
    }

    ##
    # wait
    #   Wait either for a timeout or the variable to be modified.
    #
    #  @return bool - true if the variable was modified false if timed out.
    #
    method wait {} {
	
	#  Set the timeout and trace.

	set timerId [after $options(-timeout) incr [myvar waitvar]]
	trace add variable $options(-variable) [list write] [mymethod _traceHandler]

	# Wait for one or the other to fire:

	vwait [myvar waitvar]

	# If timed out timerId is not -1.

	if {$timerId != -1} {
	    trace remove variable $options(-variable) [list write] [mymethod _traceHandler]
	    return 0
	} else {
	    return 1                       ; # trace handler cleaned up for us
	}


    }
    #-------------------------------------------------------------------------
    #   Private methods
    #

    ##
    # _traceHandler
    #    Invoked when the variable is modified while vwaiting.
    #
    method _traceHandler {args} {
	after cancel $timerId
	set timerId -1
	trace remove variable $options(-variable) [list write] [mymethod _traceHandler]

	incr waitvar                     ; # release the vwait.


    }


    

}