#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


##
# @file gretinaglomPrompter.tcl
# @brief Prompter for GRETINA tapcat | glom  pipe.
# @author Ron Fox <fox@nscl.msu.edu>
#

##
# GRETINA provide a tap of GEB fragments.  We an pipe this into
# the GRETINA Glom program to get event built data from the tap.
# In the original S800/Gretina integration, the integration was a monster
# monolith.  To better support aux. detectors, I've been pulling each chunk
# of parameterized GRETINA interface code  into a data source provider.
#
#  Thi provider handles the tapcat |glom pipeline.

package provide GretinaTapGlom_Prompter 1.0
package require Tk
package require snit


lappend auto_path [file join $::env(DAQROOT) TclLibs]
package require  DataSourceUI

##
#  Note the namespace below will be the name of the provider.

namespace eval GretinaTapGlom {
    variable daqbin $::env(DAQBIN)
    variable gretinabin $::env(GRBIN)
    
    #  This dict provides the "data source" parameterization.
    #  Note the host is gotten prior to each node from EPICS.
    
    variable parameterization [dict create                             \
        type [list {Mask of detector types to get in tap}]        \
        grouping [list {Number of consecutive events from tap}]   \
        timeout  [list {Timeout for a tap read}]                  \
        dt      [list {glom coincidence interval}]               \
        controltype [list {Type of control items}]               \
        scalertype  [list {Type of scaler items}]                \
        tag     [list {Event tag to use}]                        \
        ring   [list {Output Ring Buffer}]                       \
    ]
    
    #  Maps from parameter keys to prompter options:
    
    variable parameterMap
    array set parameterMap [list                               \
        type       -type                                       \
        grouping   -grouping                                   \
        timeout    -timeout                                    \
        dt         -window                                     \
        controltype -ctltype                                   \
        scalertype  -scltype                                   \
        tag         -evtag                                     \
        ring        -ring
    ]
    
}

##
# @class GretinaTapGlom::PromptForm
#
#    This megawidget provides a prompter for all of the parameters needed
#    to run the tap | glom pipeline.
#
snit::widgetadaptor  GretinaTapGlom::PromptForm {
    option -type       -default 0xfffffffd;    # Amlmost everything.
    option -grouping   -default 100;           # units of fragments.
    option -timeout    -default 1.0;           # units of seconds
    option -window     -default 1
    option -ctltype    -default 6
    option -scltype    -default 10
    option -evtag      -default 0x66eb;        # G GEB as best as hex can do.
    option -ring
    
    
    ##
    #  Really the constructor does all the work.
    #
    constructor args {
        installhull using ttk::frame;           # Use a ttk frame hull - looks better.
        
        ttk::label $win.tapcatlabel -text {Parameters that control the TAP client}
        
        ttk::label $win.masklbl -text {Mask of received types}
        ttk::entry $win.mask    -textvariable [myvar options(-type)]
        
        ttk::label $win.grplabel -text {Consecutive frags}
        ttk::spinbox $win.grouping -from 10 -to 10000 -textvariable [myvar options(-grouping)]
        
        ttk::label $win.tolabel -text {Read timeout}
        ttk::spinbox $win.timeout -from 1 -to 60 -textvariable [myvar options(-timeout)]
        
        ttk::label $win.glomlabel -text {Parameters that control event building}
        
        ttk::label $win.winlabel -text {Concidence window}
        ttk::spinbox $win.win    -from 1 -to 10000 -textvariable [myvar options(-window)]
        
        ttk::label $win.ctllabel -text {Control data type}
        ttk::entry $win.ctltype  -textvariable [myvar options(-ctltype)]
        
        ttk::label $win.scllabel -text {Scaler data type}
        ttk::entry $win.scltype  -textvariable [myvar options(-scltype)]

        ttk::label $win.taglbl -text {Event tag}
        ttk::entry $win.tag    -textvariable [myvar options(-evtag)]
        
        ttk::label $win.ringlbl -text {Ring Buffer (not URI)}
        ttk::entry $win.ring   -textvariable [myvar options(-ring)] -width 32
        
        grid $win.tapcatlabel - - - - - - -
        grid $win.masklbl $win.mask $win.grplabel $win.grouping $win.tolabel \
            $win.timeout -sticky w
        
        grid $win.glomlabel - - - - - - -
        grid $win.winlabel $win.win $win.ctllabel $win.ctltype $win.scllabel \
            $win.scltype $win.taglbl $win.tag -sticky w
        
        grid $win.ringlbl $win.ring - - - - - - -sticky w
        
        set options(-ring) $::tcl_platform(user); #       Default ring.
        
        $self configurelist $args
    }
}

##
# validate
# @param parameters - parameter dict.
# @return string - error messages for validation fails
# @retval ""   - For success.
#
proc GretinaTapGlom::validate parameters {
    set result ""
    
    # Integers that must be integers > 0
    
    foreach key [list type grouping dt] {
        set meaning [lindex [dict get $GretinaTapGlom::parameterization $key] 0]
        set proposed [lindex [dict get $parameters $key] end]
        if {![string is integer -strict $proposed] || ($proposed <= 0)} {
            append result "$meaning must be an integer > 0 was '$proposed'\n"
        }
    }
    set timeout [lindex [dict get $parameters timeout] end]
    if {![string is double -strict $timeout] || ($timeout <= 0.0)} {
        append result [lindex [dict get $GretinaTapGlom::parameterization $key] 0]
        append result " must be floating point seconds > 0.0 was '$timeout'\n"
    }
    foreach key [list controltype scalertype] {       # integer in range: [0,32).
        set meaning [lindex [dict get $GretinaTapGlom::parameterization $key] 0]
        set proposed [lindex [dict get $parameters $key] end]
        if {($proposed < 0) || ($proposed > 31)} {
            append result "$meaning must be an integer in the range \[0,32) was '$proposed'";
        }
    }
    set tag [lindex [dict get $parameters tag] end]
    set meaning [lindex [dict get $::GretinaTapGlom::parameterization tag] 0]
    if {($tag < 0) || ($tag > 0xffff)} {
        append result "$meaning must be a 16 bit unsigned integer"
    }
    
    
    return $result
}

##
# promptParameters
#    Display the prompt form inside a dialog wrapper.
#    Accept and validate the parameters from the dialog.
#    If all's good, return this all as a parameterization dict for a
#    GretinaTapGlom data source.
#
# @return dictionary - parameterization of a GretinaTapGlom data source.
# @retval empty      - If either the user cancelled or there was a validation error.
#
proc GretinaTapGlom::promptParameters {} {
    destroy .grtapglomtoplevel
    set result [dict create]
    
    set top [toplevel .grtapglomtoplevel]
    set dlg [DialogWrapper $top.dialog]
    set container [$dlg controlarea]
    
    set form [GretinaTapGlom::PromptForm $container.form]
    $dlg configure -form $form
    
    pack $dlg -fill both -expand 1
    
    set response [$dlg modal]
    if {$response eq "Ok"} {
        set result $GretinaTapGlom::parameterization
        dict for {key value} $result {
            set val [$form cget $GretinaTapGlom::parameterMap($key)]
            dict lappend result $key [list] [string trim $val]
        }
                set validationErrors [GretinaTapGlom::validate $result]
        if {$validationErrors ne ""} {
            tk_messageBox -icon error -parent $top -title {Validation Errors} \
                -type ok -message $validationErrors
            set result ""
        }
    }
    
    destroy $top
    return $result
}
