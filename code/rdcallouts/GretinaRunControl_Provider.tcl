#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
    exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#            NSCL
#            Michigan State University#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


##
# @file GretinaRunControlProvider.tcl
# @brief Data 'source' provider to control GRETINA runs.
# @author Ron Fox <fox@nscl.msu.edu>
#

package provide GretinaRunControl_Provider 1.0

package require Tk
package require snit

lappend auto_path [file join $::env(DAQROOT) TclLibs]
lappend auto_path [file join $::env(GRTCLLIBS)]
lappend auto_path $::env(EPICSTCL_TCLLIBS)

# package require GretinaRunControl_Prompter
package require gretinaRctl
package require ReadoutGUIPanel;              # lets us set the run number.
package require ui;                           # Lets us manage a status area.
package require RunstateMachine
##
# Note, this provider is for a singleton (the assumption is that there's only
# one GRETINA).   It enforces that in its start proc.  If a second
# GRETINA compatible device ever exists that restriction can be removed there.
#
#  In addition to starting/stopping runs, this provider:

#   - Maintains synchronization between the GRETINA run number and the run number
#     in the readout GUI.
#   - Adds a status bar to the ReadoutGUI that shows:
#     *   The GRETINA Run state.
#     *   The directory in which GRETINA is recording data.
#

namespace eval GretinaRunControl {
    variable instances;                     # id indexed array of instance data.
    array set instances [list]
    variable pollInterval 1000;             # ms between updates of status bar.

    variable lastState    unknown;          # last known gretina tate.
}

snit::widgetadaptor GretinaRunControl::statusbar {
    option -state -default "*Unknown*    ";         # Gretina run state.
    option -datadir -default "*Unknown*";       # Gretain recording dir.
    
    constructor args {
        installhull using ttk::frame
        
        ttk::label $win.statelbl -text "State: "
        ttk::label $win.state    -textvariable [myvar options(-state)]
        ttk::label $win.dirlbl   -text "Gretina event data path: "
        ttk::label $win.dir      -textvariable [myvar options(-datadir)]
        
        grid $win.statelbl $win.state $win.dirlbl $win.dir -stick ew
        
        # The stuff below allows the directory path to epxand but no other
        # subwidgets:
        
        grid columnconfigure $win 0 -weight 0
        grid columnconfigure $win 1 -weight 0
        grid columnconfigure $win 2 -weight 0
        grid columnconfigure $win 3 -weight 1
        
        $self configurelist $args
    }
}


##
# parameters
#     @return dict - parameterization dict.
#
proc GretinaRunControl::parameters {} {
    return [dict create]
}

##
# start
#     Starts a new  GRETINA run control data source.  Currently this requires
#     only one data source managed by this provider is active at a time
#     however, other than the code that enforces that here multiple sources
#     should otherwise be supported.  Instance data for a Gretina instance
#     are a dict that contains:

#       afterid      - Timer id used to monitor GRETINA state etc.
#       expectingrun - True if when state becomes Run we expect it.
#       expectingend - True if we're expecting transitions out of Run.
#       statuswidget - Status line widget for this source.
#
# @param parameters - source parameterization. sourceid  is our id.
# @note there can be only one.
#
proc GretinaRunControl::start parameters {
    #
    #  Enforce the only-one rule:
    #
    if {[array size GretinaRunControl::instances] > 0} {
        error "More than one GretinRunControl provider is not permitted"
    }
    set id [dict get $parameters sourceid]
    #  Construct the status line.
    
    set statusBar [::StatusBar::getInstance]
    set statusWidget [$statusBar addWidget GretinaRunControl::statusbar]
    
    #  Figure out the rest of the dict:
    
    set stateInfo [dict create \
	      expectingrun 0 expectingend 1 \
	      statuswidget $statusWidget afterid "" ]; #  We're expecting to be in setup.
    set GretinaRunControl::instances($id) $stateInfo
    GretinaRunControl::_updateStatus $id;   # Will set afterid.

}
##
# check
#    Checks if the specified id is running.  For our case,
#   running means the afterid is non-empty...that is the
#   status bar can continue to be updated.
#
# @param  id - the id of the instance.
# @return bool - true if the source is alive.
#
proc GretinaRunControl::check id {
    if {[array names GretinaRunControl::instances $id] eq ""} {
        error "Gretina run control check - no such instance $id"
    }
    set state $GretinaRunControl::instances($id)
    set afterId [dict get $state afterid]
    return [expr {$afterId ne ""}]
}

##
# begin
#   Try to begin a run.
#   - The run stat for Gretina must be "Setup".
#
#  @param id  - the id of the instance to begin.
#  @param run - the run number of the run to start (ignored),.
#  @param title - The run title.
#
proc GretinaRunControl::begin {id run title} {
    if {[array names GretinaRunControl::instances $id] eq ""} {
        error "Gretina run control check - no such instance $id"
    }
    set state $GretinaRunControl::instances($id)

    set currentState [gretina::getState]

    if {$currentState ne "Setup"} {
        error \
            "To begin a run, GRETINA must be in the 'Setup' but is in the \
'$currentState' state"
    }
    dict set state expectingrun 1
    dict set state expectingend 0; 
    set GretinaRunControl::instances($id) $state

    gretina::start;                      # Get GRETINA lumbering towards Run.

}
##
# pause/resume are not supported by Gretina
#

proc GretinaRunControl::pause {id} {}
proc GretinaRunControl::resume {id} {}

##
# end a run.
#   The run state must be "Run"
#
# @param id = id of the instance to end.
#
proc GretinaRunControl::end {id} {
    if {[array names GretinaRunControl::instances $id] eq ""} {
        error "Gretina run control check - no such instance $id"
    }
    set state $GretinaRunControl::instances($id)
    set currentState [gretina::getState]
    if {$currentState ne "Run" } {
        error "Gretina is 'the '$currentState' state but must be in 'Run' to end."
    }
    
    dict set state expectingrun 0
    dict set state expectingend 1
    set GretinaRunControl::instances($id) $state
    
    ::gretina::stop;                        # Stop the run.
}
##
# stop
#   Stop stops the data source and removes it.
#   - Kill off the timer if its active.
#   - Kill off the  status bar widget.
#   - Remove us from the instances array.
#
# @param id - instance id of the source.
#
proc GretinaRunControl::stop id {
    if {[array names GretinaRunControl::instances $id] eq ""} {
        error "Gretina run control check - no such instance $id"
    }
    set state $GretinaRunControl::instances($id)
    
    #  If the state is "Run" we stop the run... unceremoniously.
    
    if {[::gretina::getstate] eq "Run"} {
        ::gretina::stop
    }
    
    #  Kill off any active after timer:
    
    set afterId [dict get $state afterid]
    if {$afterId ne ""} {
        after cancel $afterId
    }
    # Kill off any status bar widget:
    
    set widget [dict get $state statuswidget]
    if {$statuswidget ne ""} {
        destroy $statuswidget
    }
    
    # Remove the instance:
    
    array unset GretinaRunControl::instances $id
    
}
##
# init is a no-op.

proc GretinaRunControl::init id {}

##
# capabilities
#   Return the driver capabilities:
#
proc GretinaRunControl::capabilities {} {
    return [dict create                            \
        canPause false                             \
        runsHaveTitles  true                           \
        runsHaveNumbers true                       \
    ]
}

##
# GretinaRunControl::_updateStatus
#    Called in response to an update timer expiration.
#    - reschedule next one.
#    - Update the status bar.
#    - If we've gone into Run and were not expecting, that's an error.
#    - If we've gone into Setup and were not expecting, that's an error
#      too.
#   Errors are just reported via the error command.
#
#  @param id - Id of the instanceto update.
#
proc GretinaRunControl::_updateStatus id {

    if {[array names GretinaRunControl::instances $id] eq ""} {
        error "Gretina run control check - no such instance $id"
    }
    set state $GretinaRunControl::instances($id)
    
    # Repropagate.
    
    dict set state \
        afterid [after $GretinaRunControl::pollInterval GretinaRunControl::_updateStatus $id]
    
    # Update the graphical user interface::
    
    
    set gState [::gretina::getState]
    set lastState $::GretinaRunControl::lastState
    set GretinaRunControl::lastState $gState
    set run    [::gretina::runNumber]
    set dir    [::gretina::runDir]
    
    ::ReadoutGUIPanel::setRun $run
    set w [dict get $state statuswidget]
    $w configure -state $gState -datadir $dir
    
    #  Now look for state transitions:
    
    if {($gState eq "Run") && [dict get $state expectingrun]} {

        dict set state expectingrun 0;        # no longer need to catch transition.
    }
    
    # Unexpected drop out of run:

    if {$lastState eq "Run"} {
	if {($gState ne "Run") && ![dict get $state expectingend]} {
	    dict set state expectingend 0
	    tk_messageBox -title {Unexpected Gretina End} -type ok -icon error \
		-message {GRETINA's run unexpectedly ended}

	    #Transition to not ready -- not sure what else to do.

	    set m [RunstateMachineSingleton %AUTO%]
	    $m transition NotReady
	    $m destroy
	 }
    }
    set GretinaRunControl::instances($id) $state
}   
