
#
#   This package provides a top level integration with
#   The s800 and Gretina readout callouts so that we can perform some checks and 
#   stuff that crosses detector boundaries.
#   Normally a user's ReadoutCallouts will require this package call the
#   mergedCallouts::Initialize method and then our OnBegin/OnEnd methods.
#
#
#
package provide mergedCallouts 1.0
package require S800ToGretina
package require gretinaRdoCallouts
package require RunstateMachine
package require Thread
#
#  Create the namespace that will hold our procs
#
namespace eval mergedCallouts {
    #variable s800Host spdaq48;	# Default host running the s800 event builder/run control.
    #variable controlS800 1
    variable controlGretina 1
    variable gretinaServicesAlive 0

    #if {[array names ::env S800_HOST] ne ""} {
    #	set s800Host $::env[S800_HOST]
    #}
    
    variable ImpSynchRequired   1;             # Default to needed each run.
    
    
}

##
#  Schedule s800/gretina init.  This is scheduled so that
#  the rest of the GUI can build first.
#
proc mergedCallouts::Initialize {} {
    after 500 ::mergedCallouts::_Initialize
}

#
# Initialize both the S800 and Gretina software.
#   S800 is a first class data source - can't have a checkbox for it
#
proc mergedCallouts::_Initialize {} {
    # Create a pair or checkbuttons that allow us to independently start/stop
    # Gretina/s800.  When the run is active these will be ghosted.
    #

    frame .gcontrols
    checkbutton .gcontrols.controlGretina -text {Gretina } \
	-variable ::mergedCallouts::controlGretina -onvalue 1 -offvalue 0
    checkbutton .gcontrols.impsyncperrun -text {Auto IMPSYNC} -onvalue 1 -offvalue 0 \
	-variable ::mergedCallouts::ImpSynchRequired
    button .impsync -text {IMPSYNC} -command ::gretina::sendImpSynch
    
    #checkbutton .controlS800    -text {S800} \
    #	-variable ::mergedCallouts::controlS800 -onvalue 1 -offvalue 0

    set rows [lindex [grid size .] 1]
    incr rows



    grid .gcontrols.controlGretina .gcontrols.impsyncperrun -sticky w 
    grid .gcontrols -row $rows -sticky w

    grid .impsync -sticky w
    
    # grid .controlS800    -row $rows -column 4


    ::gretina::Initialize; # Let the main build a gui
    #::s800::Initialize $::mergedCallouts::s800Host

    # Monitor gretina service exits:

    set ::gretina::unexpectedExitScript mergedCallouts::_gretinaServiceExit
    set ::gretina::unexpectedEndScript  mergedCallouts::_unexpectedGretEnd
}

#
#  Start the S800 then gretina as that's the proper order.
#
proc mergedCallouts::OnBegin {} {

    #
    # If we are not in a good state to start, abort the
    # begin run.

    if {![::mergedCallouts::_okToStart]} {
	error "Gretina and/or the S800 are not in a state that allow us to start a run"
    }

    if {$::mergedCallouts::controlGretina} {
	puts "starting gretina"
	set mergedCallouts::gretinaServicesAlive 1; # They will be soon.
	gretina::OnBegin
	puts "started"
    }
#     if {$::mergedCallouts::controlS800} {
#     	 puts "starting s800 (merged)"
#	s800::OnBegin
#	 puts "started"
    #    }

    after 1000;                    # Wait a second for the GEB(?)
    S800ToGretina::OnBegin


   ::mergedCallouts::_disableCheckbuttons 
    
    
    ##
    #  Note this needs some work for inifinity clock synch.
    #
    if {$::mergedCallouts::controlGretina &&
	$::mergedCallouts::ImpSynchRequired }  {

	# Send gretina's IMPSync to synch the timestamps/triggers.
	
	puts "Scheduling impsynch to happen when s800 finishes initializing"
	
	_scheduleImpSynch
    }
    ##
    # Because of the order of operations on the end run, we need to get the
    # end run watcher going here, otherwise we can start our watcher _after_
    # the end run flushes through.
    #
    # We don't want to end the run until we see at least one end in the
    # s800 filtered ring...that indicates that all the data has filtered
    # through the system.  Therefore, schedule a thread to monitor for
    # that condition:

    if {$::mergedCallouts::controlGretina} {
	::mergedCallouts::_scheduleEndRun
    }

}

#
# Stops are also s800 first then gretina ..since s800 stops 
# prevent additional triggers from going to gretina.
#
proc mergedCallouts::OnEnd {} {
    
    #
    #  Because of when OnEnd is called we need to watch for runs to end
    #  from the very beginning.
    
    # Turn off the ability for the user to bang on the end button since
    # the end may take a bit:
    
   


}



#-------------------------------------------------------------------
#
# Private methods
#
##
# Actually end the run.
#
#  _endGretinaRun
#
#   Called when we see an end run in the s800filtered data ring.
#   This is scheduled by the _scheduleEndRun thread started by
#   the OnEnd proc called when Gretina is running and we have
#   an end run request.
#
proc ::mergedCallouts::_endGretinaRun {} {
   if {$::mergedCallouts::controlGretina} {

	if {$::gretina::state eq "Run"} {
	    gretina::OnEnd
	}
    }

    ::mergedCallouts::_enableCheckbuttons
    
    # Note this is harmelss even if they are not disabled.
    
    set rctl [::RunControlSingleton::getInstance]
    $rctl configure -state normal
}


##
#   disable the checkbuttons.
#
proc mergedCallouts::_disableCheckbuttons {} {
    .gcontrols.controlGretina configure -state disabled
#    .controlS800    configure -state disabled
}

##
# Enable the checkbuttons.
#
proc mergedCallouts::_enableCheckbuttons {} {
    .gcontrols.controlGretina configure -state normal
#    .controlS800    configure -state normal
}
##
# Check to see if gretina and/or the s800 can
# start a run.  This is conditional on the checkbox states:
# - if controlsS800 is true, the s800 state must be "Inactive"
# - if controlGretina is true, the Gretina state must be "Setup"
#
# @return  1 for ok, 0 for not ok.
#
proc mergedCallouts::_okToStart {} {

#    if {$::mergedCallouts::controlS800} {
#	if {$::s800::runState ne "inactive"} {
#	    return 0
#	}
#    }

    if {$::mergedCallouts::controlGretina} {
	if {[::gretina::getState] ne "Setup"} {
	    return 0
	}
    }
    return 1
}
##
# _unexpectedGretEnd
#   Gretina ended a run when it was not expected...
#   force an end run at the UI as well.
#
proc ::mergedCallouts::_unexpectedGretEnd {} {
    end

    tk_messageBox -title "Unexpected End" -icon error -message {Gretina unexpectedly stopped Running}
}
##
# Called by the gretina callouts if services exit unexpectedly.
# we watch GEBpush and if it exits, set gretinaServicesAlive to 0
#
#
proc mergedCallouts::_gretinaServiceExit app {
    if {$app eq "GEBPush"} {
	set mergedCallouts::gretinaServicesAlive 0
    }
}


##
# _scheduleImpSynch
#    Called to monitor the CCUSBReadout output ring of the s800
#    once a begin run is seen on that ring, we send an impsync
#    to GRETINA.
#  @note - the ring is attached via a synchronous thread to prevent a timing
#          hole (very unlikely however) between thread start and s800
#          sending the begin item.
#
#  External variables:
#   *  ::S800ToGretina::S800Host      - Host in which the s800 software is running.
#   *  ::S800ToGretina::S800CCUSBRing - Output ring for CCUSBReadout.
#
proc _scheduleImpSynch {} {

    # Create the the thread and source the packages it needs
    # (note the thread does not inherit our auto path):
    
    set impsyncTid [::thread::create]
    thread::send $impsyncTid [list set auto_path $::auto_path]
    thread::send $impsyncTid {
	    package require TclRingBuffer
    }
    thread::send $impsyncTid [list set parentTid [thread::id]]
    
    # Set a global variable containing the CCUBRING URL
    # and attach it:
    
    thread::send $impsyncTid [list \
      set ccusbRingUrl tcp://$::S800ToGretina::S800Host/$::S800ToGretina::S800CCUSBRing]
    thread::send $impsyncTid [list \
	set vmusbRingUrl tcp://$::S800ToGretina::S800Host/$::S800ToGretina::S800VMUSBRing ]
    thread::send $impsyncTid {ring attach $ccusbRingUrl}
    thread::send $impsyncTid {ring attach $vmusbRingUrl}
    
    #  Now start the asynchronous operation - when the first ring item is seen,
    #  the run started so we:
    #  *  Detach the ring.
    #  *  Tell the parent thread to impsync.
    #  *  Exit the thread:
    
    thread::send -async $impsyncTid {
	ring get $ccusbRingUrl;                # We don't care what comes through.
	ring detach $ccusbRingUrl
	ring get $vmusbRingUrl
	ring detach $vmusbRingUrl
	# after 30000
	thread::send $parentTid ::gretina::sendImpSynch
	puts "ImpSync sent to GRETINA"
	thread::release;                  # forces thread exit.
	thread::exit
    }
    
}
##
# _scheduleEndRun
#   Starts a thread that monitors the s800filter for an end run.
#   When seen, a small delay is performed to ensure that data has filtered to
#   gretina and then the parent thread is asked to ::mergedCallouts::_endGretinaRun
#   Which actually ends the gretina run.  Note that we're not going to get
#   extraneous Gretina triggers because ending the s800 run also disables
#   the trigger box which, in turn, disables the Gretina external
#   trigger accept.
#
proc ::mergedCallouts::_scheduleEndRun {} {
    set endRunTid [::thread::create]
    
    # Ensure the end run thread has the needed auto path and packages:
    
    thread::send $endRunTid [list set auto_path $::auto_path]
    thread::send $endRunTid {
	    package require TclRingBuffer
    }
    thread::send $endRunTid [list set parentTid [thread::id]]
    
    # Set a global variable containing the s800 filtered ring URI:
    
    thread::send $endRunTid [list 						\
	set filterUri 								\
	    tcp://$::S800ToGretina::S800Host/$::S800ToGretina::S800Ring	\
	]
    #  Send the async code that watches for an en end run in the filter ring.
    #  Since the filter ring is on the back end of the filter we can assume that
    #  the two end runs happen pretty close together and, in any event, if not,
    #  our delay for pushToGeb to send that stuff can incorporate this.
    #
    
    thread::send -async $endRunTid  {
	puts "Waiting for end to come out of s800filter:"
	
	ring attach $filterUri
	ring get $filterUri 2;              #End run  only.
	ring get $filterUri 2;              # CCUSB and VMUSB ends.
	ring detach $filterUri
	puts "Saw both end runs"
	puts "Delaying to let stuff filter into Gretina."
	after 2000;                         # Give time to get to the GEB.
	puts "Ending GRETINA Run"
	thread::send $parentTid ::mergedCallouts::_endGretinaRun;
	puts "end run thread exiting"
	thread::release;                    # Causes thread exit.
	thread::exit
    }
}

proc ::mergedCallouts::attach {state} {}
proc ::mergedCallouts::enter {from to} {}
proc ::mergedCallouts::leave {from to} {
    if {($from in {Active Paused} ) && ($to eq "Halted")} {
	set rctl [::RunControlSingleton::getInstance]
	$rctl configure -state disabled	
    }
}

namespace eval mergedCallouts {
    namespace export attach enter leave
}
set sm [RunstateMachineSingleton %AUTO%]
$sm addCalloutBundle mergedCallouts rdoCallouts
$sm destroy
