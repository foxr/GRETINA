<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                      "file:///usr/share/xml/docbook/schema/dtd/4.5/docbookx.dtd
"
>
<book>
    <bookinfo>
      <title>Merged Callouts</title>
      <author><firstname>Ron</firstname><surname>Fox</surname></author>
      <revhistory>
          <revision>
             <revnumber>1.0</revnumber>
             <date>June 13, 2012</date>
             <authorinitials>RF</authorinitials>
             <revremark>Original Release</revremark>
          </revision>
      </revhistory>
    </bookinfo>
    <chapter>
        <title>Introduction</title>
        <para>
            Merged callouts integrates the Readout callouts for the s800
            and the GRETINA detectors so that they can run as an integrated
            set under the control of the NSCL Run control GUI.
        </para>
        <para>
            The merged callouts load like a normal Tcl package via
            <command>package require mergedCallouts</command>. It provides
            serveral commands that live in the <literal>mergedCallouts</literal>
            namespace.  To load it you must first have run the
            <command>gretinaSetup</command> script to extend the Tcl library
            search paths to include both the NSCLDAQ distribution and the
            Gretina Tcl Library paths.
        </para>
        <para>
            See the reference chapter that folllows.
        </para>
    </chapter>
    <chapter>
        <title>Reference</title>
        <para>
            This chapter provides reference material about the mergedCallouts
            package and is also used to generate online manpages.
        </para>
        <refentry id="mergedCallouts">
          <refmeta>
             <refentrytitle>mergedCallouts</refentrytitle>
             <manvolnum>3tcl</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>mergedCallouts</refname>
             <refpurpose>ReadoutCallouts for the Gretina &amp; S800.</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <cmdsynopsis>
            <command>
package require mergedCallouts
            </command>
            <command>
proc mergedCallouts::Initialize {} {
            </command>
            <command>
proc mergedCallouts::OnBegin {} {
            </command>
            <command>
proc mergedCallouts::OnEnd {} {
            </command>
            </cmdsynopsis>
  
          </refsynopsisdiv>
          <refsect1>
             <title>DESCRIPTION</title>
             <para>
                This package is intended to be used within the
                <filename>ReadoutCallouts.tcl</filename> file of an experiment.
                That file allows users to extend the functionality of the
                Readout GUI in application specific ways.
             </para>
             <para>
                The <literal>mergedCallouts</literal> package provides commands
                that can be called that extend the Readout GUI to control both
                the S800 and the GRETINA detectors so that they can run with
                a common timestamp/trigger system.
             </para>
             <para>
                In addition elements are added to the ReadoutGUI to support
                starting runs in only one of the detectors.
                See COMMANDS below for the command reference and
                EXAMPLES for a minimal <filename>ReadoutCallouts.tcl</filename>
                file that uses this package.
             </para>
          </refsect1>
          <refsect1>
             <title>
                COMMANDS
             </title>
             <variablelist>
                <varlistentry>
                    <term><command>Initialize</command></term>
                    <listitem>
                        <para>
                            Initializes the package.  This should be called
                            prior to invoking any of the other package commands.
                            Normally this is called at the outer level of the
                            <filename>ReadoutCallouts.tcl</filename> rather than
                            in one of the <command>proc</command>s that extend
                            the functionality.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><command>OnBegin</command></term>
                    <listitem>
                        <para>
                            Should be called from the user's
                            <command>OnBegin</command> extension.   This
                            actually starts GRETINA and S800 data taking in a
                             coherent manner and ensureas that the two systems
                             have a synchronized timestamp.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><command>OnEnd</command></term>
                    <listitem>
                        <para>
                            Should be called from the user's
                            <command>OnEnd</command> extension.
                            This ends data taking on the S800 and Gretina
                            in the proper way.
                        </para>
                    </listitem>
                </varlistentry>
                
             </variablelist>
          </refsect1>
          <refsect1>
             <title>
                EXAMPLES
             </title>
             <para>
                The example below shows a minimal <filename>ReadoutCallouts.tcl</filename>
                file that uses this package.
             </para>
             <example>
                <title>Sample mergedcallouts ReadoutCallouts</title>
                <programlisting>
package require mergedCallouts

proc OnBegin run {
    mergedCallouts::OnBegin
}
proc OnEnd run {
    mergedCallouts::OnEnd
}
                </programlisting>
             </example>
          </refsect1>
  
        </refentry>
  
    </chapter>
</book>