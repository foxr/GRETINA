/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/


#include "cmdline.h"
#include "header.h"
#include "buffer.h"
#include "buftypes.h"


#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <list>
#include <iostream>
#include <set>

#include <DataFormat10.h>
using namespace NSCLDAQ102;
using namespace std;

struct fragment {
  uint32_t  type;
  uint32_t  size;
  uint64_t  timestamp;
  char*  pData;
};

static size_t readEvent(istream& in, uint64_t dt,  void*& pDest);
static void   putEvent(ostream& out, size_t nBytes, void* pEvent);
static void   outputControlBuffer(ostream& out, struct fragment* );



static int16_t run = 0;
static uint16_t packetId;

static std::set<int>    specialEventType;

static inline bool In(int value, std::set<int>& aset)
{
  return (aset.find(value) != aset.end());
  
}

/**
 * Format a physics event item.  The payload is put in the event body preceded
 * by the uin32-T
 * nWords.
 *
 * @param nWords - Number of uint16_t items in the event payload.
 * @param pPayload - The payload of the event.
 *
 *
 * @return pPhysicsEventitem - Pointer to the new item.
 *         at some point the caller must free(3) the item.
 */
static  pPhysicsEventItem  formatEventItem(size_t nWords, void* pPayload)
 {
   /* The size of the resulting physics even item will be
      Size of a RingItemHeader +
      size of a uint32_t (leading word count)
      nWords*sizeof(uint16_t):
   */
   size_t itemSize = sizeof(RingItemHeader) + sizeof(uint32_t)
                     + nWords*sizeof(uint16_t);
   pPhysicsEventItem pItem;
   uint32_t*              pBody;

   pItem = (pPhysicsEventItem)malloc(itemSize);
   if (!pItem) {
     return pItem;
   }
   pItem->s_header.s_size = itemSize;
   pItem->s_header.s_type = PHYSICS_EVENT;

   pBody = (uint32_t*)(pItem->s_body);
   *pBody++ = nWords + sizeof(uint32_t)/sizeof(uint16_t);
   if (nWords) {
     memcpy(pBody, pPayload, nWords * sizeof(uint16_t)); /* In case memcpy is ill defined for n = 0. */
   }

   return pItem;
 }
/**
 * Create/format a scaler item.
 * @param scalerCount - Number of 32 bit scaler items in the item.
 * @param timestamp   - Absolute timestamp.
 * @param btime       - Offset to beginning of interval from start of run.
 * @param etime       - Offset to end of interval from start of run.
 * @param pCounters   - The counters.
 *
 * @return pScalerItem Pointer to the scaler aitme we are going to return.  Must
 be passed to
 *                     free(3) to release storage.
 */
pScalerItem
static formatScalerItem(
    unsigned scalerCount, time_t timestamp, uint32_t btime, uint32_t etime,
    void* pCounters
)
{
  size_t allocationSize = sizeof(ScalerItem) + (scalerCount)*sizeof(uint32_t) -
sizeof(uint32_t);
  pScalerItem pItem = (pScalerItem)malloc(allocationSize);


  if(!pItem) {
    return pItem;
  }

  pItem->s_header.s_size = sizeof(ScalerItem) + (scalerCount)*sizeof(uint32_t) -
 sizeof(uint32_t);
  pItem->s_header.s_type = INCREMENTAL_SCALERS;
  pItem->s_intervalStartOffset = btime;
  pItem->s_intervalEndOffset   = etime;
  pItem->s_timestamp           = timestamp;
  pItem->s_scalerCount         = scalerCount;
  memcpy(pItem->s_scalers, pCounters, scalerCount*sizeof(uint32_t));

  return pItem;
}


/**
 * Filter to build NSLCDAQ buffers from gretina ordered data files.
 * Ordered data files have event fragments in total time order.
 * Each fragment has a header much like the gebData header (old style) but
 * with the payload pointer replaced by the data itself.
 */
int main(int argc, char** argv)
{
  // Parse the arguments.
  
  struct gengetopt_args_info args;

  cmdline_parser(argc, argv, &args);
  uint64_t deltaT = args.dt_arg;
  
  // The following dirt allows the default values to be inserted in the set:
  
  if (args.controltype_given == 0) args.controltype_given = 1;
  if (args.scalertype_given == 0) args.scalertype_given = 1;
  
  for (int i =0; i < args.controltype_given; i++) {
    specialEventType.insert(args.controltype_arg[i]);
  }
  for (int i = 0; i < args.scalertype_given; i++) {
    specialEventType.insert(args.scalertype_arg[i]);
  }
  packetId  = args.tag_arg;

  try {
    while (!cin.eof()) {
      void* pEvent;
      size_t nBytes = readEvent(cin, deltaT, pEvent);
      if (nBytes) {
	putEvent(cout, nBytes, pEvent);
	free(pEvent);
      }
  
    }
  } catch (...) {
    std::cerr << "glom -main caught an exception of some sort.\n";
    return EXIT_FAILURE;
  }
  std::cerr << "glom exiting - EOF on stdin\n";
  return EXIT_SUCCESS;
}
/**
 * Read data from an istream ...performing multiple reads if
 * needed to ensure the entire read is satisfied or
 * an eof/error occurs.
 *
 * @param in      - istream& from which data are read.
 * @param pBuffer - Pointer to the buffer into which data will
 *                be read.
 * @param nBytes  - Number of bytes to read.
 *
 * @return - void.
 */
static void
readBlock(istream& in, void* pBuffer, size_t nBytes)
{
  char* p = reinterpret_cast<char*>(pBuffer);
  streamsize nRead;
  
  while(nBytes != 0) {
    in.read(p, nBytes);
    nRead = in.gcount();
    if (in.fail() && (nRead == 0)) {
      return;
    }
    nBytes -= nRead;
    p+=       nRead;
  }
  return;
}

/**
 * Read an event from the the input device.
 * @param in - input stream that provides a source of data.
 * @param dt - Time window that defines the event.
 * @param pDest - Reference to a pointer that will be filled with a dynamically
 *               allocated block of storage that will contain the event.
 * @return size_t
 * @retval - total number of bytes in the event.
 * @note - end of file detection implies end of event as well.
 * @note - Return value of 0 implies an end of file reading the first fragment header
 *         in that case the contents of dest are undefined and no storage is allocated.
 *
 */
static size_t
readEvent(istream& in, uint64_t dt, void*& pDest)
{

  list<fragment*> fragments;	// Fragments buffered here until assmembly complete.
  size_t        totalSize = 0;      // total number of bytes of data
  uint64_t      initialTime;
  header        fragmentHeader;


  // Read the header of the first fragment.. if that failed we're done and just return 0.

  readBlock(in, reinterpret_cast<char*>(&fragmentHeader), sizeof(fragmentHeader));
  if (in.eof()) {
    return 0;
  }
  // There are headers that have size 0 ...these are going to be skipped.

  if (fragmentHeader.length == 0) return 0;

  initialTime = fragmentHeader.timestamp; // The first fragment starts the coincidence interval.

  // Accumulate fragments until either EOF on header read or
  // the timestamp of the fragment differs too much from the initial interval:
  //
  while(!(cin.eof())) {


    // Accumulate this fragment in the vector of fragments.

    fragment* pFragment = new fragment;
    pFragment->size      = fragmentHeader.length;
    pFragment->type      = fragmentHeader.type;
    pFragment->timestamp = fragmentHeader.timestamp;

    pFragment->pData     = reinterpret_cast<char*>(malloc(fragmentHeader.length));
    readBlock(in, pFragment->pData, fragmentHeader.length);


    // If this fragment has timestamp 0 and our special type code
    // 1. flush the buffer if there's anything in it.
    // 2. Build and flush the appropriate type of buffer from the
    //    event we have.
    // Otherwise add the fragment to the queue.

    if (In(fragmentHeader.type, specialEventType)) {

      outputControlBuffer(cout, pFragment);
      free(pFragment->pData);	// Free the dynamic storage associated with he fragment.
      delete pFragment;		// Delete the frag.. it gets created at the loop top.
    } else {
      
      fragments.push_back(pFragment);
      
      // length of payload + length of gretina header without the payload pointer +
      // length of an NSCL packet header.

      totalSize += fragmentHeader.length + sizeof(fragment) - sizeof (char*) + 2*sizeof(uint16_t);
    }
    
    // read the next fragment header:


    readBlock(in, reinterpret_cast<char*>(&fragmentHeader), sizeof(fragmentHeader));
    if (fragmentHeader.length == 0) break; // Zero length thingy.
    if (((fragmentHeader.timestamp - initialTime) > dt) &&
	(!In(fragmentHeader.type, specialEventType))
    ) {
      break;			// We have an event in the output queue.
    }

  }

  // At this point there must be at least one output fragment and the total
  // length of the event body is totalSize.

  char* d = reinterpret_cast<char*>(malloc(totalSize + 2*sizeof(uint16_t)));
  uint16_t* dw;			//  for building packet headers.
  pDest = d;

  while (!fragments.empty()) {
    fragment* fp = fragments.front();
    fragments.pop_front();

    // Packet header
    
    dw  = reinterpret_cast<uint16_t*>(d);
    *dw++ = (sizeof(fragment) - sizeof(char*) + fp->size)/sizeof(uint16_t) + 2;
    *dw++ = packetId;
    d = reinterpret_cast<char*>(dw);

    // gretina header

    memcpy(d, fp, sizeof(fragment) - sizeof(char*));
    d += sizeof(fragment) - sizeof(char*);

    // Gretina body

    memcpy(d, fp->pData, fp->size);
    d += fp->size;
    free(fp->pData);
    delete fp;
  }
  


  // There are two possible reasons we exited the loop
  // If the time range got too big we need to backtrack in the file one header length
  // as that header will set the start time for the next event.
  // if we hit an EOF we don't need to do that however.

  if (!cin.eof()) {

    // cin.seekg(-(static_cast<streampos>(sizeof(fragmentHeader))), ios::cur);
    
    char* p = reinterpret_cast<char*>(&fragmentHeader);
     p += sizeof(fragmentHeader);
    for (int i =0; i < sizeof(fragmentHeader); i++) {
      char c = *--p;
      in.putback(c);
   }
  }

  return totalSize;
  
}

/**
 * Put an event in the output buffer and, if needed, write it out to file.
 * @param out - stream to which the buffer is written.
 * @param nBytes number of bytes in the event.
 * @param pEvent pointer to the event.
 */
static void   putEvent(ostream& out, size_t nBytes, void* pEvent)
{

  pPhysicsEventItem pItem = formatEventItem(nBytes/sizeof(uint16_t), pEvent);
  if (!pItem) {
    perror("Unable to create a physics event item");
  } else {
    cout.write(reinterpret_cast<char*>(pItem), pItem->s_header.s_size);
    free(pItem);
  }



}

/**
 * write a state change buffer to stdout.
 * 
 * @param out - Reference to the ostream& on which the buffer is written
 * @param pItem - Pointer to the RingItemHeader which is then followed by
 *                the body of the ring item (in other words this will
 *                eventualy be cast to a pStateChangeItem (sooner rather than later in fact).
 */
static void
outputStateChange(ostream& out, pRingItemHeader pItem)
{

  cout.write(reinterpret_cast<char*>(pItem), pItem->s_size);

}

/**
 * Create a string list buffer.  This comes from any of a PACKET_TYPES or a MONITORED_VARIABLES
 * ring item.
 *
 * @param out - reference to the iostream on which the data should be written.
 * @param pItem - Pointer to the ring item (as a pRingItemHeader).
 */
static void
outputStringList(ostream& out, pRingItemHeader pItem)
{

  cout.write(reinterpret_cast<char*>(pItem), pItem->s_size);

}

/**
 * Output an incremental scalers buffer.  Unlike the string buffers by definition,
 * all scalers must fit in a single output buffer,.
 *
 * @param out - reference to ostream to which the buffer will be written.
 * @param pHeader - Pointer to the header of the entire ring item.
 */
static void
outputScalers(ostream& out, pRingItemHeader pHeader)
{

  pNonIncrTimestampedScaler pItem = reinterpret_cast<pNonIncrTimestampedScaler>(pHeader);

  // For now, transform these into the same shape as incremental scaler ring items:

  pScalerItem pOutputItem = formatScalerItem(pItem->s_scalerCount,
					     pItem->s_clockTimestamp,
					     pItem->s_intervalStartOffset,
					     pItem->s_intervalEndOffset,
					     pItem->s_scalers);

  cout.write(reinterpret_cast<char*>(pOutputItem), pOutputItem->s_header.s_size);
  free(pOutputItem);
}


/**
 * figure out which type of control buffer to create and output
 * based on the fragment body which is a ring item.
 * 
 * @param out - Output stream on which the data will be put.
 * @param pFrag  - Pointer to the fragment header and all.
 *
 */
static void
outputControlBuffer(ostream& out, struct fragment* pFrag )
{
  pRingItemHeader pItem = reinterpret_cast<pRingItemHeader>(pFrag->pData);


  switch (pItem->s_type) {
  case BEGIN_RUN:
  case END_RUN:
  case PAUSE_RUN:
  case RESUME_RUN:
    outputStateChange(out, pItem);
    break;
  case PACKET_TYPES:
  case MONITORED_VARIABLES:
    outputStringList(out, pItem);
    break;
  case INCREMENTAL_SCALERS:	// get scaler data from timestamped ones.
    break;
  case  TIMESTAMPED_NONINCR_SCALERS:
    outputScalers(out, pItem);
    break;
  case PHYSICS_EVENT_COUNT:
    break;			// Ignore these.
  default:
    cerr << "Encountered an unknown event type: " << pItem->s_type << endl;
    break;
  }
}
