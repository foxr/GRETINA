/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include <GEBLink.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>


/**
 * \file testHanress.c This file contains code to simulate the gretina tap software.
 *       it provides simulations of the gretTapConnect and gretTapData.
 *       the data provided by the 'tap' consists of simple counting patterns of variable
 *       lengths.  The lengths have sensible defaults shown below however the mean and
 *       spread in the sizes can also be set by the environment variables:
 *
 *       - TAP_MEAN - mean event size.
 *       - TAP_SPREAD spread in event sizes.  Event sizes are uniformly distributed between
 *                    TAP_MEAN - TAP_SPREAD and TAP_MEAN+TAP_SPREAD.  If TAP_SPREAD > TAP_MEAN
 *         event sizes are between 0 and TAP_MEAN+TAP_SPREAD.
 *
 */

/* Default values for the tap event sizes: */

const int TAP_MEAN   = 100;
const int TAP_SPREAD = 50;


/* The simulated gretTap* type: */

struct gretTap {
  int junk;
}  tapClient = {0};	/* gotta use something. */


/*--------------------------------------------------------------------------------*
 *                                                                                *
 * Private (static) functions                                                     *
 *                                                                                *
 *--------------------------------------------------------------------------------*/

/**
 * Get the size of the event.  The consts TAP_MEAN and TAP_SPREAD are
 * default values that can be overidden by env vars of the same name.
 * We take a non-uniform approach to dealing with negative sizes..just take abs value.
 * However size statistics are not important so...
 * @return int
 * @retval size of event.
 */
static int
getEventSize()
{
  int mean   = TAP_MEAN;
  int spread = TAP_SPREAD;
  int size;
  char* envVar;

  /* If TAP_MEAN is defined override mean: */

  envVar = getenv("TAP_MEAN");
  if (envVar) {
    mean = atoi(envVar);
    if (mean <= 0) {
      fprintf(stderr, "Invalid value for TAP_MEAN: %s\n", envVar);
      exit(EXIT_FAILURE);
    }
  }
  /* if TAP_SPREAD is defined, override spread: */

  envVar = getenv("TAP_SPREAD");
  if (envVar) {
    spread = atoi(envVar);
    if (spread <= 0) {
      fprintf(stderr, "Invalid value for TAP_SPREAD: %s\n", envVar);
    }
  }

  /* figure out the event size: */

  
  size = mean + spread*2*drand48() - spread +1;
  return abs(size) + 1;		

}

/**
 * Create an event with specified payload size (in uint16_t units).
 * The full gebData structure is created and a pointer to it returned.
 *
 * @param size - Number of uint16_t's in the event.
 * @return struct gebData*
 * @retval dynamically allocated gebData with a dynamically allocated payload.
 */
struct gebData*
makeEvent(int size)
{
  /* Alloate the gebData struct */

  struct gebData* pData = calloc(1, sizeof(struct gebData));
  uint16_t*       pDest;
  uint16_t        i;

  if (!pData) {
    fprintf(stderr, "Unable to allocated a gebData in tap simulator\n");
    exit(EXIT_FAILURE);
  }
  /* Allocate and fill the payload...linking it to pData */;

  pData->payload = malloc(size*sizeof(uint16_t));
  pDest = (uint16_t*)pData->payload;

  for (i =0; i < size; i++) {
    *pDest++ = i;
  }


  /* Now fill in the size field */
  

  pData->length = size*sizeof(uint16_t);


  return pData;
}

/*--------------------------------------------------------------------------------*
 *                                                                                *
 * Public entry points.                                                           *
 *                                                                                *
 *--------------------------------------------------------------------------------*/

/**
 *  Connection to the simulation just returns a pointer to something (see above) but does nothing.
 *  @param addr - IP address of the node to connect to.
 *  @param position - Which tap to use.  See the defines at the top of gretTapClient.h
 *  @param gebType  - What to get from the GRETTAP_GEB tap if that's theone we selected.
 *  @return struct gretTap*
 *  @retval &tapClient (junk).
 */
struct gretTap*
gretTapConnect(char *addr, int position, int gebType)
{
  return &tapClient;
}

/**
 * Generate a counting pattern event that consists of 
 * a random number of uint16_t values.  The size of the event is
 * determined by adding a signed random number [0..1)*2*TAP_SPREAD - TAP_SPREAD to the
 * the TAP_MEAN parameter.
 *
 * for each event a gebData* struct is allocated and filled in.  The payload field will
 * point to dynamically allocated storage for the event itself.
 * If several events are asked for, they are linked together via the next field.
 * 
 * @param pTap - Pointer to a gretTap that identifies the tap connection.
 * @param nreq - Number of events requested.
 * @param timeout - Number of seconds max to wait.
 * @return gebData*
 * @retval Pointer to first geb data item.
 *
 */
struct gebData*
gretTapData(struct gretTap* pTap, int nreq, float timeout)
{
  struct gebData root;		/* Root of the linked list. */
  int    eventSize;
  struct gebData* pEvent = &root;
  int    i;

  /* Clear the linked list head: */

  memset(&root, 0, sizeof(root));



  /* Generate the events:  */

  for (i = 0; i < nreq; i++) {
    eventSize = getEventSize();	/* event size changes from event to event: */

    struct gebData* nextEvent = makeEvent(eventSize);
    pEvent->next = nextEvent;
    pEvent = pEvent->next;
  }
  return root.next;
}
