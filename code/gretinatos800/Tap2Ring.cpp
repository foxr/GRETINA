/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "Tap2Ring.h"
#include "cmdline.h"
#include "gretTapClient.h"
#include <GEBLink.h>
#include <CRingBuffer.h>
#include <CRingItem.h>
#include <DataFormat.h>

#include <string.h>

#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>


/* Manifest constants:  */

static const int TapLocation(GRETTAP_TRACK);
static const int TapSelector(0);	// If GEB, take assembled data.



///////////////////////////////////////////////////////////////////////////////////////////
//
// Constructors and other canonical methods:
//
///////////////////////////////////////////////////////////////////////////////////////////


/**
 * Construction is mostly a no-op.  The m_pRing is nulled out
 */
Tap2Ring::Tap2Ring() :
  m_pRing(0)
{}
/**
 * Destruction deletes the ring which, in turn notifies the ring master we're done
 * with that consumer pointer set.
 */
Tap2Ring::~Tap2Ring()
{
  delete m_pRing;
  m_pRing = 0;			// Just in case.
}


////////////////////////////////////////////////////////////////////////////////////////
//
// Implementation of the public interface
//
////////////////////////////////////////////////////////////////////////////////////////

/**
 * Main entry point of the application.
 * - Figure out which host the tap is on and connect to it
 * - Get the ring buffer ringname and connect to it as a consumer.
 * - pass control to the main loop.
 *
 * @param args - Reference to the parsed command line parameter structure.
 *        
 */

void
Tap2Ring::operator()(gengetopt_args_info& args)
{
  m_sHostIP            = args.host_arg;
  m_nRequestCount      = args.requestcount_arg;
  std::string ringName = getRingName(args);

  // connectToRing will also create the ring if needed

  connectToRing(ringName);

  // Connect to the gretina output tap if we can:

  struct gretTap* pTap = gretTapConnect(const_cast<char*>(m_sHostIP.c_str()), 
					TapLocation, TapSelector);
  if (!pTap) {
    std::string msg = "Failed to connect to Gretina tap on host: ";
    msg            += m_sHostIP;
    throw msg;
  }

  // Process events from the tap:

  mainLoop(pTap);
  
}


//////////////////////////////////////////////////////////////////////////////////////
//
// Implementation of the private interfaces.
//

/**
 * connect to a ring as a producer.
 * @param name - Name of the ring to which we are connecting.
 *
 * @note - side effects:
 *  - m_pRing is set to be a  pointer to the CRingBuffer that represents the ring.
 */
void 
Tap2Ring::connectToRing(std::string name)
{
  // If necessary we need to create the ring:

  if (!CRingBuffer::isRing(name)) {
    CRingBuffer::create(name);	// For now all the default values for the parameters.
  }

  m_pRing = new CRingBuffer(name, CRingBuffer::producer);
}

/**
 * Main loop:   Connect Gretina...get data from it, put it in the ring 
 *              rinse and repeat.
 * @param pTap - Pointer to the gretTap opaque data structure that 
 *               defines the connection with the output tap.
 * @note implicit inputs;
 *   - m_pRing - ring buffer object that is the data destination.
 *   - m_nRequestCount - Number of events to try to get each time around.
 *   -m_sHostIP - Dotted IP address of the Gretina tap node.
 */
void
Tap2Ring::mainLoop(gretTap* pTap)
{


  while(1) {
    struct gebData* pEvents = gretTapData(pTap, m_nRequestCount, 0);
    if (!pEvents) {
      std::string msg = "Failed to get data from gretina tap on host: ";
      msg            += m_sHostIP;
      throw msg;
    }
    submitEvents(pEvents);
    releaseEvents(pEvents);
  }
}
/**
 * Submit the events from a gretina gretTapData call to 
 * the ring buffer.
 * Since we don't know exactly what we're getting when m_nRequestCount > 1,
 * We're going to assume that we can write an iterator that will get us'
 * to the next element or indicate end..and use it to go through the set
 * of events.
 * @param pEvents a struct gebData* containing the events.
 *
 * @note event structure in ring:
 *    Each event will have the following structure:
 *
 *\verbatim
 * +-----------------------------------+
 * | bytes in event (32 bits)          |
 * +-----------------------------------+
 * |  timestamp (64 bits)              |
 * +-----------------------------------+
 * | Payload in each gebData item.     |
 * +-----------------------------------+
 * 
 *\endverbatim
 */
void
Tap2Ring::submitEvents(gebData* pEvents)
{
  iterator p(pEvents);


  // Loop over all the events in the set:

  while (!p.end()) {

    // Extract the stuff we need from the gebData struct:

    int length     = p->length;
    void* payload  = p->payload;
    long long timestamp = p->timestamp;

    // Generate the physics event and locate where we're going to put the data:

    CRingItem item(PHYSICS_EVENT, 100+ length*2); // Will ensure I never resize; handles 0 edge case too.
    uint8_t*  pDest      = reinterpret_cast<uint8_t*>(item.getBodyPointer());

    uint32_t totalLength = length + sizeof(length) + sizeof(timestamp); // self-inclusive length.

    // Copy in the data and compute the end pointer.

    memcpy(pDest, &totalLength, sizeof(length));
    pDest += sizeof(uint32_t);
    memcpy(pDest, &timestamp, sizeof(timestamp));
    pDest += sizeof(timestamp);
    memcpy(pDest, payload, length);
    pDest += length;

    // Close out the event and commit it to the ring.

    item.setBodyCursor(pDest);
    item.updateSize();
    item.commitToRing(*m_pRing);

    // Do the same for the next event in the set.

    p++;
  }
}

/**
 * Free data associated with events from the gretina tap.  The assumption here is that
 * events can be freed individually (that's actually the hardest case).
 *
 * @param pEvents - pointer to the events gotten from the tap.
 */
void
Tap2Ring::releaseEvents(struct gebData* pEvents)
{
  iterator p(pEvents);
  while(!p.end()) {
    //
    // We need to advance to the next item before deleting the prior one
    // just in case deleting makes navigation impossible:

    gebData* deleteMe = p.get();
    p++;
    free(deleteMe->payload);	// Assume payload is dynamic too.
    free(deleteMe);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Implement the gebData iterator:
//
// Assumption:
//   The gebData items are a linked list linked via the next pointer in the gebData
//   items themselves...and that they are grounded in a NULL Pointer at the last item.
//
////////////////////////////////////////////////////////////////////////////////////////

/**
 * construct the iterator.
 * @param pEvents - pointer to the first event in the chain.
 *
 */
Tap2Ring::iterator::iterator(gebData* pEvents) :
  m_pEventList(pEvents),
  m_pLocation(pEvents)
{}

/**
 * Return a reference to the current item the iterator points at.
 * this is a disaster if this->end() is true.  Specifically a 
 * SEGFAULT will occur.
 *
 * @return gebData&
 * @retval *m_pLocation;
 */
gebData&
Tap2Ring::iterator::operator*()
{
  return *m_pLocation;
}
/**
 * Dereferencing operator for the current item.  This is likely
 * to result in a segfault if this->end() is true.
 * @return gebData*
 * @retval m_pLocation 
*/
gebData*
Tap2Ring::iterator::operator->()
{
  return m_pLocation;
}
/**
 * Post decrement operator.. this sets m_pLocation -> m_pLocation->next
 * @return iterator
 * @retval *this
 */
Tap2Ring::iterator
Tap2Ring::iterator::operator++(int junk)
{
  m_pLocation = m_pLocation->next;
  return *this;
}
/**
 * Return the pointer to the current location
 * @return gebData*
 * @return m_pLocation
 */
gebData*
Tap2Ring::iterator::get() 
{
  return m_pLocation;
}

/**
 * Return true if the location is now off the end of the linked list.
 * @return bool
 * @retval false if operator* et fils is valid.
 * @retval true if the iterator has gone off the end of the list.
 */
bool
Tap2Ring::iterator::end()
{
  return m_pLocation == 0;
} 
/**
 * Rewind the iterator to the beginning of the sequence.
 */
void
Tap2Ring::iterator::rewind()
{
  m_pLocation = m_pEventList;
}
/**
 * Return the name of the ring to use as a destination.
 * if the ring name was suppplied we use that otherwise -gretina is appended
 * to the logged in username.
 * @param args result of the command line parse.
 * @return std::string
 * @retval the ring name.
 */
std::string
Tap2Ring::getRingName(gengetopt_args_info& args)
{
  /* If the user supplied the name, use it: */

  if (args.ring_given) {
    return std::string(args.ring_arg);
  }

  // Figure out our username by looking up our uid and our pwentry from the
  // uid:

  uid_t uid = getuid();
  struct passwd Entry;
  struct passwd* pEntry;
  char          stuff[1024];
  
  if (getpwuid_r(uid, &Entry, stuff, sizeof(stuff), &pEntry)) {
    throw std::string("Failed getting password entry for this user");
  }
  else {
    std::string result = Entry.pw_name;
    result += "-gretina";
    return result;
  }
  
}
 
