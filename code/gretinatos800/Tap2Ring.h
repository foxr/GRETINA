/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef __TAP2RING_H
#define __TAP2RING_H


#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#define __STL_STRING
#endif
#endif



struct gengetopt_args_info;

class CRingBuffer;
struct gebData;
struct gretTap;

/**
 * This class is the main application class of 
 * gretina2ring  Fundamentally, once initialized it just loops getting data
 * from the Gretina tap and shoving it in the indicated ring buffer.
 *
 */
class Tap2Ring
{
private:
  CRingBuffer* m_pRing;
  int          m_nRequestCount;
  std::string  m_sHostIP;

public:
  Tap2Ring();
  ~Tap2Ring();


public:
  void operator()(gengetopt_args_info& args);

private:
  void connectToRing(std::string ringName);
  void mainLoop(gretTap* pTap);
  void submitEvents(struct gebData* pEvents);
  void releaseEvents(struct gebData* pEvents);
  std::string getRingName(gengetopt_args_info& args);

  // Internal classes:

  /**
   * The iterator class below provides a method to iterate through the set of events
   * returned by the gretTapData function.  Since it's  not well documented how multiple events
   * are linked together this detail is encapsulated in an interator so that it can easily be
   * changed should I have guessed wrong.
   */

public:
  class iterator {
    struct gebData* m_pEventList;
    struct gebData* m_pLocation;
  public:
    iterator(struct gebData* pEvents);
    gebData& operator*();
    gebData* operator->();
    iterator operator++(int);
    gebData* get();
    bool     end();
    void     rewind();
  };

};

#endif
