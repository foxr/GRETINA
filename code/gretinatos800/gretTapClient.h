
#ifndef __GRETTAPCLIENT_H
#define __GRETTAPCLIENT_H

#define GRETTAP_RAW		1
#define GRETTAP_CRYSTAL_EVENT	2
#define GRETTAP_POSITION	3
#define GRETTAP_GEB		4
#define GRETTAP_TRACK		5

#ifdef __NSCL_SIMULATOR
typedef int epicsMutexId;
#endif

struct gretTap {
   int inSock;
   epicsMutexId connectionMutex;
}; 


#ifdef __cplusplus
extern "C" {
#endif
/* 
 * Connect to a tap.  
 * addr is the ip address of the node to connect to in dotted numeric format. 
 * position indicates which of the available taps are 
 * to be used, from the list above.  GRETTAP_RAW can be found in digitizer 
 * crates, CRYSTAL_EVENT and POSITION are in signal decomposition nodes, 
 * GEB in the global event builder, and TRACK in tracking nodes.
 * gebType indicates what type of data is desired; this is only necessary in 
 * the case of GRETTAP_GEB where various auxiliary detector data may be 
 * available.
 *
 * The struct returned is used only for calling the other functions below and
 * is not for user manipulation.
 */
struct gretTap *gretTapConnect(char *addr, int position, int gebType); 
/* 
 * get tap data
 * Returns a gebData struct containing some data.  The gebData struct itself
 * gives a type, timestamp and length of the attached buffer.
 * 
 * nreq specifies amount of data to get.  This call actually goes and gets the
 * info.
 * 
 * Timeout implementation low priority
 */
struct gebData *gretTapData(struct gretTap *, int nreq, float timeout);
/* 
 * close tap
 */
void gretTapClose(struct gretTap *);
/* 
 * check tap.  return of 0 indicates things are fine, 1 that there is some
 * fatal problem.
 */
int checkGretTap(struct gretTap *);

#ifdef __cplusplus 
}
#endif

#endif
