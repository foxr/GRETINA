/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/



#include "cmdline.h"
#include "Tap2Ring.h"

#include <stdlib.h>
#include <string>
#include <iostream>

/**
 * Entry point for the main program.
 *
 * gretina2ring --host={ip address} [--ring={ringname}] [--requestcount={nevents}]
 *
 * Where:
 *   -  --host specifies the ip address of the tap server.  At present this is
 *      specified in ip address format, dns does not operate in the Gretina cluster.
 *      This switch is mandatory.
 *   -  --ring specifies a destination ring other than the default.
 *      The default ring name is {username}-gretina
 *   -  --requestcount Specifies the number of events to get from each call to gretTapData
 *      default value is 1.
 *
 * @param argc  - The number of command line words after substitution.
 * @param argv  - Vector of argc pointers to the command line words after substitution.
 */
int
main(int argc, char* const* argv)
{
  try {
    // Use the gengetopt generated parser to extract the command line switches

    struct gengetopt_args_info parsedArgs;
    int status = cmdline_parser(argc, argv, &parsedArgs);
    if (!status) {

      // The command line successfully parsed so create and run the application
      // as described by the switches.

      Tap2Ring application;
      application(parsedArgs);
    }
    else {
      throw std::string("Failed to parse command line args see 'gretina2ring --help' for advice");
    }
  }
  catch (std::string msg) {
    std::cerr << "Main level code caught a string exception: " << msg << std::endl;
    exit(EXIT_FAILURE);
  }
  catch (const char* msg) {
    std::cerr << "Main level code caught a const char* exception " << msg << std::endl;
    exit(EXIT_FAILURE);
  }
  catch (...) {
    std::cerr  << "Main level code caught an exception of an ananticipated type.  Rethrowing" 
	       << std::endl;
    throw;
  }
  exit(EXIT_SUCCESS);

}
