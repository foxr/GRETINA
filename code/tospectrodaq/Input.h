/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef _INPUT_H
#define _INPUT_H

#include <unistd.h>

/**
 * \file Input.h
 * \class CInput
 *
 *  This class knows how to read data from an input stream created by the
 *  Global eventbuilder.
 *  Each event int this stream looks like:
 * \verbatim
 *    +--------------------------+
 *    |  no. fragments   (int)   |
 *    +--------------------------+
 *    | frag 1                   |
 *    \                          \
 *    /                          /
 *    +--------------------------+
 *       ...
 *\endverbatim
 *
 * Where each fragment is of the form:
 *
 * \verbatim
 *
 *   +----------------------------+
 *   | fragment id   (int)        |
 *   +----------------------------+
 *   | payload size in bytes (int)|
 *   +----------------------------+
 *   | timestamp (long long)      |
 *   +----------------------------+
 *   |  Payload ...               |
 *   
 *\endverbatim
 *
 */
class CInput
{
 public:
  size_t getEvent(void** ppData);
 private:
  size_t readFragment(void** pBuffer, size_t offset, size_t currentSize);
  ssize_t read(void* pBuffer, size_t nBytes);
};


#endif
