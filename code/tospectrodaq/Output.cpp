/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include <config.h>
#include "Output.h"
#include <buffer.h>
#include <string.h>
#include <iostream>

using namespace std;
#include <spectrodaq.h>
/**
 * \file Output.cpp - implementation of COutput.
 */


/**
 * Construct the COutput class:
 * Create the storage,
 * Initialize the next buffer. (We don't care about the fact that
 * the first buffers sequence will be 1 not 0.
 * @param number of uint16_t words in a buffer.
 * @note all units of size in this module are uint16_t
 */
COutput::COutput(size_t bufferSize) :
  m_nBufferSize(bufferSize),
  m_pStagingBuffer(0),
  m_pHeader(0),
  m_pTarget(0),
  m_nBufferPayloadSize(bufferSize - sizeof(bheader)/sizeof(uint16_t)),
  m_nSequence(0)
  
{
  m_pStagingBuffer = new uint16_t[bufferSize];
  m_pHeader       = reinterpret_cast<bheader*>(m_pStagingBuffer);

  initNextBuffer();		// Gets the rest of the book keeping right.
}
/**  
 * Destruction.. just get rid of the staging buffer:
 */
COutput::~COutput()
{
  delete []m_pStagingBuffer;
}
/**
 * Put the next event in the buffer.
 * @param number of words in the event (uint16_t)
 * @param pData - Pointer to the event...which needs to also have
 *                 the word count prepended to it.
 * @note the caller has ensured that events fit in a full buffer.
 */
void
COutput::putEvent(size_t eventSize, void* pData)
{
  uint16_t fullSize = eventSize + sizeof(uint16_t); // event counts/size include a count.

  if (!canFit(fullSize)) {
    flush();
    initNextBuffer();
  }
  // Copy the data:

  *m_pTarget++ = fullSize;
  memcpy(m_pTarget, pData, eventSize*sizeof(uint16_t));
  m_pTarget += eventSize;

  // Update header fields:

  m_pHeader->nwds += fullSize;
  m_pHeader->nevt++;
  
}
/**
 * Initialize the next buffer:
 * - increment the sequence number.
 * - Reset various pointers.
 * - Set a bunch of header fields.
 */
void
COutput::initNextBuffer()
{
  m_pTarget = reinterpret_cast<uint16_t*>(&(m_pHeader[1]));

  m_nSequence++;

  m_pHeader->nwds = sizeof(bheader)/sizeof(uint16_t); // Reset buffer size.
  m_pHeader->type = 1;
  m_pHeader->cks = 0;
  m_pHeader->run = 0;
  m_pHeader->seq = m_nSequence;
  m_pHeader->nevt= 0;
  m_pHeader->nlam = 0;
  m_pHeader->cpu = 0;
  m_pHeader->nbit = 0;
  m_pHeader->buffmt = BUFFER_REVISION;
  m_pHeader->ssignature = 0x0102;
  m_pHeader->lsignature = 0x01020304;

  
}
/**
 * dump the buffer to spectrodaq.  A daqwordbuffer is created of the appropriate size
 * and copy in is used to fill it from our buffer.
 * The buffer is then routed to spectrodaq and released.
 */
void
COutput::flush()
{

  DAQWordBuffer buffer(m_nBufferSize);
  buffer.SetTag(2);		// Event data tag.
  buffer.CopyIn(m_pStagingBuffer, 0,
		m_pHeader->nwds);


  buffer.Route();		// Send to spectrodaq.


}
/**
 * Returns true if there is space for an event in the remaining free space of the buffer.
 * @param size - number of words in the event (including the word count).
 * @return bool
 * @retval true - sufficient space.
 * @retval false - insufficient space.
 */
bool
COutput::canFit(unsigned size)
{
  return (size + m_pHeader->nwds) < m_nBufferSize;
}
