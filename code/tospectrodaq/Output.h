/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef _OUTPUT_H
#define _OUTPUT_H

#include <stdint.h>
#include <stdlib.h>

/**
 * \file Output.h
 *  The COutput class takes assembled events, buffers them up
 *  and ships them off to spectrodaq.
 *  Event data are buffered in a local buffer until the next event
 *  won't fit.  At that time
 *  - The output buffer is flushed to spectrodaq
 *  - A new output buffer is started.
 *  - The event is shoved into the new buffer. rather than the old buffer.
 *
 */
struct bheader;

class COutput
{
  /* Local data: */
private:
  size_t    m_nBufferSize;	// Full size of buffer in words.
  uint16_t* m_pStagingBuffer;	// where events are put before flushing to spectrodaq.
  bheader*  m_pHeader;		// Buffer header.
  uint16_t* m_pTarget;		// Where data are put int the buffer.
  size_t    m_nBufferPayloadSize; // size of the buffer payload (words)
  uint32_t  m_nSequence;	  // Buffer sequence number.

  /* Public methods. */

public:
  COutput(size_t bufferSize);
  ~COutput();
  void putEvent(size_t eventSize, void* pData);

  // utility functions

private:
  void initNextBuffer();
  bool canFit(size_t eventSize);
  void flush();
};


#endif
