/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/


/**
 * \file Input.cpp Implementation of the CInput class.
 */  
#include "Input.h"
#include <string>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>

/* Local data types: */

typedef struct _fragmentHeader {
  int type;			// Fragment id.
  int length;			// Length of fragment payload.
  long long timestamp;		// Timestamp of fragment.

} fragmentHeader, *pFragmentHeader;

/**
 *  Get an event fragment from stdin (STDIN_FILENO).
 *  If there an error is detected, a string exception is fabricated and thrown. 
 *  @param ppData  - A pointer to a void* that will be filled in with a pointer
 *                   to dynamically allocated storage that will hold the event.
 *                   Note that this storage is allocated with the malloc
 *                   family of functions and should, therefore, be released
 *                   by the caller with free(3).
 * @return size_t
 * @retval Number of 16 bit words of storage consumed by the event.
 *
 */
size_t
CInput::getEvent(void** ppData)
{
  /* Start out by allocating storage for the fragment count and the first fragment header: */

  size_t currentSize = sizeof(int) + sizeof(fragmentHeader);
  void* pResult(0);
  size_t offset = sizeof(int);	// Where the first fragment lives.

  pResult = realloc(pResult, currentSize);
  if (!pResult) {
    throw std::string("Allocation failure in getEvent");
  }

  

  ssize_t n  = read(pResult, sizeof(int)); // Get the fragment count.
  if (n <= 0) {
    throw std::string("Read of fragment count failed -- or EOF");
  }
  int nFragments = *(reinterpret_cast<int*>(pResult));
  if (!nFragments) {
    throw std::string("Some big nasty error - an event with  no fragments!!");
  }

  /* Read the fragments: */

  for (int i = 0; i < nFragments; i++) {
    size_t nFragmentSize = readFragment(&pResult, offset, currentSize);
    offset      += nFragmentSize;
    currentSize += nFragmentSize;
  }

  *ppData = pResult;
  return offset/sizeof(uint16_t);


     
}
/**
 * read a fragment from the input stream.
 * - If current buffersize - offset  < sizeof(fragmentHeader), expand the buffer.
 * - Read the fragment header
 * - Figure out the size.
 * - Expand the buffer to contain the fragment and the next fragment header (to reduce
 *   malloc count).
 * - Read the fragment body.
 * 
 * @param ppBuffer - Pointer to the dynamically allocated data stroage (in/out).
 * @param offset   - Where in *ppBuffer data should be stored.
 * @param currentSize - Number of bytes of buffer space currently available.
 * @return size_t
 * @retval Number of bytes read.
 */
size_t
CInput::readFragment(void** pBuffer, size_t offset, size_t currentSize)
{

  void* pTarget;
  size_t nTotalRead = 0;

  /* If we don't have sufficient storage to read the header, expand the buffer. */

  if ((currentSize - offset) < sizeof(fragmentHeader)) {
    currentSize += sizeof(fragmentHeader); // sloppy but easy.
    *pBuffer     = realloc(*pBuffer, currentSize);
    if (!*pBuffer) {
      throw std::string("Unable to resize buffer in readFragment to fit headers\n");
    }
  }
  /* Read the header  */
  
  pTarget = reinterpret_cast<uint8_t*>(*pBuffer) + offset;
  ssize_t nRead = read(pTarget, sizeof(fragmentHeader));
  if (nRead <= 0) {
    throw std::string("Failed on fragment header read --or EOF");
  }
  
  offset     += nRead;
  nTotalRead += nRead;
  
  /* Figure out the size of the event fragment and expand the buffer to hold it
   * (the buffer is expanded by size + sizeof(fragmentHeader)...lookahead expansion).
   * We assume the buffer will not be big enough (sloppy but easy and safe).
   */
  
  pFragmentHeader pHeader      = reinterpret_cast<pFragmentHeader>(pTarget);
  int             fragmentSize = pHeader->length;
  currentSize    += fragmentSize + sizeof(fragmentHeader);
  *pBuffer        = realloc(*pBuffer, currentSize);
  if (!pBuffer) {
    throw std::string("Unable to resize input buffer to read fragment body");
  }
  
  /* Read the fragment body: */
  
  pTarget =  reinterpret_cast<uint8_t*>(*pBuffer) + offset; // Must do this as buffer base may have moved.
  nRead   =  read(pTarget, fragmentSize);

  if (nRead <= 0) {
    throw std::string("Read of fragment body failed, or EOF");
  }
  nTotalRead += nRead;
    
  return nTotalRead;
    
    
}
/**
 *  Reads a block of data from STDIN_FILENO.. handles all the sorts of things that can
 *  happen including:
 *  - incomplete reads due to pipe/socket buffering.
 *  - restarting reads due to EINTR or EAGAIN or EWOULDBLOCK
 * @param pBuffer - Pointer to where data should be stored. The caller must ensure
 *                  sufficient writable storage is available.
 * @param nBytes  - Number of bytes desired.
 * @return ssize_t
 * @retval Number of bytes read on success.
 * @retval 0    if eof prior to completing the requested read.
 * @retval <0   if error.
 */
ssize_t 
CInput::read(void* pBuffer, size_t nBytes)
{
  size_t   nRemaining(nBytes);

  uint8_t* pTarget(reinterpret_cast<uint8_t*>(pBuffer));

  while (nRemaining > 0) {
    ssize_t n = ::read(STDIN_FILENO, pTarget, nRemaining);
    if (n < 0) {		// error if not EAGAIN, EWOULDBLOCK or EINTR return -1.
      if ((errno != EAGAIN)      &&
	  (errno != EWOULDBLOCK) &&
	  (errno != EINTR)) {
	return -1;
      }
    }
    else if (n == 0) {		// EOF before comopleting read:
      return 0;
    }
    else {			// normal completion:
      pTarget    += n;
      nRemaining -= n;
    }

  }

  return nBytes;		// Gauranteed.
}
