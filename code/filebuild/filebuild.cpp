/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

/**
 * This is a simple program to feed data from a gretina event file to the gretina global event builder.
 * Usage:
 *    filebuild [options] input-file output-file
 *
 * Options:
 *      --dt   - Timestamp difference for event building.
 *      --geb  - Directory that has the GEB binary.  This defaults to the value of the
 *               GEBDIR env variable if defined.
 *
 * The GEB is run as a child process to us via fork()/system()
 *
 */

#include "cmdline.h"
#include "GretinaEvent.h"
#include <GEBClient.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <fstream>

static const char* GEBENVNAME="GEBDIR";
#define GRETINA_FRAGMENT 0x12345678


/**
 * Determine the full path to the GEB.
 * - If --geb is specified, use that as the directory name.
 * - If not try to translate GEBENVNAME
 * if that all fails toss a string exception indicating 
 *
 * @param cmdData - reference to the gengetopt_args that represent the pased
 *                  argc/argv.
 * @return std::string
 * @retval String containing the GEB image full path.
 */
static std::string
getGEBPath(struct gengetopt_args_info& cmdData)
{

  // Figure out which directory the GEB image lives in.

  const char* pPath(0);
  if (cmdData.geb_given) {
    pPath = cmdData.geb_arg;
  } 
  else {
    pPath = getenv(GEBENVNAME);
  }
  if (!pPath) {
    throw std::string("No --geb option supplied and GEBDIR env variable does not translate");
  }

  // Construct the path to the GEB image:

  std::string geb = pPath;
  geb += "/bin/GEB";

  return geb;
}

/**
 * Start the global event builder:
 * - Construct the full command string needed to run the GEB.
 * - Fork to start a subproces.
 * - In the fork, use system to start up the GEB pipeline.
 * @param gebPath - full path to the global event builder executable.
 * @param delta   - Ticks in the timestamp that represent an event.
 * @param outfile - Path to the output file.
 * @return pid_t
 * @retval the process id the event builder is running under.
 *         (actually the pid of the fork that runs the system(3) call that runs the
 *          GEB.
 * @retval -1 if the process could not be started or the GEB path is invalid.
 */
pid_t
startGeb(std::string gebPath, int delta, std::string outputFile)
{
  // Check there's a GEB at that path 

  int status = access(gebPath.c_str(), X_OK);
  if (status == -1) {
    return -1;
  }
  
  // fork and system:

  pid_t result = fork();
  if (result == 0) {
    char intbuf[100];
    std::string command = gebPath;
    command            += " ";
    sprintf(intbuf, "%d", delta);
    command += intbuf;
    command += " > ";
    command += outputFile;

    // Command should be of the form .../GEB delta > outputFile

    system(command.c_str());

    exit(EXIT_SUCCESS);
  }
  else {
    return result;
  }

}
/**
 * Send an event file to the builder an event at a time for building.
 * @param file Name of the file to build.
 */

static
void
buildEvents(std::string file)
{
  //  Check we can read the file:

  if(access(file.c_str(), R_OK) == -1) {
    std::string msg = "Failed to open in put file: ";
    msg            += file;
    throw msg;
  }

  // wait for the GEB to start and listen for connections:

  sleep(1);

  // Open the file

  std::ifstream f(file.c_str());
  if (!f) {
    throw std::string("Open failed on input event file");
  }


  // Initialize the event builder connection

  struct gebClient* pGEB = GEBClientInit();
  if(!pGEB) {
    throw std::string("Initialization of GEB access failed");
  }

  int status = setGEBClient(pGEB, "localhost", GEB_PORT);
  if (status != 0) {
    throw std::string("Failed to connect to GEB");
  }


  // Send the file

  while (!f.eof()) {
    Crystal fragment;
    struct gebData ebFragment = {
      GRETINA_FRAGMENT,		// fragment type.
      sizeof(Crystal),		// Size of fragment payload.
      0,			//Timestamp ..from each event.
      &fragment			// Fragment payload.
    };

    f.read(reinterpret_cast<char*>(&fragment), sizeof(Crystal));
    if(f.gcount() == sizeof(Crystal)) {
      ebFragment.timestamp = fragment.timestamp;
      status = sendGEBData(pGEB, &ebFragment);
      if (status != 0) {
	throw std::string("failed to send fragment to event builder.");
      }
    }

  }

  //Close down the connection
}


/*------------------------------------------------------------------------------- */

/**
 * Parse the parameters,
 * Start up the GEB,
 * Process the input file.
 * wait a bit for the GEB to finish
 * kill the GEB
 * exit victoriously.
 */
int main(int argc, char**argv)
{
  struct gengetopt_args_info cmdData;

  cmdline_parser(argc, argv, &cmdData);

  // Require an input and output file:

  if (cmdData.inputs_num != 2) {
    std::cerr << "Missing input or output data filename\n";
    cmdline_parser_print_help();
  }

  std::string inputFile  = cmdData.inputs[0];
  std::string outputFile = cmdData.inputs[1];

  // Figure out the GEB name and start it if we can:
  
  pid_t gebPid; 
  try {
    std::string gebPath = getGEBPath(cmdData);
    gebPid              = startGeb(gebPath, cmdData.dt_arg, outputFile);
    if (gebPid == -1) {
      int         e   = errno;
      std::string msg = "Failed to start  GEB : ";
      msg            += strerror(e);
      throw e;
    }
    // Now send the file an event at a time to the event builder:

    buildEvents(inputFile);

    // Wait a bit for the event builder to finish

    std::cout << "Waiting for the event builder to complete\n";
    sleep(1);
    
    // Kill it off:


    system("killall -9 GEB");
  }
  catch(std::string msg) {
    std::cerr << msg << std::endl;
    cmdline_parser_print_help();
    exit(EXIT_FAILURE);
  }

}
