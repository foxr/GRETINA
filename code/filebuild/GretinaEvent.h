/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef __GRETINAEVENT_H
#define __GRETINAEVENT_H


#define GRETINA_MAX_CRYSTAL_INTERACTIONS 16
/**
 *  The struct below defines an interaction point:
 */
typedef struct _InteractionPoint {
  float x;
  float y;
  float z;
  float e;
} InteractionPoint;

typedef struct _Crystal {
  int                 nInteractions;
  int                 crystalId;
  float               totalE;
  float               t0;
  float               chiSquared;
  float               chiSquaredN;
  long long           timestamp;
  InteractionPoint    points[GRETINA_MAX_CRYSTAL_INTERACTIONS];
} Crystal;


#endif
