#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Jeromy Tompkins 
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



##
# @file bigled.tcl
# @brief Provide the big alarm LED gretina shows.
# @author Ron Fox <fox@nscl.msu.edu>
#

package require Tk
package require epics
package require led

set size 200
if {[llength $argv] > 0} {
    set provSize [lindex $argv 0]
    if {[string is integer -strict $provSize] && ($provSize > 0)} {
	set size $provSize
    }
}
set Alarm 0
::controlwidget::led .led -size $size -off red -on green -variable Alarm

proc monitor timing {
    catch {
	if {[SystemAlarmCheck.SEVR get] eq "NO_ALARM"} {
	    set ::Alarm 1
	} else {
	    set ::Alarm 0
	}
    }
    after $timing [list monitor $timing]
}


epicschannel SystemAlarmCheck.SEVR

monitor 500

pack .led
