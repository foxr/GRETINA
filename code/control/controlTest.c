/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include <check.h>
#include <gretinaControl.h>
#include <simControl.h>
#include <stdlib.h>
#include <unistd.h>

void setupSimulator()
{
  simulator_init();
  gretina_rctl_init();
  sleep(1);
  simulator_state_setup();
  usleep(500);
  gretina_rctl_stop();
  usleep(500);

}



/**
 ** If the simulator is in the initial state a begin should fail:
 */
START_TEST(test_start_fails)
{
  int status;
  simulator_state_initial();
  usleep(250);			/* Wait for this to propagate. */
  status = gretina_rctl_start();
  fail_unless(status == GRETINA_RCTL_BADSTATE);
  
}
END_TEST

/*
** Force the simulator into the 
** Setup state.. should then be able to start the run.
*/

START_TEST(test_start_ok)
{
  int status;
  simulator_state_setup();
  usleep(500);
  gretina_rctl_stop();		/* incase start is asserted. */
  usleep(500);
  status = gretina_rctl_start();
  fail_unless(status == GRETINA_RCTL_OK);
}
END_TEST
/*
** With the simulator in any state but Run, stopping the run should fail:
*/

START_TEST(test_stop_fails)
{
  int status;
  simulator_state_inconsistent();
  usleep(250);
  status = gretina_rctl_stop();
  fail_unless(status == GRETINA_RCTL_BADSTATE);
}

END_TEST

/**
 ** forcethe state to running .. should be able to stop.
 */
START_TEST(test_stop_ok)
{
  int status;
  simulator_state_run();
  usleep(250);			/* wait for start to propagate->running */
  gretina_rctl_start();
  usleep(250);			/* in case stop was asserted. */
  status = gretina_rctl_stop();
  fail_unless(status == GRETINA_RCTL_OK);
}
END_TEST


/**
 * In any state but Setup, can_start gives false
 */
START_TEST(test_nostart)
{
  simulator_state_wfd();
  usleep(250);
  fail_if(gretina_rctl_can_start());
}
END_TEST

START_TEST(test_startok)
{
  simulator_state_setup();
  usleep(250);
  gretina_rctl_stop();		/* In case start is asserted. */
  usleep(250);
  fail_unless(gretina_rctl_can_start());
}
END_TEST


/*
** In any case but running can_stop should give false:
*/
START_TEST(test_nostop)
{
  simulator_state_wait();
  usleep(250);
  fail_if(gretina_rctl_can_stop());
}
END_TEST
START_TEST(test_stopok)
{
  simulator_state_run();
  usleep(500);
  gretina_rctl_start();		/* in case stop is asserted. */
  usleep(250);
  simulator_state_run();
  usleep(250);
  fail_unless(gretina_rctl_can_stop());
}
END_TEST


int main(int argc, char** argv)
{
  Suite* s1          = suite_create("gretina");
  TCase *tc_gretina = tcase_create("gretina_control");
  TCase *tc_gretina_check = tcase_create("gretina_check");

  SRunner* sr       = srunner_create(s1);
  int      failures;



  setupSimulator();

  /* Add/run the tests */

  /* State transitions */

  tcase_add_test(tc_gretina, test_start_fails);
  tcase_add_test(tc_gretina, test_start_ok);
  tcase_add_test(tc_gretina, test_stop_fails);
  tcase_add_test(tc_gretina, test_stop_ok);


  suite_add_tcase(s1, tc_gretina);

  /* Checking ability to start/stop runs */

  tcase_add_test(tc_gretina_check, test_nostart);
  tcase_add_test(tc_gretina_check, test_startok);
  tcase_add_test(tc_gretina_check, test_nostop);
  tcase_add_test(tc_gretina_check, test_stopok);

  suite_add_tcase(s1, tc_gretina_check);

  srunner_set_fork_status(sr, CK_NOFORK);
  srunner_run_all(sr, CK_NORMAL);
  failures = srunner_ntests_failed(sr);
  srunner_free(sr);

  exit(failures ? EXIT_FAILURE : EXIT_SUCCESS);
}
