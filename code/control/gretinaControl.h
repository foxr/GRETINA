/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef __GRETINA_CONTROL_H
#define __GRETINA_CONTROL_H

#ifndef __CRT_UNISTD_H
#include <unistd.h>
#ifndef __CRT_UNISTD_H
#define __CRT_UNISTD_H
#endif
#endif


/**
 ** Header to include in applications that need to control the Gretina
 ** detector
 */


/** Status returns:
 */
#define GRETINA_RCTL_OK        0
#define GRETINA_RCTL_NOTCONN   -1 /* Not connected to gretina. */
#define GRETINA_RCTL_BADSTATE  -2 /* Invalid state. to attemp transition */
#define GRETINA_RCTL_TRUNCATE  -3 /* State copy truncated the string */

/*  These symbols must be kept in sync with the gretina record defs */

#define GRETINA_RCTL_STATE_INITIAL 0
#define GRETINA_RCTL_STATE_FAILED  1
#define GRETINA_RCTL_STATE_CHECK   2
#define GRETINA_RCTL_STATE_RUN     3
#define GRETINA_RCTL_STATE_WAIT    4
#define GRETINA_RCTL_STATE_SETUP   5
#define GRETINA_RCTL_STATE_WAITFORDONE 6
#define GRETINA_RCTL_STATE_INCONSISTENT 7


#define GRETINA_RCTL_STATE_MAXLEN  20

/*
** Functions you can call:
*/
#ifdef __cplusplus
extern "C" {
#endif

  int  gretina_rctl_init();	/* One time initialization */
  int  gretina_rctl_start();	/* Start taking data */
  int  gretina_rctl_stop();	/* Stop taking data  */
  int  gretina_rctl_can_start();	/* Test correct state for taking data */
  int  gretina_rctl_can_stop();	/* Test correct state for stopping data taking. */
  int  gretina_rctl_statet(char*  pState, 
			   size_t maxlen); /* Return the current gretina state as a text string */
  int  gretina_rctl_state();    /* Return integer version of gretina state.    */
  

#ifdef __cplusplus
}
#endif


#endif	/* __GRETINA_CONTROL_H */
