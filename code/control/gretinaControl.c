/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "gretinaControl.h"
#include <cadef.h>
#include <epicsThread.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

/** stdbool is _supposed_ to define TRUE/FALSE but...
 */

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE (!FALSE)
#endif


/*
** these variables contain the state.
*/

static int isConnected = 0;
static char*      pCurrentValue = NULL;
static int        currentState  = 0;
static chid       statusChannel;
static chid       controlChannel;

pthread_mutex_t   lock = PTHREAD_MUTEX_INITIALIZER; /* Syncrhonizes access to pCurrentValue. */

static const char*  controlPV = "Online_CS_StartStop";
static const char*  statePV   = "Cluster_CV_State";

/*-----------------------------------------------------------------------------
** Private functions.
*-----------------------------------------------------------------------------*/
/**
 ** Event pump.. this is a thread that just runs ca_pend_event in an infinite
 ** loop
 */
static void* 
eventPump(void* junk)
{
  while(1) {
    ca_pend_io(1.0);
    ca_pend_event(.5);
  }
}

/**
 ** Lock the mutex that protecdts access to the current value string.
 */
static void
lockMutex()
{
  pthread_mutex_lock(&lock);
}
/**
 ** Unlock the mutex that protects access to the current string value.
 */
static void
unlockMutex()
{
  pthread_mutex_unlock(&lock);
}

/**
 ** Connection state change handler for the status variable.
 ** We are only interested in disconnects.. using that to set isConnected false.
 ** isConnected becomes true as a result of value change operations as for our
 ** purposes, connection without knowing the value is worthless.
 */
static void 
connectionHandler(struct connection_handler_args args)
{
  if(args.op == CA_OP_CONN_DOWN) {
    isConnected = FALSE;
  }

}

/**
 ** Change handler for the string version of the state.
 ** - Lock the mutex.
 ** - free any pre-existing string
 ** - malloc a new string big enough for the new state.
 ** - copy the new state in
 ** - Unlock the mutex.
 */
static void
stringChangeHandler(struct event_handler_args args)
{
  lockMutex();

  if (pCurrentValue) {
    free(pCurrentValue);
  }

  const char* pNewValue = (const char*)(args.dbr);
  pCurrentValue = malloc(strlen(pNewValue) + 1);
  if(!pCurrentValue) {
    perror("Malloc failed on stringChangeHandler");
    exit(EXIT_FAILURE);		/* not much else we can do. */
  }
  strcpy(pCurrentValue, pNewValue);

  isConnected = TRUE;

  unlockMutex();
}
/**
 ** Change handler tha maintains the currentState variable.
 ** this does not require mutex handling as we assume an integer store
 ** is processor atomic.
 */
static void
intChangeHandler(struct event_handler_args args)
{
  /* more casts than you can shake a stick at but...
  ** I'm getting a pointer which is actually pointing at a long which I then have to
  ** stuff into an integer.
  */
  currentState = (int)*((long*)(args.dbr));
  isConnected  = TRUE;
}

/*----------------------------------------------------------------------------
** Public functions.
**----------------------------------------------------------------------------*/

/**
 ** Initialize the gretina control library:
 ** - We search for and connect to both the control and the
 **   status channel
 ** - Establish a connect/disconnect callback for the status channel.
 ** - Establish a value changed callback for the status channel
 ** - Allow I/O and events to complete.
 ** - Start a thread whose purpose in life is to ensure that events are dispatched.
 ** @return int
 ** @retval 0     - Success.
 ** @retval other - an EPICS error value describing the problem (ECA_xxxx()
 */
int
gretina_rctl_init()
{
  int status;
  epicsThreadId threadId;

  status = ca_task_initialize();
  if (status != ECA_NORMAL) {
    return status;
  }
  /* Simplest is the control PV as we don't bother with a
  ** callback for it since it's an output only variable
  ** and the system status is reflected in the status channel.
  */
  status = ca_search_and_connect(controlPV,
				 &controlChannel,
				 NULL, NULL);
  if (status != ECA_NORMAL) {
    return status;
  }
  /*
  ** Connect to the status channel.. establish the callbacks to keep track of connection
  ** and value changes; both an integer and string change handler are established
  ** to maintain in turn currentState and pCurrentValue
  */

  status = ca_search_and_connect(statePV,
				 &statusChannel,
				 connectionHandler,
				 NULL);
  if (status != ECA_NORMAL) {
    return status;
  }
  status = ca_add_event(DBR_STRING,
			statusChannel,
			stringChangeHandler,
			NULL,NULL);
  if (status != ECA_NORMAL) {
    return status;
  }
  status = ca_add_event(DBR_INT,
		       statusChannel,
		       intChangeHandler,
		       NULL, NULL);
  if (status != ECA_NORMAL) {
    return status;
  }

  /** Let the pending I/Os run and the pendnig events fire..
   ** These are assumed to work always..
   */
  ca_pend_io(1.0);
  ca_pend_event(0.5);		/* Hopefully this allows the connections to start. */

  /**
   ** Start the event thread..also assumed to work.
   */
#if 0
  status = pthread_create(&threadId,
			  NULL, eventPump,
			  NULL);
#endif

  threadId = epicsThreadCreate("EventPump",
			       epicsThreadPriorityHigh,
			       epicsThreadGetStackSize(epicsThreadStackMedium),
			       eventPump, NULL);
  
  return GRETINA_RCTL_OK;

}
/**
 ** Start a run.  In order to start we must be:
 ** - Connected.
 ** - IN GRETINA_RCTL_STATE_SETUP
 ** @return int
 ** @retval GRETINA_RCTL_OK      - run state change requested.
 ** @retval GRETINA_RCTL_NOTCONN - we are not connected to the status variables yet.
 ** @retval GRETINA_RCTL_BADSTATE - We are connected but gretina is not in GRETINA_RCTL_STATE_SETUP.
 ** @retval other: ECA_xxxx from epics.
 */
int
gretina_rctl_start()
{
  int status;
  int startValue = 1;

  /* Check connectivity and correct gretina global state */

  if (!isConnected) {
    return GRETINA_RCTL_NOTCONN;
  }
  if (currentState != GRETINA_RCTL_STATE_SETUP) {
    return GRETINA_RCTL_BADSTATE;
  }
  
  /* Start the run */
  status = ca_put(DBR_INT,
		  controlChannel,
		  &startValue);
  if (status != ECA_NORMAL) return status;
  ca_pend_io(1.0);
  return GRETINA_RCTL_OK;
}
/**
 **  Stop an active run.  In order to stop a run:
 ** - We must be connected.
 ** - Current state must be GRETINA_RCTL_STATE_RUN
 ** @return int
 ** @retval GRETINA_RCTL_OK -- successful.
 ** @retval GRETINA_RCTL_NOTCONN -- not yet connected to status PV.
 ** @retval GRETINA_RCTL_BADSTATE - Not in GRETINA_RCTL_STATE_RUN
 ** @retval other -- ECA_xxx from epics.
 */
int
gretina_rctl_stop()
{
  int status;
  int stopValue = 0;
  if(!isConnected) {
    return GRETINA_RCTL_NOTCONN;
  }
  if (currentState != GRETINA_RCTL_STATE_RUN) {
    return GRETINA_RCTL_BADSTATE;
  }

  /* Stop the run as everything appears propitious. */

  status  = ca_put(DBR_INT, controlChannel, &stopValue);
  if (status != ECA_NORMAL) return status;
  ca_pend_io(1.0);
  return GRETINA_RCTL_OK;
}

/**
 ** Returns our best guess about whether or not one can start a gretina run.
 ** The run can be started iff:
 ** - we are connected
 ** - the state is GRETINA_RCTL_STATE_SETUP
 ** @return int
 ** @retval TRUE - Unless something changes a run start will work.
 ** @retval FALSE - Unless something changes a run start will fail.
 */
int 
gretina_rctl_can_start()
{
  return isConnected && ( currentState == GRETINA_RCTL_STATE_SETUP);
}
/**
 ** Returns our best guess about whether or not one can stop an active run:
 ** - We must be connected.
 ** - The state must be GRETINA_RCTL_STATE_RUN
 ** @retval TRUE - Unless something changes a run stop  will work.
 ** @retval FALSE - Unless something changes a run stop will fail.
 **
 ** @note because of the many states gretina may have gretina_rctl_can_stop != !gretina_rctl_can_start.
 **  
 */
int
gretina_rctl_can_stop()
{
  return isConnected && (currentState == GRETINA_RCTL_STATE_RUN);
}
/**
 **  Returns the current textual run state of gretina:
 ** @param pState - Pointer to  abuffer large enough to hold the state.
 **                 The constant GRETINA_RCTL_STATE_MAXLEN is a nice minimum buffers size you can yuse.
 ** @param maxlen - The size of the buffer. 
 ** @return int
 ** @retval GRETINA_RCTL_OK - Good fetch of the state.
 ** @retval GRETINA_RCTL_NOTCONN - Don't yet have a value for the state.
 ** @retval GRETINA_RCTL_TRUNCATE - Had to truncate the state because maxlen was too small.
 */
int
gretina_rctl_statet(char* pState, size_t maxlen)
{
  int status;

  if (!isConnected) {
    return GRETINA_RCTL_NOTCONN;
  }
  /* Null filling the user buffer ensures that it is null terminated; */
 
  memset(pState, 0, maxlen);
  maxlen--;			/* Ensures we have at least one null terminator. */
  
  /**** Synchornize access to pCurrentValue ****/

  lockMutex();
  status = GRETINA_RCTL_OK;	/* have confidence in the caller for now */
  if(pCurrentValue) {
    strncpy(pState, pCurrentValue, maxlen);
    if (strlen(pCurrentValue) >= maxlen) {
      status = GRETINA_RCTL_TRUNCATE;
    }
  }
  else {
    status = GRETINA_RCTL_NOTCONN;
  }
  unlockMutex();
  
  /**** End of synchronization ****/

  return status;
}
/**
 ** Return the integer value of the state.
 ** @return int
 ** @retval GRETINA_RCTL_NOTCONN - Don't yet have a connection to the status channel.
 ** @retval GRETINA_RCTL_STATE_XXX - A state variable value from gretina.
 */

int gretina_rctl_state()
{
  if (!isConnected) {
    return GRETINA_RCTL_NOTCONN;
  }
  return currentState;
}
