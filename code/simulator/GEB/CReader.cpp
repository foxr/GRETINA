/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CReader.h"
#include "Fragment.h"
#include "GEBLink.h"
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <iostream>

/*------------------------------Canonicals   -------------------*/


/**
 ** construction requires that we save the Tcl channel so that we can do I/O on it
 ** when this object is triggered.
 ** @param channel - Tcl_Channel that connects us to the data source.
 */
CReader::CReader(Tcl_Channel channel) :
  m_dataSource(channel)
{
}
/**
 ** Nothing needs to be done on destruction.  Ensuring our observers are destroyed
 ** is up to our client.
 */

CReader::~CReader()
{
}


/*--------------------------- Observer interface -------------------------*/

/**
 ** This function is visited when there's input available on our channel.
 ** - Read an event fragment from the data source.
 ** - Ship it off to all our visitors.
 ** - free the dynamic storage part of our fragment.
 ** We don't assume the observers won't throw exceptions
 ** if they do, we catch them, clean up our dynamic storage and re-throw.
 ** @param p - Unused but required by the interface.
 ** @throws int - If error the errno will be  thrown.
 **               the ultimate invoker of of us will need to handle this properly to 
 **               shut down the channel and kill us off.
 */
void
CReader::operator()(void* p) 
{
  gebData fragment;
  
  this->read((&fragment), sizeof(fragment));

  // If the size of the payload is zero this is a keep alive (assert that).

  if (fragment.length == 0) {
    assert (fragment.type == GEB_TYPE_KEEPALIVE);
    Tcl_Write(m_dataSource, (const char*)(&fragment), sizeof(int));
    Tcl_Flush(m_dataSource);
    return;			// No visitation, no frag insertion.
  }

  fragment.payload = malloc(fragment.length);
  assert(fragment.payload);

  this->read(fragment.payload, fragment.length);

  m_observers.invoke(&fragment);
  free(fragment.payload);	// Properly manage memory.
}

/*---------------------- Expose observer list interfaces ----------------*/

/** These functions wrap corresponding functions in m_obsevers, see the
 ** similarly named functions in CObserverList for comments.
 */

void 
CReader::addObserver(CObserver* pObserver) {
  m_observers.add(pObserver);
}
void
CReader::removeObserver(CObserver* pObserver) {
  m_observers.remove(pObserver);
}
size_t
CReader::observerCount() const
{

  return m_observers.size();
}

/*-------------------- Utility functions -----------------------------*/

/**
 ** Read and error handling from a tcl channel
 ** @param pData - Pointer to the buffer to fill with data.
 ** @param nbytes - Number of bytes to read.
 ** @throw int in the event of an error that cannot be recovered, this throws the errno.
 **         0 is thrown if the EOF is reached prior to nbytes being read.
 **         The assumption is that the data coming down the channel must be complete event fragments.
 **
 ** @note Tcl_Read i sused to read the data from m_dataSource
 ** @note If necessary Tcl_Read is called several times until nbytes is read or until it's clear
 **       nbytes cannot be read because of the destruction of the channel.
 ** @note m_dataSource should be in blocking mode, while EAGAIN is properly handled,
 **       it will be handled in a compute bound manner.
 */
void
CReader::read(void* pData, size_t nBytes)
{
  char*   p           = reinterpret_cast<char*>(pData); // Tcl likes this an it's easier to compute with.
  size_t  remaining = nBytes;

  while (remaining) {
    int nread = Tcl_Read(m_dataSource, p, remaining);
    // Error.
    if (nread == -1) {
      int e = Tcl_GetErrno();
      // If an error throw it:

      if ((e != EAGAIN)      &&
	  (e != EWOULDBLOCK) &&
	  (e != EINTR)) throw e;
    }
    else if (nread == 0) {
      throw 0;			// Premature EOF
    }
    else {			// Good stuff.
      p +=- nread;
      remaining -= nread;
    }
  }
}
