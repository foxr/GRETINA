/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

      http://www.gnu.org/licenses/gpl.txt

      Author:
	      Ron Fox
	      NSCL
	      Michigan State University
	      East Lansing, MI 48824-1321
 */
#include "CObserverList.h"
#include "CObserver.h"
#include <algorithm>

/*-------------------------- Canonical functions -----------------------*/

/**
 ** construction
 */
CObserverList::CObserverList() {}

/**
 ** Destruction.. the observers themselves are not our property, someone else must
 ** destroy them. The list will destroy itself.
 */
CObserverList::~CObserverList() {}

/*----------------------- Observer list manipulators -----------------*/

/**
 ** Add an observer to the list.
 ** @param pObserver - pointer to the observer to add.
 */
void
CObserverList::add(CObserver* pObserver)
{
  m_Observers.push_back(pObserver);
}
/**
 ** Remove an observer from the list.
 ** this is a no-op if the observer is not in the list.
 ** @param pObserver- Pointer to the observer to remove.
 */
void
CObserverList::remove(CObserver* pObserver)
{
  ObserverList::iterator p = find(m_Observers.begin(), m_Observers.end(), pObserver);
  if (p != m_Observers.end()) {
    m_Observers.erase(p);
  }
}

/*--------------------- selectors ----------------------------------*/

/**
 ** Return the number of observers that are in the list.
 */

size_t
CObserverList::size() const
{
  return m_Observers.size();
}

/*----------------------- invocation ------------------------------*/

/**
** Invokwe all of the observers in the list
** @param pPayload - passed in to all of the observeras called.
*/
void
CObserverList::invoke(void* p) const
{
  Invoker i(p);
  for_each(m_Observers.begin(), m_Observers.end(), i);
}

/*------------------------- private utilities ----------------------*/

/**
 ** Invoker class is really too simple to comment.
 ** It's provides a function object fo rinvoking the individual observers
 ** while being able to pass them the parameter:
 */
CObserverList::Invoker::Invoker(void* p) : m_pPayload(p) {}
void

CObserverList::Invoker::operator()(CObserver* pObserver) {
  (*pObserver)(m_pPayload);
}
