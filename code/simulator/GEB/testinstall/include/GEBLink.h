/*
** This is stolen from Gretina with a bit of double include prevention window dressing
*/

#ifndef __GEBLINK_H
#define __GEBLINK_H


struct gebData {
  int type;
  int length;     /* of payload in bytes */
  long long timestamp;
  void *payload;
};
#define GEB_TYPE_KEEPALIVE 	0
#define GEB_TYPE_DECOMP 	1

#define GEB_PORT 9005
#define GEB_HEADER_BYTES (sizeof(struct gebData) - sizeof(void*))


#endif
