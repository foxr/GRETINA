/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

      http://www.gnu.org/licenses/gpl.txt

      Author:
	      Ron Fox
	      NSCL
	      Michigan State University
	      East Lansing, MI 48824-1321
 */
#ifndef _INJECTOR_H
#define _INJECTOR_H

#ifndef _OBSERVER_H
#include "CObserver.h"
#endif

/**
 ** This class is an observer which is handed a fragment
 ** as it's payload.  It is registered as a data source
 ** on builder and injects its payload into the builder
 ** where it can later be built.
 ** The purpose of this class is to decouple fragment I/O
 ** from fragment handling.  This both makes the software
 ** more loosely coupled as well as making more chunks testable.
 */
class CInjector : public CObserver
{
  // private data:

private:
  int m_SourceId;

  // Canonicals:
public:
  CInjector(int id);
  ~CInjector();

private:
  CInjector(const CInjector& rhs);
  CInjector& operator=(const CInjector& rhs);
  int operator==(const CInjector& rhs) const;
  int operator!=(const CInjector& rhs) const;

  // CObserver interface:
public:
  virtual void operator()(void* pFragment);
};
#endif
