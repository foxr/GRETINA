/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef _READER_H
#define _READER_H

#ifndef _OBSERVERLIST_H
#include "CObserverList.h"
#endif

#ifndef _OBSERVER_H
#include "CObserver.h"
#endif


#ifndef _TCL_H
#include <tcl.h>
#ifndef _TCL_H
#define _TCL_H
#endif
#endif

/**
 ** This class is intended to read event fragments from an input source
 ** that is connected via a Tcl_Channel and and pass the resulting gebData
 ** struct on to a set of observers that can be registered on it.
 ** (A sample observer might be an injector into the CBuilder object)
 ** An arbitrary number of observers can be registered which provides
 ** hooks for status monitoring as well.
 */
class CReader : public CObserver
{
private:

  CObserverList m_observers;
  Tcl_Channel   m_dataSource;

  // Canonicals:

public:
  CReader(Tcl_Channel channel);
  virtual ~CReader();

private:
  CReader(const CReader& rhs);
  CReader& operator=(const CReader& rhs);
  int operator==(const CReader& rhs) const;
  int operator!=(const CReader& rhs) const;

  // Observer interface:  This is called when there's data available on the channel.
public:
  virtual  void operator()(void* p);

  // Expose interfaces to the observer list to allow observers to be added/removed/etc.

  void addObserver(CObserver* pObserver);
  void removeObserver(CObserver* pObserver);
  size_t observerCount() const;
  
  // utility functions.
private:

  void read(void* pData, size_t nbytes);
};
#endif
