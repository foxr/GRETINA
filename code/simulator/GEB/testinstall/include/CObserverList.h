/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef _OBSERVERLIST_H
#define _OBSERVERLIST_H

#ifndef __STL_LIST
#include <list>
#ifndef __STL_LIST
#define __STL_LIST
#endif
#endif


class CObserver;
/**
 ** This class provides interfaces that allow clients to implement
 ** the observer pattern.   The idea is that for each set of observers,
 ** the client will encapsulate an observer list and expose the
 ** desired parts of the interface by wrapping it into their interface.
 ** This class provides
 ** - mechanisms to manipulate the observer list.
 ** - mechanisms to invoke listeners providing them with an abstract payload.
 ** The meaning of the payload must be agreed upon between the observer and observed.
 */
class CObserverList 
{
  // Internal data types:

private:
  typedef std::list<CObserver*> ObserverList;

  // Attributes:
private:
  ObserverList  m_Observers;
  // Canonicals
public:
  CObserverList();
  ~CObserverList();
private:
  CObserverList(const CObserverList& rhs); // don't require copy construction for the observers.
  CObserverList& operator=(const CObserverList& rhs);
  int operator==(const CObserverList& rhs) const;
  int operator!=(const CObserverList& rhs) const;

  // Manipulators of the list.
public:
  void add(CObserver* pObserver);
  void remove(CObserver* pObserver);

  // Selectors of the list.

  size_t size() const;		// Number of observers.

  // Invoking the list.

  void invoke(void* pPayload) const;

  // Utilities:
private:
  // This class allows the use of for_each to do the observer invocation.
  class Invoker {
  private:
    void* m_pPayload;
  public:
    Invoker(void* p);
    void operator()(CObserver* pObserver);
  };


};


#endif
