/*
** This is stolen from Gretina with a bit of double include prevention window dressing
*/

#ifndef __GEBCLIENT_H
#define __GEBCLIENT_H


#ifndef __GEBLINK_H
#include "GEBLink.h"
#endif

#ifdef _NSCL_SIMULATOR
#ifndef __EPICSMUTEXID_
#define __EPICSMUTEXID_
typedef int epicsMutexId;
#endif
#endif

struct gebClient {
   epicsMutexId connectionMutex;
   int outSock;
   int bigendian;
   struct gebData zeromsg;
};


#ifdef __cplusplus
extern "C" {
#endif
struct gebClient *GEBClientInit();

int sendGEBData(struct gebClient *, struct gebData *);
int setGEBClient(struct gebClient *, const char *addr, int port);
void closeGEBClient(struct gebClient *);
int checkGEBClient(struct gebClient * );

#ifdef __cplusplus
}
#endif

#endif
