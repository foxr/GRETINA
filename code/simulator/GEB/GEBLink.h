#ifndef __GEB_H
#define __GEB_H
struct gebData {
  int type;
  int length;     /* of payload in bytes */
  long long timestamp;
  void *payload;
  short refCount;		/* for data tap upgrade, all programs */
  short refIndex;
  struct gebData *next;	/* used in TrackIF.c */
};
#define GEB_TYPE_KEEPALIVE 	0
#define GEB_TYPE_DECOMP 	1
#define GEB_TYPE_RAW		2
#define GEB_TYPE_TRACK		3
#define GEB_TYPE_BGS		4

#define GEB_PORT 9005
#define GEB_HEADER_BYTES (sizeof(struct gebData))

#endif
