#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include "Fragment.h"
#include "GEBLink.h"
#include "CBuilder.h"

#include <vector>

#include <time.h>

using namespace std;

int main(int argc, char** argv)
{

  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"



class builder : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(builder);
  CPPUNIT_TEST(construct);
  CPPUNIT_TEST(addsource);
  CPPUNIT_TEST(removesource);
  CPPUNIT_TEST(addDupSource);
  CPPUNIT_TEST(removeNosuch);
  CPPUNIT_TEST(queue1);
  CPPUNIT_TEST(queuenonexistent);
  CPPUNIT_TEST(buildempty);
  CPPUNIT_TEST(buildcomplete);
  CPPUNIT_TEST(buildpartial);
  CPPUNIT_TEST(builduntilemtpy);
  CPPUNIT_TEST(builduntilcomplete);
  CPPUNIT_TEST(buildnewdata);
  CPPUNIT_TEST(builduntilpartiallyemtpy);
  CPPUNIT_TEST(empty);
  CPPUNIT_TEST(notempty);
  CPPUNIT_TEST_SUITE_END();



private:

public:
  void setUp() {
  }
  void tearDown() {
    CBuilder::getInstance()->reinit(); // Leaks memory like a sieve but...
  }
protected:
  void construct();
  void addsource();
  void removesource();
  void addDupSource();
  void removeNosuch();
  void queue1();
  void queuenonexistent();
  void buildempty();
  void buildcomplete();
  void buildpartial();
  void builduntilemtpy();
  void builduntilcomplete();
  void buildnewdata();
  void builduntilpartiallyemtpy();
  void empty();
  void notempty();

};

CPPUNIT_TEST_SUITE_REGISTRATION(builder);

// Singleton so there's a get instance that should always give the same
// value:

void builder::construct()
{

  CBuilder* pB1 = CBuilder::getInstance();
  CBuilder* pB2 = CBuilder::getInstance();

  EQ(pB1,pB2);
  
}

// Adding sources should.. uh.. add sources

void builder::addsource()
{
  CBuilder* pBuilder = CBuilder::getInstance();

  for (int i =0; i < 10; i++) {
    pBuilder->addSource(i);
  }

  vector<int> sourceids = pBuilder->sourceIds();
  EQ(static_cast<size_t>(10), sourceids.size());

  // They come out in order because it's a map.

  for (int i=0; i < 10; i++) {
    EQ(i, sourceids[i]);
  }

  // test reinit too...
  pBuilder->reinit();

  sourceids = pBuilder->sourceIds();
  EQ(static_cast<size_t>(0), sourceids.size());
}
// Removing a source should remove it.

void builder::removesource()
{
  CBuilder* pBuilder = CBuilder::getInstance();

  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->removeSource(1);

  vector<int> ids = pBuilder->sourceIds();
  EQ(static_cast<size_t>(1), ids.size());
  EQ(2, ids[0]);
}
// Should not be allowed to add a duplicate  source..

void builder::addDupSource()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  pBuilder->addSource(1);

  bool threw = false;
  try {
    pBuilder->addSource(1);	// Should throw.
  }
  catch(string msg) {
    threw = true;
  }
  ASSERT(threw);
}
// Remove a source not in the builder should throw.

void builder::removeNosuch()
{
  CBuilder* pBuilder = CBuilder::getInstance();

  bool threw = false;
  try {
    pBuilder->removeSource(1);
  }
  catch (string msg) {
    threw = true;
  }
  ASSERT(threw);
}
// Queuing a fragment to a queue should add it to one and only one, and the correct one
// at that.
//
void builder::queue1()
{
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  CBuilder* pBuilder = CBuilder::getInstance();
  pBuilder->addSource(1);
  pBuilder->addSource(2);
  pBuilder->queueFragment(1, &fragment);

  EQ(static_cast<size_t>(1), pBuilder->depth(1));
  EQ(static_cast<size_t>(0), pBuilder->depth(2));
}
// queueing a fragment on a nonexistent queue is an error:

void builder::queuenonexistent()
{
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  CBuilder* pBuilder = CBuilder::getInstance();
  
  bool threw = false;
  try {
    pBuilder->queueFragment(1, &fragment);
  }
  catch (string msg) {
    threw = true;
  }
  ASSERT(threw);
}
// Building with no fragments should give us a null.

void builder::buildempty()
{
  CBuilder* pBuilder   =  CBuilder::getInstance();
  CBuilder::pEvent pE  =  pBuilder->build();

  ASSERT(!pE);
}

// Building with a complete set of fragments gives us an event
// that has all of those fragments.
//
void builder::buildcomplete()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &fragment);
  pBuilder->queueFragment(2, &fragment);

  CBuilder::pEvent pe = pBuilder->build();

  // Should get an event.

  ASSERT(pe);

  CBuilder::Event& e(*pe);

  // Should have two fragments in it.

  EQ(static_cast<size_t>(2), e.size());

  // The fragments should match our fragment.

  for(int i =0; i < e.size(); i++) {
    gebData& frag(e[i]->s_fragment);

    EQ(fragment.type, frag.type);
    EQ(fragment.length, frag.length);
    EQ(fragment.timestamp, frag.timestamp);
    int* payload = reinterpret_cast<int*>(frag.payload);

    for (int b = 0; b < sizeof(data)/sizeof(int); b++) {
      EQ(data[b], payload[b]);
    }
    
  }

  
}
// building with some matching fragments but not all matching fragments..
// I should only get the fragments that match the minimum time fragment.
//
void builder::buildpartial()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData frag1 = {
    1, sizeof(data), 1, data
  };
  gebData frag2 = {
    2, sizeof(data), 5, data
  };

  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &frag1);
  pBuilder->queueFragment(2, &frag2);

  CBuilder::pEvent pe = pBuilder->build();

  ASSERT(pe);
  CBuilder::Event& e(*pe);

  EQ(static_cast<size_t>(1), e.size()); // only one fragment should come out.

  gebData& frag(e[0]->s_fragment);
 
  EQ(frag1.type, frag.type);
  EQ(frag1.length, frag.length);
  EQ(frag1.timestamp, frag.timestamp);
  int* payload = reinterpret_cast<int*>(frag.payload);
  
  for (int b = 0; b < sizeof(data)/sizeof(int); b++) {
    EQ(data[b], payload[b]);
  }
  // Furthermore... the build should now fail because one of the queues is empty:

  pe = pBuilder->build();
  ASSERT(!pe);
}


// Doing a build until with an empty set of event queues should give me a NULL:
//
void builder::builduntilemtpy()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  CBuilder::pEvent pE = pBuilder->build(time(NULL));

  ASSERT(!pE);
}

// Doing a build with matching data int he queues that is older thant
// The until time is fine.. and gets us a complete event.
//
//
void builder::builduntilcomplete()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &fragment);
  pBuilder->queueFragment(2, &fragment);

  time_t future = time(NULL) + 10; // 10 seconds into the future.

  CBuilder::pEvent pe = pBuilder->build(future);

  // Should get an event.

  ASSERT(pe);

  CBuilder::Event& e(*pe);

  // Should have two fragments in it.

  EQ(static_cast<size_t>(2), e.size());

  // The fragments should match our fragment.

  for(int i =0; i < e.size(); i++) {
    gebData& frag(e[i]->s_fragment);

    EQ(fragment.type, frag.type);
    EQ(fragment.length, frag.length);
    EQ(fragment.timestamp, frag.timestamp);
    int* payload = reinterpret_cast<int*>(frag.payload);

    for (int b = 0; b < sizeof(data)/sizeof(int); b++) {
      EQ(data[b], payload[b]);
    }
  }
  
}
// If the data in are newer than the time limit we should not get anything.

void builder::buildnewdata()
{

  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &fragment);
  pBuilder->queueFragment(2, &fragment);

  time_t past = time(NULL) - 10; // 10 seconds into the future.

  CBuilder::pEvent pe = pBuilder->build(past);

  // Should not get an event.

  ASSERT(!pe);


}
//  If there are data that are older thatn the time limit, I should 
// get a built event, even if there are some empty queues.
//
void builder::builduntilpartiallyemtpy()
{
  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &fragment);

  time_t future = time(NULL) + 10; // 10 seconds into the future.

  CBuilder::pEvent pe = pBuilder->build(future);

  // Should get an event.

  ASSERT(pe);

  CBuilder::Event& e(*pe);

  // Should have two fragments in it.

  EQ(static_cast<size_t>(1), e.size());

  // The fragments should match our fragment.

  gebData& frag(e[0]->s_fragment);
  
  EQ(fragment.type, frag.type);
  EQ(fragment.length, frag.length);
  EQ(fragment.timestamp, frag.timestamp);
  int* payload = reinterpret_cast<int*>(frag.payload);
  
  for (int b = 0; b < sizeof(data)/sizeof(int); b++) {
    EQ(data[b], payload[b]);
  }
}

// Builder with no fragments is empty... if there are sources or not:
//
void builder::empty()
{
  CBuilder* pBuilder = CBuilder::getInstance();

  ASSERT(pBuilder->empty());

  pBuilder->addSource(1);
 
  ASSERT(pBuilder->empty());
}
// Builder with fragments is not empty.

void builder::notempty()
{  CBuilder* pBuilder = CBuilder::getInstance();
  int data[] = {
    1,2,3,4,5,6,7
  };
  gebData fragment = {
    1, sizeof(data), 12345, data
  };
  pBuilder->addSource(1);
  pBuilder->addSource(2);

  pBuilder->queueFragment(1, &fragment);

  ASSERT(!pBuilder->empty());
  
}
