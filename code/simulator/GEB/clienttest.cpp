#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include <time.h>
#include "CInjector.h"
#include "CBuilder.h"
#include "Fragment.h"

using namespace std;

int main(int argc, char** argv)
{

  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <sys/types.h>
#include <sys/socket.h>
#include "GEBLink.h"
#include "GEBClient.h"
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <wait.h>


class client : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(client);
  /// Add CPPUNIT_TEST directives here.

  CPPUNIT_TEST(init_test1);
  CPPUNIT_TEST(open_badhost);
  CPPUNIT_TEST(open_nolistener);
  CPPUNIT_TEST(open_ok);
  CPPUNIT_TEST(close_ok);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void init_test1();
  void open_badhost();
  void open_nolistener();
  void open_ok();
  void close_ok();
  
};

CPPUNIT_TEST_SUITE_REGISTRATION(client);

// Test implementations.

/** After initialization, 
 ** - sends on the output socket should fail with EBADF
 ** - endianness should be right.
 */
void client::init_test1()
{
  gebClient* pLink = GEBClientInit();
  ASSERT(pLink != NULL);

  int status = send(pLink->outSock, &pLink, sizeof(gebClient*), MSG_NOSIGNAL);
  int error  = errno;
  ASSERT(status == -1);
  EQ(EBADF, error);
  free(pLink);			// Since close/free is not yet tested.
}
/** Bad hostnames fail with status 4. */

void client::open_badhost()
{
  const char* host = "this.host.does.not.exist";
  gebClient* pLink = GEBClientInit();
  int        status = setGEBClient(pLink, host, GEB_PORT);
  EQ(4, status);
  free(pLink);
}
/** Connecting when there's no listener should fail with status 3
*/
void client::open_nolistener()
{
  const char* host = "localhost";
  gebClient* pLink = GEBClientInit();
  int        status = setGEBClient(pLink, host, GEB_PORT);
  EQ(3, status);
  free(pLink);
}


/**
 ** Connecting to a server in localhost (forked off by me)
 ** Should work find.
 */

static void server()		// server just listens accepts, closes and exits.
{
  int                sockfd;
  struct sockaddr_in self;
  struct sockaddr_in client;
  socklen_t          csize = sizeof(sockaddr_in);
  int                on = 1;
  int                status;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    perror("server socket failed");
    exit(-1);
  }
  bzero(&self, sizeof(self));
  self.sin_family  = AF_INET;
  self.sin_port    = htons(GEB_PORT);
  self.sin_addr.s_addr = INADDR_ANY;

  
  status = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
  if (status == -1) {

    perror("server setsockopt failed");
    exit(-1);

  }


  status=  bind (sockfd, (struct sockaddr*)&self, sizeof(self));
  if(status == -1) {
    perror("server Bind failed");
    exit(-1);

  }
  status =   listen(sockfd, 1);
  if (status == -1) {
    perror("server listen failed");
    exit(-1);

  }

  int clientfd = accept(sockfd, (struct sockaddr*)&client, &csize);
  if (clientfd == -1) {
    perror("accept failed");
    exit(-1);

  }
  shutdown(clientfd, SHUT_RDWR);
  close(sockfd);
  exit(0);
}

void client::open_ok() 
{
  int serverpid;
  if ((serverpid = fork())) {
    sleep(1);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    EQ(0, status);
    shutdown(pLink->outSock, SHUT_RDWR);
    waitpid(serverpid, &status, 0);		// Reap the server ssubprocess.
    free(pLink);
  }
  else {
    server();
  }

}
/*
** once one closes, the socket should also be closed.
*/
void client::close_ok()
{
  int serverpid;

  if((serverpid = fork())) {
    usleep(2000);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    EQ(0, status);
    closeGEBClient(pLink);
    status = send(pLink->outSock, &pLink, sizeof(gebClient*), MSG_NOSIGNAL);
    int err = errno;
    ASSERT(status == -1);
    EQ(EBADF, errno);
    waitpid(serverpid, &status, 0);
    free(pLink);

  }
  else {
    server();
  }
}

class sendtests : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(sendtests);
  /// Add CPPUNIT_TEST directives here.

  CPPUNIT_TEST(fail1);
  CPPUNIT_TEST(fail2);
  CPPUNIT_TEST(fail3);
  CPPUNIT_TEST(sendok);
  CPPUNIT_TEST(sendkeepalive);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void fail1();
  void fail2();
  void fail3();
  void sendok();
  void sendkeepalive();
};

CPPUNIT_TEST_SUITE_REGISTRATION(sendtests);


/** write to the socket fails with a 1..This can happen if the GEBLink strut has an invalid
 ** fd
 */
void sendtests::fail1()
{
  int payload;
  gebData data = {
    123,
    sizeof(int),
    1234676,
    &payload
  };
  gebClient* pLink = GEBClientInit();
  pLink->outSock = 123;

  int status = sendGEBData(pLink, &data);
  free(pLink);
  EQ(1, status);
			

}
/** If the payload is not set that should earn us a code 2:
 */
void sendtests::fail2()
{
  gebData data = {
    0 , 1234567, NULL};
  gebClient* pLink = GEBClientInit();
  int status = sendGEBData(pLink, &data);
  free(pLink);
  EQ(2, status);
}
/**
 ** Sending with a  closed link earns 3 back:
 */
void sendtests::fail3()
{
  int payload;
  gebData data = {
    123,
    sizeof(int),
    1234676,
    &payload
  };

  gebClient* pLink = GEBClientInit();
  int status = sendGEBData(pLink,&data);
  free(pLink);
  EQ(3, status);
  
}
/** Sending data to an echo server should allow us to get it back from that server
 **/

static void echoserver()		// echoserver just listens accepts, closes and exits.
{
  int                sockfd;
  struct sockaddr_in self;
  struct sockaddr_in client;
  socklen_t          csize = sizeof(sockaddr_in);
  int                on = 1;
  int                status;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    perror("echoserver socket failed");
    exit(-1);
  }
  bzero(&self, sizeof(self));
  self.sin_family  = AF_INET;
  self.sin_port    = htons(GEB_PORT);
  self.sin_addr.s_addr = INADDR_ANY;

  
  status = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
  if (status == -1) {

    perror("echoserver setsockopt failed");
    exit(-1);

  }


  status=  bind (sockfd, (struct sockaddr*)&self, sizeof(self));
  if(status == -1) {
    perror("echoserver Bind failed");
    exit(-1);

  }
  status =   listen(sockfd, 1);
  if (status == -1) {
    perror("echoserver listen failed");
    exit(-1);

  }

  int clientfd = accept(sockfd, (struct sockaddr*)&client, &csize);
  if (clientfd == -1) {
    perror("accept failed");
    exit(-1);

  }
  char msg[1000];
  gebData* pGEB = (gebData*)msg;

  int bytes = recv(clientfd, msg, sizeof(msg),  0);
  int type = GEB_TYPE_DECOMP;
  if (bytes > sizeof(int)) {
    type = pGEB->type;
  }
  if(type == GEB_TYPE_KEEPALIVE) {
    bytes = sizeof(int);
  }
  send(clientfd, msg, bytes, 0);

  shutdown(clientfd, SHUT_RDWR);
  close(sockfd);
  exit(0);
}

void sendtests::sendok()
{
  int echoserverpid;
  int payload = 666;
  gebData data = {
    123,
    sizeof(int),
    1234576,
    &payload
  };
  struct reply {
    int type;
    int length;
    long long timestamp;
    int returnedpayload;
  } replybuf;

  if((echoserverpid = fork())) {
    usleep(2000);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    EQ(0, status);
    status = sendGEBData(pLink, &data);
    EQ(0, status);
    status = recv(pLink->outSock, &replybuf, sizeof(replybuf), 0);
    EQ(sizeof(replybuf), (size_t)status);
    EQ(123, replybuf.type);
    EQ(sizeof(int), (size_t)(replybuf.length));
    EQ((long long)(1234576), replybuf.timestamp);
    EQ(666, replybuf.returnedpayload);

    closeGEBClient(pLink);
    waitpid(echoserverpid, &status, 0);
    free(pLink);

  }
  else {
    echoserver();
  }
}

/** Sending a keepalive to the echo server will just give us an int back...of undetermined content
 */
void sendtests::sendkeepalive()
{
  int echoserverpid;
  int payload = 666;
  gebData data = {
    123,
    sizeof(int),
    1234576,
    &payload
  };
  struct reply {
    int type;
    int length;
    long long timestamp;
    int returnedpayload;
  } replybuf;

  if((echoserverpid = fork())) {
    usleep(2000);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    EQ(0, status);
    status = sendGEBData(pLink, NULL);
    EQ(0, status);
    status = recv(pLink->outSock, &replybuf, sizeof(replybuf), 0);
    EQ(sizeof(int), (size_t)status);

    closeGEBClient(pLink);
    waitpid(echoserverpid, &status, 0);
    free(pLink);

  }
  else {
    echoserver();
  }
}


class checktests : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(checktests);
  /// Add CPPUNIT_TEST directives here.

  CPPUNIT_TEST(fail1);
  CPPUNIT_TEST(fail2);
  CPPUNIT_TEST(ok);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void fail1();
  void fail2();
  void ok();
};

CPPUNIT_TEST_SUITE_REGISTRATION(checktests);

/**
 ** doing a keepalive on a dead server will fail.
 */
void checktests::fail1()
{
  int serverpid;
  if ((serverpid = fork())) {
    sleep(1);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    status = checkGEBClient(pLink); // connecting shut the server down so this should fail:
    EQ(0, status);
    waitpid(serverpid, &status, 0);		// Reap the server ssubprocess.
    free(pLink);
  }
  else {
    server();
  }
}
/**
 ** Doing a keepalive to a server that will exit once it's received data should also fail
 */
static void writeonlyserver()		// echoserver just listens accepts, closes and exits.
{
  int                sockfd;
  struct sockaddr_in self;
  struct sockaddr_in client;
  socklen_t          csize = sizeof(sockaddr_in);
  int                on = 1;
  int                status;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    perror("echoserver socket failed");
    exit(-1);
  }
  bzero(&self, sizeof(self));
  self.sin_family  = AF_INET;
  self.sin_port    = htons(GEB_PORT);
  self.sin_addr.s_addr = INADDR_ANY;

  
  status = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
  if (status == -1) {

    perror("echoserver setsockopt failed");
    exit(-1);

  }


  status=  bind (sockfd, (struct sockaddr*)&self, sizeof(self));
  if(status == -1) {
    perror("echoserver Bind failed");
    exit(-1);

  }
  status =   listen(sockfd, 1);
  if (status == -1) {
    perror("echoserver listen failed");
    exit(-1);

  }

  int clientfd = accept(sockfd, (struct sockaddr*)&client, &csize);
  if (clientfd == -1) {
    perror("accept failed");
    exit(-1);

  }
  char msg[1000];
  gebData* pGEB = (gebData*)msg;

  int bytes = recv(clientfd, msg, sizeof(msg),  0);


  shutdown(clientfd, SHUT_RDWR);
  close(sockfd);
  exit(0);
}
void checktests::fail2()
{
  int serverpid;
  if ((serverpid = fork())) {
    sleep(1);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    status = checkGEBClient(pLink); // connecting shut the server down so this should fail:
    EQ(0, status);
    waitpid(serverpid, &status, 0);		// Reap the server ssubprocess.
    free(pLink);
  }
  else {
    writeonlyserver();
  }
}
/**
 ** doing a check on an echo server works the first time.
 */
void checktests::ok()
{
  int serverpid;
  if ((serverpid = fork())) {
    sleep(1);		// give the child a 2ms head start on us.
    const char* host = "localhost";
    gebClient* pLink = GEBClientInit();
    int        status = setGEBClient(pLink, host, GEB_PORT);
    status = checkGEBClient(pLink); // connecting shut the server down so this should fail:
    ASSERT( status != 0);
    waitpid(serverpid, &status, 0);		// Reap the server ssubprocess.
    free(pLink);
  }
  else {
    echoserver();
  }
}
