#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include "Fragment.h"
#include <time.h>

using namespace std;

int main(int argc, char** argv)
{

  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"



class fragments : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(fragments);
  CPPUNIT_TEST(construct);
  CPPUNIT_TEST(assign);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void construct();
  void assign();

};

CPPUNIT_TEST_SUITE_REGISTRATION(fragments);

// Should not be able to find any thing in an empty guy: 

void fragments::construct()
{
  int payload[] = {0,1,2,3,4,5,6,7,8,9};
  gebData fragment = {
    0x12, sizeof(payload), 0x12345678, payload
  };
  time_t now = time(NULL);
  Fragment f(fragment);

  EQ(now, f.s_whenCreated);
  EQ(0x12, f.s_fragment.type);
  EQ(sizeof(payload), (size_t)f.s_fragment.length);
  EQ((long long)0x12345678, f.s_fragment.timestamp);
  
  for (int i=0; i < 10; i++) {
    EQ(i, reinterpret_cast<int*>(f.s_fragment.payload)[i]);
  }
  
}


// Assignment should work fine too:

void fragments::assign()
{
  int payload[] = {0,1,2,3,4,5,6,7,8,9};
  gebData fragment = {
    0x12, 10*sizeof(int), 0x12345678, payload
  };
  time_t now = time(NULL);
  Fragment f(fragment);
  Fragment f2;

  f2  = f;

  EQ(f.s_whenCreated, f2.s_whenCreated);
  EQ(f.s_fragment.type, f2.s_fragment.type);
  EQ(f.s_fragment.length, f2.s_fragment.length);
  EQ(f.s_fragment.timestamp, f2.s_fragment.timestamp);


  for (int i =0; i < 10; i++) {
    EQ(reinterpret_cast<int*>(f.s_fragment.payload)[i], 
       reinterpret_cast<int*>(f2.s_fragment.payload)[i]);
  }
}
