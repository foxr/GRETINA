
#ifndef __GEBCLIENT_H
#define __GEBCLIENT_H

#ifndef _NSCL_SIMULATOR
#include <epicsMutex.h>
#else
typedef int epicsMutexId;
#endif

#include "GEBLink.h"

struct gebClient {
   epicsMutexId connectionMutex;
   int outSock;
   int bigendian;
   struct gebData zeromsg;
};

#ifdef __cplusplus
extern "C" {
#endif

struct gebClient *GEBClientInit();

int sendGEBData(struct gebClient *, struct gebData *);
int setGEBClient(struct gebClient *, char *addr, int port);
void closeGEBClient(struct gebClient *);
int checkGEBClient(struct gebClient * );

#ifdef __cplusplus
}
#endif

#endif
