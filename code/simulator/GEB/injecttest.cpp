#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include <time.h>
#include "CInjector.h"
#include "CBuilder.h"
#include "Fragment.h"

using namespace std;

int main(int argc, char** argv)
{

  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  cerr << "exiting: " << (wasSucessful ? "OK\n" : "Failed\n");
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"



class injector : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(injector);
  /// Add CPPUNIT_TEST directives here.
  
  CPPUNIT_TEST(construct);
  CPPUNIT_TEST(destroy);
  CPPUNIT_TEST(inject);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
    CBuilder* pBuilder = CBuilder::getInstance();
    pBuilder->reinit();
  }
  void tearDown() {
  }
protected:
  void construct();
  void destroy();
  void inject();
};

CPPUNIT_TEST_SUITE_REGISTRATION(injector);

// The act of constructing an injector should create an event source in the builder.

void injector::construct()
{
  CInjector i(1);
  CBuilder* pBuilder= CBuilder::getInstance();

  vector<int> ids = pBuilder->sourceIds();

  EQ(static_cast<size_t>(1), ids.size());
  EQ(1, ids[0]);
  
}

// Destruction of an injector should result in removal of it as an event source.
//
void injector::destroy()
{
  {
    CInjector i(1);		// Creates event source.
  }				// should delete event source.

  CBuilder* pBuilder= CBuilder::getInstance();

  vector<int> ids = pBuilder->sourceIds();

  EQ(static_cast<size_t>(0), ids.size());
  

}
// calling operator() on an injector should insert an element in that 
// data source's fragment queue.

void injector::inject()
{
  CInjector i(1);
  int payload[] = {0,1,2,3,4,5,6,7,8,9};
  gebData fragment = {
    0x12, sizeof(payload), 0x12345678, payload
  };

  i(&fragment);

  CBuilder* pBuilder = CBuilder::getInstance();
  EQ(static_cast<size_t>(1), pBuilder->depth(1));
}
