/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef _CONNECTION_H
#define _CONNECTION_H

#ifndef _OBSERVERLIST_H
#include "CObserverList.h"
#endif


#ifndef _TCL_H
#include <tcl.h>
#ifndef _TCL_H
#define _TCL_H
#endif
#endif


class CReader;
class CInjector;

/**
 ** Objects of this class manage a single connection.
 ** They are created by the connection manager.
 ** - They establish a Reader observer.
 ** - The establish an Injector observer for the reader.
 ** - They set the reader to be invoked when the channel becomes readable.
 ** - They handle connection losses by scheduling a Tcl event to 
 **   destroy this whole framework when the observers indicate the channel
 **   must be closed.
 ** A production event builder may well use a derived object to be able to 
 ** set up other observers for various events.
 ** These observers should be set up at construction and torn down at destruction
 **
 */
class CConnection 
{
  /** object attributes:
   */
private:
  Tcl_Channel   m_dataSource;
  Tcl_Interp*   m_pInterpreter;
  CObserverList m_InputHandlers;
  CReader*      m_pReader;
  CInjector*    m_pInjector;
  bool          m_deleteScheduled;
protected:
  unsigned        m_mySourceId;
  static unsigned m_sourceId;

  // Cannonicals
public:
  CConnection(Tcl_Interp* pInterp, Tcl_Channel source);
  virtual ~CConnection();
private:
  CConnection(const CConnection& rhs);
  CConnection& operator=(const CConnection& rhs);
  int operator==(const CConnection& rhs) const;
  int operator!=(const CConnection& rhs) const;

  // object interface. 

public:
  virtual void onReadable();
  virtual void onEnd();
  virtual void onError();

  // Observer wrapers (for derived classes and for testability).

  void addObserver(CObserver* pObserver);
  void removeObserver(CObserver* pObserver);

  // for testability:

  unsigned getId();

protected:
  void invoke();

  // For testability.
public:
  static void ready(ClientData pData, int mask);
  static void shutdown(ClientData pData);
};

#endif
