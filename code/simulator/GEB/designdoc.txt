Scribblings about design documentation for the GEB output tap simulator.

GEB:
  - Take the Sean Lydick approach.
  - Event builder is a Tcp/IP server.
  - Each connection has associated with it an event queue.
  - Data read from the channel is in chunks of gebData and
    is inserted in the corresponding event queue.
  - event queues are processed into events when:
    * New data are available.
    * Data have not been received after n seconds.

 Two types of event processing.
   * Event processing that requires matching;  Events are matched up
     until one of the event fragment queues is empty.
   * Event processing that will continue even if event queues are empty until
     events in the queue are newer than some time.

Leverges:
   This will be a Tcl application leveraging the Tcl Event loop as a dispatch
   mechanism for server connections 


    
