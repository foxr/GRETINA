/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#include "CConnection.h"
#include "CReader.h"
#include "CInjector.h"
#include <errno.h>


/**
 ** Class level data:
 */
/** This variable is incremented on construction at this level and is used to provide
 ** source ID's for event builder data sources (our injectors).
 */

unsigned CConnection::m_sourceId(0);


/*---------------------------------------- Canonicals ----------------------------------*/
/**
 ** Construction is a matter of:
 ** - saving the interpreter and channel.
 ** - Creating a reader for the channel
 ** - Creating an injector for the reader.
 ** - Setting the reader as an input handler observer.
 ** - Seting up ready as a function called when data is available on the channel.
 ** @param interp - Tcl interpreter that is running the event loop that drives all of this.
 ** @param source - Tcl_Channel that is where the input will come from.
 */
CConnection::CConnection(Tcl_Interp* interp, Tcl_Channel source) :
  m_dataSource(source),
  m_pInterpreter(interp),
  m_deleteScheduled(0),
  m_mySourceId(m_sourceId++)
{
  m_pReader   = new CReader(source);
  m_pInjector = new CInjector(m_mySourceId); 

  m_pReader->addObserver(m_pInjector);
  m_InputHandlers.add(m_pReader);

  Tcl_CreateChannelHandler(m_dataSource, TCL_READABLE | TCL_EXCEPTION, CConnection::ready, this);
}
/**
 ** Destruction means:
 ** - Removing observers.
 ** - Deleting observers.
 ** - Removing the input handler.
 ** - Closing the channel.
 */
CConnection::~CConnection()
{
  removeObserver(m_pReader);
  m_pReader->removeObserver(m_pInjector);

  delete m_pReader;
  delete m_pInjector;

  Tcl_DeleteChannelHandler(m_dataSource, CConnection::ready, this);

  Tcl_Close(m_pInterpreter, m_dataSource);
}

/*------------------------------------- Object interface --------------------------*/

/**
 ** called whenthe channel becomes readable.
 ** this just requires invoking the observers to process the channel input.
 */
void CConnection::onReadable() 
{
  try {
    m_InputHandlers.invoke(m_dataSource);
  }
  catch (int error) {
    if (error == 0) {
      onEnd();
    } 
    else {
      onError();
    }
  }



}
/**
 ** On end of file we queue a timer event to 
 ** kill us off...we do this this way so that the delete is done outside
 ** of object context otherwise e.g. delete this can be a bit 
 ** bad.
 **/
void CConnection::onEnd() 
{
  if(!m_deleteScheduled) {
    m_deleteScheduled = true;
    Tcl_CreateTimerHandler(0, CConnection::shutdown, this); 
  }
}
/**
 ** default onError action is the same as onEnd.
 */
void CConnection::onError() 
{
  onEnd();
}

/*------------------------------------ Observer interfaces ------------------------*/

/**
 ** add an input observer
 ** @param pObserver - pointer to the observer to add.
 */
void
CConnection::addObserver(CObserver* pObserver)
{
  m_InputHandlers.add(pObserver);
}
/**
 ** Remove an input observer
 ** @param pObserver  Pointer to the observer to remove.
 */
void
CConnection::removeObserver(CObserver* pObserver)
{
  m_InputHandlers.remove(pObserver);
}

/*------------------------------------ testability hooks --------------------------*/

/**
 ** Return the event source id that is associated with this chain of stuff.
 ** @return unsigned
 ** @retval The event source Id associated with this object's injector.
 */
unsigned
CConnection::getId() 
{
  return m_mySourceId;
}


/*---------------------------------- Static methods [callback relays] -------------*/



/**
 ** called when an object's channel is readable.
 ** @param pData - Pointer to a CConnection object whose channel is readable.
 ** @param mask  - Mask of events.
 */
void CConnection::ready(ClientData pData, int mask) 
{
  CConnection* pConnection = reinterpret_cast<CConnection*>(pData);
  pConnection->onReadable();
}

/**
 ** Called when a connection object has been scheduled for deletion.
 ** @param pData - Poniter to a CConnection object.
 */
void
CConnection::shutdown(ClientData pData)
{
  CConnection* pConnection = reinterpret_cast<CConnection*>(pData);
  delete pConnection;
}
