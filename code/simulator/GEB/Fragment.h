/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef _FRAGMENT_H
#define _FRAGMENT_H



#ifndef _CRT_TIME_H
#include <time.h>
#ifndef _CRT_TIME_H
#define _CRT_TIME_H
#endif
#endif

#ifndef _GEBLINK_H
#include "GEBLink.h"
#ifndef _GEBLINK_H
#define _GEBLINK_H
#endif
#endif

struct gebData;
/**
 ** This structure takes a global event structure item and
 ** wraps it with a bit of extra data so that it's receipt time
 ** is known.  It also provides methods that
 ** Allow safe memory management, coyping and assignment.
 */

struct Fragment
{
  time_t   s_whenCreated;
  gebData  s_fragment;

  // Methods:

  // canonicals ensure memory management safety.;

  Fragment() {
    s_fragment.payload = (void*)0;
  }
  Fragment(gebData& data);
  ~Fragment();
  Fragment& operator=(const Fragment& rhs);
  

};

#endif
