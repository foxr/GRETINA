#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include <time.h>
#include "CBuilder.h"
#include "CReader.h"
#include "CObserver.h"
#include "Fragment.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h> 
#include <stdlib.h>
#include "CInjector.h"
#include "CBuilder.h"
#include "CConnection.h"
#include <limits.h>

using namespace std;

int main(int argc, char** argv)
{
    
  CppUnit::TextUi::TestRunner   
    runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
    registry(CppUnit::TestFactoryRegistry::getRegistry());
  
  runner.addTest(registry.makeTest());
  
  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;

}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"

static int payload[] = {0,1,2,3,4,5,6,7,8,9};
static gebData fragment = {
  0x12, sizeof(payload), 0x12345678, payload
};


class reader : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(reader);
  /// Add CPPUNIT_TEST directives here.
  CPPUNIT_TEST(testdata);
  CPPUNIT_TEST(testinject);
  CPPUNIT_TEST(testeof);
  CPPUNIT_TEST(connectconstruct);
  CPPUNIT_TEST(connecttransfer);
  CPPUNIT_TEST(connecteof);
  CPPUNIT_TEST(connecteof_event);
  CPPUNIT_TEST_SUITE_END();


private:
  Tcl_Interp* m_pInterp;
  Tcl_Channel m_channel;
  char*       m_filename;
public:
  void setUp() {

    m_pInterp = Tcl_CreateInterp();
    Tcl_Init(m_pInterp);

    int buffer[1000];
    
    // Generate the test data file:
    
    char* fileName = tmpnam(NULL);
    int fd = creat(fileName, S_IRUSR | S_IWUSR);
    
    // write a couple of messages:
    
    write(fd, &fragment, sizeof(fragment) - sizeof(void*));
    write(fd, payload,   sizeof(payload));
    write(fd, &fragment, sizeof(fragment) - sizeof(void*));
    write(fd, payload,   sizeof(payload));
    close(fd);
    
    // Create a Tcl_Channel open on the file for read:
    errno = 0;
    
    Tcl_Obj* tclFilename = Tcl_NewStringObj(fileName, -1); // For Tcl_FSOpenFileChannel
    Tcl_IncrRefCount(tclFilename);
    Tcl_Channel chan     = Tcl_FSOpenFileChannel(m_pInterp, tclFilename, "r",  0);
    if (!chan) {
      int e = Tcl_GetErrno();
      cerr << "Failed to connect Tcl Channel\n";
      cerr << Tcl_ErrnoMsg(e) << endl;
      cerr << Tcl_GetStringResult(m_pInterp) << endl;
      exit(-1);
    }
    m_channel = chan;
    m_filename= fileName;

    CBuilder* pBuilder = CBuilder::getInstance();
    pBuilder->reinit();
    
  }
  void tearDown() {
    Tcl_DeleteInterp(m_pInterp);
    Tcl_Close(m_pInterp, m_channel);
    unlink(m_filename);  
  }
protected:
  void testdata();
  void testinject();
  void testeof();
  void connectconstruct();
  void connecttransfer();
  void connecteof();
  void connecteof_event();
};

CPPUNIT_TEST_SUITE_REGISTRATION(reader);

// my observer:

class obs : public CObserver
{
public:
  Fragment Frag;

  virtual void operator()(void * ptr);
};

void
obs::operator()(void* ptr)
{


  Frag = Fragment(*(reinterpret_cast<gebData*>(ptr)));
}

/** This is a pretty complex test:
 ** We create a temp file
 ** We load it with an event fragment is it would look coming down the wire
 ** at us.
 ** We then open a Tcl_Channel on that file for read.
 ** We Create a Reader on that channel
 ** We create/register an observer that will capture data it receives fromt the channel
 **  as a Fragment (that will do a deep copy of the data).
 ** We trigger the reader
 ** Check that the observer received a true copy of the data.
 ** Destroy the observer
 ** Destroy the Reader
 ** close the Tcl_Channel
 ** destroy the temp file.
 */

void reader::testdata()
{
  CReader r(m_channel);
  obs     observer;
  r.addObserver(&observer);
  r(NULL);
  gebData& geb(observer.Frag.s_fragment);
  
  // the geb struct should 'pretty much' match the fragment (payload pointers will differ, but
  // contents should be the same:
  
  EQ(fragment.type,      geb.type);
  EQ(fragment.length,    geb.length);
  EQ(fragment.timestamp, geb.timestamp);
  EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  
  // Clear the received fragment and do another read:
  
  free(geb.payload);
  memset(&geb, 0, sizeof(gebData));
  
  r(NULL);
  
  
  EQ(fragment.type,      geb.type);
  EQ(fragment.length,    geb.length);
  EQ(fragment.timestamp, geb.timestamp);
  EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  
  


}

// Check to see if we can insert fragments into the builder via the reader:

void reader::testinject()
{
  CReader    r(m_channel);
  CBuilder*  pBuilder = CBuilder::getInstance();
  CInjector  i(1);
  
  r.addObserver(&i);
  
  
  r(NULL);			// Should now be a fragment in the builder:
  {  
    CBuilder::pEvent pe = pBuilder->build();
    ASSERT(pe != NULL);
    CBuilder::Event& e(*pe);
    EQ(static_cast<size_t>(1), e.size());
    Fragment& f(*(e[0]));
    gebData& geb(f.s_fragment);
  
    EQ(fragment.type,      geb.type);
    EQ(fragment.length,    geb.length);
    EQ(fragment.timestamp, geb.timestamp);
    EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  }
  r(NULL);			// Another fragment...

  {
    CBuilder::pEvent pe = pBuilder->build();
    ASSERT(pe != NULL);
    CBuilder::Event& e(*pe);
    EQ(static_cast<size_t>(1), e.size());
    Fragment& f(*(e[0]));
    gebData& geb(f.s_fragment);
  
    EQ(fragment.type,      geb.type);
    EQ(fragment.length,    geb.length);
    EQ(fragment.timestamp, geb.timestamp);
    EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  }
  
}
//  EOF should cause the reader to throw a 0.

void reader::testeof()
{
  CReader r(m_channel);

  r(NULL);			// First fragment.
  r(NULL);			// secon fragment.

  // We should be out of data now:

  bool thrown = false;
  try {
    r(NULL);
  }
  catch (int error) {
    thrown = true;
    ASSERT(error == 0);
  }
  ASSERT(thrown);
}

// Test that the connection object  constructs all the way through to an
// event source of the right type.
//

void reader::connectconstruct()
{
  // Have to open a new channel since cleanup would double kill otherwise:

  Tcl_Obj* tclFilename = Tcl_NewStringObj(m_filename, -1); // For Tcl_FSOpenFileChannel
  Tcl_IncrRefCount(tclFilename);
  Tcl_Channel chan     = Tcl_FSOpenFileChannel(m_pInterp, tclFilename, "r",  0);

  CBuilder* pBuilder = CBuilder::getInstance();
  CConnection connection(m_pInterp, chan);


  unsigned i = connection.getId();


  // The above should have made an event source 
  // with the id i;

  vector<int> sources = pBuilder->sourceIds();
  EQ(static_cast<size_t>(1), sources.size());
  EQ(i, static_cast<unsigned>(sources[0]));
}
// test that triggering the channel handler manually will allow
// event fragments to get injected.
//
void reader::connecttransfer()
{
  // Have to open a new channel since cleanup would double kill otherwise:

  Tcl_Obj* tclFilename = Tcl_NewStringObj(m_filename, -1); // For Tcl_FSOpenFileChannel
  Tcl_IncrRefCount(tclFilename);
  Tcl_Channel chan     = Tcl_FSOpenFileChannel(m_pInterp, tclFilename, "r",  0);

  CBuilder* pBuilder = CBuilder::getInstance();
  CConnection connection(m_pInterp, chan);

  // Invoking ready should read/inject a fragment:

  CConnection::ready(&connection, TCL_READABLE);

  {  
    CBuilder::pEvent pe = pBuilder->build();
    ASSERT(pe != NULL);
    CBuilder::Event& e(*pe);
    EQ(static_cast<size_t>(1), e.size());
    Fragment& f(*(e[0]));
    gebData& geb(f.s_fragment);
  
    EQ(fragment.type,      geb.type);
    EQ(fragment.length,    geb.length);
    EQ(fragment.timestamp, geb.timestamp);
    EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  }
  // Should be able to do this again:

  CConnection::ready(&connection, TCL_READABLE);
  {  
    CBuilder::pEvent pe = pBuilder->build();
    ASSERT(pe != NULL);
    CBuilder::Event& e(*pe);
    EQ(static_cast<size_t>(1), e.size());
    Fragment& f(*(e[0]));
    gebData& geb(f.s_fragment);
  
    EQ(fragment.type,      geb.type);
    EQ(fragment.length,    geb.length);
    EQ(fragment.timestamp, geb.timestamp);
    EQ(0, memcmp(fragment.payload, geb.payload, sizeof(payload)));
  }
  
}
// On eof, the channel should get shut down.
//

class TestConnection : public CConnection
{
public:
  bool shutdown;
public:
  TestConnection(Tcl_Interp* pInterp, Tcl_Channel source) :
    CConnection(pInterp, source),
    shutdown(false)
  {
    
  }
  virtual ~TestConnection() {}
  virtual void onEnd() 
  {
    shutdown = true;
  }
};
void reader::connecteof()
{
  // Have to open a new channel since cleanup would double kill otherwise:

  Tcl_Obj* tclFilename = Tcl_NewStringObj(m_filename, -1); // For Tcl_FSOpenFileChannel
  Tcl_IncrRefCount(tclFilename);
  Tcl_Channel chan     = Tcl_FSOpenFileChannel(m_pInterp, tclFilename, "r",  0);

  CBuilder* pBuilder = CBuilder::getInstance();
  TestConnection connection(m_pInterp, chan);

  CConnection::ready(&connection, TCL_READABLE); // First event
  CConnection::ready(&connection, TCL_READABLE); // last event.
  CConnection::ready(&connection, TCL_READABLE); // Should trigger the eof so:

  ASSERT(connection.shutdown);

}
//
// Running the event loop should eventually cause the
// connection to shut down naturally and blow away.
// this test requires that the connection be dynamically created as it will delete itself
// when the EOF occurs.
//
void reader::connecteof_event()
{
  // Have to open a new channel since cleanup would double kill otherwise:

  Tcl_Obj* tclFilename = Tcl_NewStringObj(m_filename, -1); // For Tcl_FSOpenFileChannel
  Tcl_IncrRefCount(tclFilename);
  Tcl_Channel chan     = Tcl_FSOpenFileChannel(m_pInterp, tclFilename, "r",  0);

  CBuilder* pBuilder = CBuilder::getInstance();

  CConnection* pConnection = new CConnection(m_pInterp, chan);

  // Now run the Tcl Event loop:
  //  When there are no event handlers left.. which will happen when
  //  the connection deletes itself..the loop will exit.

  while (Tcl_DoOneEvent(TCL_ALL_EVENTS | TCL_DONT_WAIT) == 1) {
  }
  // Force builds until nothing comes out... that should kill off the source:

  while (pBuilder->build(INT_MAX)) 
    ;

  vector<int> ids    = pBuilder->sourceIds();
  EQ(static_cast<size_t>(0), ids.size());
}
