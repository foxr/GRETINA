/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "Fragment.h"
#include <stdlib.h>
#include <string.h>


/*
** Assign data from one gebdata to another... the timestamp of the target is not modified
*/
static void
assign(gebData& lhs, const gebData& rhs)
{
  lhs.type      = rhs.type;
  lhs.length    = rhs.length;
  lhs.timestamp = rhs.timestamp;
  lhs.payload   = malloc(rhs.length);
  memcpy(lhs.payload, rhs.payload, rhs.length);
}

/**
 ** Construct a fragment. 
 ** - The dynamic storage of the input fragment is duplicated 
 ** - The current time is associated with the new fragment.
 ** @param data - reference to a gebData that will be wrapped in this struct.
 **
 ** @note malloc/free will be used for memory management as the gebData is necessarily not
 **       a C++ object.
 */
Fragment::Fragment(gebData& data)
{
  time(&s_whenCreated);
  assign(s_fragment, data);
}

/**
 ** Destruction - simply have to free the payload of the fragment.
 */
Fragment::~Fragment()
{
  if(s_fragment.payload) free(s_fragment.payload);
}

/**
 ** Assignment.. assignment is timestamp preserving (for now).
 */
Fragment&
Fragment::operator=(const Fragment& rhs)
{
  s_whenCreated = rhs.s_whenCreated;
  assign(s_fragment, rhs.s_fragment);
}
