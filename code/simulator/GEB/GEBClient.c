/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>
#include <errno.h>
#include "GEBClient.h"
/**
 ** @file Client libraries for the Gretina GEB simulator. 
 **     -   GEBClientInit - initialize the connetion to the GEB.
 **     -   sendGEBData   - send data to the GEB
 **     -   setGEBClient  - sets the connection parameters
 **                        and opens the connection 
 **     -   closeGEBClient- Shutdown connections with the GEB.
 **     -   checkGEBClient- Check status of the GEB (are we connected)?
 */


/*--------------------------------------------------------------------------------------*/

/**
 ** Produce the geb client data structure.
 ** @reuturn struct gebClient*
 ** @retval pointer to a newly created GEBClient data structure
 **                 that can be used in other calls to this library.
 */
struct gebClient*
GEBClientInit()
{
  struct gebClient* result = (struct gebClient*)calloc(1, sizeof(struct gebClient));
  if (!result) {
    return NULL;
  }

  /** Inititialize the data we can:  */


  result->outSock = -1;		/* Illegal socket. */
 
  if(htonl(1) == 1) {
    result->bigendian  = 1;
  }
  else {
    result->bigendian = 0;
  }
  
  return result;
}
/**
 ** setGEBClient sets the client data and attempts a connection.
 ** to the host requested.
 ** @param pLink - Pointer to a GEBClient struct gotten from GEBClientInit().
 ** @param addr  - DNS name of the host with which we are establishing the connection.
 ** @param port  - Port number we are going to use to establish the connection
 ** @return int
 ** @retval 0  Ok completion.
 ** @retval 1  - Unable to convert xxx.yyy.zzz.qqqq Ip address to internal format.
 ** @retval 2  - failure to open the socket.
 ** @retval 3  - Unable to connect to the server.
 ** @retval 4  - Unable to resolve the hostname via DNS.
 **
 ** @note In most failure cases you cannot rely on the errno to give more details because output will
 **       be attempted by the failure handling code.
 */
int setGEBClient(struct gebClient *i, char * geb_addr, int port) {

   struct sockaddr_in adr_srvr;  /* AF_INET */
   struct hostent *hp;

   if (i->outSock != -1) {
      close(i->outSock);
   }
   memset(&adr_srvr,0,sizeof adr_srvr);
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(port);

   hp = gethostbyname(geb_addr);
   if (!hp) {
      printf("hostname %s not resolved\n", geb_addr);
      return 4;
   }
   adr_srvr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *) 
                                (hp->h_addr_list[0]))));

   if ( adr_srvr.sin_addr.s_addr == INADDR_NONE ) { /* Don't know how to test this. */
      printf("bad geb address %s port %d\n", geb_addr, port);
      return 1;
   }

   i->outSock = socket(AF_INET, SOCK_STREAM, 0); /* or this short of exhausting fds. */
   if (i->outSock == -1) {
      printf("Unable to open output socket.\n");
      return 2;
   }

   if (connect(i->outSock, 
        (struct sockaddr *)&adr_srvr, sizeof(adr_srvr)) < 0) {
      printf("Connect to GEB failed\n");
      close(i->outSock);
      i->outSock = -1;
      return 3;
   }

   return 0;

}
/**
 ** Close a GEB client link.
 ** @param i - Pointer tothe geb link struct.
 ** @note once called the socket is unusable.
 ** @note The struct is not freed but is available for another reconnection via
 **       a new call to setGEBClient, call free on the pointer to release its storage.
 */ 
void closeGEBClient(struct gebClient *i) 
{
  
  if (i->outSock != -1) {
    close(i->outSock);
    i->outSock=-1;
  }
}  

/**
 ** Send data to the GEB (simulator).
 ** @param i      - Pointer returned from a call to GEBClientInit()
 ** @param outmsg - Pointer to a struct gebData which describes the data to send.
 **
 ** @return int
 ** @retval 0 - Success.
 ** @retval 1 - Write to socket failed.
 ** @retval 2 - Payload is not set or has a length < 0
 ** @retval 3 - Socket  link is closed.
 **
 */
int sendGEBData(struct gebClient *i, struct gebData *outmsg) {

   unsigned int *outdata;
   int  sendsize;
   unsigned long long temp;

   if (0 == outmsg) {
     outmsg = &i->zeromsg;
     outdata = (int*)outmsg;
     printf("Sending keepalive message to geb\n");
     sendsize = GEB_HEADER_BYTES;
   } else {
     if (!outmsg->payload || outmsg->length < 1) {
        return 2;
     }
     sendsize = GEB_HEADER_BYTES + outmsg->length;
     outdata = calloc(sendsize, 1);

     if (!(i->bigendian)) {
        bcopy(outmsg, &outdata[0], GEB_HEADER_BYTES);
        bcopy(outmsg->payload, &outdata[4], outmsg->length);
     } else {
        swab(outmsg, &outdata[0], GEB_HEADER_BYTES);
        swab(outmsg->payload, &outdata[4], outmsg->length);
     }
   }

   if (i->outSock == -1) {
      if (outmsg != &i->zeromsg) {
          free(outdata);
      }
      return 3;
  }

   if ( send(i->outSock, outdata, sendsize, MSG_NOSIGNAL) < 0) {
     printf("output to GEB failed %s\n", strerror(errno));
     close(i->outSock);
     i->outSock = -1;
     printf("GEB disconnected\n");
     if (outmsg != &i->zeromsg) {
         free(outdata);
     }
     return 1;
   }
   if (outmsg != &i->zeromsg) {
      free(outdata);
   }
   return 0;
}
/**
 ** Check the status of the connection.. This is done by sending a keepalive 
 ** message and consuming the output.
 ** @param i - link gotten from GEBClientInit and opened with setGEBClient.
 ** @return int
 ** @retval  sizeof(int)  - link is ok.
 ** @retval  0            - link is done for.
 **
 ** @note original code depended on the caller to ensure the GEB was sending an int back?
 **       this code posts a keepalive message to the server instead.
 */
int checkGEBClient(struct gebClient *i) 
{

   int status = 1;
   int data;


   if (i->outSock == -1) {
      return 0;
   }

   status = sendGEBData(i, NULL);
   if (status != 0) {
     close(i->outSock);
     i->outSock = -1;
     printf("Could not send keepalive to GEB");
     return 0;
   }
   status = read(i->outSock, &data, sizeof(int));

   if (status == 0) {
         close(i->outSock);
         i->outSock = -1;
         printf("GEB disconnected\n");
   }

   return status;
}
