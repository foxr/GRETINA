/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

      http://www.gnu.org/licenses/gpl.txt

      Author:
	      Ron Fox
	      NSCL
	      Michigan State University
	      East Lansing, MI 48824-1321
 */

#include "CInjector.h"
#include "CBuilder.h"
#include "Fragment.h"
#include <iostream>

extern void writeEvent(std::ostream& o, CBuilder::pEvent pe);
/**
 ** Construct the injector... this will also create an event source
 ** for the CBuilder singleton.
 ** @param id - Id of the event source to create.
 */
CInjector::CInjector(int id) :
  m_SourceId(id) 
{
  CBuilder* pBuilder = CBuilder::getInstance();
  pBuilder->addSource(id);
}
/**
 ** Destruction has to remove the event source and hence any fragments
 ** fromt he queue.
 */
CInjector::~CInjector() 
{
  CBuilder* pBuilder = CBuilder::getInstance();
  pBuilder->removeSource(m_SourceId);
}

/**
 ** Takes a fragment sent to us and inserts it in the
 ** event builder's fragment queue for us.
 ** @param pFragment - actualy a pointer to global event builder data.
 */
void CInjector::operator()(void* pFragment)
{
  CBuilder* pBuilder = CBuilder::getInstance();
  gebData* pf = reinterpret_cast<gebData*>(pFragment);

  pBuilder->queueFragment(m_SourceId, 
			  reinterpret_cast<gebData*>(pFragment));

  // If the oldest fragment's timestamp differs from the timestamp on
  // this fragment by more than the time tolerance, trigger building:


  CBuilder::pEvent pe; 
  while (((pf->timestamp - pBuilder->oldestTime(false)) > CBuilder::m_nTimeTolerance)  &&
	 (pe = pBuilder->build())) {
     
    writeEvent(std::cout, pe);
    CBuilder::freeEvent(pe);
  }
 
}
