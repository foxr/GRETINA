#!/bin/sh
# start wish \
    exec tclsh ${0} ${@}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2009.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Author:
#             Ron Fox
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


#
#  This file contains code that manages the GEB.
#  Usage is as follows:
#
#  GEBController.tcl gebdir outputspec
#
#  where
#   gebdir     - is the name of the directory in which GEB the executable is living.
#   outputspec - specifies what the GEB will output to.
#
# Notes:
#   In general outputspec will need to be quoted and must be an acceptable set of parameters
#   to the Tcl [exec] command. e.g. "| /usr/opt/daq/gretina/bin/recordData" would specify that
#   that data would be piped to a program and "> /dev/null" specifies data will be discarded.'
#

package require epics
set lastState "*UNKNOWN*";	# Prior GEB state.
set gebPath   [list];		# GEB executable path.
set outputSpec [list];		# Where output data will go.
set finished    0;		# vwait on this to get an event loop going.
set pid         0

#
#  Handle changes to program state..we care about:
#
#   Setup->Run - start the GEB.
#   Run->Setup - Request an end to the GEB.
# Parameters:
#    var   -  Variable nanme
#    index -  Index of array if an array element was written
#    op    -  Operation that fired the trace.
proc stateChanged {var index op} {
    puts "Changed From $::lastState  to $::currentState"
    if {($::lastState eq "Setup")  && ($::currentState eq "Run")} {
	    puts starting
	    set ::pid [exec $::gebPath {*}$::outputSpec & ]
    }
    if {($::lastState eq "Run")   && ($::currentState eq "Setup")} {
	    puts ending
	catch {exec kill INT [lindex $::pid 0]}; # request GEB exit.
	puts killd
    }
    set ::lastState $::currentState
    puts "Prior state set to $::lastState"
	
}


#
#  output information about the usage to the user:
#
proc usage {} {
    puts "Usage:"
    puts "   GEBController.tcl gebdir outputspec"
    puts "Where:"
    puts "   gebdir      - Is the directory that has the GEB executable"
    puts "   outputspec  - Is an output specification acceptable the tcl \[exec\] command"
}

#
# Entry point:
#
# validate the parameters, set up the EPICS channels, link them 
# to variables and trace those vars
#


if {[llength $argv] < 2} { 
    usage
    exit -1
}


# Global event builder:

set gebDir [lindex $argv 0]
set gebPath [file join $gebDir GEB]
if {![file executable $gebPath]} {
    puts "Unable to find GEB executable: $gebPath"
    usage
    exit -1
}

set outputSpec [join [lrange $argv  1 end] " "]
puts "'$outputSpec'"

epicschannel Cluster_CV_State
Cluster_CV_State link currentState
trace add variable currentState write stateChanged

vwait finished