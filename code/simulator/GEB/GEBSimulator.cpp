
#include "GEBLink.h"
#include "CConnection.h"
#include <tcl.h>
#include <iostream>
#include <time.h>
#include "CObserver.h"
#include "CBuilder.h"
#include "Fragment.h"
#include <stdint.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>


const int TimedBuildPeriod(500);

// Send a built event fragment to a stream..

void writeEvent(std::ostream& o, CBuilder::pEvent pe)
{
    CBuilder::Event& e(*pe);	// Much easier to deal with.
 

    // We'll write the number of fragments in the event as well as the payload for each fragment.

    size_t fragCount = e.size();
    uint32_t frags   = fragCount; // ensure the length of the fragment count.
    o.write(reinterpret_cast<char*>(&frags), sizeof(uint32_t));

    for (int i = 0; i < e.size(); i++) {
      Fragment* f = e[i];
      o.write(reinterpret_cast<char*>(&(f->s_fragment)), sizeof(gebData) - sizeof(void*));
      o.write(reinterpret_cast<char*>((f->s_fragment.payload)), f->s_fragment.length);
    }
    o.flush();			// Ensure the entire event got written out.
    

}




/**
 ** Trigger a timed event queue build.
 ** We'll take all events up until now - TimedBuildPeriod.
 */ 
void timedBuilding(ClientData p)
{
  time_t now = time(NULL);
  CBuilder* pBuilder = CBuilder::getInstance();
  CBuilder::pEvent pe;
  while (pe = pBuilder->build(now-TimedBuildPeriod)) {
    writeEvent(std::cout, pe);
    CBuilder::freeEvent(pe);
  }


  Tcl_CreateTimerHandler(TimedBuildPeriod, timedBuilding,NULL);
}

/**
 ** Complete a connection by attaching a new CConnection object to it and 
 ** setting an observer on it which will trigger event building when data is present.
 ** @param pData      - really a Tcl_Interp* pointer to our interpreter.
 ** @param connection - Channel on which to communicate with the peer.
 ** @param hostName   - Host we are connected to.
 ** @param port       - Tcp port number allocated for the connection.
 */
void completeConnection(ClientData pData, Tcl_Channel connection, char* hostName, int port)
{
  Tcl_Interp* pInterp = reinterpret_cast<Tcl_Interp*>(pData);
  Tcl_SetChannelOption(pInterp, connection, "-encoding", "binary"); 
  Tcl_SetChannelOption(pInterp, connection, "-translation", "binary binary");
  CConnection* pConnection = new CConnection(pInterp, connection);
}

/**
 ** Handle signals for exiting
 */
bool done = false;		// request to exit.

/** set the done flag...signal functino
 */

static void 
doneHandler(int signal)
{
  done = true;
}

/*
** Set up the done hanldler for SIGINT
*/
static void
setDoneHandler()
{
  struct sigaction requestedAction;
  requestedAction.sa_handler = doneHandler;;
  requestedAction.sa_flags   = 0;
  __sigemptyset(&(requestedAction.sa_mask));

  int status = sigaction(SIGINT, &requestedAction , NULL);
  if (status) {
    perror("Failed to establish exit request handler");
    exit(EXIT_FAILURE);
  }

  
}
/**
 ** This program simulates the Gretina event builder in a very crude way.
 ** The program filters built data to stdout... in normal use,
 ** This would be a stage in a pipeline where the second stage would emulate the output tap.
 ** Input is from TCP clients that connect to GEB_PORT.
 **
 ** We are going to run event building based on a pair of 
 ** triggers:
 **   -  Each connection will have an observer registered that will
 **      trigger event building if a fragment has been received.
 **   -  A timer will trigger event building for data older than 5 seconds ago.
 **
 **/


int main(int argc, char** argv)
{

  // If an integer parameter has been supplied, treat it as the builder time tolerance:

  if (argc > 1) {
    CBuilder::m_nTimeTolerance = atoll(argv[1]);
  }

  // We're going to use the Tcl Event loop to run this so:

  Tcl_Interp* pInterp = Tcl_CreateInterp();

  // Create the socket listener:

  Tcl_Channel server = Tcl_OpenTcpServer(pInterp, GEB_PORT, 
					 NULL, completeConnection, pInterp);
  Tcl_CreateTimerHandler(TimedBuildPeriod,timedBuilding, NULL);


  // Establish a signal handler to set done true:

  setDoneHandler();

  while (Tcl_DoOneEvent(TCL_ALL_EVENTS) && !done)
    ;

  timedBuilding(NULL);		// Flush the build queues.
  exit(EXIT_SUCCESS);
}
