/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef _BUILDER_H
#define _BUILDER_H

#ifndef _STL_MAP
#include <map>
#ifndef _STL_MAP
#define _STL_MAP
#endif
#endif

#ifndef _STL_LIST
#include <list>
#ifndef _STL_LIST
#define _STL_LIST
#endif
#endif

#ifndef _STL_VECTOR
#include <vector>
#ifndef _STL_VECTOR
#define _STL_VECTOR
#endif
#endif

#ifndef _STL_STRING
#include <string>
#ifndef _STL_STRING
#define _STL_STRING
#endif
#endif

#ifndef _CRT_TIME_H
#include <time.h>
#ifndef _CRT_TIME_H
#define _CRT_TIME_H
#endif
#endif



struct Fragment;
struct gebData;



/**
 ** This is the event builder part of the simulator.
 ** The event builder accepts fragments,
 ** It can be asked to build fragments into complete events.
 ** There are two types of builds it can do.
 ** one that requires all input queues to have some data in them.
 ** another that allows builds up to some creation time
 ** regardless of the 'emptiness' of existing event queues.
 ** This class is a singleton.
 */

class CBuilder {
  // types:
  // An assembled event is a vector of fragments:
public:
  typedef std::vector<Fragment*> Event, *pEvent;

  // a fragment queue is a list of fragments:


  typedef std::list<Fragment*> FragmentQueue, *pFragmentQueue;
  typedef struct _FragQueueData {
    bool            s_markedForDelete;
    FragmentQueue   s_queue;
  } FragQueueData, *pFragQueueData;

  // The event builder needs a bunch of fragment queues that are
  // identifies by the id of the contributor

  typedef std::map<int, pFragQueueData> Queues, *pQueues;


  // object data:

private:

  Queues  m_FragmentQueues;
  

  // class data:
private:
  static CBuilder* m_pInstance;

public:
  static long long m_nTimeTolerance;
  /** Canonicals */
private:
  CBuilder();
  ~CBuilder();
  CBuilder& operator=(const CBuilder&);
  int operator==(const CBuilder&) const;
  int operator!=(const CBuilder&) const;

  // Object interface for the event builder.
public:
  void addSource(int id);
  void removeSource(int id);

  void queueFragment(int id, gebData* pData);

  pEvent build();
  pEvent build(time_t until);

  bool empty();
  bool allQueues();

  static void freeEvent(pEvent p);
  long long oldestTime(bool failempty);

  // Hooks for status monitoring and testing.
public:					
  std::vector<int> sourceIds();
  void reinit();
  size_t depth(int id);

  // Singleton interface:
public:
  static CBuilder* getInstance();

  // Utility functions:
private:
  Queues::iterator findQueueOrThrow(int id, std::string method) throw(std::string);

  pEvent matchFragments(long long stamp);
  void destroySource(int id);

  
};

#endif
