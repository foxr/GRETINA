/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef _OBSERVER_H
#define _OBSERVER_H

/**
 ** This class is an abstract base class for the classes that can act as observers
 ** for other objects.  An observer is an object that is notified when something
 ** changes of 'interest' for an object.  It is one way to decouple actions from
 ** trigger as well.
 */
class CObserver {
public:
  virtual void operator()(void* pData) = 0; // Function called on observation.
};

#endif
