#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"
#include <string>
#include <iostream>
#include "CObserver.h"
#include "CObserverList.h"
#include <time.h>

using namespace std;

int main(int argc, char** argv)
{

  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"



class observers : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(observers);
  CPPUNIT_TEST(add);
  CPPUNIT_TEST(remove);
  CPPUNIT_TEST(invoke);
  CPPUNIT_TEST(payload);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void add();
  void remove();
  void invoke();
  void payload();
};

CPPUNIT_TEST_SUITE_REGISTRATION(observers);

// Observers used by the tests:

class NullObserver : public CObserver
{
public:
  virtual  void operator()(void* p) {}
};


class CountingObserver : public CObserver
{
public:
  int nTimes;
  CountingObserver() : nTimes(0) {}
  virtual void operator()(void* p) {
    nTimes++;
  }
};

class PointerObserver : public CObserver
{
public:
  void* pPayload;
  PointerObserver() : pPayload(0) {}
  virtual void operator() (void* p) {
    pPayload = p;
  }
};
/// Each time I add an observer to the list it size() of the list shouild match


void observers::add()
{
  CObserverList observers;
  NullObserver  observer;
  
  for (int i=0; i < 100; i++) {
    observers.add(&observer);
    EQ(static_cast<size_t>(i+1), observers.size());
  }
}

// Each invocation of remove will remove one of my observers.

void observers::remove()
{
  // Stock the observer list

  CObserverList observers;
  NullObserver  observer;
  
  for (int i=0; i < 100; i++) {
    observers.add(&observer);
  }

  for (int i =100; i > 0; i--) {
    observers.remove(&observer);
    EQ(static_cast<size_t>(i-1), observers.size());
  }
}

//  Should have the right number of invocations.

void observers::invoke()
{
  CObserverList observers;
  CountingObserver observer;

  for (int i=0; i < 100; i++) {
    observers.add(&observer);
  }
  observers.invoke(NULL);

  EQ(100, observer.nTimes);

}

void observers::payload()
{

  CObserverList observers;
  PointerObserver  observer;
  

  observers.add(&observer);
  observers.invoke(&observers);

  EQ(reinterpret_cast<void*>(&observers), observer.pPayload);
}
