/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CBuilder.h"

#include "Fragment.h"
#include "GEBLink.h"
#include <stdio.h>
#include <limits.h>
#include <iostream>

using namespace std;

// Class level data:

CBuilder* CBuilder::m_pInstance(0);
long long CBuilder::m_nTimeTolerance(0);


/**
 ** In order to have private constructors we need to define constructors in the first place:
 */
CBuilder::CBuilder()
{
}
/**
 ** Destructor should never be called but may be needed by linker
 */

CBuilder::~CBuilder() 
{
}

/**
 ** The other canonicals are explicitly not implemented.
 */

/**
 ** Get the singleton instance, creating it if needed.
 ** @return CBuilder*
 ** @retval pointer to the singleton event builder.
 */
CBuilder*
CBuilder::getInstance() 
{
  if (!m_pInstance) {
    m_pInstance = new CBuilder;
  }
  return m_pInstance;
}

/**
 ** Free an event; frees all resources associated with a built event.
 ** @param p
 */
void 
CBuilder::freeEvent(CBuilder::pEvent p)
{
  Event& e(*p);

  // Delete the fragments:

  for (int i =0; i < e.size(); i++) {
    delete e[i];
  }
  delete p;			// Delete the vector itself.
}


/*------------------------------- object interface implementation ------------------------*/

/**
 ** Add a data source to the list of data sources.
 ** A fragment queue is created, and the new queue added to the map.
 ** @param id - identification of the data source.
 */
void
CBuilder::addSource(int id)
{
  if (m_FragmentQueues.find(id) != m_FragmentQueues.end()) {
    throw string("Error - CBuilder::addSource: duplicate source id");
  }
  m_FragmentQueues[id] = new FragQueueData;
  m_FragmentQueues[id]->s_markedForDelete = false;
}
/**
 ** Removes a data source from the event builder.  At this point we assume
 ** there may still be fragments in the queue to build.  The queue is just
 ** marked for delete.  Once it becomes empty the actuall delete is done.
 **
 ** @param id - the id of the sourcde to remove.
 **
 */
void
CBuilder::removeSource(int id)
{
  Queues::iterator p = findQueueOrThrow(id, __func__);
  FragQueueData& Frags(*(p->second));
  Frags.s_markedForDelete = true;

  // If the queue is empty we can remove it now:

  if (Frags.s_queue.empty()) {
    destroySource(id);
  }
}

/**
 ** Queue a fragment to a specific event queue:
 ** @param id - the id of the queue to put it in.
 ** @param pData - Pointer to the gebData to queue.  This will be wrapped in a fragment.
 */
void
CBuilder::queueFragment(int id, gebData* pData)
{
  Queues::iterator p = findQueueOrThrow(id, __func__);
  FragQueueData& Frags(*(p->second));
  Fragment* pFragment = new Fragment(*pData);
  Frags.s_queue.push_back(pFragment);


}

/*-------------------------------monitoring/test support ---------------------------------*/

/**
 ** Return a vector containing the ids of the sources.
 ** @return std::vector<int>
 ** @retval vector containing the ids of the existing sources.
 **         these will be in ascending id order due to the nature of the underlying storage.
 */
vector<int>
CBuilder::sourceIds()
{
  vector<int> result;

  for (Queues::iterator p = m_FragmentQueues.begin(); p != m_FragmentQueues.end(); p++) {
    result.push_back(p->first);
  }

  return result;
}
/**
 ** Kill off the existing fragment queues.
 ** using the removeSource method for each id we now know of should recover
 ** storage as well.
 */
void
CBuilder::reinit()
{
  while (!m_FragmentQueues.empty()) {
    int id = m_FragmentQueues.begin()->first;
    destroySource(id);
  }
}
/**
 ** Returns the depth of an event queue given the id.
 ** @param id - Id of event queue to check.
 */
size_t
CBuilder::depth(int id)
{
  Queues::iterator p = findQueueOrThrow(id, __func__);
  FragQueueData& Frags(*(p->second));
  
  return Frags.s_queue.size();

}

/**
 ** First of the two builder functions.  This function requires that we have
 ** elements from all fragment queues...else we don't have any event.
 ** All fronts of queues that match the earliest time will be built together into an
 ** event, and handed off to the caller.  The fragments used to form the event will
 ** be removed from their queues.
 ** @return CBuilder::pEvent
 ** @retval NULL - No event could be built.
 ** @retval other- Pointer to the  event we built.  This will be a dynamically allocated 
 **                vector with at least one fragment in it.
 */
CBuilder::pEvent
CBuilder::build()
{

  long long mintimestamp = oldestTime(true);
  if (mintimestamp == LLONG_MAX) {
    return reinterpret_cast<pEvent>(NULL);
  }
  else {
    return matchFragments(mintimestamp);
  }
}
/**
 ** Build events until the received time of the oldest fragment is
 ** At most some time.
 ** This buiding will go on regardless of whether or not there are empty queues.
 ** This is used to timeout unmatched fragments.
 ** @param until -  limiting time on the received value.
 ** @return CBuilder::pEvent
 ** @retval NULL - All fragment queues are either empty or
 **                the oldest fragment is newer than until.
 ** @retval Other - An event.
 */
CBuilder::pEvent
CBuilder::build(time_t until)
{
  // Spin through the event queues looking for fragment that was received oldest
  // that one needs to be younger than until for us to do a build.

  time_t oldest = LONG_MAX;	// assumes time_t is a long.
  for(Queues::iterator p = m_FragmentQueues.begin(); p != m_FragmentQueues.end(); p++) {
    pFragmentQueue q = &(p->second->s_queue);
    if (!q->empty()) {
      Fragment* f = q->front();
      if(f->s_whenCreated < oldest) oldest = f->s_whenCreated;
    }
  }
  //If the oldest is old enough build:

  if (oldest < until) {

    long long mintimestamp = oldestTime(false);
    return matchFragments(mintimestamp);

  }
  else {
    return reinterpret_cast<pEvent>(NULL);
  }

}

/**
 ** Returns true if there are no fragment queues with data.
 */
bool
CBuilder::empty()
{
  for (Queues::iterator p = m_FragmentQueues.begin(); p != m_FragmentQueues.end(); p++) {
    pFragmentQueue q = &(p->second->s_queue);
    if (!q->empty()) return false;
  }
  return true;
 
 }
/*-------------------------- Utility (private) methods --------------------------------------*/

/**
 ** Locate the specified queue or throw an error message
 ** @param id - id of the queue to find.
 ** @param method - Name of the method calling this.
 ** @return Queues::iterator 
 ** @retval iterator 'pointing' to the queue being located.
 **/
CBuilder::Queues::iterator
CBuilder::findQueueOrThrow(int id, string method) throw(string)
{
    Queues::iterator p = m_FragmentQueues.find(id);
    if (p != m_FragmentQueues.end()) {
      return p;
    }
    else {
      char message[1000];
      sprintf(message, "ERROR - CBuilder::%s: nonexistent event source", method.c_str());
      throw string(message);
    }

}

/**
 ** Determine the oldest time of any fragment
 ** @param failempty - if true, this 'fails' if there are any empty queues.
 ** @return long long
 ** @retval LLONG_MAX - Either there are no queues with fragments or
 **                     there failempty is true and there is at least one empty queue.
 ** @retval other      - The timestamp of the oldest generated fragment.
 */
long long
CBuilder::oldestTime(bool failempty) 
{
  long long mintimestamp = LLONG_MAX;

  for (Queues::iterator p = m_FragmentQueues.begin(); p != m_FragmentQueues.end(); p++) {
    pFragmentQueue q = &(p->second->s_queue);
    if (!q->empty()) {
      
      Fragment* f = q->front();
      if(f->s_fragment.timestamp < mintimestamp) {
	mintimestamp = f->s_fragment.timestamp;
      }
    }
    else if (failempty) {
      return LLONG_MAX;
    }
  }
  return mintimestamp;

}
/**
 ** Return fragments from the queues that have a timestamp that matches the parameter.
 ** This could yield an empty event in theory though the caller will generally
 ** ensure this cannot happen
 ** @param stamp - Timestamp to match
 ** @return pEvent - dynamically allocated event with matching data.
 */
CBuilder::pEvent
CBuilder::matchFragments(long long stamp)
{
  pEvent pE= new Event;
  Event& e(*pE);
  std::vector<int> killSet;
  
  for (Queues::iterator p = m_FragmentQueues.begin(); p != m_FragmentQueues.end(); p++) {
    pFragmentQueue q = &(p->second->s_queue);
    Fragment* f = q->front();
    while ((!q->empty()) && ((f->s_fragment.timestamp - stamp) <= m_nTimeTolerance)) {
            e.push_back(f);
      q->pop_front();
      if (q->empty() && p->second->s_markedForDelete) { // If empty destroy if pending delete after loop.
	killSet.push_back(p->first);
      }
      if (!q->empty()) {
	f = q->front();
      }

    } 
  }
  // This must be done in a second pass in order to maintain the validity of the iterator inthe
  // loop above... in general, however killSet.size() will be 0
  //
  if (killSet.size() != 0) {
  }
  for (int i =0; i < killSet.size(); i++) {
    destroySource(killSet[i]);
  }
  return pE;  
}
/**
 ** Destroy a queue, and all the stuff in it.
 ** @param id  - Id of the queue.
 */
void
CBuilder::destroySource(int id)
{
  // Locate the fragment queue:'

  Queues::iterator pQ = findQueueOrThrow(id, "CBuilder::destroySource");

  // Normally there should be no pending fragments...but in testing there may be so
  // we delete them anyway:

  FragmentQueue& Frags = (pQ->second->s_queue);
  for(FragmentQueue::iterator pf=  Frags.begin(); pf != Frags.end(); pf++) {
    delete *pf;
  }
  Frags.clear();
  m_FragmentQueues.erase(pQ);
  
}

/**
 ** Return true if all fragment queues have a fragment
 */
bool CBuilder::allQueues()
{
  Queues::iterator pQ= m_FragmentQueues.begin();
  while (pQ != m_FragmentQueues.end()) {
    FragmentQueue& Frags = pQ->second->s_queue;
    if (Frags.size() == 0) {
      return false;
    }
    pQ++;
  }
  return true;
}
