/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "testoptions.h"

#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include "GEBClient.h"
#include <time.h>

/**
 ** This file contains a simple test data source for the gretina GEB simulator.
 ** It can send events of a specific structure to the GEB with specified timings
 ** Usage:
 **    testSource options...
 ** Options:
 ** \verbatim
 **    --timing(-t)   - Integer milliseconds between events. defaults to 1.
 **    --pattern(-e)  - Pattern to send for events:    
 **                     countup, countdown, shift, randoms
 **                     defaults to countup
 **    --sizelow(-l)  - smallest event fragment sent, defaults to 10
 **    --sizehi(-h)   - largest event fragment sent, defaults to 100
 **    --tag(-g)      - Event type to use defaults to 0x12345
 **    --port(-p)     - Port on which the GEB is to be contacted, defaults to GEB_PORT (9005).
 **    --host(-h)     - Host on which the GEB is running, defaults to localhost.
 **    --tinc(-i)    - Increment in timestamp between events defaults to 1.
 **    --tbase(-z)    - First timestamp value.
 ** \endverbatim
 */

/** classes that produce event body patterns.
 */

/*---------------- pattern base class ----------------*/

/* Uses strategy pattern to farm out the filling of the event but
** standardizes memory management
*/
class Pattern
{
  int* m_pData;
public:
  Pattern();
  ~Pattern();

  virtual int* create(size_t num);
protected:
  int*         allocate(size_t num);
  void         free();
  virtual void fill(size_t num, int* pData) = 0;

};
Pattern::Pattern() : m_pData(0) {}
Pattern::~Pattern() {free(); }

/** Pattern utilities:
 ** Create storage for an event.
 */
int* Pattern::allocate(size_t num) 
{
  free();
  m_pData =  new int[num];
  return m_pData;
}
/** 
 ** Free the event
 */
void  Pattern::free() 
{
  delete []m_pData;
}
/**
 ** Creat an event
 */
int* 
Pattern::create(size_t num)
{
  int* pData = allocate(num);
  fill(num, pData);
  return pData;
}

/*-------------- count up pattern---------------------------*/

class Countup : public Pattern
{
protected:
  virtual void fill(size_t num, int* pData);
  
};
void
Countup::fill(size_t num, int* pData)
{
  for (int i =0; i < num; i++) {
    *pData++ = i;
  }
}
/*-------------- count down pattern ------------------------*/



class CountDown : public Pattern
{
  virtual void fill(size_t num, int* pData);

};
void
CountDown::fill(size_t num, int* pData)
{
  while(num) {
    *pData++ = num;
    num--;
  }
}

/*--------------- Shift bits pattern --------------------*/
class Shift  : public Pattern
{
  virtual void fill(size_t num, int* pData);

};
void
Shift::fill(size_t  num, int* pData)
{
  int bit = 1;
  for(int i =0; i < num; i++) {
    *pData++ = bit;
    bit = bit << 1;
    if (bit == 0) {
      bit = 1;
    }
  }
}

/*-------------- Random data 'pattern' ----------------------*/

class Random : public Pattern
{
  virtual void fill(size_t num, int* pData);
};
void
Random::fill(size_t num, int* pData)
{
  for (int i =0; i < num; i++) {
    *pData++ = lrand48();
  }
}
/*------------- Pattern factory creates/selects event generated desired ----*/

class PatternFactory
{
public:
  static Pattern* create(std::string type);
};
Pattern*
PatternFactory::create(std::string type)
{
  if (type == "countup") {
    return new Countup;
  }
  if (type == "countdown") {
    return new CountDown;
  }
  if (type == "shift") {
    return new Shift;
  }
  if (type == "randoms") {
    return new Random;
  }
  throw std::string("Unrecognized pattern type");
}



/**
 ** Main entry point
 */
int main(int argc, char** argv)
{
  gengetopt_args_info parsedOptions;
  srand48(time(NULL));

  if (cmdline_parser(argc, argv, &parsedOptions)) {
    cmdline_parser_print_help();
    exit(EXIT_FAILURE);
  }
  try {
    Pattern* generator = PatternFactory::create(std::string(parsedOptions.pattern_arg)); 
    gebClient* pClient = GEBClientInit();
    int        status  = setGEBClient(pClient, parsedOptions.host_arg, parsedOptions.port_arg);
    if (status != 0) {
      throw std::string("Failed connection to GEB");
    }
    gebData fragment = {
      parsedOptions.tag_arg,
      0,
      parsedOptions.tbase_arg,
      0
    };
    int smallest = parsedOptions.sizelow_arg;
    int range    = parsedOptions.sizehi_arg - smallest;
    while (1) {
      
      size_t size      = smallest + drand48()*range;
      fragment.payload = generator->create(size);
      fragment.length    = size*sizeof(int);
      
      status = sendGEBData(pClient, &fragment);
      if (status != 0) {
	throw std::string("Send to GEB failed");
      }
      fragment.timestamp += parsedOptions.tinc_arg;
      usleep(parsedOptions.timing_arg * 1000);
    }
  }
  catch (std::string msg) {
    std::cerr << msg << std::endl;
    cmdline_parser_print_help();
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);

}
