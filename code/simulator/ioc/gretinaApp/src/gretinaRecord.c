/* xxxRecord.c */
/* Example record support module */
  
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "alarm.h"
#include "dbAccess.h"
#include "recGbl.h"
#include "dbEvent.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "devSup.h"
#include "errMdef.h"
#include "recSup.h"
#include "special.h"
#define GEN_SIZE_OFFSET
#include "gretinaRecord.h"
#undef  GEN_SIZE_OFFSET
#include "epicsExport.h"

/* Gretina status values we care about: */

#define GRETINA_RUNNING      3
#define GRETINA_SETUP        5
#define GRETINA_INCONSISTENT 7

/* string size */

#define ENUM_STRING_SIZE 32


/* Gretina run transition functions */

static void tryToStart(gretinaRecord* pRec);
static void tryToStop(gretinaRecord*  pRec);

/* Create RSET - Record Support Entry Table */
#define report NULL
#define initialize NULL
static long init_record();
static long process();
static long get_units();
static long get_precision();
static long get_graphic_double_info();
static long get_control_double_info();
static long get_alarm_double_info();
static long get_enum_str();
static long put_enum_str();
static long get_enum_strs();


#define special NULL
#define get_value NULL
#define cvt_dbaddr NULL
#define get_array_info NULL
#define put_array_info NULL
 
rset gretinaRSET={
	RSETNUMBER,
	report,
	initialize,
	init_record,  
	process,      
	special,
	get_value,
	cvt_dbaddr,
	get_array_info,
	put_array_info,
	get_units,
	get_precision,
	get_enum_str,
	get_enum_strs,
	put_enum_str,
	get_graphic_double_info, 
	get_control_double_info,
	get_alarm_double_info
};
epicsExportAddress(rset,gretinaRSET);



typedef struct gretinaset { /* xxx input dset */
	long		number;
	DEVSUPFUN	dev_report;
	DEVSUPFUN	init;
	DEVSUPFUN	init_record; /*returns: (-1,0)=>(failure,success)*/
	DEVSUPFUN	get_ioint_info;
	DEVSUPFUN	read_xxx;
}gretinadset;



static long init_record(void *precord,int pass)
{
    gretinaRecord	*pGretina = (gretinaRecord *)precord;
    gretinadset	*pdset;
    long	status;

    if (pass==0) return(0);

    if(!(pdset = (gretinadset *)(pGretina->dset))) {
	recGblRecordError(S_dev_noDSET,(void *)pGretina,"xxx: init_record");
	return(S_dev_noDSET);
    }
    /* must have read_xxx function defined */
    if( (pdset->number < 5) || (pdset->read_xxx == NULL) ) {
	recGblRecordError(S_dev_missingSup,(void *)pGretina,"xxx: init_record");
	return(S_dev_missingSup);
    }

    if( pdset->init_record ) {
	if((status=(*pdset->init_record)(pGretina))) return(status);
    }
    return(0);
}

static long process(void *precord)
{
  gretinaRecord* pGretina = (gretinaRecord*)(precord);
  gretinadset*   pDset;
  long           value;
  
  pGretina->pact = TRUE;
  
  recGblGetTimeStamp(pGretina);
  
  /* If we've been hooked into our device, invoke read_xxx 
   That will set pxxx->udf FALSE if we can read the link and
   return one of
    -1  -Could not read the link
     0  -Could read the link and the BI attached is 0.
     1  -Could read the link and the BI attached is 1.
   Based on the return value and our current state, we modify our VAL accordingly.
  */ 
  
  if(pGretina->dset) {
    pDset = (gretinadset*)(pGretina->dset);
    value = (*pDset->read_xxx)(pGretina);
    if (value != -1) {
      if (value == 1) {
	tryToStart(pGretina);	/* Trying to start the run. */
      }
      else if (value == 0) {
	tryToStop(pGretina);	/* Trying to stop the run. */
      }
      else {
	printf("Invalid value from link!!!\n");
	/* Illegal value for a Binary input. */
      }
      /* For now since we are passively processed..triggered only by changes
      ** and link changes, we raise the value changed event each time.
      */
      db_post_events(pGretina, &(pGretina->val), DBE_VALUE);
    }
  }

  recGblFwdLink(pGretina);
  
  pGretina->pact=FALSE;
  return(0);
}




/*
** note that pUnits must be at least DB_UNITS_SIZE+1 bytes long and 
** initialized to 0's as strncpy may not terminate the the copy with a null.
*/

static long get_units(struct dbAddr* paddr, char* pUnits)
{
  gretinaRecord* pGretina = (gretinaRecord*)(paddr->precord);
  strncpy((char*)paddr, 
	  (const char*)pGretina->egu, DB_UNITS_SIZE); /* egu may not be null terminated? */

  return 0;

}


/*
 * Gets the precision. pfield of our record points to the
 * field in our record we want the precision for.
 */
static long get_precision(struct dbAddr* paddr, long* precision)
{
  gretinaRecord* pGretina = (gretinaRecord*)(paddr->precord);

  if (paddr->pfield == &pGretina->val) {
    *precision = pGretina->prec;
  }
  else {
    recGblGetPrec(paddr, precision);
  }
  return 0;
}
/**
 ** Low and high limits are set by Gretina's needs...currently there are values from 
 ** zero to seven or zrvl -> svvl
 */
static long get_graphic_double_info(struct dbAddr* pAddr, struct dbr_grDouble* p)
{
  gretinaRecord* pGretina = (gretinaRecord*)(pAddr->precord);
  int           fieldIdx = dbGetFieldIndex(pAddr);

  /* low and hi come from values of ZRVL and SVVL if caller wants the val field: */

  if (fieldIdx == gretinaRecordVAL) {
    p->lower_disp_limit = pGretina->zrvl;
    p->upper_disp_limit = pGretina->svvl;
  }
  else {
    recGblGetGraphicDouble(pAddr, p);
  }
  return 0;
}

/**
 ** Basically the same as get_graphic_double_info except for the result fields.
 */

static long get_control_double_info(struct dbAddr* pAddr, struct dbr_ctrlDouble *p)
{
  gretinaRecord* pGretina = (gretinaRecord*)(pAddr->precord);
  int            fieldIdx = dbGetFieldIndex(pAddr);

  if (fieldIdx == gretinaRecordVAL) {
    p->upper_ctrl_limit = pGretina->svvl;
    p->lower_ctrl_limit = pGretina->zrvl;
  }
  else {
    recGblGetControlDouble(pAddr, p);
  }

  return 0;

}
/*
** Basically the same as get_graphic_double_info except for the fields:
*/
static long get_alarm_double_info(struct dbAddr* pAddr, struct dbr_alDouble* p)
{
  gretinaRecord* pGretina = (gretinaRecord*)(pAddr->precord);
  int            fieldIdx = dbGetFieldIndex(pAddr);

  if (fieldIdx == gretinaRecordVAL) {
    p->upper_alarm_limit =
      p->upper_warning_limit = pGretina->svvl;
    p->lower_alarm_limit = 
      p->lower_warning_limit   = pGretina->zrvl;
  }
  else {
    recGblGetAlarmDouble(pAddr, p);
  }

  return 0;
}

/*
** Functions that deal witht he enumerated nature of the beast.
*/

/*  Return the enumerated string that corresponds to the current value */
static long get_enum_str(struct dbAddr* pAddr, char* p)
{
  gretinaRecord* pRecord = (gretinaRecord*)(pAddr->precord);
  int            fieldIdx= dbGetFieldIndex(pAddr);
  const char*    pSource;

  /* Require that we're asking about the val field. */


  if (fieldIdx == gretinaRecordVAL) {

    if (pRecord->val <= 7) {
      pSource = pRecord->zrst + sizeof(pRecord->zrst)*pRecord->val;
      strncpy(p, pSource, sizeof(pRecord->zrst));
    }
    else {
      strcpy(p, "Illegal value");
    }
  }
  else {
    strcpy(p, "Illegal field");
  }
  return 0;
}

/*
** if the string supplied is a legal enum value put the corresponding 
** enum value in VAL.
*/
static long
put_enum_str(struct dbAddr* pAddr, char* p)
{
  gretinaRecord* pRecord = (gretinaRecord*)(pAddr->precord);
  const char*    pStrings = pRecord->zrst;
  short          i;
  int            fieldIdx = dbGetFieldIndex(pAddr);

  /* Must be dealing with the value field */

  if (fieldIdx == gretinaRecordVAL) {
    for(i =0; i < 8; i++) {
      if (strncmp(p, pStrings, sizeof(pRecord->zrst)) == 0) {
	pRecord->val = i;
	pRecord->udf  = FALSE;
	return 0;
      }
      pStrings += sizeof(pRecord->zrst);
    }
  }

  return S_db_badChoice;
}

/*
 * Return the "menu" of enum choices to the caller.
 */
static long  get_enum_strs(struct dbAddr* pAddr, struct dbr_enumStrs* pStrings)
{
  gretinaRecord* pRecord = (gretinaRecord*)(pAddr->precord);
  const char*    pSource = pRecord->zrst;
  int            i;
  pStrings->no_str = 0;
  for (i = 0; i< 8; i++) {
    /* only count/copy nonempty strings */
    if (*pSource != 0) {
      strncpy(pStrings->strs[pStrings->no_str], pSource, sizeof(pRecord->zrst));
      pStrings->no_str++;
    }
    pSource += sizeof(pRecord->zrst);
  }
  return 0;
}

/**
 ** Try to start the run.  To be startable, the state must be GRETINA_SETUP
 ** Attempting to start the run in any other state results in a transition to
 ** GRETINA_INCONSISTENT.
 */
static void 
tryToStart(gretinaRecord* pRec)
{
 
  if ((pRec->val == GRETINA_SETUP) ||
      (pRec->val == GRETINA_RUNNING)) {
    pRec->val = GRETINA_RUNNING;
  }


}
/**
 ** Try to end the run.  To be endable the state must be GRETINA_RUNNING else the
 ** state becomes GRETINA_INCONSISTENT
 */
static void
tryToStop(gretinaRecord* pRec)
{
  if ((pRec->val == GRETINA_RUNNING) ||
      (pRec->val == GRETINA_SETUP)) {
    pRec->val = GRETINA_SETUP;
  }

}
