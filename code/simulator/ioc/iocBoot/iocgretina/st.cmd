#!../../bin/linux-x86/gretina

## You may have to change myexample to something else
## everywhere it appears in this file

< envPaths

cd ${TOP}

## Register all support components
dbLoadDatabase("dbd/gretina.dbd")
gretina_registerRecordDeviceDriver(pdbbase)

## Load record instances
dbLoadRecords("db/Gretina.db","user=foxHost")


## Set this to see messages from mySub
#var mySubDebug 1

cd ${TOP}/iocBoot/${IOC}
iocInit()

## Start any sequence programs
#seq sncExample,"user=foxHost"
