/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
/**
 *  This header describes a set of functions that control 
 *  The gretina simulator state.
 *  For state getting, you should instead use the Gretina epics library,
 *  as the simlator iOC uses the same PV's as the real thing.
 */


#ifndef __SIM_CONTROL_H
#define __SIM_CONTROL_H



#ifdef __cplusplus
//  C++ requires extern "C" to prevent name mangling.

extern "C" {

#endif

  void  simulator_init();	     /* Perform all required initializations */
  void  simulator_state_initial();   /* Set the simluator state to 'Initial' */
  void  simulator_state_failed();     /* Set the simulator state to 'Failed'  */
  void  simulator_state_check();      /* Set the state to 'Checked'          */
  void  simulator_state_run();        /*  Set the state to "Run" [active]    */
  void  simulator_state_wait();       /* Set the state to "Wait'  */
  void  simulator_state_setup();      /* Set the state to "Setup" */
  void  simulator_state_wfd();        /* Set the state to "WaitForDone" */;
  void  simulator_state_inconsistent(); /* Set the state to "Inconsistent" */


#ifdef _cplusplus
}
#endif

#endif
