/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

/**
 ** This is a simple control program for the simulator.
 ** Usage:
 **     sim [new_state}
 ** regardless of use, sim will report the current simulator state (waiting as needed)
 ** for the connection to occur.
 ** If a new state is provided, it is set and the final state reported.
 */
#include "simControl.h"
#include "simInternals.h"
#include <cadef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const int maxRetries = 10;

typedef void (*StateChanger)();

typedef struct _dispatchEntry {
  const char* state;
  StateChanger fcn;
} dispatchEntry, *pDispatchEntry;


static dispatchEntry dispatchTable[] = {
  {"Initial", simulator_state_initial},
  {"Failed",  simulator_state_failed},
  {"Check", simulator_state_check},
  {"Run",     simulator_state_run},
  {"Wait",    simulator_state_wait},
  {"Setup",   simulator_state_setup},
  {"WaitForDone", simulator_state_wfd},
  {"Inconsistent", simulator_state_inconsistent},
  {NULL, NULL}
};

/*
** Given a state name returns a pointer to the function that will request that state
** @param newValue - pointer to the string holding the new value.
** @return (*void ())
** @retval NULL - no matching state name.
** @retval not-null - the function that will request the named state.
*/
static StateChanger
getStateChange(const char* newValue)
{
  pDispatchEntry p = dispatchTable;
  while (p->state) {
    if (strcmp(p->state, newValue) == 0) {
      return p->fcn;
    }
    p++;
  }
  return NULL;
}

/*
** Set a new value.
** Retrieve the corresponding state change function pointer and call it.
**
** @param newValue - Pointer to the string that has the new desired state value
*/

static void
setNewValue(const char* newValue)
{
  StateChanger fcn = getStateChange(newValue);

  if(!fcn) {
    fprintf(stderr, "%s is not a legal gretina cluster state\n", newValue);
    exit(EXIT_FAILURE);
  }

  fcn();
}

/**
 ** Entry point.  See the header comments for a description of this
 ** program
 */

int main(int argc, const char* const* argv)
{
  int i;
  simulator_init();

  /* Connect to the simulator ..but timeout if we fail after a while */

  for (i = 0; i < maxRetries; i++) {
    if (PVConnectionStatus == HaveValue) break;
    ca_pend_io(0.5);
    ca_pend_event(0.5);
  }
  if (PVConnectionStatus != HaveValue) {
    fprintf(stderr, "Timedout connecting to the simulator\n");
    exit(EXIT_FAILURE);
  }

  /*  Report the current value */

  simulator_lockValue();
  printf("Initial: %s\n", pCurrentValue);
  simulator_unlockValue();

  /* If a new value has been provided, set it and see what the new value is */
  /* Note that the set value may not be the new value if the state is       */
  /* Setup or Run as the control PV may force the state value back          */

  if (argc > 1) {
    setNewValue(argv[1]);
    ca_pend_io(.5);
    ca_pend_event(0.5);
    simulator_lockValue();
    printf("Final: %s\n", pCurrentValue);
    simulator_unlockValue();
  }

  exit(EXIT_SUCCESS);
  
}
