/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

/**
 *   This file implements the simulator control interface defined in
 *   simControl.h
 *
 */


#include <config.h>
#include <simControl.h>
#include <simInternals.h>
#include <cadef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>



const char* pPVName = "Cluster_CV_State";


/**
 ** The following are globals rather than static in order to support unit testing:
 */

StatusState PVConnectionStatus = UnInitialized;
char*       pCurrentValue      = NULL;

pthread_mutex_t valueLock = PTHREAD_MUTEX_INITIALIZER;

static   chid   channelId;


/*---------------------------------------------------------------------------
  Static (private) functions.
----------------------------------------------------------------------------*/

/**
 * This function is called when we get a value changed event.
 * - set the state to HaveValue
 * - Copy the current value -> pCurrentValue.
 * - the mutex is used to make the pCurrentValue modification atomic
 *   so long as nobody holds a copy of the pointer outside of a lock/unlock pair.
 */
static void
changeHandler(struct event_handler_args args)
{
  simulator_lockValue();
  
  /* We have unconditinoally asked for a string conversion... */

  if (pCurrentValue) {
    free(pCurrentValue);
  }
  const char* pNewValue = (const char*)(args.dbr);
  pCurrentValue = malloc(strlen(pNewValue) + 1); /* Null terminated string. */
  strcpy(pCurrentValue, pNewValue);

  /* For sure we have a value now: */;

  PVConnectionStatus = HaveValue;
  simulator_unlockValue();
  
}

/**
 *  This function is invoked when the connection status of the
 *  channel has changed.  We are passed a pointer to a connection_handler_args 
 *  struct.  We only care about the op field of this  which will be either
 *  CA_OP_CONN_UP or CA_OP_CONN_DOWN which causes us to perform the
 *  specific state transition.
 */

static void
connectionHandler(struct connection_handler_args args)
{
  if (args.op == CA_OP_CONN_UP) {
    /* Connection up..still no value though */

    PVConnectionStatus = Connected;
  }
  else if (args.op == CA_OP_CONN_DOWN) {
    /* Connection lost.. also free the string of the last value */

    if(pCurrentValue) {
      free(pCurrentValue);
      pCurrentValue = NULL;
    }
    PVConnectionStatus == Disconnected;

  }
  else {
    fprintf(stderr, "Invalid op in connection handler call: %ld\n",
	   args.op);
  }
}

/**
 ** Common function to set the value of the PV
 ** @param pNewState - String that specifies the new state value.
 **                    since this is a static function it is left to the
 **                    caller to ensure this state is a legal string.
 **  Once the change of stat is committed, our change handler will get called
 **  to propogate that to our state value.
 **
 ** @note - to change state requires that we are in Connected or HaveValue
 */
static void
changeState(const char* pNewState)
{
  int status;

  if ((PVConnectionStatus != Connected)     &&
      (PVConnectionStatus != HaveValue)) {
    fprintf(stderr, ">>Warning attempting to change state to %s while not connected may fail\n",
	    pNewState);
  }

  status = ca_put(DBR_STRING,
		  channelId, pNewState);
  if (status != ECA_NORMAL) {
    fprintf(stderr, "ca_put %s\n", ca_message(status));
    exit(-1);
  }
  ca_pend_io(.5);
  ca_pend_event(0.5);
		   
}

/*----------------------------------------------------------------------------

  Public functions:

------------------------------------------------------------------------------*/

/**
 ** Lock access to the value string
 */
void 
simulator_lockValue()
{
  pthread_mutex_lock(&valueLock);
}

/**
 ** Unlock access to the value string:
 */
void
simulator_unlockValue()
{
  pthread_mutex_unlock(&valueLock);
}
/**
 **   Initialize the simulator library.  This involves
 **   Initializing EPICS CA, connecting to the
 **   cluster global status variable and
 **   registering a desire to know abou connections, disconnections, and changes.
 **   For now, we exit on status errors from epics functions.
 **
 */

void
simulator_init()
{
  int     status;

  status = ca_task_initialize();
  if (status != ECA_NORMAL) {
    fprintf(stderr, "ca_task_initialize - %s\n", ca_message(status));
    exit(-1);
  }
  status = ca_search_and_connect((char*)pPVName,
				 &channelId,
				 connectionHandler,
				 NULL);
  if (status != ECA_NORMAL) {
    fprintf(stderr, "ca_search_and_connect %s\n", ca_message(status));
    exit(-1);
  }

  status = ca_add_event(DBR_STRING,
 		        channelId,
			changeHandler,
			NULL,
			NULL
			);

  if (status != ECA_NORMAL) {
    fprintf(stderr, "ca_add_event %s\n", ca_message(status));
    exit(-1);
  }
  status = ca_pend_io(1.0);	/* Flush our search operation. */
  if (status != ECA_NORMAL) {
    fprintf(stderr, "ca_pend_io %s\n", ca_message(status));
    exit(-1);
  }
  PVConnectionStatus = Disconnected;

}
/**
 * The following functions are just going to call changeState with an appropriate string.
 */

void simulator_state_initial()
{
  changeState("Initial");
}
void simulator_state_failed() 
{
  changeState("Failed");
}
void simulator_state_check()
{
  changeState("Check");
}
void simulator_state_run() 
{
  changeState("Run");		/* Should not usually do this but instead use Online_CS_StartStop  */
}
void simulator_state_wait()
{
  changeState("Wait");
}
void simulator_state_setup()
{
  changeState("Setup");		
}
void simulator_state_wfd()
{
  changeState("WaitForDone");
}
void simulator_state_inconsistent()
{
  changeState("Inconsistent");
}

