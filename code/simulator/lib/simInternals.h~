/**
 ** Provides interfaces to the internals of the simulator control
 ** library.  This is uased by simControl.c and by
 ** unit testing.
 */


#ifndef __SIM_INTERNALS_H
#define __SIM_INTERNALS_H


/*
** This enum defines the possible connectedness to EPICS.
*/

typedef enum _StatusState {
  UnInitialized,
  Disconnected,
  Connected,
  HaveValue,
} StatusState;

#ifdef __cplusplus
extern "C" {
#endeif

  /**  The variable below describes the connection status
   ** of the Cluster_CS_Status PV
   */

  StatusState PVConnectionStatus;

  /**
   ** When the state is 'Connected'
   ** and we have gotten our first update this will point to the last
   ** retrieved value.  This value is only maintained for unit test purposes.
   ** If the current state is not 'HaveValue', this may be a null pointer, be forewarned.
   */
  char* pCurrentValue;

  /**
   **  The functions below lock/and unlock concurrent access to the pCurrentValue
   */
  void simulator_lockValue();
  void simulator_unlockValue();

#ifdef __cplusplus
}
#endif

#endif
