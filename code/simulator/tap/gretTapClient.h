/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef __GRETTAPCLIENT_H
#define __GRETTAPCLIENT_H

/*
 * This file is a header that describes the gretina TAP client
 * library.  If you include this you should also include
 * <GEBClient.h> as that describes data structures used by
 * this software.
 */


/* Const definitions NOTE: these may differ in value outside the simulator env.
  and have no affect on the operatio of the simulator.
*/
#define GRETTAP_RAW           0
#define GRETTAP_CRYSTAL_EVENT 1
#define GRETTAP_POSITION      2
#define GRETTAP_GEB           3
#define GRETTAP_TRACK         4



/* data type forward declarations */

struct gretTap;			/* Opaque handle type */
struct gebData;


/* If necessary the function prototypes are wrapped in extern  "C" */

#ifdef __cplusplus
extern "C" {
#endif

  struct gretTap* gretTapConnect(char* address, int position, int gebType);
  struct gebData* gretTapData(struct gretTap* pTap, int nreq, float timeout);
  void            gretTapClose(struct gretTap* pTap);
  int             gretTapCheck(struct gretTap* pTap);
  void            gretTapDataFree(struct gebData* pData);

#ifdef __cplusplus
}
#endif

extern int tapErrno;

#endif
