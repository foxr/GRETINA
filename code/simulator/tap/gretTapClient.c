/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "gretTapClient.h"
#include <GEBClient.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


#ifndef NULL
#define NULL 0
#endif

/*  Internal definitions of opaque types */

struct gretTap {
  int s_socket;			/* Socket connected to the tap.      */
  int s_position;		/* Desired position in the dataflow  */
  int s_type;     		/* Event type desired.               */
  int s_requests;		/* Number of requests.               */
  int s_eofReached;             /* true if the eof was hit           */
  
};

/* In the file the heder is compressed to: */

struct header {
  int type;
  int length;
  long long timestamp;
};

/* Our errno variable... not threadsafe.  */

int tapErrno;


/**
 * This is a simulator for the gretina tap client that operates
 * using an event file from Gretina.  The following environment
 * variables control the simulation:
 *  EVENT_FILE - path to the input file. If not defined, connections
 *               fail with EHOSTUNREACH
 *
 *  TIMEOUTS   - If defined and a non zero integer n, every n requests for
 *               data result in a timeout.
 *  gretTapCheck will be true if an eof has not been detected on the file.
 *
 */

const char* eventFile;


/*----------------------------------------------------------------------------*/
/* Private functions                                                          */

/**
 * The usual function to read a block of data from an fd.
 *
 * @param pTap - Pointer to the tap context, pTap->s_socket is the fd.
 * @param pBuffer - Pointer to the buffer.
 * @param nbytes  - Number fo bytes to read.
 *
 * @return int
 * @retval >0 - Number of bytes read.
 * @retval = 0 - end of file.
 * @retval <0 error.
 */
ssize_t
readBlock(struct gretTap* pTap, void* pBuffer, size_t nBytes)
{
  ssize_t nRead;
  ssize_t totalRead = 0;

  char* p = (char*)pBuffer;
  int  fd = pTap->s_socket;

  while (nBytes) {
    nRead = read(fd, p, nBytes);
    if (nRead < 0) {

      pTap->s_eofReached = 1;	/* Ensure stuff stops. */
      return -1;

    } else if (nRead == 0) {
      // in this application that should not happen in the middle of a read:

      pTap->s_eofReached = 1;
      return 0;
    }
    totalRead += nRead;
    p         += nRead;
    nBytes    -= nRead;
  }
  return totalRead;
}

/**
 * Read an event from the file.
 * @param pTap    - Tap context pointer.
 * @param pHeader - Pointer to storage already allocated to the event header.
 *
 * @note if an end file is found pTap->eofReached is set true.
 */
void
readEvent(struct gretTap* pTap, struct gebData* pHeader)
{
  int   status;			/* I/O status */
  void* pBody;			/* Will point to event payload. */
  struct header fileHeader;

  status = readBlock(pTap, &fileHeader, sizeof(fileHeader));
  if (status != sizeof(fileHeader)) {
    pTap->s_eofReached= 1;
    memset(pHeader, 0, sizeof(struct gebData));
    return;
  }
  pHeader->type     = fileHeader.type;
  pHeader->length    = fileHeader.length;
  pHeader->timestamp = fileHeader.timestamp;
  if (pHeader->length == 0) {
    // empty event
    return;
  }
  pBody = malloc(pHeader->length);
  if (!pBody) {
    /* TODO: Real error handling!!!! */
    
    pTap->s_eofReached = 1;
    memset(pHeader, 0, sizeof(pHeader));
    return;
  }
  status = readBlock (pTap, pBody, pHeader->length);
  if (status == pHeader->length) {
    pHeader->payload = pBody;
  } else {
    free(pBody);
    pTap->s_eofReached = 1;
    memset(pHeader, 0, sizeof(struct gebData));
  }

}

/*----------------------------------------------------------------------------*/
/* Public functions                                                           */

/**
 * Connect to a gretina tap.
 * 
 * @param address - DNS or dotted IP address of tap server.
 * @param position - where in the data flow pipeline data should be gotten fronm
 * @param gebType  - Type of data accept (this is the type field in gebData struct)
 *                 - If the type field is 0 all data types are returned.
 *
 * @return struct gretTap*
 * @retval NULL - on error.
 * @retval non-null - a pointer to an opaque type that maintains the context of the
 *                    connection to the tap server.
 */
struct gretTap*
gretTapConnect(char* address, int position, int gebType)
{
  const char*     pFilename;
  struct gretTap* pTap;

  /* If we can't get the event file name we're done. */

  pFilename = getenv("EVENT_FILE");
  if (!pFilename) {
    tapErrno = EHOSTUNREACH;
    return NULL;
  }

  /* Create the pTap ...error dutifully reported in tapErrno: */

  pTap = (struct gretTap*)malloc(sizeof(struct gretTap));
  if (!pTap) {
    tapErrno = errno;
    return NULL;
  }

  /* Init the values we can prior to opening the file -- not checking the position for'
     legality in this simulator
  */
  pTap->s_socket      = -1;		/* Not yet open. */
  pTap->s_position    = position;
  pTap->s_type        = gebType;
  pTap->s_requests    = 0;
  pTap->s_eofReached  = 0;

  /* Try the file open.. failure of will result in an EHOSTUNREACH error
     after the gretTap struct is freed.
  */
  pTap->s_socket = open(pFilename, O_RDONLY);
  if (pTap->s_socket == -1) {

    free((char*)pTap);
    tapErrno = EHOSTUNREACH;
    return NULL;
  }

  return pTap;

}

/**
 * Get data from a gretina tap.
 * This requests a set number of events from the tap connection associated
 * with a gretTap object and returns a linked list of events
 * from that tap. The event list must be freed by passing it to 
 * gretTapDataFree.
 *  
 * @param pTap - Pointer to the tap object.
 * @param nreq - Number of events we'll try to get.
 * @param timeout - Floating point number of seconds to wait for events.
 *
 * @return struct gebData*
 * @retval NULL - Could not get data, see tapErrno for why.
 *                ETIME means that timeout passed with no data.
 * @retval not null - pointer to the head of a chain of events.
 *
 */

struct gebData*
gretTapData(struct gretTap* pTap, int nreq, float timeout)
{
  const char* pTimeoutCounter;
  int   timedout;
  struct gebData* pResult;
  struct gebData* pThisHeader;
  int    nRequested = nreq;



  /* If we hit the EOF, the TAP server would exit resulting in an ENOTCONN error. */

  if (pTap->s_eofReached) {
    tapErrno = ENOTCONN;
    return NULL;
  }

  /* If there's a TIMEOUTS env var, and it does not atoi to 0 and s_requests % TIMEOUTS == 0,
     timeout
  */
  pTap->s_requests++;
  pTimeoutCounter = getenv("TIMEOUTS");
  if (pTimeoutCounter) {
    timedout = atoi(pTimeoutCounter);
    if (timedout > 0 && ((pTap->s_requests % timedout) == 0)) {
      tapErrno = ETIMEDOUT;
      return NULL;    
    }  
  }
  /**
   * The memory will be allocated in a chunk for the
   * headers which will be linked together via next. as we read matching events.
   * event payloads will each have a dynamic memory chunk that will
   * be linked to it  via the header's payload field
   *
   * Allocate memory for the headers:
   */
  
  pThisHeader = pResult = (struct gebData*)calloc(nreq, sizeof(struct gebData));
  if (!pThisHeader) {
    tapErrno = ENOMEM;
    return NULL;
  }
  
  /* Read the data for each event. Any failure other than EOF results in 
     a call to gretTapDataFree (after doing what may be needed to make
     the struct consistent)
  */

  /* TODO: Get type selectivity to work */

  while (nreq) {
    readEvent(pTap, pThisHeader);
    if(pTap->s_eofReached) {
      break;
    }
    nreq--;			/* we read an event...speculatively link in the next one. */
    if(nreq) {
      struct gebData* pNext = pThisHeader + 1;
      pThisHeader->next = pNext;
      pThisHeader = pNext;
    }
  }
  /* Case where EOF on first read */

  if (nreq == nRequested) {
    free(pResult);
    return NULL;
  }
  return pResult;
  
}

/**
 * Close a tap.
 *
 * @param pTap - pointer to the opaque tap object.
 *
 */
void
gretTapClose(struct gretTap* pTap)
{
  /* Close and free the struct. The file id is set to -1 to gaurd against immediate reuse. */

  close(pTap->s_socket);
  pTap->s_socket = -1;
  free((char*)pTap);
 
}

/**
 * Check if a tap is still alive and connected.
 * 
 * @param pTap - pointer to tap opaque data struct that defines the connection.
 * 
 * @return int
 * @retval 0 - tap is alive.
 * @retval 1   tap has a problem described by tapErrno.
 */
int
gretTapCheck(struct gretTap* pTap)
{
  if (pTap->s_socket < 0 || pTap->s_eofReached) {

    tapErrno = ECONNABORTED;
    return 1;
  } else {
    return 0;
  }
}
/**
 * Free gebData* returned from gretTapData:
 *
 * @param pData - pointer to the data.
 */
void
gretTapDataFree(struct gebData* pData)
{
  /* First free each of they payload items: */

  struct gebData* pItem = pData;
  while (pItem) {
    if (pItem->payload) {
      free(pItem->payload);
    }
    pItem = pItem->next;
  }
  /* Finally free the header block */

  free(pData);
}
