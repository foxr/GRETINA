#include "gretTapClient.h"
#include <GEBLink.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

main()
{
  struct gretTap* pTap;
  struct gebData* pData;

  pTap = gretTapConnect("charlie.nscl.msu.edu", GRETTAP_GEB, 0);
  if (!pTap) {
    fprintf(stderr, "Failed opening the tap: %s\n", strerror(tapErrno));

  }
  while (1) {
    pData = gretTapData(pTap, 10, 10.0);
    if (!pData) {
      fprintf(stderr, "Failed on tap read %s\n", strerror(tapErrno));
      if (gretTapCheck(pTap)) {
	gretTapClose(pTap);
	exit(0);
      }
    }
    gretTapDataFree(pData);
    
  }

}
