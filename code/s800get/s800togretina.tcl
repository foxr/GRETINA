#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Jeromy Tompkins 
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



##
# @file s800togreinta.tcl
# @brief tie together all the bits and pieces that send S800 data -> gretina.
# @author Ron Fox <fox@nscl.msu.edu>
#

# Note this is intended to be used from a readout callouts


package provide S800ToGretina 1.0
package require Tk
package require ReadoutGUIPanel


namespace eval ::S800ToGretina {
    # Top level GretinaSoft is 2 dirs up from this script:
    
    variable GretinaSoft [                                             \
        file normalize [file join [file dirname [info script]] ../.. ] \
    ]
    variable DAQROOT $::env(DAQROOT)
    
    variable S800Host       spdaq50;           # Default host with s800 rings
    variable S800CCUSBRing  rawccusb;          # CCUSBReadout output ring.
    variable S800VMUSBRing  rawvmusb;          # VMUSBReadout output ring.
    variable S800EvbRing    s800built;         # Event builder output ring.
    variable S800Ring       s800filter;        # Reformatter/checker output ring
    
    
    variable S800RingURI tcp://$S800Host/$S800Ring
    
    variable PipePids [list]
    variable PipeOutputFd ""
    
    variable TsExtractLib [file join $GretinaSoft lib libs800.so]
    
    variable lastMessage
    
}

#  Add Gretina's TclLibs to the auto_path.

lappend auto_path [file join $::S800ToGretina::GretinaSoft TclLibs]

#------------------------------------------------------------------------------
# Private entries:
#

##
#  ::S800ToGretina::KillPipeline
#     *   Kill the pipeline elements
#     *   Close off the file descriptor open on the pipeline (cancelling the
#         fileevent [though that's not strictly speaking needed]).
#
proc ::S800ToGretina::KillPipeline {} {
    # Kill the pipeline:
    
    foreach pid $::S800ToGretina::PipePids {
        catch {exec kill -9 $pid}

    }
    set ::S800ToGretina::PipePids [list]
    
    #  Let the file handler catch the eof.
    
#    if {$::S800ToGretina::PipeOutputFd ne ""} {
#        fileevent $::S800ToGretina::PipeOutputFd readable ""
#        catch {close $::S800ToGretina::PipeOutputFd}
#        set ::S800ToGretina::PipeOutputFd ""
#    }
    
}

##
# ::S800ToGretina::StartPipeline
#   Starts the pipeline of processes that hoists data from the S800 to
#   the Gretina GEB.   pushToGeb |& cat
#   The last element allows us to  obtain the stdout and stderr from pushToGeb 
#
proc ::S800ToGretina::StartPipeline {} {
    
    
    # To build up the pushToGEB we need the GEB host and Port
    
    set GrBindir [file join $::S800ToGretina::GretinaSoft bin]
    set GEBHost [exec -- [file join $GrBindir gebnode]]
    set GEBPort [exec -- [file join $GrBindir gebport]]
    
    # Build the pushToGEB command:
    
    set pushCommand \
        [list [file join $GrBindir pushToGEB]                       \
         --source $::S800ToGretina::S800RingURI                     \
         --gebhost $GEBHost --gebport $GEBPort                      \
         --usercode $::S800ToGretina::TsExtractLib                  \
	     --tsmultiplier 8                                       \
    ]
    puts "Push command: $pushCommand"
    set ::S800ToGretina::PipeOutputFd [open "| $pushCommand |& cat" r]
    set ::S800ToGretina::PipePids [pid $::S800ToGretina::PipeOutputFd]
    fileevent                                                   \
        $::S800ToGretina::PipeOutputFd readable \
        [list ::S800ToGretina::OnInput $::S800ToGretina::PipeOutputFd]
    
}

##
# ::S800ToGretina::OnInput
#    Called when the gretina push pipeline stdout/stderr fd is readable:
#    -  If the fd is at EOF, pop up a bad error dialog and close off the pipe.
#    -  Set the fd to nonblocking
#    -  Read the data
#    -  Set the fd back to blocking mode.
#    -  Output lines to the console.
#
#  @param fd - the file descriptor to read from
proc ::S800ToGretina::OnInput {fd} {
    if {![eof $fd]} {
        fconfigure $fd -blocking 0
        set data [read $fd]
        fconfigure $fd -blocking 1
        
        # Log the lines to the ouptut window and
        # save the last line so that it can go into an exit popup.
        
        foreach line [split $data "\n"] {
            ::ReadoutGUIPanel::Log S800Pipe output $line
            set ::S800ToGretina::lastInput $line
        }
    } else {
        # Shutdown the pipeline.
        puts "OnInput -- EOF case"
        catch {::S800ToGretina::KillPipeline}
        # Pop up an error message:

	set baseMessage "The pipeline sending data to gretina just exited"

	if {[catch {close $fd} msg]} {
	    append baseMessage " because: $msg"
	}
	set ::S800ToGretina::PipeOutputFd ""; # No longer open.
	if {$msg ne ""} {
	    #	if {! $::gretina::expectingExit} {}
            set message $baseMessage
            append message "last output line: $::S800ToGretina::lastInput "
            append message "check  the S800Pipe output tab for more information"
	    tk_messageBox -title $message -icon error -message $baseMessage"
            catch {end}
        }
    }
    
    
}

#------------------------------------------------------------------------------
# Public entries:
#

##
# ::S800ToGretina::OnBegin
#   Call this proc at the beginning of a run.
#   Note that after the run ends, the pipeline gets destroyed because
#   the GEB exits.   The pipe may not notice this because it has not
#   yet received data.  We'll kill any existing pipeline now.
#   We will maintain contact with the pipeline's stdout and stderr
#   so that we can relay error messages and output etc.
#  Note the user may have changed the s800 host so regenerate the ring e.g.
#
proc ::S800ToGretina::OnBegin {} {
    set ::S800ToGretina::S800RingURI \
	tcp://$::S800ToGretina::S800Host/$::S800ToGretina::S800Ring
    ::S800ToGretina::KillPipeline
    ::S800ToGretina::StartPipeline
}


##
#  Initialization.
#   Kill any instances of pushToGEB:
#
if {[catch {exec killall pushToGEB} msg]} {
    puts "Attempted kills of old pushToGEB processes: $msg"
} else {
   puts "Successfully killed some stale pushToGEB processes"
}
