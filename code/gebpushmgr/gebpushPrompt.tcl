#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


##
# @file gebpushPrompt.tcl
# @brief Prompt for the parameters needed for a GEB Push client "data source"
# @author Ron Fox <fox@nscl.msu.edu>
#

package provide GEBClient_Prompter 1.0
package require Tk
package require snit

lappend auto_path [file join $::env(DAQROOT) TclLibs]

package require  DataSourceUI ;            # for dialog wrapper.

namespace eval GEBClient {
    ##
    #  variables:
    #   daqbin     - where NSCLDAQ binariis are.
    #   gretinabin - where GRETINA interface binaries are.
    #   gretinatcllibs - where GRETINA interface Tcl librariesare.
    
    set daqbin $::env(DAQBIN)
    set gretinabin $::env(GRBIN)
    set gretinatcllibs $::env(GRTCLLIBS)
    
    ##
    #  This dict provides parameterization and help text
    #
    
    variable parameterization [dict create \
        TimestampExtractor [list {Timestamp extraction library }]     \
        ControlType        [list {Control item GEB data type}]        \
        PhysicsType        [list {Phsics item GEB data type}]         \
        TSScalerType       [list {Time stamped scaler type}]          \
        DataSourceRingURI  [list {Data source ringbuffer URI}]        \
        TimestampMultiplier [list {Time stamp mulitplier to get to 100MHz}] \
    ]
    
    ##
    #  This array defines a correspondence between Keys
    #  above and options for the prompter widget:
    #  Note that we use the same options as the GEBPush manager snit class.
    
    variable parameterMap
    array set parameterMap [list   \
        TimestampExtractor  -usercode   \
        ControlType         -ctltype    \
        PhysicsType         -phystype   \
        TSScalerType        -tsscalertype \
        DataSourceRingURI   -source       \
        TimestampMultiplier -tsmultiplier  \
    ]
    
}

##
# @class GEBClientt::PromptForm
#
#   Prompts for the parameters the user can provide the
#   GebPush snit type.
#
snit::widgetadaptor GEBClient::PromptForm {
    option -usercode
    option -ctltype      -default 6
    option -phystype
    option -tsscalertype -default 10
    option -source
    option -tsmultiplier -default 1 
    #
    #  The Object really just has to pop up the widget and
    #  let its client fish out option values:
    #
    constructor args {
        installhull using ttk::frame
        
        # First row of widgets - user code.
        
        ttk::label $win.ucodelbl -text "Timestamp extractor: "
        ttk::entry $win.ucode    -textvariable [myvar options(-usercode)]
        ttk::button $win.ucodebrowse -command [mymethod _browseUserCode] \
            -text Browse...
        ttk::label $win.codehelp -text {Normally not needed - leave empty}
        
        ttk::label $win.typelabel1 -text \
            {This next row prompts for GEB data types. }
        ttk::label $win.typelabel2 -text \
            {Where defaults are provided they are usually appropriate.}
        
        ttk::label $win.phystypelabel -text {Physics type: }
        ttk::entry $win.phystype -textvariable [myvar options(-phystype)]
        ttk::label $win.ctltypelabel -text {Control type: }
        ttk::entry $win.ctltype -textvariable [myvar options(-ctltype)]
        ttk::label $win.scltypelabel -text {Scaler type: }
        ttk::entry $win.scltype -textvariable [myvar options(-tsscalertype)]
        
        ttk::label $win.ringurilabel -text {URI Of data source ring: }
        ttk::entry $win.ringuri -textvariable [myvar options(-source)] -width 32
        
        ttk::label $win.tsmultlabel -text {Timestamp multiplier}
        ttk::spinbox $win.tsmult    -from 1 -to 100 -wrap 1 \
            -textvariable [myvar options(-tsmultiplier)]
        
        
        # Lay all this out.
        
        grid $win.ucodelbl $win.ucode - - $win.ucodebrowse $win.codehelp -sticky w
        grid $win.typelabel1 - - $win.typelabel2  - -sticky w
        
        grid $win.phystypelabel $win.phystype   \
            $win.ctltypelabel $win.ctltype      \
            $win.scltypelabel $win.scltype -sticky w
        
        grid $win.ringurilabel $win.ringuri - - - -sticky w
        grid $win.tsmultlabel $win.tsmult -sticky w
        
        $win.tsmult set 1
        
        $self configurelist $args    
    }

}

##
# validate
#   Ensure the values of a parameter dict are ok semantically.
#
# @param params - parameter dict.
# @return string
# @retval empty - no errors.
# @retval nonempty - error message for popup dialog.
#
proc GEBClient::validate {params} {
    
    set result [list]
    
    #   Types must be [0, 31]:
    
    foreach key [list ControlType PhysicsType TSScalerType] {
        set value [lindex [dict get $params $key] end]
        if {![string is integer -strict $value]  || ($value < 0) || ($value > 31)} {
            lappend result "$key must be an integer between 0 and 31 was '$value'"
        }
    }
    if {[lindex [dict get $params DataSourceRingURI] end] eq ""} {
        lappend result "DataSourceRingURI must be a valid ringbuffer URI"
    }
    set tsMult [lindex [dict get $params TimestampMultiplier] end]
    if {![string is integer -strict $tsMult] || ($tsMult < 0)} {
        lappend result "TimestampMultiplier must be a positive integer was '$tsMult'"
    }
    set result [join $result "\n"]
    return $result
}

##
# promptParameters
#    Prompts for the parameters associated with a GEBPush data
#    source.
#    - Wraps a GEBClient::promptForm in a modal dialog.
#    - Runs the dialog.
#    - pulls the values from the dialog and builds the
#      parameter dict from them.
#

proc GEBClient::promptParameters {} {
    destroy .gebcliprompttoplevel
    
    set top [toplevel .gebcliprompttoplevel]
    set dlg [DialogWrapper $top.dialog]
    set container [$dlg controlarea]
    
    set form [GEBClient::PromptForm $container.form]
    $dlg configure -form $form
    
    pack $dlg -fill both -expand 1
    set response [$dlg modal]
    
    set result [dict create]
    if {$response eq "Ok"} {
        set result $GEBClient::parameterization
        dict for {key value} $result {
            set val [$form cget $GEBClient::parameterMap($key)]
            dict lappend result $key [list] [string trim $val]
        }
        set validationErrors [GEBClient::validate $result]
        if {$validationErrors ne ""} {
            tk_messageBox -icon error -parent $top -title "Validation Errors" -type ok -message $validationErrors
            set result ""          
        }
    }
    destroy $top
    return $result
}