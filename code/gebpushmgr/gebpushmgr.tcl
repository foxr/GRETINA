#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Jeromy Tompkins 
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



##
# @file gebpushmgr.tcl
# @brief Manages generic GEB pushers.
# @author Ron Fox <fox@nscl.msu.edu>
#

##
# This file is an 11.0 callback bundle that should be registered first.
# It allows users to create/run a set of gebpushers for gretina integration.
#
# A GEBPusher is encapsulated in a snit object that registers itself with the
# bundle when created.  The bundle iterates through the registered objects at
# the begin and end of runs.
# Furthermore, the gebpushmgr wathes pushers for exits and yells loudly if one
# does exit.
#

package provide gebpushmgr 2.0
package require snit
package require RunstateMachine
package require ui

namespace eval ::GebPushMgr {
    variable encapsulations [list]
    variable GRETROOT       $::env(GRTOP);    #/usr/opt/gretina/current;
    variable GRETBIN        $::env(GRBIN);    # [file join $GRETROOT bin]
    namespace export enter leave attach
}
##
# GebPush
#
#   Encapsulates a GEB Push program.
#
# OPTIONS
#   -usercode   - User code for timestamp extraction.
#   -ctltype    - Control data type.
#   -phystype   - Physics event type.
#   -tsscalertype - Timestamped scaler type.
#   -gebnode     - GEB hostname.
#   -gebport      - GEB poortn name.
#   -source       - Data source ring URI.
#   -tsmultiplier - Multipler for the timestamps.
#   -onexit       - Script if exits.
#   -no-size16    - Size word at front of physics events is not 16 bits.
#
    
    
snit::type GebPush {
    option -usercode -default ""
    option -ctltype  -default 6
    option -phystype -default 5
    option -tsscalertype -default 10
    option -gebnode  -default ""
    option -gebport  -default 9005
    option -source   -default ""
    option -tsmultiplier -default 1
    option -onexit   [list]
    option -no-size16 0
    
    variable exitExpected 0
    variable pipefd       ""
    
    ##
    # constructor
    #   Just ensure, after argument processing that
    #   - gebnode is not empty
    #   - -source is not empty.
    #   all others can default though many should not.
    #
    constructor {args} {
        $self configurelist $args
        
        if {$options(-gebnode) eq ""} {
            puts stderr "*WARNING* -gebnode is not defined hopefully it gets configured before BEGIN"
        }
        if {$options(-source) eq ""} {
            puts stderr "*WARNING* -source is not defined hopefully it gets configured before BEGIN"
        }
        lappend ::GebPushMgr::encapsulations $self;       # Register.
    }
    
    ##
    # onBegin
    #   Called at the beginning of the run.
    #   gebpush is started with stdout/stderr piped to a file that we monitor.
    #
    method onBegin {} {
        # Ensure that mandatory parameters are defined.  It's an error now
        # If they are not.

	if {$pipefd ne ""} {
	    set pids [pid $pipefd]
	    foreach pid $pids {
		catch {exec kill 9 $pid}
	    }
	    catch {close $pipefd}
	}
	
        if {$options(-gebnode) eq ""} {
            puts stderr "*ERROR* -gebnode is not defined at run start"
        }
        if {$options(-source) eq ""} {
            puts stderr "*ERROR* -source is not defined at run start"
        }

        if {"GRBIN" ni [array names ::env]} {
          return -code error "GRBIN environment variable is not SET!"
        }

        set gebport [exec [file join $::env(GRBIN) gebport] ]
        if { $gebport != $options(-gebport) } {
          puts stderr "*ERROR* -gebport option is incorrect. Using port value returned by $GRBIN/gebport program"
        }

        set gebnode [exec [file join $::env(GRBIN) gebnode] ]
        if { $gebnode != $options(-gebnode) } {
          puts stderr "*ERROR* -gebnode option is incorrect. Using host name returned by $GRBIN/gebnode program"
        }

        set command "[list [file join $::GebPushMgr::GRETBIN pushToGEB] \
            --usercode $options(-usercode) --ctltype $options(-ctltype) \
            --phystype $options(-phystype) --tsscalertype $options(-tsscalertype) \
            --gebhost $gebnode --gebport $gebport  \
            --source $options(-source) --tsmultiplier $options(-tsmultiplier) \
        ]"

        if {$options(-no-size16)} {
            lappend command --no-size16
        }
        set pipefd [open "| $command |& cat"]
        set exitExpected 0;                        # Should not expect pipeline to exit.
        fileevent $pipefd readable [mymethod _onInput]
        
        
    }
    ##
    # onEnd
    #   Note that the GEB exits at the end of the run so all we need to do is
    #   indicate we're expecting that:
    #
    method onEnd {} {
        set exitExpected 1

    }

    ##
    # _onInput
    #   Called when the input fd gets readable.
    #   THe input is echoed to the screen.   If there's an EOF we handle
    #   program exit
    
    method _onInput {} {
        
        if {[eof $pipefd]} {
	    puts "EOF on push to geb $exitExpected"
	    
            catch {close $pipefd}
            set pipefd ""
            if {!$exitExpected} {
                if {$options(-onexit) ne ""} {
                    uplevel #0 $options(-onexit) $self  
                } else {
                    tk_messageBox -icon error -title {gebpush exited} \
                        -message "gebpush from $options(-source) exited unexpectedly"
                }
                set exitExpected 1
            }

            
        }  else {
            fconfigure $pipefd -blocking 0
            set input [read $pipefd]
            set src "gebpush($options(-source))"
            ::ReadoutGUIPanel::Log $src output $input
            fconfigure $pipefd -blocking 1
        } 
  }

}


#-------------------------------------------------------------------------------
#  The bundle and its registration.
#

#
#  enter - start the gebPushes when leaving the Halted state for Active:
#
proc ::GebPushMgr::enter {from to} {
    if {($from eq "Halted") && ($to eq "Active")} {
        foreach gebPush $::GebPushMgr::encapsulations {
            $gebPush onBegin
        }
    }
        
}
##
# leave -  onEnd when the Active/Paused state -> Halted.

proc ::GebPushMgr::leave {from to} {
    if {($from in [list Active Paused]) && ($to eq "Halted")} {
        foreach gebPush $::GebPushMgr::encapsulations {
            $gebPush onEnd
        }
    }
}

proc ::GebPushMgr::attach {state} {
}


##
#   Register the bundle to the runstate machine
#
#  @param bundleName	name of bundle to insert this before
#
proc ::GebPushMgr::register {{bundleName {}}} {
	set sm [RunstateMachineSingleton %AUTO%]
	if {$bundleName ne {}} {
		$sm addCalloutBundle GebPushMgr $bundleName
	} else {
		$sm addCalloutBundle GebPushMgr
	}
	$sm destroy
}
