#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Giordano Cerriza
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321


##
# @file gebpushProvider.tcl
# @brief Data source provider for gebPush processes.  Includes a prompter
# @author Ron Fox <fox@nscl.msu.edu>
#

##
#  This package provides a data source provider that lets you easily
#  add gebPush programs for an experiemnt at the GUI (very little
#  overhead/knowledge required in ReadoutCallouts.tcl).
#
# GEBClient_Prompter provides a companion prompter for interactively
# and visually prompting for the parameters of a gebPush instance.
#
#  We really just build and maintain a set of GebPush objects.
#  One for each "data source"
#
# @note this code must run on a dual homed system because
#        we need access to the Gretina network.
#
# @note We don't register the GEBPushMgr bundle because it does not
#       handle the possibility that the GEB migrates from run to run.
#
#  Finally these data sources should be registered early as
#  otherwise the gebpush associated with a read readout program
#  may not be established before the actual readout starts tossing data
#  around.
#
package provide GEBClient_Provider 1.0
package require GEBClient_Prompter
package require gebpushmgr
package require ssh;		# To make the ringbuffer.


#  provider variable:

namespace eval GEBClient {
    variable instances;             # GebPush objects indexed by source id.
    array set instances [list]
}


##
# GEBClient::parameters
#    @return dict - the parameterization dict held by the promper.
#
proc GEBClient::parameters {} {
    return $::GEBClient::parameterization
}
##
# start
#    Starts a data source.  This consists of
#    - Createing a GEBPush and configuring it as indicate by the
#      parameterization.
#    - adding that source to the instances array
#
# @param params - The parameterization dict. a sourceid
#                  key has been added to the dict to tell us the
#                  source id of the source being started.
#
proc GEBClient::start params {
    set id [dict get $params sourceid]
    set client [GebPush %AUTO%]
    
    # Configure all non empty parameters:


    foreach name [array names GEBClient::parameterMap] {
        set option $GEBClient::parameterMap($name)
        set value [lindex [dict get $params $name] end] 
        if {$value ne ""} {
            $client configure $option $value
        }
    }
    
    
    set GEBClient::instances($id) $client

    #  Create the ring buffer (if needed) note it's remote in tcp://host/ringname

    set ringUri [dict get $params DataSourceRingURI]
    set uriParts [split $ringUri :/];  # tcp {}  {} host ring
    set ringHost [lindex $uriParts 3]
    set ringName [lindex $uriParts 4]

    #  Assume that DAQBIN is the same in both systems.

    puts "Making ring $ringName in host $ringHost"

    set command [list [file join $::env(DAQBIN) ringbuffer] create $ringName]
    puts "Executing '$command' in $ringHost"
    ssh::ssh $ringHost $command
    
    catch {exec killall pushToGEB};               # Ensure there are none to begin with.
    
}
##
# check
#    @param id - the id of the source to check
#    @return 1 - since the processes are not persistent, this
#                source is always considered to be running.
#
proc GEBClient::check id {
    if {[array names GEBClient::instances $id] eq ""} {
        error "GEBClient  provider - no such instance $id"
    }
    return 1
}
##
# GEBClient::stop
#   @param id - the id of the source to stop.
#     We just destroy the driver object and remove it from
#     the instances array.
#
proc GEBClient::stop id {
    if {[array names GEBClient::instances $id] eq ""} {
        error "GEBClient  provider - no such instance $id"
    }
    set driver $GEBClient::instances($id)
    array unset GEBClient::instances $id
    $driver destroy
}
##
# begin
#    Begin for a data source:
#    - Get the GEB node and port
#    - Find the driver in the instances list
#    - Configure it's current -gebnode and -gebport options.
#    - Invoke it's onBegin method.
#
# @param id - the id of the source to begin.
# @param run - the run number (ignored -comes from GRETINA).
# @param title - the run title (ignored)
#
proc GEBClient::begin {id run title} {

    
    if {[array names GEBClient::instances $id] eq ""} {
        error "GEBClient  provider - no such instance $id"
    }
    set driver $GEBClient::instances($id)
    
    set host [exec [file join $GEBClient::gretinabin gebnode]]
    set port [exec [file join $GEBClient::gretinabin gebport]]

    
    $driver configure -gebnode $host -gebport $port
    $driver onBegin
}
##
# pause/resume are not supported by this data source:

proc GEBClient::pause id {}
proc GEBClient::resume id {}


##
# end
#   End run.
#   - get the driver.
#   - invoke its OnEnd method.
#
# @param id - id of the object.
#
proc GEBClient::end id {
    catch {

      if {[array names GEBClient::instances $id] eq ""} {
        error "GEBClient  provider - no such instance $id"
    }
    set driver $GEBClient::instances($id)
    
	$driver onEnd
    } msg

}
##
# init
#   If there are any gebpush processes alive, kill them all
#   The assumption is that this gets called when the run is halted.,
proc GEBClient::init id {
    catch {exec killall pushToGEB}
}

##
# capablities
#    @return dict - capabilities dict  the key/values are:
#                   - canPause - false - Gretina does not support pausing.
#                   - rusHaveTitles - true Don't disable global run titling.
#                   - runsHaveNumber - true Don't disable global run numbering.
#
proc GEBClient::capabilities {} {
    return [dict create                                \
		canPause   false                              \
		runsHaveTitles true                           \
		runsHaveNumbers true                            \
	       ]
    
}
