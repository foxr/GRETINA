#
#  Sample Readout Callouts for gretina.
#
#
package require GretinaCallouts

#
#   This proc is called by the readout GUI when a run is about to begin.
#  Parameters:
#     runNumber - The run number of the run about to start.
#
proc OnBegin runNumber {
    GretinaCallouts OnBegin

    # Disable our toggle.

    .gretinaControls.coupled config -state disabled

    # Insert experiment specific code here.
}

#
#  This proc is called when the readout GUI ennds a run.  Note that
#  Gretina is not asked to end by us but by the data pusher..when the
#  end run is finally received.  This ensures that the end run marker
#  makes it into the data stream.
# Parameters:
#   run   - Run number being ended.
#
proc OnEnd run {
    #Re-enable the toggle.

    .gretinaControls.coupled config -state normal
}

#
#  The code here adds a checkbox to the GUI that supprorts decoupling Gretina
#  from us.  The delay allows the GUI to be built before we add the checkbox.
#
after 100 addCouplingCheckbox

#
# Make the GUI and paste it below the existing one.

proc addCouplingCheckbox {} {

    set ::gretinaCoupled  1
    GretinaCallouts coupled; # start coupled.

    frame .gretinaControls 
    checkbutton .gretinaControls.coupled -variable gretinaCoupled \
	-command gretinaToggleCoupling -text {Couple to Gretina}
    grid .gretinaControls.coupled -sticky w
    grid .gretinaControls -sticky ew -columnspan 3

}

#  Process the checkbox:

proc gretinaToggleCoupling {} {
    if {$::gretinaCoupled} {
	GretinaCallouts coupled
    } else {
	GretinaCallouts standalone
    }
}

#
#   Gretina does not use the callouts for  OnPause, OnResume, and OnStart
#
#  Therefore you can just paste in experiment specific code here.



