#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2009.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Author:
#             Ron Fox
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321

#
#  This file contains code that should be pulled in by the ReadoutCallouts.tcl
#  file used by a gretina experiment:
#  - It provides GretinaCallouts OnBegin which should be invoked when a run
#    begins (e.g. from your OnBegin in ReadoutCallouts.
#    That function will request gretina start a run.
# - It provides a periodic function which:
#   - Permanently disables the "Pause" button as that operation has no meaning in 
#     Gretina.
#   - If the run is halted, disables the Begin button when Gretina is not in the
#     Setup state (the state from which runs can be started
# - It provides a GretinaCallouts standalone which indicates the system is running
#   standalone (without Gretina) (the timer functions are disabled)
#   In standalone mode, OnBegin is a no-op.
# - It provides a GretinaCallouts coupled which indicates the system is running coupled and
#   therefore the timer function is enabled
#
#  Note the system starts out in uncoupled mode.  It is up to the client to couple the system.
#

package provide GretinaCallouts 1.0
package require snit
package require GretinaControl
package require ReadoutState

snit::type GretinaCallouts {
    typevariable coupled 0;	# nonzero (true) if coupled mode on.
    typevariable timerid -1;	# If not -1, the timer id for the next scheduled AdjustButtons run.

    typeconstructor {

    }
    # Should be called when a run is starting.
    # If we are in coupled mode, attempt to start Gretina.
    #
    typemethod OnBegin {} {
	if {$coupled} {
	    GretinaControl start
	    .pauseres configure -state disabled
	}
    }
    #
    # Called to enter standalone mode.  
    # If we are coupled and there's a pending timer id we cancle it.
    # Regardless the state is set to uncoupled.
    #
    typemethod standalone {} {
	if {$coupled && ($timerid != -1)} {
	    after cancel $timerid
	    set timerid -1
	}
	set coupled 0
	.startstop configure -state normal

    }
    #
    # Called to enter coupled mode.
    # If we are not in coupled mode,
    # AdjustButtons is invoked in order to ensure that
    # the buttons have the right state and
    # that they periodically will.
    # regardlesss, coupled is set to 1.
    typemethod coupled {} {
	if {!$coupled} {
	    GretinaCallouts AdjustButtons
	}
	set coupled 1
    }
    typemethod AdjustButtons {} {
	set timerid [after 1000 {GretinaCallouts AdjustButtons}]; # reschedule

	# Adjust the Left and Right buttons.
	#  The right button is always disabled.

	.pauseres configure -state disabled

	# The left button state depends on our state and Gretina's.

	if {([ReadoutControl::getReadoutState] ne "Active") && 
	    ([GretinaControl getStatus] ne "Setup")} {
	    # Ghost start button.
	    
	    .startstop configure -state disabled

	} else {
	    # Enable start button:

	    .startstop configure -state normal
	}

    }

}

