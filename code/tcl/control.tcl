#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2009.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Author:
#             Ron Fox
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



#  This file contains a simple run control package for Tcl.
#  The code allows you to determine what the current state of 
#  Gretina is as well as to turn on/off run (assuming Gretina is
#  in a state that allows taht to happen.
#

package provide GretinaControl 1.0
package require snit
package require epics

#
#  The snit type below encapsulates everything we need.
#  The assumption is that this software is being used in
#  the context of an event loop.
#

snit::type GretinaControl {
    typevariable status 0

    #
    #  Type construction creates the channel objects
    #  and binds the state to the state typevariable.
    #
    typeconstructor {
	epicschannel Online_CS_StartStop
	epicschannel Cluster_CV_State
	Cluster_CV_State link [mytypevar status]
    }
    # The method below Returns the textual version of the status:
    # This is an automatic consequence of the fact that the
    # Cluster_CV_State is an enumerated type.
    #
    typemethod getStatus {} {
	return $status
    }

    #
    #  Attempt to start the run.
    #  The only way you will know if this is successful is if
    #  the status changes.
    typemethod start {} {
	Online_CS_StartStop set 1
    }
    #
    #  Attempt to stop the run.
    #  The only way you will know if this is successful is if
    #  the status changes.
    #
    typemethod stop {} {
	Online_CS_StartStop set 0
    }
    
}