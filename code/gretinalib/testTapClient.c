
#include <stdio.h>
#include <stdlib.h>
#include <GEBLink.h>
#include "gretTapClient.h"

/*
 * gretTapTest <ip addr> <position> <type>
 */

int main(int argc, char **argv) {

   int position, type, amt, i, times, j;
   float timeout;
   struct gretTap *tap;
   struct gebData *datain;

   if (argc != 7) {
      printf("Usage: %s <ip addr> <position> <type> <amt> <timeout> <times>\n",
                   argv[0]);
      return 1;
   }
   position = strtol(argv[2], 0, 0);
   type = strtol(argv[3], 0, 0);
   amt = strtol(argv[4], 0, 0);
   timeout = strtod(argv[5], 0);
   times = strtod(argv[6], 0);

   tap = gretTapConnect(argv[1], position, type);

   if (tap) {
       printf ("connection to %s position %d type %d succeeded.\n", argv[1],
                 position, type);
   } else {
       printf ("connection to %s position %d type %d failed reason %s\n", 
                argv[1], position, type, 
                gretTapClientErrorStrings[gretTapClientError]);
      return 2;
   }
   j = 0;
   while (gretTapClientError != GTC_CLOSED && (times == 0 || j < times)) {
      j++;
      datain = gretTapData(tap, amt, timeout);

      if (!datain) {
         printf("gretTapData with amt %d and timeout %f failed reason %s\n", 
                 amt, timeout , gretTapClientErrorStrings[gretTapClientError]);
      } else {
         printf("actually got something...\n");
         if (gretTapClientError != GTC_NOERROR) {
            printf("But got [soft] error %s\n", 
                   gretTapClientErrorStrings[gretTapClientError]);
         }
         i = 1;
         while (datain) {
            printf("%d: type: %d length: %d data: %p\n", i, datain->type, 
                   datain->length, datain->payload);
            datain = datain->next;
            i++;
         }
      }
   }
   gretTapClose(tap);
   return 0;
}
   
