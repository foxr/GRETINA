/**
 *  This is a faked GEB consumer.
 *  It tries to receive data as quickly as possible from  a GEB data
 *  contributor.  Purpose is to understand the rate at which pushToGEB can'
 *  submit data.
 */

#include "GEBLink.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>


static dataBuffer[1024*1024*8];       /* 8Mbyte buffer should hold all messages */

void exitError(const char* msg)
{
    perror(msg);
    exit(EXIT_FAILURE);
}

/**
 * connectToClient
 *    -  Advertises as a server on the port described.
 *    -  Accepts the first connection to the port.
 *    -  Closes down the service port.
 *
 * @param port  - IP port on which we advertise our service.
 * @return int  - The socket on which the data will be transferred.
 * 
 */
int connectToClient(int port)
{
    int listenSock;
    int dataSock;
    struct sockaddr_in serv_addr;
    
    /* Set up the listen on port */
    
    listenSock = socket(AF_INET, SOCK_STREAM,0);
    if (listenSock < 0) {
        exitError("Failed to open listen socket");
    }
    memset(&serv_addr, 0, sizeof(struct sockaddr_in));
    serv_addr.sin_family      = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port        = htons(port);
    if (bind(listenSock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        exitError("Bind failed");
    }
    
    /* accept the first connection: */
    
    if (listen(listenSock, 1) < 0) {
        exitError("listen failed");
    }
    dataSock = accept(listenSock, (struct sockaddr*)NULL, NULL);
    if (dataSock < 0) {
        exitError("Accept failed");
        
    }
    close(listenSock);
    return dataSock;
    
}

/**
 * goAhead
 *    Send the peer a GEB go-ahead message.   This is an int containing a 0.
 * @param fd   - file descriptor that carries data transfers:
 */
void goAhead(int fd)
{
    int data = 0;
    
    if (write(fd, &data, sizeof(int)) < 0) {
        exitError("Unable to send go-ahead");
    }
}
/**
 * getMessage
 *    We're going to assume the GEB needs two reads to do this, one for the
 *    header and one for the body, since the length of the body is in the header.
 *    We'll use the predefined buffer dataBuffer above for the body.
 *
 * @param fd   - File descriptor that carries the data.
 * @return int - if zero we had an error (most likely peer exited).
 */
int
getMessage(int fd)
{
    struct gebData header;
    int    nr;
    
    nr = read(fd, &header, GEB_HEADER_BYTES);
    if (nr != GEB_HEADER_BYTES) {
        return 0;                        
    }
    nr = 0;
    while (nr < header.length) {
        int status = read(fd, dataBuffer, header.length - nr);
        if (status <= 0) return 0;
        nr += status;
    }
    return 1;
}

/**
 * Imitates the receiver for GEB.  After a connection, the GEB sends an int
 * containing zero to accept the connection.  It also sends a 'go ahead' prior
 * to each message it receives that is also an int containing 0.
 * messages received are a GEBData Struct (payload is meaningless) followed by
 * 'length' bytes of data.
 */ 
void suckBits(int fd)
{
    goAhead(fd);
    
    while (1) {
        goAhead(fd);
        if (!getMessage(fd)) return;
    }
}

/**
 *  Usage:
 *     fakeGEB portnum
 *  Where:
 *     portnum is the number of the port on which we'll listen for the connection.
 *  NOTE:
 *     Only the first connection request is honored.  Once that sender
 *     drops its connection, we'll exit.
 */


int main(int argc, char** argv)
{
    int port;
    int fd;
    
    
    if (argc < 2) {
        fprintf(stderr, "Usage: \n");
        fprintf(stderr, "   fakeGEB portnum\n");
        fprintf(stderr, "Where\n");
        fprintf(stderr, "    portnum - the port listened to for connections\n");
        
        exit(EXIT_FAILURE);
    }
    port = atoi(argv[1]);
    
    fd = connectToClient(port);
    suckBits(fd);
    
    close(fd);
    
    exit(EXIT_SUCCESS);
}