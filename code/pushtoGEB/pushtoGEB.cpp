/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CApplication.h"
#include <stdlib.h>
#include <iostream>

/**
 ** The main entry point of the application.
 ** Just wraps a creation and invocation of an application
 ** object in exception handling blocks:
 */

int main(int argc, char** argv)
{
  try {
    CApplication app;
    return app(argc, argv);
  }
  catch(...) {
    std::cerr << "Unhandled exception caught at main level, probably something seriously wrong\n";
    return EXIT_FAILURE;
  }
  std::cerr << "pushToGEB exiting without an exception\n";
  return EXIT_SUCCESS;
}
