/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

#ifndef __CUSERCODE_H
#define __CUSERCODE_H

#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#define __STL_STRING
#endif
#endif

/**
 * This class encapslates a user code shared object that provides software that
 * extracts the timestamp from an event.  W
 */
class CUserCode
{
private:
  // private types:'

  typedef int       (*initFunction)();
  typedef long long (*extractFunction)(void*);

  // Object attributes.
private:
  void*             m_dlHandle;
  initFunction      m_initFunction;
  extractFunction   m_extractTimestamp;
  std::string       m_absolutePath;

  // Canonicals
public:
  CUserCode(std::string libPath);
  ~CUserCode();

  // Object methods:

public:
  bool initialize();
  long long timestamp(void* pEvent);
  
};


#endif
