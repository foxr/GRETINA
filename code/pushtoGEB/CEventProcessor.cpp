/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CEventProcessor.h"
#include "CUserCode.h"
#include <CRingItem.h>
#include <DataFormat.h>
#include <string>
#include <time.h>
#include <daqshm.h>

/* #include <GEBLink.h> */
extern "C" {
#include <GEBClient.h>
}
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <DataFormat10.h>
#include <set>
#include <sys/time.h>

static const unsigned NUM_BINS(1000);   // Number of bins in performance histogram.
static const std::string SHM_NAME("/gebPushPerformance");
/**
 * Construct the object
 * @param pUserCode  - Points to an encpapsulation of the .so that was loaded with
 *                     user code to fish the timestamp from each event.
 *                     Null if no user code was supplied.  We are going to try
 *                     to pull the timestamp from the body header.   if that does
 *                     not exist and user code is supplied we do that.
 *                     If the user code was  not supplied we fail.
 * @param controlType- type of fragment to emit if a control event has occured.
 * @param physicsType- Type of fragment to emit if a physics event has occured.
 * @param gebHost    - Name of the DNS host on which the GEB is running.
 * @param gebPort    - Port on which the GEB is accepting connections.
 * @param multiplier - Multipler for the timestamp.
 */
CEventProcessor::CEventProcessor(CUserCode*    pUserCode,
				 int           controlType,
				 int           physicsType,
				 int           tsScalerType,
				 std::string   gebHost,
				 int           gebPort,
                                 int           multiplier,
                                 bool          size32) :
  m_pUserCode(pUserCode),
  m_controlType(controlType),
  m_physicsType(physicsType),
  m_tsScalerType(tsScalerType),
  m_lastStamp(0),
  m_multiplier(multiplier),
  m_GEBHost(gebHost),
  m_GEBPort(gebPort),
  m_fSize32(size32),
  m_timingHistogram(0) 

{
  std::cout << "Connecting to " << gebHost << std::endl;
  m_GEB = GEBClientInit();
  if (!m_GEB) {
    throw std::string("GEBClientInit failed");
  }
  std::cout << "Connecting to " << gebHost << std::endl;

  while (setGEBClient(m_GEB, const_cast<char*>(gebHost.c_str()), gebPort) != 0) {
    sleep(1);			// Try again later.
  }
#ifdef INSTRUMENT_PERFORMANCE
  mapPerformanceHistograms();
  clearPerformanceHistograms();
#endif

}

/**
 * Destruction
 */
CEventProcessor::~CEventProcessor()
{
  closeGEBClient(m_GEB);
  free(m_GEB);
}

/**
 ** Deals with a ring item.  In this case we need to figure out the type, timestamp,
 ** the payload size and the payload and then ship the data
 ** off to the GEB.
 ** @param pItem - Pointer to an item we've just gotten from the ring.
 */
void 
CEventProcessor::operator()(CRingItem* pItem)
{
  uint32_t itemType     = pItem->type();
  size_t   payloadSize  = pItem->getBodySize();
  void*    payload      = pItem->getBodyPointer();

  // We need to transform scaler and control data into 10.0 versions
  // for the rest of this nonesense since users don't want any change in their
  // own paltry codes:
  
  void* pItem10;           // Dynamic size.
  pItem10 = 0;
  ::NSCLDAQ102::StateChangeItem stateChange10;
  
  long long timestamp(0);	// The if block below needs to compute an actual timestamp
  int       type;		// and fragment type.
  if (itemType == PHYSICS_EVENT) {
    if (pItem->hasBodyHeader()) {
        timestamp = pItem->getEventTimestamp();
    } else if(m_pUserCode) { 
        timestamp   = m_pUserCode->timestamp(payload);
        
    } else {
      throw std::string(
        "Event with no body header and the user did not supply an extractor lib."
      );
    }
    m_lastStamp = timestamp;
    type        = m_physicsType;

    // Make the initial size a long word (sigh)...if necessary:
    // Note the need to do a malloc/copy for m_fSize32 true because of the
    // free of pItem10 later on (double sigh).
    
    uint32_t* pNewBody;
    uint16_t* pOldBody = (uint16_t*)(payload);
    
    if (m_fSize32) {
      pNewBody = (uint32_t*)malloc(payloadSize);
      memcpy(pNewBody, pOldBody, payloadSize);
      payload = pNewBody;
      
    } else {
      pNewBody = (uint32_t*)(malloc(payloadSize + sizeof(uint16_t)));
      
      pNewBody[0] = (*pOldBody++) + 1; //  Long word word count.
      memcpy(&(pNewBody[1]), pOldBody, payloadSize - sizeof(uint16_t)); // Copy the body.
      payload     = pNewBody;
      payloadSize += sizeof(uint16_t);
    }
    pItem10 = pNewBody;
    

  } else if (itemType == PERIODIC_SCALERS) {
    pScalerItemBody pNewScaler = pScalerItemBody(payload);
    if (pNewScaler->s_isIncremental) {

      if (pItem->hasBodyHeader()) {
        timestamp = pItem->getEventTimestamp();
        m_lastStamp = timestamp;
      } else {
        timestamp = 0;
      }
      type      = m_tsScalerType;
      
      // Now transform to a 10.0 body/size:
      
      
      unsigned nScalers = pNewScaler->s_scalerCount;
      payloadSize       = sizeof(NSCLDAQ102::NonIncrTimestampedScaler) + sizeof(uint32_t)*(nScalers - 1);
      NSCLDAQ102::pNonIncrTimestampedScaler pOldScaler =
        NSCLDAQ102::pNonIncrTimestampedScaler(malloc(payloadSize));
    
      
      pOldScaler->s_eventTimestamp = timestamp;
      pOldScaler->s_intervalStartOffset = pNewScaler->s_intervalStartOffset;
      pOldScaler->s_intervalEndOffset   = pNewScaler->s_intervalEndOffset;
      pOldScaler->s_intervalDivisor     = pNewScaler->s_intervalDivisor;
      pOldScaler->s_clockTimestamp      = pNewScaler->s_timestamp;
      pOldScaler->s_scalerCount         = pNewScaler->s_scalerCount;
      memcpy(pOldScaler->s_scalers, pNewScaler->s_scalers, nScalers*sizeof(uint32_t));
      
      pOldScaler->s_header.s_size = payloadSize;
      pOldScaler->s_header.s_type = NSCLDAQ102::TIMESTAMPED_NONINCR_SCALERS;
    
      // payloadSize -= sizeof(::NSCLDAQ102::RingItemHeader);
      // payload = (uint8_t*)(pOldScaler) + sizeof(::NSCLDAQ102::RingItemHeader);
      payload = pOldScaler;
      pItem10 = (void*)pOldScaler;
    } else {
        return;                      // Only send non-incrementals.
    }
  }
  else if (isStateChange(itemType) ) {

    timestamp = 0;		// Carl L. says this should pass through.
    type      = m_controlType;
    
    // Per Dirk request make the BEGIN_RUN timestamp 1 because evidently the GEB
    // doesn't necessarily reset its idea of the timestamp when a run begins
    // so we can get a timestamp that's like the the time at which the last
    // run ended:
    
    if (itemType == BEGIN_RUN) {
      timestamp = 1;
    }
    
    // Users are expecting a 10.x statechange >sigh<
    
    pStateChangeItemBody pNewChange = pStateChangeItemBody(payload); // new style
    stateChange10.s_header.s_size = sizeof(stateChange10);
    stateChange10.s_header.s_type = pItem->type();
    stateChange10.s_runNumber     = pNewChange->s_runNumber;
    stateChange10.s_timeOffset    = pNewChange->s_timeOffset;
    stateChange10.s_Timestamp     = pNewChange->s_Timestamp;
    memcpy(
        stateChange10.s_title, pNewChange->s_title, TITLE_MAXSIZE+1
    );                  // assumes TITLE_MAXSIZE same in 10/11...and it is.
    
    
    payload         = &stateChange10;
    payloadSize     = sizeof(stateChange10);
  } else {
    return;             // Not passing items we don't understand.
  }
  timestamp  *= m_multiplier;               // Scale for gretina.
  
  send(type, payloadSize, timestamp, payload);
  
  free(pItem10);                    // Free 0 is ok.
}
/**
 ** Send a fragment to the GEB:
 ** @param type      - fragment type to use.
 ** @param size      - Size of the payload.
 ** @param timestamp - Timestamp to associated with the fragment.
 ** @param payload   - Fragment payload.
 */
void
CEventProcessor::send(int type, size_t size, long long timestamp, void* payload)
{
  gebData data = {
    type, size, timestamp, payload, 1,2,(struct gebData*)3
  };

  // Loop over trying to send until the status is not 1
  // if we need to retry output that message.


  int status;
  int retries = 0;
  do {
#ifdef INSTRUMENT_PERFORMANCE
    struct timespec t1;
    struct timespec t2;
    clock_gettime(CLOCK_REALTIME, &t1);
#endif
    status = sendGEBData(m_GEB, &data);
#ifdef INSTRUMENT_PERFORMANCE
    clock_gettime(CLOCK_REALTIME, &t2);
    histogramTiming(t1, t2);
#endif
    retries++;

    if (retries > 1) {
      std::cout << "Retried send to GEB " << retries << std::endl;
    }

    
    // if status is 2 need to reconnect to the GEB:
    
    if (status == 2) {
      std::cout << "GEB dropped its connection, reconnecting and retrying\n";
      closeGEBClient(m_GEB);
      if(setGEBClient(
        m_GEB, const_cast<char*>(m_GEBHost.c_str()), m_GEBPort) != 0) {
        std::cout << "GEB push not able to reconnect with the GEB -- will retry\n";
      }
    }
    
  } while (status != 0);	// status == 1 means soft error, retry.

  // For now hard failures are not recovered (status == 2).

  // For now assume we failed to send an event because the run ended.

  if (status != 0) {
    time_t now = time(NULL);
    std::cout << ctime(&now)
	      <<" - GEBPush - failed to send event to GEB: " 
	      << status << std::endl;
    exit(0);
  }
}
/**
 *  isStateChange
 *    Determines if an item is a state change or not.
 * @param type  Item type.
 * @return bool - true if item is a state change item type
 */
bool
CEventProcessor::isStateChange(int type)
{
  static std::set<int> controlTypes;
  if (controlTypes.size() == 0) {
    controlTypes.insert(BEGIN_RUN);
    controlTypes.insert(END_RUN);
  }
  return (controlTypes.count(type)  > 0);
}
/**
 * mapPerformanceHistograms
 *    If necessary create the shared memory region for theperf histograms.
 *    map if already exists.  The shm region will be called
 *    "/gebPushPerformance"
 *
 * @note sets m_timingHistogram with a pointer to the histogram region.
 * @note The size is NUM_BINS*sizeof(uint32_t) [at least].
 */
void
CEventProcessor::mapPerformanceHistograms()
{
  // Try to create the region. It's not an error if we create it and it
  // already exists:
  
  bool status = CDAQShm::create(
    SHM_NAME, NUM_BINS*sizeof(uint32_t),
    CDAQShm::GroupRead | CDAQShm::OtherRead
  );
  if(status && (CDAQShm::lastError() != CDAQShm::Exists)) {
    std::cerr
      << "GEBPUSH: Failed to create performance monitoring shared memory: "
      << CDAQShm::errorMessage(CDAQShm::lastError()) << std::endl;
      exit(EXIT_FAILURE);
  }
  // Map to the shared memory.  This is required to succeed:
  
  void* pHistograms = CDAQShm::attach(SHM_NAME);
  if (!pHistograms) {
    std::cerr
      << "GEBPUSH: Faile dto map to performance monitoring shared memory: "
      << CDAQShm::errorMessage(CDAQShm::lastError()) << std::endl;
      exit(EXIT_FAILURE);
  }
  m_timingHistogram = reinterpret_cast<uint32_t*>(pHistograms);
}
/**
 *  clearPerformanceHistograms
 *     Clear the values of the performance histograms.
 *     
 *  @note If we are not mapped (m_timningHistograms is zero) this is  no-op.
 */
void
CEventProcessor::clearPerformanceHistograms()
{
  if(m_timingHistogram) {
    memset(m_timingHistogram, 0, NUM_BINS*sizeof(uint32_t));   
  }
}
/**
 *  histogramTiming
 *     Given a before and after time -- histogram the time difference.
 *     The assumption is that bins are one millisecond long.
 *     - Convert the times to timevals.
 *     - Use timersub to get the difference.
 *     - Convert the difference to milliseconds.
 *     - If the resulting timing is past the last bin, put it in the last bin.
 *     
 *   @param begin - timespec& for the time at the start of the interval.
 *   @param end   - timespec& for the time at the end of the interval.
 *
 *   @note a segfault will very likely happen if this is called prior to
 *         mapping the shared memory.  If there's no segfault in that case
 *         still no good will come of this.
 */
void
CEventProcessor:: histogramTiming(struct timespec& begin, struct timespec& end)
{
  // Convert the timespecs to timevals:
  
  struct timeval b = {begin.tv_sec, begin.tv_nsec/1000};
  struct timeval e = {end.tv_sec,   end.tv_nsec/1000};
  
  // Get the difference:
  
  struct timeval dt;
  timersub(&b, &e, &dt);
  
  // Compute dt in integer, unrounded ms:
  
  uint64_t ms = dt.tv_usec/1000;     // usec -> msec.
  ms         += dt.tv_sec * 1000;  // sec -> msec.
  
  // histogram the result:
  
  if (ms >= NUM_BINS) ms = NUM_BINS-1;
  m_timingHistogram[ms]++;
}