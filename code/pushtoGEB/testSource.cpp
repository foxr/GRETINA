/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/


/**
 ** This beast just sends a run of data to a ring until control-C is hit. 
 */

#include <string>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <CRingBuffer.h>
#include <CRingStateChangeItem.h>
#include <time.h>
#include <string.h>


static int finish(0);
CRingBuffer* pRing;

long long timestamp = 1;	// physics event timestamp.


struct Event {
  long long     s_timestamp;
  unsigned long s_body[10];	// Counting pattern body.
};

/**
 ** Create or attach a ring buffer named after this user.
 */
static
void createOrAttachRing()
{
  std::string me = getlogin();
  if (!CRingBuffer::isRing(me)) {
    CRingBuffer::create(me);
    
  }
  pRing = new CRingBuffer(me, CRingBuffer::producer);
}


/**
 ** Send begin - send a begin run item to the ring.
 */
static void
sendBegin()
{
  CRingStateChangeItem item;
  item.commitToRing(*pRing);
}
/**
 ** Send end - send an end run item to the ring:
 **/
static void
sendEnd()
{
  CRingStateChangeItem item(END_RUN);
  item.commitToRing(*pRing);
}

/**
 ** Send a physics event.  The timestamp is incremented by a random amout between
 ** 2 and 12. The body of the event is a 10 element counting pattern.
 **
 */
static void
sendEvent()
{
  // figure out the new timestamp
  
  int delta  = static_cast<int>(10.0*drand48() + 2);
  timestamp += delta;

  struct Event event;
  event.s_timestamp = timestamp;
  for (int i =0; i < 10; i++) {
    event.s_body[i] = i;
  }
  CRingItem rEvent(PHYSICS_EVENT);
  unsigned char* pBody = reinterpret_cast<unsigned char*>(rEvent.getBodyCursor());
  memcpy(pBody, &event, sizeof(event));
  pBody += sizeof(event);
  rEvent.setBodyCursor(pBody);
  rEvent.updateSize();
  rEvent.commitToRing(*pRing);

}

/**
 ** Signal handler for SIGINT -- just increments the done flag.
 ** Sufficient bangs on the control-C key will cause an exit here
 ** if the normal exit does not happen first.
 */
static void interrupt(int ingored)
{
  finish++;
  if (finish > 5) {
    std::cerr << "Program not exiting after interrupt -- killing in handler\n";
    exit(EXIT_FAILURE);
  }
}

int main(int argc, char** argv)
{
  struct sigaction intr;
  intr.sa_handler = interrupt;
  __sigemptyset(&(intr.sa_mask));
  intr.sa_flags    = 0;


  int status = sigaction(SIGINT, &intr, NULL);
  if(status) {
    perror("Failed to establish signal handler");
  }

  // Main loop:
  //    begin run event,
  //    physics events
  //    when signalled, end run and exit.

  try {
    srand48(static_cast<long int>(time(NULL)));
    createOrAttachRing();
    sendBegin();
    
    while (finish == 0) {
      sendEvent();
      usleep(10000);		// 10ms between events is a bit more considerate of the world.

    }
    sendEnd();
    
  }
  catch (std::string msg) {
    std::cerr << msg << std::endl;
    exit(EXIT_FAILURE);
  }
  catch(...) {
    std::cerr << "Unexpected exception type caught" << std::endl;
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}
