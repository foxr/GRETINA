/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef __CEVENTPROCESSOR_H
#define __CEVENTPROCESSOR_H

#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#define __STL_STRING
#endif
#endif

#include <time.h>
#include <stdint.h>

class CUserCode;
class CRingItem;
struct gebClient;



/**
 ** This class is resposible for processing events from the NSCL DAQ ring buffer.
 ** It encapsulates the user code jacket object which allows us to figure out what
 ** the timestamp for an event is without having to hard code it into this software.
 */
class CEventProcessor 
{
  // Attributes:
private:
  CUserCode* m_pUserCode;
  int        m_controlType;
  int        m_physicsType;
  int        m_tsScalerType;
  long long  m_lastStamp;
  int        m_multiplier;
  gebClient* m_GEB;
  std::string m_GEBHost;
  int        m_GEBPort;
  uint32_t*  m_timingHistogram;
  bool       m_fSize32;
  // Canonicals:
public:
  CEventProcessor(CUserCode*  pUserCode,
		  int         controlType,
		  int         physicsType,
		  int         tsScalerType,
		  std::string gebHost,
		  int         gebPort,
		  int         multipler = 1,
		  bool        size32 = false);
  ~CEventProcessor();

  // Methods:

public:
  void operator()(CRingItem* pItem);
  void send(int type, size_t size, long long timestamp, void* payload);
private:
  bool isStateChange(int type);
  void mapPerformanceHistograms();
  void clearPerformanceHistograms();
  void histogramTiming(struct timespec& begin, struct timespec& end);

};

#endif
