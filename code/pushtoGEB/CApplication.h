/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#ifndef __CAPPLICATION_H
#define __CAPPLICATION_H

#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#define __STL_STRING
#endif
#endif

class CGretinaRunControl;
class CEventProcessor;
class CRingItem;
class CUserCode;
/**
 ** This class is the main application entry.  The main() instantiates one of these
 ** and invokes its operator()
 */
class CApplication
{
private:
  std::string         m_UserCode;         // name of user code .so.
  int                 m_controlType;	  // Type of item to use for control types.
  int                 m_physicsType;      // Type of item to use for physics types.
  int                 m_tsScalerType;	  /* Type of item for timestamped scalers. */
  std::string         m_GEBHost;          // Host running the global event builder.
  int                 m_GEBPort;          // Port on which the GEB listens for connections.
  std::string         m_ringURL;           // URL of ring buffer data source.
  int                 m_tsMultiplier;      // Multiplier for timestamp.
  CUserCode*          m_pUserCode;	  // User code object.
  CGretinaRunControl*        m_pRunControl;
  CEventProcessor*    m_pEventProcessor;
  bool                m_fSize32;           // Event size is 32 bits wide in source.

  /** Canonicals    */

public:
  CApplication();
  virtual ~CApplication();	// probably no need for virtual as this is most likely final.

  // Methods:

public:
  int operator()(int argc, char** argv);

  // Main utility methods:

protected:
  int         checkUsage(int argc, char** argv);
  CUserCode*  loadUserCode();
  void        mainLoop();
  void        createEventProcessor();
  void        destroyEventProcessor();
  void        preProcessEvent(CRingItem& item);
  void        postProcessEvent(CRingItem& item);
private:
  void        usage();

};


#endif
