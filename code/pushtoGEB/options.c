/*
  File autogenerated by gengetopt version 2.22.5
  generated with the following command:
  gengetopt --input=options.ggo --file-name=options 

  The developers of gengetopt consider the fixed text that goes in all
  gengetopt output files to be in the public domain:
  we make no copyright claims on it.
*/

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef FIX_UNUSED
#define FIX_UNUSED(X) (void) (X) /* avoid warnings for unused params */
#endif

#include <getopt.h>

#include "options.h"

const char *gengetopt_args_info_purpose = "Push data from NSCLDAQ ring buffer to Gretina GEB";

const char *gengetopt_args_info_usage = "Usage: pushtoGEB [OPTIONS]...";

const char *gengetopt_args_info_description = "";

const char *gengetopt_args_info_help[] = {
  "      --help              Print help and exit",
  "  -V, --version           Print version and exit",
  "  -u, --usercode=STRING   Path to user's timestamp extraction .so  (default=`')",
  "  -c, --ctltype=INT       Control event type  (default=`6')",
  "  -e, --phystype=INT      Physics event type  (default=`5')",
  "  -t, --tsscalertype=INT  Timestamped scaler event type  (default=`10')",
  "  -h, --gebhost=STRING    GEB Host DNS name",
  "  -p, --gebport=INT       GEB Port number  (default=`9005')",
  "  -s, --source=STRING     Source Ring URL",
  "  -m, --tsmultiplier=INT  Time stamp multipler  (default=`1')",
  "  -S, --no-size16         Event size is not 16 bits",
    0
};

typedef enum {ARG_NO
  , ARG_STRING
  , ARG_INT
} cmdline_parser_arg_type;

static
void clear_given (struct gengetopt_args_info *args_info);
static
void clear_args (struct gengetopt_args_info *args_info);

static int
cmdline_parser_internal (int argc, char **argv, struct gengetopt_args_info *args_info,
                        struct cmdline_parser_params *params, const char *additional_error);

static int
cmdline_parser_required2 (struct gengetopt_args_info *args_info, const char *prog_name, const char *additional_error);

static char *
gengetopt_strdup (const char *s);

static
void clear_given (struct gengetopt_args_info *args_info)
{
  args_info->help_given = 0 ;
  args_info->version_given = 0 ;
  args_info->usercode_given = 0 ;
  args_info->ctltype_given = 0 ;
  args_info->phystype_given = 0 ;
  args_info->tsscalertype_given = 0 ;
  args_info->gebhost_given = 0 ;
  args_info->gebport_given = 0 ;
  args_info->source_given = 0 ;
  args_info->tsmultiplier_given = 0 ;
  args_info->no_size16_given = 0 ;
}

static
void clear_args (struct gengetopt_args_info *args_info)
{
  FIX_UNUSED (args_info);
  args_info->usercode_arg = gengetopt_strdup ("");
  args_info->usercode_orig = NULL;
  args_info->ctltype_arg = 6;
  args_info->ctltype_orig = NULL;
  args_info->phystype_arg = 5;
  args_info->phystype_orig = NULL;
  args_info->tsscalertype_arg = 10;
  args_info->tsscalertype_orig = NULL;
  args_info->gebhost_arg = NULL;
  args_info->gebhost_orig = NULL;
  args_info->gebport_arg = 9005;
  args_info->gebport_orig = NULL;
  args_info->source_arg = NULL;
  args_info->source_orig = NULL;
  args_info->tsmultiplier_arg = 1;
  args_info->tsmultiplier_orig = NULL;
  
}

static
void init_args_info(struct gengetopt_args_info *args_info)
{


  args_info->help_help = gengetopt_args_info_help[0] ;
  args_info->version_help = gengetopt_args_info_help[1] ;
  args_info->usercode_help = gengetopt_args_info_help[2] ;
  args_info->ctltype_help = gengetopt_args_info_help[3] ;
  args_info->phystype_help = gengetopt_args_info_help[4] ;
  args_info->tsscalertype_help = gengetopt_args_info_help[5] ;
  args_info->gebhost_help = gengetopt_args_info_help[6] ;
  args_info->gebport_help = gengetopt_args_info_help[7] ;
  args_info->source_help = gengetopt_args_info_help[8] ;
  args_info->tsmultiplier_help = gengetopt_args_info_help[9] ;
  args_info->no_size16_help = gengetopt_args_info_help[10] ;
  
}

void
cmdline_parser_print_version (void)
{
  printf ("%s %s\n",
     (strlen(CMDLINE_PARSER_PACKAGE_NAME) ? CMDLINE_PARSER_PACKAGE_NAME : CMDLINE_PARSER_PACKAGE),
     CMDLINE_PARSER_VERSION);
}

static void print_help_common(void) {
  cmdline_parser_print_version ();

  if (strlen(gengetopt_args_info_purpose) > 0)
    printf("\n%s\n", gengetopt_args_info_purpose);

  if (strlen(gengetopt_args_info_usage) > 0)
    printf("\n%s\n", gengetopt_args_info_usage);

  printf("\n");

  if (strlen(gengetopt_args_info_description) > 0)
    printf("%s\n\n", gengetopt_args_info_description);
}

void
cmdline_parser_print_help (void)
{
  int i = 0;
  print_help_common();
  while (gengetopt_args_info_help[i])
    printf("%s\n", gengetopt_args_info_help[i++]);
}

void
cmdline_parser_init (struct gengetopt_args_info *args_info)
{
  clear_given (args_info);
  clear_args (args_info);
  init_args_info (args_info);
}

void
cmdline_parser_params_init(struct cmdline_parser_params *params)
{
  if (params)
    { 
      params->override = 0;
      params->initialize = 1;
      params->check_required = 1;
      params->check_ambiguity = 0;
      params->print_errors = 1;
    }
}

struct cmdline_parser_params *
cmdline_parser_params_create(void)
{
  struct cmdline_parser_params *params = 
    (struct cmdline_parser_params *)malloc(sizeof(struct cmdline_parser_params));
  cmdline_parser_params_init(params);  
  return params;
}

static void
free_string_field (char **s)
{
  if (*s)
    {
      free (*s);
      *s = 0;
    }
}


static void
cmdline_parser_release (struct gengetopt_args_info *args_info)
{

  free_string_field (&(args_info->usercode_arg));
  free_string_field (&(args_info->usercode_orig));
  free_string_field (&(args_info->ctltype_orig));
  free_string_field (&(args_info->phystype_orig));
  free_string_field (&(args_info->tsscalertype_orig));
  free_string_field (&(args_info->gebhost_arg));
  free_string_field (&(args_info->gebhost_orig));
  free_string_field (&(args_info->gebport_orig));
  free_string_field (&(args_info->source_arg));
  free_string_field (&(args_info->source_orig));
  free_string_field (&(args_info->tsmultiplier_orig));
  
  

  clear_given (args_info);
}


static void
write_into_file(FILE *outfile, const char *opt, const char *arg, const char *values[])
{
  FIX_UNUSED (values);
  if (arg) {
    fprintf(outfile, "%s=\"%s\"\n", opt, arg);
  } else {
    fprintf(outfile, "%s\n", opt);
  }
}


int
cmdline_parser_dump(FILE *outfile, struct gengetopt_args_info *args_info)
{
  int i = 0;

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot dump options to stream\n", CMDLINE_PARSER_PACKAGE);
      return EXIT_FAILURE;
    }

  if (args_info->help_given)
    write_into_file(outfile, "help", 0, 0 );
  if (args_info->version_given)
    write_into_file(outfile, "version", 0, 0 );
  if (args_info->usercode_given)
    write_into_file(outfile, "usercode", args_info->usercode_orig, 0);
  if (args_info->ctltype_given)
    write_into_file(outfile, "ctltype", args_info->ctltype_orig, 0);
  if (args_info->phystype_given)
    write_into_file(outfile, "phystype", args_info->phystype_orig, 0);
  if (args_info->tsscalertype_given)
    write_into_file(outfile, "tsscalertype", args_info->tsscalertype_orig, 0);
  if (args_info->gebhost_given)
    write_into_file(outfile, "gebhost", args_info->gebhost_orig, 0);
  if (args_info->gebport_given)
    write_into_file(outfile, "gebport", args_info->gebport_orig, 0);
  if (args_info->source_given)
    write_into_file(outfile, "source", args_info->source_orig, 0);
  if (args_info->tsmultiplier_given)
    write_into_file(outfile, "tsmultiplier", args_info->tsmultiplier_orig, 0);
  if (args_info->no_size16_given)
    write_into_file(outfile, "no-size16", 0, 0 );
  

  i = EXIT_SUCCESS;
  return i;
}

int
cmdline_parser_file_save(const char *filename, struct gengetopt_args_info *args_info)
{
  FILE *outfile;
  int i = 0;

  outfile = fopen(filename, "w");

  if (!outfile)
    {
      fprintf (stderr, "%s: cannot open file for writing: %s\n", CMDLINE_PARSER_PACKAGE, filename);
      return EXIT_FAILURE;
    }

  i = cmdline_parser_dump(outfile, args_info);
  fclose (outfile);

  return i;
}

void
cmdline_parser_free (struct gengetopt_args_info *args_info)
{
  cmdline_parser_release (args_info);
}

/** @brief replacement of strdup, which is not standard */
char *
gengetopt_strdup (const char *s)
{
  char *result = 0;
  if (!s)
    return result;

  result = (char*)malloc(strlen(s) + 1);
  if (result == (char*)0)
    return (char*)0;
  strcpy(result, s);
  return result;
}

int
cmdline_parser (int argc, char **argv, struct gengetopt_args_info *args_info)
{
  return cmdline_parser2 (argc, argv, args_info, 0, 1, 1);
}

int
cmdline_parser_ext (int argc, char **argv, struct gengetopt_args_info *args_info,
                   struct cmdline_parser_params *params)
{
  int result;
  result = cmdline_parser_internal (argc, argv, args_info, params, 0);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser2 (int argc, char **argv, struct gengetopt_args_info *args_info, int override, int initialize, int check_required)
{
  int result;
  struct cmdline_parser_params params;
  
  params.override = override;
  params.initialize = initialize;
  params.check_required = check_required;
  params.check_ambiguity = 0;
  params.print_errors = 1;

  result = cmdline_parser_internal (argc, argv, args_info, &params, 0);

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser_required (struct gengetopt_args_info *args_info, const char *prog_name)
{
  int result = EXIT_SUCCESS;

  if (cmdline_parser_required2(args_info, prog_name, 0) > 0)
    result = EXIT_FAILURE;

  if (result == EXIT_FAILURE)
    {
      cmdline_parser_free (args_info);
      exit (EXIT_FAILURE);
    }
  
  return result;
}

int
cmdline_parser_required2 (struct gengetopt_args_info *args_info, const char *prog_name, const char *additional_error)
{
  int error = 0;
  FIX_UNUSED (additional_error);

  /* checks for required options */
  if (! args_info->gebhost_given)
    {
      fprintf (stderr, "%s: '--gebhost' ('-h') option required%s\n", prog_name, (additional_error ? additional_error : ""));
      error = 1;
    }
  
  if (! args_info->source_given)
    {
      fprintf (stderr, "%s: '--source' ('-s') option required%s\n", prog_name, (additional_error ? additional_error : ""));
      error = 1;
    }
  
  
  /* checks for dependences among options */

  return error;
}


static char *package_name = 0;

/**
 * @brief updates an option
 * @param field the generic pointer to the field to update
 * @param orig_field the pointer to the orig field
 * @param field_given the pointer to the number of occurrence of this option
 * @param prev_given the pointer to the number of occurrence already seen
 * @param value the argument for this option (if null no arg was specified)
 * @param possible_values the possible values for this option (if specified)
 * @param default_value the default value (in case the option only accepts fixed values)
 * @param arg_type the type of this option
 * @param check_ambiguity @see cmdline_parser_params.check_ambiguity
 * @param override @see cmdline_parser_params.override
 * @param no_free whether to free a possible previous value
 * @param multiple_option whether this is a multiple option
 * @param long_opt the corresponding long option
 * @param short_opt the corresponding short option (or '-' if none)
 * @param additional_error possible further error specification
 */
static
int update_arg(void *field, char **orig_field,
               unsigned int *field_given, unsigned int *prev_given, 
               char *value, const char *possible_values[],
               const char *default_value,
               cmdline_parser_arg_type arg_type,
               int check_ambiguity, int override,
               int no_free, int multiple_option,
               const char *long_opt, char short_opt,
               const char *additional_error)
{
  char *stop_char = 0;
  const char *val = value;
  int found;
  char **string_field;
  FIX_UNUSED (field);

  stop_char = 0;
  found = 0;

  if (!multiple_option && prev_given && (*prev_given || (check_ambiguity && *field_given)))
    {
      if (short_opt != '-')
        fprintf (stderr, "%s: `--%s' (`-%c') option given more than once%s\n", 
               package_name, long_opt, short_opt,
               (additional_error ? additional_error : ""));
      else
        fprintf (stderr, "%s: `--%s' option given more than once%s\n", 
               package_name, long_opt,
               (additional_error ? additional_error : ""));
      return 1; /* failure */
    }

  FIX_UNUSED (default_value);
    
  if (field_given && *field_given && ! override)
    return 0;
  if (prev_given)
    (*prev_given)++;
  if (field_given)
    (*field_given)++;
  if (possible_values)
    val = possible_values[found];

  switch(arg_type) {
  case ARG_INT:
    if (val) *((int *)field) = strtol (val, &stop_char, 0);
    break;
  case ARG_STRING:
    if (val) {
      string_field = (char **)field;
      if (!no_free && *string_field)
        free (*string_field); /* free previous string */
      *string_field = gengetopt_strdup (val);
    }
    break;
  default:
    break;
  };

  /* check numeric conversion */
  switch(arg_type) {
  case ARG_INT:
    if (val && !(stop_char && *stop_char == '\0')) {
      fprintf(stderr, "%s: invalid numeric value: %s\n", package_name, val);
      return 1; /* failure */
    }
    break;
  default:
    ;
  };

  /* store the original value */
  switch(arg_type) {
  case ARG_NO:
    break;
  default:
    if (value && orig_field) {
      if (no_free) {
        *orig_field = value;
      } else {
        if (*orig_field)
          free (*orig_field); /* free previous string */
        *orig_field = gengetopt_strdup (value);
      }
    }
  };

  return 0; /* OK */
}


int
cmdline_parser_internal (
  int argc, char **argv, struct gengetopt_args_info *args_info,
                        struct cmdline_parser_params *params, const char *additional_error)
{
  int c;	/* Character of the parsed option.  */

  int error = 0;
  struct gengetopt_args_info local_args_info;
  
  int override;
  int initialize;
  int check_required;
  int check_ambiguity;
  
  package_name = argv[0];
  
  override = params->override;
  initialize = params->initialize;
  check_required = params->check_required;
  check_ambiguity = params->check_ambiguity;

  if (initialize)
    cmdline_parser_init (args_info);

  cmdline_parser_init (&local_args_info);

  optarg = 0;
  optind = 0;
  opterr = params->print_errors;
  optopt = '?';

  while (1)
    {
      int option_index = 0;

      static struct option long_options[] = {
        { "help",	0, NULL, 0 },
        { "version",	0, NULL, 'V' },
        { "usercode",	1, NULL, 'u' },
        { "ctltype",	1, NULL, 'c' },
        { "phystype",	1, NULL, 'e' },
        { "tsscalertype",	1, NULL, 't' },
        { "gebhost",	1, NULL, 'h' },
        { "gebport",	1, NULL, 'p' },
        { "source",	1, NULL, 's' },
        { "tsmultiplier",	1, NULL, 'm' },
        { "no-size16",	0, NULL, 'S' },
        { 0,  0, 0, 0 }
      };

      c = getopt_long (argc, argv, "Vu:c:e:t:h:p:s:m:S", long_options, &option_index);

      if (c == -1) break;	/* Exit from `while (1)' loop.  */

      switch (c)
        {
        case 'V':	/* Print version and exit.  */
          cmdline_parser_print_version ();
          cmdline_parser_free (&local_args_info);
          exit (EXIT_SUCCESS);

        case 'u':	/* Path to user's timestamp extraction .so.  */
        
        
          if (update_arg( (void *)&(args_info->usercode_arg), 
               &(args_info->usercode_orig), &(args_info->usercode_given),
              &(local_args_info.usercode_given), optarg, 0, "", ARG_STRING,
              check_ambiguity, override, 0, 0,
              "usercode", 'u',
              additional_error))
            goto failure;
        
          break;
        case 'c':	/* Control event type.  */
        
        
          if (update_arg( (void *)&(args_info->ctltype_arg), 
               &(args_info->ctltype_orig), &(args_info->ctltype_given),
              &(local_args_info.ctltype_given), optarg, 0, "6", ARG_INT,
              check_ambiguity, override, 0, 0,
              "ctltype", 'c',
              additional_error))
            goto failure;
        
          break;
        case 'e':	/* Physics event type.  */
        
        
          if (update_arg( (void *)&(args_info->phystype_arg), 
               &(args_info->phystype_orig), &(args_info->phystype_given),
              &(local_args_info.phystype_given), optarg, 0, "5", ARG_INT,
              check_ambiguity, override, 0, 0,
              "phystype", 'e',
              additional_error))
            goto failure;
        
          break;
        case 't':	/* Timestamped scaler event type.  */
        
        
          if (update_arg( (void *)&(args_info->tsscalertype_arg), 
               &(args_info->tsscalertype_orig), &(args_info->tsscalertype_given),
              &(local_args_info.tsscalertype_given), optarg, 0, "10", ARG_INT,
              check_ambiguity, override, 0, 0,
              "tsscalertype", 't',
              additional_error))
            goto failure;
        
          break;
        case 'h':	/* GEB Host DNS name.  */
        
        
          if (update_arg( (void *)&(args_info->gebhost_arg), 
               &(args_info->gebhost_orig), &(args_info->gebhost_given),
              &(local_args_info.gebhost_given), optarg, 0, 0, ARG_STRING,
              check_ambiguity, override, 0, 0,
              "gebhost", 'h',
              additional_error))
            goto failure;
        
          break;
        case 'p':	/* GEB Port number.  */
        
        
          if (update_arg( (void *)&(args_info->gebport_arg), 
               &(args_info->gebport_orig), &(args_info->gebport_given),
              &(local_args_info.gebport_given), optarg, 0, "9005", ARG_INT,
              check_ambiguity, override, 0, 0,
              "gebport", 'p',
              additional_error))
            goto failure;
        
          break;
        case 's':	/* Source Ring URL.  */
        
        
          if (update_arg( (void *)&(args_info->source_arg), 
               &(args_info->source_orig), &(args_info->source_given),
              &(local_args_info.source_given), optarg, 0, 0, ARG_STRING,
              check_ambiguity, override, 0, 0,
              "source", 's',
              additional_error))
            goto failure;
        
          break;
        case 'm':	/* Time stamp multipler.  */
        
        
          if (update_arg( (void *)&(args_info->tsmultiplier_arg), 
               &(args_info->tsmultiplier_orig), &(args_info->tsmultiplier_given),
              &(local_args_info.tsmultiplier_given), optarg, 0, "1", ARG_INT,
              check_ambiguity, override, 0, 0,
              "tsmultiplier", 'm',
              additional_error))
            goto failure;
        
          break;
        case 'S':	/* Event size is not 16 bits.  */
        
        
          if (update_arg( 0 , 
               0 , &(args_info->no_size16_given),
              &(local_args_info.no_size16_given), optarg, 0, 0, ARG_NO,
              check_ambiguity, override, 0, 0,
              "no-size16", 'S',
              additional_error))
            goto failure;
        
          break;

        case 0:	/* Long option with no short option */
          if (strcmp (long_options[option_index].name, "help") == 0) {
            cmdline_parser_print_help ();
            cmdline_parser_free (&local_args_info);
            exit (EXIT_SUCCESS);
          }

        case '?':	/* Invalid option.  */
          /* `getopt_long' already printed an error message.  */
          goto failure;

        default:	/* bug: option not considered.  */
          fprintf (stderr, "%s: option unknown: %c%s\n", CMDLINE_PARSER_PACKAGE, c, (additional_error ? additional_error : ""));
          abort ();
        } /* switch */
    } /* while */



  if (check_required)
    {
      error += cmdline_parser_required2 (args_info, argv[0], additional_error);
    }

  cmdline_parser_release (&local_args_info);

  if ( error )
    return (EXIT_FAILURE);

  return 0;

failure:
  
  cmdline_parser_release (&local_args_info);
  return (EXIT_FAILURE);
}
