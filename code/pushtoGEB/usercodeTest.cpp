#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <string>
#include <iostream>
#include "CUserCode.h"

using namespace std;

int main(int argc, char** argv)
{
  CppUnit::TextUi::TestRunner   
               runner; // Control tests.
  CppUnit::TestFactoryRegistry& 
               registry(CppUnit::TestFactoryRegistry::getRegistry());

  runner.addTest(registry.makeTest());

  bool wasSucessful;
  try {
    wasSucessful = runner.run("",false);
  } 
  catch(string& rFailure) {
    cerr << "Caught a string exception from test suites.: \n";
    cerr << rFailure << endl;
    wasSucessful = false;
  }
  return !wasSucessful;
}


// Template for a test suite.

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/Asserter.h>
#include "Asserts.h"



class userCodeTests : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(userCodeTests);
  CPPUNIT_TEST(testNoSuch);
  CPPUNIT_TEST(testLoad);
  CPPUNIT_TEST(testInit);
  CPPUNIT_TEST(testTimestamp);
  CPPUNIT_TEST_SUITE_END();


private:

public:
  void setUp() {
  }
  void tearDown() {
  }
protected:
  void testNoSuch();
  void testLoad();
  void testInit();
  void testTimestamp();
};

CPPUNIT_TEST_SUITE_REGISTRATION(userCodeTests);

void userCodeTests::testNoSuch() {
  bool threw= false;


  try {
    CUserCode Code(std::string("nosuchfile.so"));
  }
  catch(...) {
    threw=true;
  }
  ASSERT(threw);

}

void userCodeTests::testLoad()
{
  bool threw= false;
  std::string  path = "testUsercode.so";
  try {
    CUserCode Code(path);
  }
  catch(...) {
    threw=true;
  }
  ASSERT(!threw);
  
}


void userCodeTests::testInit()
{
  CUserCode code("testUsercode.so");
  ASSERT(code.initialize());	// first one should be true.
  ASSERT(!code.initialize());
}

void userCodeTests::testTimestamp()
{
  CUserCode code("testUsercode.so");
  EQ((long long)0, code.timestamp(NULL)); 
  EQ((long long)1, code.timestamp(NULL));
  EQ((long long)2, code.timestamp(NULL));
}
