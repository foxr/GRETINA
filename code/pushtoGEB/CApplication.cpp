/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CApplication.h"
#include "CEventProcessor.h"
#include "CUserCode.h"
#include <CRemoteAccess.h>
#include <CRingItem.h>
#include <CAllButPredicate.h>
#include "options.h"
#include <stdlib.h>
#include <iostream>
#include <DataFormat.h>
#include <stdio.h>

/*----------------------------- Canonicals ----------------------------------------*/

/**
 ** Construction: Doesn't do a damned thing except initialize the membe pointers  
 ** We follow the two phase initialization model (Construct the initialize), where
 ** operator() drivesr the second phase.
 */
CApplication::CApplication() :
  m_pUserCode(0),
  m_pRunControl(0),
  m_pEventProcessor(0)
{
}
/** 
 ** Destruction we'll just delete anything that happens to be hanging around.
 ** this implies that eg. m_pEventProcessor is set to null between runs when it is
 ** deleted.
 */
CApplication::~CApplication()
{
  delete m_pUserCode;
  delete m_pEventProcessor;
}

/*------------------------- Entry point for application ----------------------*/

/**
 ** @param argc  - Number of words on the command line.
 ** @param argv  - Pointer to pointers to the command line words.
 ** @return int
 ** @retval EXIT_SUCCESS - Successful completion (probably should never happen).
 ** @retval EXIT_FAILURE - Something failed.
 */
int 
CApplication::operator()(int argc, char** argv)
{
  try {
    // This will populate most of the member data.
    if(!checkUsage(argc, argv)) {
      usage();
      return EXIT_FAILURE;
    }
    else {
      m_pUserCode = loadUserCode();
      
      
      mainLoop();
    }
  }
  catch (std::string msg) {
    std::cerr << msg << std::endl;
    return EXIT_FAILURE;
  }
  catch (...) {
    std::cerr << "pushToGEB - ";
    std::cerr << "An unexpected exception was caught, exiting\n";
    return EXIT_FAILURE;
  }
  std::cerr << "Main loop exited\n";
  return EXIT_SUCCESS;
}

/*------------------------------ Utility and functional decompositions ------------------*/

/**
 ** Check the program usage, process the command line parameters and 
 ** store the stuff we get from the command line in the appropriate member variables.
 */
int 
CApplication::checkUsage(int argc, char**argv)
{
  struct gengetopt_args_info parsedParams;
  int status = cmdline_parser(argc, argv, &parsedParams);
  if (status) {
    return 0;
  }
  // Parse worked load member variables:

  m_UserCode    = parsedParams.usercode_arg;
  m_controlType = parsedParams.ctltype_arg;
  m_physicsType = parsedParams.phystype_arg;
  m_GEBHost     = parsedParams.gebhost_arg;
  m_GEBPort     = parsedParams.gebport_arg;
  m_ringURL     = parsedParams.source_arg;
  m_tsScalerType = parsedParams.tsscalertype_arg;
  m_tsMultiplier = parsedParams.tsmultiplier_arg;
  m_fSize32      = parsedParams.no_size16_given;

  return 1;
}
/**
 ** Load the user code library using m_UserCode to identify the .so file.
 ** The .so is encapsulated in a CUserCode object which hides the details of
 ** dlopen, dlclose dlsym and the function calls actually work.
 ** @throws std::string - If the .so could not be loaded or key symbols not found.
 **
 */
CUserCode* 
CApplication::loadUserCode()
{
  if (m_UserCode != "") {
    CUserCode* pResult = new CUserCode(m_UserCode); // thows on error.
    pResult->initialize();			  // Do one-time initialization.
    return pResult;
  } else {
    return 0;                                  // There is no user code.
  }
}
/**
 ** Main loop funtion.
 ** - connect to the ring buffer identified by m_ringURL
 ** - accept items from the ring buffer.
 ** - each item is passed to preProcessEvent
 ** - if there's an event processor items are then passed to it.
 ** - each item is then passed to postProcessEvent.
 ** @throws std::string on error.
 */
void
CApplication::mainLoop()
{
  CRingBuffer*      pDataSource = CRingAccess::daqConsumeFrom(m_ringURL); // throws string on error.
  CAllButPredicate  acceptAll;

  createEventProcessor();

  while(1) {
    // Get any item from the ring..

    CRingItem* pItem = CRingItem::getFromRing(*pDataSource, acceptAll);

    // Preprocess, process and post process:

    preProcessEvent(*pItem);
    if (m_pEventProcessor) {
      (*m_pEventProcessor)(pItem);
    }
    postProcessEvent(*pItem);

    delete pItem;		// no leaks from me.

  }
  
}
/**
 ** create an event processor using the information we have in member variables.
 ** An event processor is created at the beginning of a run and destroyed after the end.
 ** this encpapsulates the fact that the gretina GEB only allows us to hold a connection for
 ** the duration of a run...once we've passed the end of run to the GEB, the event processor
 ** is destroyed.
 */
void
CApplication::createEventProcessor()
{
  m_pEventProcessor =  new CEventProcessor(m_pUserCode,
					   m_controlType,
					   m_physicsType,
					   m_tsScalerType,
					   m_GEBHost,
					   m_GEBPort,
					   m_tsMultiplier, m_fSize32);
}
/**
 ** Destroy the event processor (hence disconnecting from 
 ** GEB.  In addition, m_pEventProcessor => 0./
 **/
void 
CApplication::destroyEventProcessor()
{
  delete m_pEventProcessor;
  m_pEventProcessor = 0;
}

/**
 ** Event pre-processing:
 ** If there is no event processor we need to create one so that we can send events into
 ** the GEB...by not requiring a begin run event we take care of the case where we came
 ** on the scene in the middle of a run in a consistent way (assuming that the
 ** run control started the gretina run so that there is a GEB to connect with.
 ** @param item - reference to a ring item.
 */
void
CApplication::preProcessEvent(CRingItem& item)
{
  if (item.type() == BEGIN_RUN) {
    std::cout << "Begin run record seen\n";
    std::cout.flush();
  }

}
/**
 ** Event post processing:
 ** We just care if this is an end of run. If it is:
 ** - Kill  off the event processor since we'll get disconnected anyway.
 ** - Ask Gretina to end the run.
 */
void
CApplication::postProcessEvent(CRingItem& item)
{
  if (item.type() == END_RUN) {
    std::cout << "End run record seen\n";
    std::cout.flush();
    delete m_pEventProcessor;
    m_pEventProcessor = 0;
    exit(0);			// We're about to lose the connection in any event.
  }
}
/**
 ** Report the program usage to stderr
 */
void
CApplication::usage()
{
  // use stdio because that's what gengetopt code will use:

  fprintf(stderr, "Usage:\n");
  cmdline_parser_print_help();
}
