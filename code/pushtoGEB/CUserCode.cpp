/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include "CUserCode.h"
#include <dlfcn.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


/**
 ** Constructor 
 ** - Translate the path to an absolute path
 ** - load the dl saving the handle
 ** @param libPath - Path to the .so file.
 ** @throw string - Reason for error on failure.
 **
 */
CUserCode::CUserCode(std::string libPath)  :
  m_dlHandle(0),
  m_initFunction(0),
  m_extractTimestamp(0)

{
  // Make an absolute path from the pathanme:

  char* absolutePath = realpath(libPath.c_str(), NULL);
  if (!absolutePath) {
    throw std::string(strerror(errno));
  }
  m_absolutePath = absolutePath;

  // Attempt to load the .so

  m_dlHandle = dlopen(m_absolutePath.c_str(), RTLD_NOW);
  if (!m_dlHandle) {
    std::string error = dlerror();
    std::string msg = "Failed to load dynamic library: ";
    msg += error;
    throw msg;
  }
  m_initFunction = reinterpret_cast<initFunction>(dlsym(m_dlHandle, "initialize"));
  m_extractTimestamp = reinterpret_cast<extractFunction>(dlsym(m_dlHandle, "extractTimestamp"));
  if (!m_extractTimestamp) {
    dlclose(m_dlHandle);
    throw "Shared object does not have an extractTimestamp function";
  }


 

}
/**
 ** Destructor
 */
CUserCode::~CUserCode()
{
  dlclose(m_dlHandle);
}

/**
 * Invoke the initialization entry point.
 * @return bool
 * @retval true  - if there is no value for the init pointer.
 * @retval value from init function.
 */
bool
CUserCode::initialize()
{
  if (!m_initFunction) return true;

  return (*m_initFunction)();
}

/**
 ** Invoke the user's timestamp extraction program.
 ** @param pEvent - pointer to the event from which the timestamp must be extracted.
 ** @return long long 
 ** @retval  timestamp
 */
long long
CUserCode::timestamp(void* pEvent)
{
  return (*m_extractTimestamp)(pEvent);
}
