/*
   his software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2008

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Author:
             Ron Fox
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <GEBClient.h>
#include <gretTapClient.h>

#include "tapcatcmd.h"

static const int ConnectionRetryWait = 2000*1000; /* usec between con. retries. */
static const int ConnectionRetryCount = 600;	 /* Times we retry connect. */

struct Header {
  int type;
  int length;
  long long timestamp;
};

/**
 * Write a block of data to a specicif fd.
 * Works in the presence of signals and other things that could
 * go recoverably wrong on the write.
 *
 * @param fd - File descriptor number to which the data are written.
 * @param pBuffer - Pointer to the data that holds the buffer.
 * @param length  - Number of bytes to write.
 *
 * @return ssize_t
 * @retval <= 0 error.
 * @retval > 0 number of bytes written - because of the way this works, this should be
 *            the same as length.
 */

ssize_t
writeBlock(int fd, void* pBuffer, size_t length)
{
  const char* p = (const char*)pBuffer;
  ssize_t nWritten;
  ssize_t nTotalWritten = 0;

  while (length) {
    nWritten = write(fd, p, length);
    if (nWritten <= 0) {
      if ((errno == EAGAIN) || (errno == EWOULDBLOCK) || (errno == EINTR)) {
	nWritten = 0;
      } else {
	return -1;
      }
    }
    length        -= nWritten;
    nTotalWritten += nWritten;
    p             += nWritten;

  }
  return nTotalWritten;
}

/**
 * Write data from a linked list of gebData elements and their payloads
 * to stdout.  Each header is written with its payload field set to zero
 * followed by the payload itself.
 * 
 * @param fd    - File number on which to write the data.
 * @param pData - pointer to the first of a linked list of gebData structures.
 *
 * @note - If there are errors they are reported to stderr and the program
 *         exits since the last thing we want to do is to shovel around
 *         corrupted data or just run and put the data nowhere.
 * @note - Events with empty payloads are ok they just don't have a payload field
 *         following their header.
 */
static void 
writeEvents(int fd, struct gebData* pData)
{
  struct gebData* p = pData;
  int    status;
  struct Header header;
  int error;

  while (p) {
    header.type      = p->type;
    if ((header.type != GEB_TYPE_RAW) || 1) {
      header.length    = p->length;
      header.timestamp = p->timestamp;
      
      status = writeBlock(fd, &header, sizeof(header));
      error = errno;
      if (status != sizeof(header)) {
	fprintf(stderr, "Write of header failed: %s\n", strerror(error));
	exit(EXIT_FAILURE);
      };
      /* Only write the payload if there is one: */
      
      if (header.length) {
	status  = writeBlock(fd, p->payload, header.length);
	error = errno;
	if (status != header.length) {
	  fprintf(stderr, "Write of payload failed: %s\n", strerror(error));
	  exit(EXIT_FAILURE);
	}
      }
    }
      p = p->next;
  }


}


/*------------------------------------------------------------------------*/

/**
 * This program is the feeder for a pipeline of gretina data that comes 
 * from a Gretina output TAP.  Command usage is described by
 * tapcatcmd.ggo and can be gotten by the user via tapcat --help
 * The tap is unconditionally pointed at GRETTAP_GEB. 
 *
 */
int
main(int argc, char** argv)
{
  struct gengetopt_args_info parsedArgs;
  char*                pHost;
  int                  grouping;
  int                  type;
  float                timeout;  

  struct gretTap*            pTap;
  struct gebData*            pData;

  int error;
  int i;


  /* Proccess the command line parameters: */

  if(cmdline_parser(argc, argv, &parsedArgs)) {
      perror("Command line parse failed");
      exit(EXIT_FAILURE);
    }

    pHost    = parsedArgs.host_arg;
    grouping = parsedArgs.grouping_arg;
    type     = parsedArgs.type_arg;
    timeout  = parsedArgs.timeout_arg;

    /* Connect to the tap and report errors/exit if failure: */
    
    sleep(1);

    for (i = 0 ; i < ConnectionRetryCount; i++) {
      pTap = gretTapConnect(pHost, GRETTAP_GEB, type);
      if (pTap ||
	  (gretTapClientError != GTC_TAPCONN)) {
	/* Either success or a failure other than connection failure */

	break;
      }
      usleep(ConnectionRetryWait);
      /*      fprintf(stderr, "Retry number %d\n", i); 
	      fflush(stderr); */

    }
    fprintf(stderr, "Retries: %d\n", i);
    fflush(stderr);
    if (!pTap) {
      fprintf(stderr, "Unable to connect to tap server at %s : %s\n", pHost, 
	      gretTapClientErrorStrings[gretTapClientError]);
      exit(EXIT_FAILURE);
    }

    /* The main loop gets event data as long as the tap is valid.  
       Timeout is currently a second or so.
    */

    while(!gretTapCheck(pTap)) {
      pData = gretTapData(pTap, grouping, timeout);
      if (!pData && (gretTapClientError != GTC_TIMEOUT)) {
	error = gretTapClientError;
	if (gretTapCheck(pTap)) {
	  gretTapClose(pTap);
	  fprintf(stderr, "tapcat exiting normally - GEB closed on us\n");
	  exit(EXIT_SUCCESS);
	} else {
	  fprintf(stderr, "Failed to read data from the tap: %s",
		  gretTapClientErrorStrings[error]);
	  exit(EXIT_FAILURE);
	}

      } else if (!pData && (gretTapClientError == GTC_TIMEOUT)) {
#ifdef DEBUG
	fprintf(stderr, "Timeout\n");
#endif
      }
      else {
	writeEvents(STDOUT_FILENO, pData);
	gretTapDataFree(pData);
      }
       
    }
    gretTapClose(pTap);
    fprintf(stderr, "tapcat Exiting normally  - GEB closed socket.");
    exit(EXIT_SUCCESS);
}
