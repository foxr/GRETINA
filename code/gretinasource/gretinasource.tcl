#!/bin/sh
# -*- tcl -*-
# The next line is executed by /bin/sh, but not tcl \
exec tclsh "$0" ${1+"$@"}

#    This software is Copyright by the Board of Trustees of Michigan
#    State University (c) Copyright 2014.
#
#    You may use this software under the terms of the GNU public license
#    (GPL).  The terms of this license are described at:
#
#     http://www.gnu.org/licenses/gpl.txt
#
#    Authors:
#             Ron Fox
#             Jeromy Tompkins 
#	     NSCL
#	     Michigan State University
#	     East Lansing, MI 48824-1321



##
# @file gretinasource.tcl
# @brief  ReadoutGUI data source for GRETINA auxilliary detectors.
# @author Ron Fox <fox@nscl.msu.edu>
#

package provide gretinasource_Provider 1.0
package require RemoteGUI_Provider

##  The assumption is that the following env vars have been
#   defined:
#
#   GRTOP   - Root of gretina software installation.
#   GRBIN   - Gretina software executables.
#   GRLIB   - Gretina libraries.
#   GRTCLLIBs - Tcl libraries.

lappend auto_path $env(GRTCLLIBS)
package require gebpushmgr

namespace eval ::gretinasource {
    #
    #  State for each active provider indexed by source id and
    #  contains a dict with the keys:
    #
    #  - pusher GebPush object.
    #  - host   - host in which the remote's readoutGUI is run.
    #  - user   - user in which the remote's readoutGUI is run.
    #
    array set providerStates [list]
}

##
# parameters
#    Returns the set of parameters needed to create the source.
#    this is used to construct the source prompter:
#
# @return dict - keys are parameter names and values descriptions.
#    the parameter names are the keys for the dict used in the start
#    operation.
#
proc ::gretinasource::parameters {} {
    return [dict create \
        ring {URI of the ring from which data get pushed to GRETINA}    \
        host {Host in which the remote ReadoutGUI is running}           \
        user {User running the remote ReadoutGUI}                       \
	detectorId {Id of the detector (assigned by GRETINA)}    \
        tsmultiplier {Timestamp Multiplier}                     \
        size32       {Event sizes are 32 bits wide (flag) }      \
    ]
}
##
# start
#    We live on top of the RemoteGUI Provider.  That  provider is used
#    to control the readout program we get data from.
#    In addition we'll need to manage a GEBPush object to manage the GEBPush
#    object.
#
# @param params - a dict containing the parameters defined by [parameters]
#                 above along with sourceId - the source id.
#

proc ::gretinasource::start params {
    #  Pull out the parameters. Start the ReadoutGUI provider:

    set ring [dict get $params ring]
    set host [dict get $params host]
    set user [dict get $params user]
    set id   [dict get $params sourceId]
    set gid  [dict get $params detectorId]
    set size32 [dict get $params size32]
    
    set tsmult [dict get $params tsmultiplier]
    ::ReadoutGUI::start $params
    
    set bindir $::env(GRBIN)
    set pusher [GebPush %AUTO%    \
        -source $ring -phystype $gid -gebhost [$bindir/gebhost] \
        -gebport [$bindir/gebport] -tsmultiplier $tsmultiplier  \
        -size32 $size32                                         \
    ]
    set ::gretinasource::providerStates($id) \
        [dict create pusher $pusher host $host user $user]
    
}

##
# check
#    See if we are still alive.   The only active element we check is the liveness
#    of the remotely controlled ReadoutGUI:
#
# @param   id   id of the data source to check.
# @return bool  True ifr the system is alive.
#
proc ::gretinasource::check id {
    return [::RemoteGUI::check $id]
}
##
# stop
#   Stop an instance of the data source.
#
# @param id - id of the data source to stop.
#
proc ::gretinasource::stop id {
    ::RemoteGUI::stop $id
    set infoDict $::gretinasource::providerStates($id)
    [dict get $infoDict pusher] destroy
    array unset ::gretinasource::providerStates $id
}
##
# begin
#    begin  new run.  Note that by beginning the run in the remote,
#    the normal state transition hooks will get gebpush started.
#
# @param id    - id of the data source
# @param run   - run number of the run to start.
# @param title - Title of the run to start.
#
proc ::gretinasource::begin {id run title} {
    ::RemoteGUI::begin $id $run $title
}

##
# end
#   Ask the source to end its run.  This is done by asking the remote  GUI to
#   end the run.  The GEBPush object registers a callback bundle which takes
#   care of understanding that since the run is ending it's ok for its
#   gebpush object to also fade away.
#
# @param id  - source id of the source being asked to end its run.
#
proc ::gretinasource::end id {
    ::RemoteGUI::end $id
}
##
# init
#   Initialize the data source:
# @param id - the data source id.
#
proc ::gretinasource::init id {
    ::RemoteGUI::init $id
}
##
# capabilities
#   Return the data source capabilities - these are the same as the RemoteGUI's:
#
proc gretinasource::capabilities {} {
    return [::RemoteGUI::capabilities]
}
