///////////////////////////////////////////////////////////
//  CRingFactory.h
//  Implementation of the Class CRingFactory
//  Created on:      21-Jun-2011 1:17:59 PM
//  Original author: fox
///////////////////////////////////////////////////////////

#if !defined(EA_7238D451_EF98_4f4c_A3D1_0AC75DA5024E__INCLUDED_)
#define EA_7238D451_EF98_4f4c_A3D1_0AC75DA5024E__INCLUDED_

#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#include <string>
#endif
#endif


class CRingInserter;

/**
 * Create, if necessary, the ring inserter that will manage our access to the
 * output ring.
 */
class CRingFactory
{

public:
  static CRingInserter* create(std::string name);
  
};
#endif // !defined(EA_7238D451_EF98_4f4c_A3D1_0AC75DA5024E__INCLUDED_)
