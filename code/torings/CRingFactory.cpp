///////////////////////////////////////////////////////////
//  CRingFactory.cpp
//  Implementation of the Class CRingFactory
//  Created on:      21-Jun-2011 1:17:59 PM
//  Original author: fox
///////////////////////////////////////////////////////////

#include "CRingFactory.h"
#include "CRingInserter.h"
#include <CRingBuffer.h>



/**
 * Creates a ring inserter that is attached to the named ring buffer as its
 * producer.
 * @param name - The name of the ring to which we are going to be 
 *               connecting.
 */
CRingInserter* CRingFactory::create(std::string name)
{
  // If needed, create the new ring:

  if (!CRingBuffer::isRing(name)) {
    CRingBuffer::create(name);
  }
  // Create the ring inserter and return it to the caller:

  return new CRingInserter(name);

}
