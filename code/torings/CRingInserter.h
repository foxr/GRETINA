///////////////////////////////////////////////////////////
//  CRingInserter.h
//  Implementation of the Class CRingInserter
//  Created on:      21-Jun-2011 1:18:01 PM
//  Original author: fox
///////////////////////////////////////////////////////////

#if !defined(EA_A8716B67_AC6C_4c6c_A5BF_205D2E945D99__INCLUDED_)
#define EA_A8716B67_AC6C_4c6c_A5BF_205D2E945D99__INCLUDED_

#ifndef __STL_STRING
#include <string>
#ifndef __STL_STRING
#define __STL_STRING
#endif
#endif


class CRingBuffer;

/**
 * Maintains a destination ring and provides methods for inserting events in the
 * ring.
 */
class CRingInserter
{
private:
  /**
   * Ring buffer to which data will be submitted.
   */
  CRingBuffer* m_pDestination;
  /**
   * Number of triggers that have been generated to the ring.
   */
  size_t m_nTriggers;
  /**
   * Last timestamp seen:
   */
  unsigned int m_nLastTimestamp;
  
public:
  CRingInserter(std::string name);
  virtual ~CRingInserter();
  
  void mark();
  void event(size_t nBytes, void* pEvent);
  
};
#endif // !defined(EA_A8716B67_AC6C_4c6c_A5BF_205D2E945D99__INCLUDED_)
