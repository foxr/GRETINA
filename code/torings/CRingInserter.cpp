///////////////////////////////////////////////////////////
//  CRingInserter.cpp
//  Implementation of the Class CRingInserter
//  Created on:      21-Jun-2011 1:18:02 PM
//  Original author: fox
///////////////////////////////////////////////////////////

#include "CRingInserter.h"
#include <CRingBuffer.h>
#include <CRingItem.h>
#include <CRingPhysicsEventCountItem.h>
#include <DataFormat.h>
#include <string.h>

/**
 * Construct a ring inserter for a specific
 * ring buffer.  The ring buffer specified must already exist.
 * We are going to attach to the ring buffer as a producer.
 * this allows us to insert data into the ring for other consumers.
 *
 * @param name - name of the ring buffer.
 */
CRingInserter::CRingInserter(std::string name) :
  m_pDestination(0),
  m_nTriggers(0),
  m_nLastTimestamp(0)
{
  m_pDestination = new CRingBuffer(name, CRingBuffer::producer);
}



CRingInserter::~CRingInserter()
{
  delete m_pDestination;
}





/**
 * Inserts a trigger count event in the ring.  This event allows consumers to
 * gague their sampling efficiency.
 */
void CRingInserter::mark()
{
  CRingPhysicsEventCountItem item(m_nTriggers, m_nLastTimestamp);
  item.commitToRing(*m_pDestination);
}


/**
 * Inserts an event in the ring buffer.  The event is tagged as a physics event.
 * For now we are not doing anything with the time offset.
 * @param nBytes - Number of bytes in the event.
 * @param pEvent - Pointer to the event itself.
 */
void CRingInserter::event(size_t nBytes, void* pEvent){
  CRingItem item(PHYSICS_EVENT, nBytes+10); // Probably don't need the slop.

  uint8_t* pBody = reinterpret_cast<uint8_t*>(item.getBodyPointer());
  memcpy(pBody, pEvent, nBytes);
  item.setBodyCursor(pBody + nBytes);

  item.commitToRing(*m_pDestination);
  m_nTriggers++;

}
