#include <iostream>
#include "cmdline.h"
#include "CRingFactory.h"
#include "CRingInserter.h"
#include "Input.h"
#include <string>
#include <stdlib.h>
#include <Exception.h>
#include <stdint.h>

static const char* LAST_RESORT_RINGNAME="builtevents";
static const unsigned int MARK_INTERVAL(1000);

/**
 *   Figure out the default ring buffer (which will be the username).
 *  of the current user:
 */
const char*
defaultRing()
{
  const char* result = cuserid(NULL);
  if (!result) {
    result = LAST_RESORT_RINGNAME;
  }
  return result;
}
/**
** Main program for the GEB to ringbuffer output stage.
** For usage see options.ggo
*/

int main(int argc, char** argv) 
{
  // Process the arguments to create the output ring buffer
  // connection.
  //
  try {
    struct gengetopt_args_info parsedArgs;
    
    int status = cmdline_parser(argc, argv, &parsedArgs); // exits on error actually.
    
    std::string ringName = defaultRing();
    
    if (parsedArgs.ring_given) {
      ringName = parsedArgs.ring_arg;;
    }
    std::cerr << "Attaching to " << ringName << std::endl;
    CRingInserter& output(*CRingFactory::create(ringName));

  // Create the input 


    CInput input;

    int triggers = 0;
    while (!std::cin.eof()) {
      void* pEvent;
      size_t eventSize = input.getEvent(&pEvent);
      
      output.event(eventSize, pEvent);
      free(pEvent);
      
      triggers++;
      if ((triggers % MARK_INTERVAL) == 0) {
	output.mark();		// trigger count element.
      }
    }
  }
  catch (std::string msg) {
    std::cerr << " Error: " << msg << std::endl;
    exit(-1);
  }
  catch (CException& fail) {
    std:: cerr << fail.ReasonText() << std::endl;
    exit(-1);
  }


  std::cerr << "End of input file\n";
  exit(0);

  
  
}
