program  ReadSend ("instance=1,cryid=4") 

option +d;

%%#include <string.h>
%%#include "readFile.h"
%%#include "sender.h"
%%#include "cesort.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define SENDSTATE(s)            \
        strcpy(statepv, #s);    \
        pvPut(statepv);         \
        pvFlush();


/* file name of input data file */
DECLARRAY(char, indata, Det{cryid}_CS_InData, 100)

DECLMON(short,AcqRun,Online_CS_StartStop)
DECLMON(int,eventGap,DAQG_CS_EventGap)   /* timestamp units, ie, 10 ns */

DECL(string, statepv, Decomp{instance}_CV_State)
DECL(int, Status, Decomp{instance}_CV_Status)
/* offline condition not currently supported */
DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECL(int, eventrate, Decomp{instance}_CV_Rate)
DECL(int, buffersleft, Decomp{instance}_CV_BuiltChans)

evflag requestAvail;
evflag senderWaiting;
evflag senderRan;
evflag sendInit;
evflag requestInit;
evflag senderInitE;
evflag senderWriteEvent;
int senderWrite;
int senderIStat;

%%extern int readFIFOBufs;
int outputList;
int AcqRunRO;
int instat;

unsigned long clientReq;	/* actually struct incoming * */
unsigned long eventBd;	        /* board number */

ss eventBuild {
   state init {
      when() {
         initCEB();
         AcqRunRO = 0;
         senderIStat =  senderInit();
      } state checkSender
   }
   state checkSender {
      entry {
         SENDSTATE(Check)
      }
      when (senderIStat == 0) {
         efSet(sendInit);
      } state checkRun
      when(delay(1)) {
         senderIStat =  senderInit();
      } state checkSender
   }
   state checkRun {
      when (!AcqRun) {
      } state setup
   }
   state setup {
      entry {
         SENDSTATE(Setup)
      }
      when (AcqRun) {
         resetCEBCounters();
         pvGet(indata);
         instat = startFileReader(indata);
      } state checkopen
   }
   state checkopen {
      when (!instat) {
         AcqRunRO = 1;
         senderWrite = 1;	/* turn on main sender */
         efSet(senderWriteEvent);
         SENDSTATE(Run)
      }  state runSaveBuild
      when() {
      } state setup
   }
   state runSaveBuild {
      when (!AcqRun) {
         stopFileReader();
         resetCEB();
      } state setup
      when(delay(0.05)) {
        eventBd = listRawEvents(1);  
      } state checkReadSB
   }
   state checkReadSB {
      when (eventBd == -3) {
      } state endofrun
      when() {
        sortToOutput(eventBd);
        if (outputList) {
            outputList = 0;
            harvestOutput();
        }
      } state runSaveBuild
   }
   state endofrun {
      entry {
        SENDSTATE(Done)
      }
      when (!AcqRun) { 	
         stopFileReader();
         resetCEB();
      } state setup
   }
}

ss sendCounters {
   state send {
      when (delay(1.0)) {
         eventrate = getEventsSentDiff();
         buffersleft = getBuffersLeft();
         pvPut(buffersleft);
         pvPut(eventrate);
      }  state send
   }
}

ss newList {
   state run {
      when (delay(0.2)) {
         outputList = 1;
         Status = 1;
         pvPut(Status);
      } state run
   }    
}       

ss requestTest {
   state init {
      when (efTestAndClear(sendInit)) {
         efSet (requestInit);
      } state waitfordone
   }
   state waitforrequest {
      when() {
         clientReq = getRequest();
      } state testclient
   }
   state testclient {
      when(clientReq) {
         efClear(senderWaiting);
         efSet(requestAvail);
      } state waitfordone
      when(delay(1.0)) {
         /* something went wrong.  slow down. */
      }  state waitforrequest
   }
   state waitfordone {
      when (efTestAndClear(senderWaiting)) {
      } state waitforrequest
   } 
}

ss senderControl {
   state init {
       when (efTestAndClear(senderInitE)) {
       } state senderNotRunning
   }
   state senderRunning {
       when(efTestAndClear(senderWriteEvent)) {
       } state senderSwitch
       when (efTestAndClear(senderRan)) {
       }  state senderRunning
       when (delay(5)) {
       } state senderRunning
   }
   state senderNotRunning {
       when(efTestAndClear(senderWriteEvent)) {
       } state senderSwitch
       when (delay(0.1)) {
          /* discard output data */
       } state senderNotRunning
   }
   state senderSwitch {
       when(senderWrite) {
          senderControl(1);
       }  state senderRunning
       when() {
          senderControl(0);
       }  state senderNotRunning
   }
}


ss sender {
   state init {
       when (efTestAndClear(requestInit)) {
          efSet(senderInitE);
       } state senderWait
   }
   state senderWait {
       entry {
         efSet(senderWaiting);
       }
       when(efTestAndClear(requestAvail)) {
         /* return with error if sending not enabled */
         /* otherwise run as separate task until sending is turned off */
         runServerInstance(clientReq, eventGap);
         efSet(senderWaiting);
       } state senderWait
       when() {
       } state senderWait
       when (!AcqRun && AcqRunRO) {
         AcqRunRO = 0;
         senderWrite = 0;
         efSet(senderWriteEvent);
       } state senderWait
   }
}

