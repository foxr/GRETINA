#include <sysLib.h>
#include <vxWorks.h>
#include <stdio.h>
#include <stdlib.h>

#include "devLib.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "recGbl.h"
#include "recSup.h"
#include "devSup.h"
#include "aiRecord.h"
#include "boRecord.h"

#include "epicsExport.h"

#include "devGVME.h"
#include "cesort.h"
#include "sender.h"

#define ALLOCDPVT       dpvt = calloc(1, sizeof(struct daqDevPvt));     \
                        if (dpvt == NULL) {                             \
                           printf("Out of memory in devGData init\n");   \
                           return ERROR;                                \
                        }


static long init_record_ai(aiRecord *pai)
{
    struct vmeio *pvmeio;
    struct daqDevPvt *dpvt;

    pvmeio = (struct vmeio *)&(pai->inp.value);

    switch (pai->inp.type)
    {
       case (VME_IO):
          ALLOCDPVT
          switch (pvmeio->signal) {
              case 0x01: case 0x02: case 0x03: case 0x04:
              case 0x05: case 0x06: case 0x07: case 0x08:
              case 0x09: case 0x0a: case 0x0b:
                 /* channel event rate from crystal event builder */
                 /* valid if processed at 1 hz scan */
                 dpvt->signal = pvmeio->signal;
                 dpvt->card = pvmeio->card;
                 break;
              case 0xd: case 0xe:
                 /* sender data rate */
                 dpvt->signal = pvmeio->signal;
                 /* any card number within the crystal will do  */
                 dpvt->card = pvmeio->card;
                 break;
              case 0xc:
                 /* channel latest timestamp from crystal event builder */
                 dpvt->signal = pvmeio->signal;
                 dpvt->card = pvmeio->card;
                 break;
              default:
              recGblRecordError(S_db_badField,(void *)pai,
                    "devGData_AI (init_record) Illegal signal specified");
              return(S_db_badField);
          }
          pai->dpvt = dpvt;
          return 2; /* no conversion */
       default:
          recGblRecordError(S_db_badField,(void *)pai,
                "devGData_AI (init_record) Illegal I/O link type");
          return(S_db_badField);
    }
}

static long read_ai(struct aiRecord *pai)
{
   struct daqDevPvt *dpvt;

   dpvt = (struct daqDevPvt *)pai->dpvt;
   if (!dpvt) return -1;

   switch (dpvt->signal) {

      case 0x01: case 0x02: case 0x03: case 0x04:
      case 0x05: case 0x06: case 0x07: case 0x08:
      case 0x09: case 0x0a: case 0x0b:
         pai->val = getChanRate(dpvt->card, dpvt->signal - 1);
         break;
      case 0xd:
         pai->val = getKBSentDiff();
         break;
      case 0xe:
         pai->val = getKBReadDiff();
         break;
      case 0xc:
         pai->val = getCardLatest(dpvt->card);
         break;
      default:
         return -1;
   }
   pai->udf = 0;
   return 2;    /* do not convert */
}

struct {
        long            number;
        DEVSUPFUN       report;
        DEVSUPFUN       init;
        DEVSUPFUN       init_record;
        DEVSUPFUN       get_ioint_info;
        DEVSUPFUN       read_ai;
        DEVSUPFUN       special_linconv;
}devAiGData={
        6,
        NULL,
        NULL,
        init_record_ai,
        NULL,
        read_ai,
        NULL
};
epicsExportAddress(dset,devAiGData);

static long init_record_bo(boRecord *pbo)
{
    struct vmeio *pvmeio;
    struct daqDevPvt *dpvt;

    pvmeio = (struct vmeio *)&(pbo->out.value);

    switch (pbo->out.type)
    {
       case (VME_IO):
         ALLOCDPVT
         dpvt->card = pvmeio->card;
         switch (pvmeio->signal) {
             case 0x0:  /* enabled flag */
                 dpvt->signal = 1;
                 pbo->mask = 0x1;
                 pbo->dpvt = dpvt;
                 return 2;      /* no convert */
                 break;
             default:
                 recGblRecordError(S_db_badField,(void *)pbo,
                 "devGData_BO (init_record) Illegal signal specified");
                 return(S_db_badField);
          }
       default:
          recGblRecordError(S_db_badField,(void *)pbo,
                "devGData_BO (init_record) Illegal I/O link type");
          return(S_db_badField);
    }
}

static long write_bo(struct boRecord *pbo)
{
   struct daqDevPvt *dpvt;

   dpvt = (struct daqDevPvt *)pbo->dpvt;
   if (!dpvt) return -1;

   if (dpvt->signal == 1) {
      setBoardEnable(dpvt->card, (int)pbo->rval);
      return OK;
   }
   return -1;

}
struct {
        long            number;
        DEVSUPFUN       report;
        DEVSUPFUN       init;
        DEVSUPFUN       init_record;
        DEVSUPFUN       get_ioint_info;
        DEVSUPFUN       write_bo;
}devBoGData={
        5,
        NULL,
        NULL,
        init_record_bo,
        NULL,
        write_bo
};
epicsExportAddress(dset,devBoGData);


