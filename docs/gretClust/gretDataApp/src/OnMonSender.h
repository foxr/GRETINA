#ifndef OnMonSENDER_H

#define OnMonSENDER_H

int OnMonInit();
int OnMonAttach(char *serverName);
int OnMonSend(int boardno, int numtosend);
int OnMonDetach();

#endif
