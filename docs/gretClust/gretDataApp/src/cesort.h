
/*
 * Endianness, the way CA does it.
 */
#if defined (_M_IX86) || defined (_X86_) || defined (__i386__)
#       define CEB_LITTLE_ENDIAN
#else
#       define CEB_BIG_ENDIAN
#endif

#include "eventSource.h"

#define ONLINE_PACKET_SIZE	(7*1024)
#define ONLINE_BUF_SIZE (ONLINE_PACKET_SIZE - 4)

#define MAXRECLEN 513   /* 32 bit words */

/* refer to canonical drawing of data package in which the bit order is 
 * from bit 0 on the left to 31 on the right.
 */
#define rightCol(a) ntohs(*((unsigned short *)(a)))
#define leftCol(a) ntohs(*(((unsigned short *)(a))+1))


struct eNode {  long long timestamp;
                unsigned int *bptr;
                struct eNode *prev;
                struct eNode *next;
                rawEvt *delPtr;
                int len;
 		int recs;
#ifdef GARBAGE_COLLECT
                int traceinfo;
                int memindex;
#endif
             };


int getNumEvents();
int getRawTrace(int card, int chan, short *data);
long long getNextTime(void);
long long getCurrentTime(void);
int clearMonList(void);
int resetCEB(void);
int resetCEBCounters(void);
int setCESortInfo(int card, int chan);
int setBoardEnable(int card, int enab);
int getBoardEnable(int card);
int initCEB();
void printCounters(); 
int listRawEvents(int sort);
struct eNode *getOutput(void);
int harvestOutput(void);
int grabOutput(void);
int purgeOutput(void);
int sortToOutput(int boardno);
int clearEvents(void);
int listToOutput(int boardno);
int getChanDiff(short card, short channo);
int getChanRate(short card, short channo);
int getRawData(int boardno, char *buf, struct eNode **where);
long long getCardLatest(short card);
int getLastBoard(void);
int getOQueueCount(void);

