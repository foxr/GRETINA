
/*
 * sga.h:  Header file for simplified Gammasphere acquisition including 
 *         specification of data formats for raw and processed data streams.
 */

/* Mario Cromaz, LBNL, Oct. 2000 */
/* Oval modifications (MC), Dec 02 */

#ifndef _sga_h
#define _sga_h

#include <stdio.h>


#define RAW_BUF_SIZE	(2048 * 1024)	/* bytes */	
#define RAW_Q_SIZE	 400


#define READOUT_VAR_LEN		/* use smart readout? */

int startFileReader(char *fname);
int stopFileReader();
int getBuffersLeft();

#endif  /* _sga_h */
