#include <stdlib.h>
#include <string.h>

#include <epicsThread.h>
#include <seqCom.h>
#include <iocsh.h>

extern struct seqProgram ReadSend;
extern void PCRegistrar();

/*
   The only reason for having this code instead of using -m in ReadSend.st is
   that I can call PCRegistrar(), which registers the printCounters command
   from cesort.c with the ioc shell.
*/
int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    seqRegisterSequencerCommands();
    PCRegistrar();
    threadId = seq((void *)&ReadSend, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        epicsThreadExitMain();
    }
    return(0);
}

