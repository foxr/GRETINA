/* readFile.c : code to read GRETINA data from file for test purposes
 *
 */ 

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <netinet/in.h> /* for byte order */


#include <freeList.h>

#include <epicsThread.h>
#include <epicsMutex.h>
#include <epicsEvent.h>

#include "eventSource.h"
#include "cesort.h"
#include "readFile.h"

int readFIFOBufs = RAW_Q_SIZE;
int recLenGDig;

extern void *eNodeList;

int getBuffersLeft() {
   return readFIFOBufs;
}

int rawEvtGC() {

       int garbageCollected = 0;
       return garbageCollected;
}

int rawEvtReserve(rawEvt *rawBuf, long long timestamp, struct eNode *this) {

    if (!rawBuf) {
       printf("Null raw buffer reserved!!\n");
       return -1;
    }
    epicsMutexLock(rawBuf->refMutex);
    rawBuf->refcount++;
    epicsMutexUnlock(rawBuf->refMutex);
    return 0;
}
    

void rawEvtFree(rawEvt *rawBuf, struct eNode *this) {


    if (!rawBuf) {	/* only happens with carryovers */
       if (this) {
          free(this->bptr);
          freeListFree(eNodeList, this);
       }
       return;
    }
    /* It might be zero the first time if it just gets returned without
     * processing into any complete events.
     */
    epicsMutexLock(rawBuf->refMutex);
    if (rawBuf->refcount <= 0 && this != 0) {
       printf("raw buffer freed too many times!!\n");
       epicsMutexUnlock(rawBuf->refMutex);
       return;
    }
    if (rawBuf->refcount > 0) rawBuf->refcount--;

    if (this) {
       freeListFree(eNodeList, this);
    }
    if (rawBuf->refcount == 0) {
       /* all sub-buffers free */
       epicsMutexUnlock(rawBuf->refMutex);
       epicsMutexDestroy(rawBuf->refMutex);
       free(rawBuf->bptr);
       free(rawBuf);
       readFIFOBufs++;
    } else {
       epicsMutexUnlock(rawBuf->refMutex);
    }
}

static  int ifd;
static  rawEvt *rawBuf;
static  int readret;
epicsEventId readSem;

void readFromDisk(void *parm) {

  int numread;

  rawBuf = calloc(1, sizeof(rawEvt));
  rawBuf->bptr = (unsigned int *)malloc(RAW_BUF_SIZE);
  rawBuf->refMutex = epicsMutexCreate();

  if (!rawBuf || !rawBuf->bptr || !rawBuf->refMutex) {
     /* none of these are recoverable errors. program dying */
     readret = 63;
     return;
  }

  numread = read(ifd, rawBuf->bptr, RAW_BUF_SIZE);

  if (numread == 0) {	/* end of file */
     readret = 255;
     epicsMutexDestroy(rawBuf->refMutex);
     free(rawBuf->bptr);
     free(rawBuf);
     rawBuf = 0;
     epicsEventSignal(readSem);
     return;
  }

  if (numread < 0) {	/* various possible errors */
     perror("ReadSend read ");
     readret = 127;
     epicsMutexDestroy(rawBuf->refMutex);
     free(rawBuf->bptr);
     free(rawBuf);
     rawBuf = 0;
     epicsEventSignal(readSem);
     return;
  }

  rawBuf->len = numread/4;
  rawBuf->board = 1;
  readret = 1;

  epicsEventSignal(readSem);
}


   

/* Unlike in the real-time situation, we overload boardno to indicate some
   non-fatal error occurred.
 */ 
rawEvt *getRawBuf(unsigned short *boardno) {

  rawEvt *retval;

  *boardno = 0;

  if (readFIFOBufs < 1) {
     return 0;
  }

  epicsEventWait(readSem);

  if (rawBuf && readret == 1) {
     *boardno = rawBuf->board;
     rawBuf->refcount = 0;
     rawBuf->curptr = rawBuf->bptr;
     readFIFOBufs--;
     retval = rawBuf;
  } else  {
     *boardno = readret;
     retval = 0;
  }

  epicsThreadCreate("diskread", 0, 16384, readFromDisk, 0);

  return retval;
}

#define MAXRECLEN 513	

/* 

Data test file no longer specified in code. Comment is for  reference only

char *filenames[] = { 	"/global/data1a/q1b1/co60/Q1July09001.dat",
			"/global/data1a/q2a6/co60/Q2Aug09007.dat",
			"/global/data1a/q2b4/co60/Q2Aug09006.dat",
			"/global/data1a/q2b6/co60/Q2Aug09008.dat",
			"/global/data1a/q2a5/pencils/Q2Aug09015.dat" };
*/

/*
 * This is run at the start of each run.  
 */

int startFileReader(char *fname) {

  int i=0, inbuf=0, numread;

  if (ifd > 0) {
      close(ifd);
      ifd = 0;
  }

  printf("opening file %s\n", fname);

  ifd = open(fname, O_RDONLY);

  if (ifd < 0) {
     ifd = 0;
     return -1;
  }

  /* find record length and sanity check data file */

  while (i < MAXRECLEN) {
     numread = read(ifd, &inbuf, 4);
     if (numread != 4) {
         printf("Read error while checking data file %s \n", fname);
         close(ifd);
         ifd = 0;
         return -1;
     }
     if (0xaaaaaaaa == inbuf) {
        break;
     }
     i++;
   }
   if (i == MAXRECLEN) {
      printf("no delimiter in data file %s\n", fname);
      close(ifd);
      ifd = 0;
      return -1;
   }
   numread = read(ifd, &inbuf, 4);
   if (numread != 4) {
       printf("Read error while checking data file %s \n", fname);
       close(ifd);
       ifd = 0;
       return -1;
   }
   recLenGDig = rightCol(&inbuf) & 0x7ff;
   if (recLenGDig > MAXRECLEN || recLenGDig < 6) {
      printf("Unreasonable length of %d found\n", recLenGDig);
       close(ifd);
       ifd = 0;
       return -1;
   }

   /* go back to beginning of file */
   lseek(ifd, 0, SEEK_SET);

   /* set up fake boards for the benefit of the event sender */
   setCESortInfo(0,1);
   setBoardEnable(0,1);

   readSem = epicsEventCreate(epicsEventEmpty);
   if (!readSem) {
       close(ifd);
       ifd = 0;
       return -1;
   }

   epicsThreadCreate("diskread", 0, 16384, readFromDisk, 0);

   return 0;

}

/* This is run at the end of each run */

int stopFileReader() {

   if (ifd) {
      close(ifd);
      ifd = 0;
      return -1;
   }
   return 0;
}
