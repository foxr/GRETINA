
#include <stdlib.h>
#include <stdio.h>

#include <epicsMutex.h>
#include <epicsMessageQueue.h>
#include <epicsEvent.h>
#include <freeList.h>
#include <iocsh.h>
#include <epicsExport.h>
#ifndef vxWorks
#include <strings.h>
#endif

#include "eventSource.h"

#include <netinet/in.h>	/* for byte order */
#include "cesort.h"


static struct eNode *eRoot;	/* sorted event list */
static struct eNode *eRootEnd;	/* sorted event list end */
static epicsMessageQueueId oQueue;	/* output message queues */
static epicsMutexId eRootSem;	/* protects eRoot */
static int oQueueCount;

#define NCOUNTERS 14	/* in errtypes */
#define NUMBOARDS 15

enum errtypes  { length, coset, coget, cosuc, neg_time, low_time, high_time, 
                 time_nonmono, board_id, events, bufs, searches, drops, 
                 truncs };

#define CHAN_RATE_POINTS 10

struct boardInfo {
   long long latestTime;
   struct eNode *rawList;
   struct eNode *rawEnd;
   struct eNode *newENode;
   unsigned int *carryover;
   unsigned int counters[NCOUNTERS];
   unsigned int chanPrevious[NUM_DIG_CHANS+1]; 
   unsigned int chanEvents[NUM_DIG_CHANS+1]; 
   long long chanPrevTS[NUM_DIG_CHANS+1];
   float chanPeriod[NUM_DIG_CHANS+1][CHAN_RATE_POINTS]; 
   int chanIndex[NUM_DIG_CHANS+1]; 
   int chanSamples[NUM_DIG_CHANS+1]; 
   int colen;
   unsigned short boardaddr;
   short enabled;
};

static int boarderrs;
static int numevents;
static int numinp;
static int g_time_nonmono;
static int g_low_time;
static int g_truncs;

static struct boardInfo boards[NUMBOARDS];
static int maxGDigCard;

#ifdef vxWorks
int getRawData(int card, char *outp, struct eNode **where ) {

   int RecLen, nb, j;

   if (*where == 0) {
     struct boardInfo *bd;

     if (card < 0 || card > NUMBOARDS) {
        return -1;
     }
     bd = &boards[card];
     *where = bd->rawList; 
     if (*where == 0) return -1;
   }

   RecLen = (recLenGDig + 1) * 4;

   nb = (ONLINE_BUF_SIZE / RecLen);

   bzero(outp, ONLINE_BUF_SIZE);

   for (j =0; j < nb; ) {
      if (*where == 0 || (*where)->bptr == 0) {
         return -2;
      }
      if ((nb - j) > (*where)->recs) {
         memcpy(outp, (*where)->bptr, RecLen * (*where)->recs);
         outp += (RecLen * (*where)->recs);
         j += (*where)->recs;
      } else {
         memcpy(outp, (*where)->bptr, RecLen * (nb - j));
         /* outp will not be used again */
         j = nb;
         continue;
      }
      *where = (*where)->next;
   }
   if (*where == 0) {
      return -2;
   }
   return 0;
}
#endif

int getOQueueCount() { return oQueueCount; }

unsigned short getChanNo(int *bptr) {
   return (leftCol(bptr + 1) & CHANNEL_MASK);
}
#ifdef vxWorks

int getRawTrace(int card, int channel, short *outp) {

   int TraceLen, i, j, bcnt;
   struct boardInfo *bd;
   struct eNode *where;
   short *optr;
   unsigned int *lbptr;
   int ntries = 20;

   if (card < 0 || card > NUMBOARDS) {
        return -1;
   }
   bd = &boards[card];
   where = bd->rawList; 
   if (where == 0) return -1;

   TraceLen = (recLenGDig - 7) * 2;

   bzero((char *)outp, 1024 * sizeof(short int));

   for (j = 0; j < ntries; j++) {
      if (where == 0 || (where)->bptr == 0) {
         return -2;
      }
      bcnt = 0;
      lbptr = where->bptr;
      while (bcnt < where->recs) {
         if (getChanNo(lbptr) == channel) {
            optr = (unsigned short *)&lbptr[8];
            for (i = 0; i < TraceLen; i+= 2) {
               outp[i+1]= optr[i];
               outp[i] = optr[i+1];
            }
            return TraceLen;
         }
         bcnt++;
         lbptr += where->len;
      }
      where = where->next;
   }
   return -3;
}


#endif


void *eNodeList;
void *outBufList;
#define OUTBUFSIZE (64 * 1024)

/*
 * Note that this should not be called while running
 */
int resetCEB() {
   int i,j, k, count, listcount;
   struct eNode *delp, *temp;

   printf("resetCEB called\n");
   fflush(stdout);
   count = 0;
   while (eRoot) {
      temp = eRoot;
      eRoot = eRoot->next;
      count++;
      rawEvtFree(temp->delPtr, temp);
   }
   eRootEnd = 0;
   printf("%d event buffers cleared in CEB sorting list\n", count);

   count = 0;
   listcount = 0;
   while (-1 != epicsMessageQueueTryReceive(oQueue,  &delp, 
                                            sizeof(void *))) {
       listcount++;
       while (delp) {
           temp = delp;
           delp = delp->next;
           count++;
           freeListFree(outBufList, temp->bptr);
           freeListFree(eNodeList, temp);
       }
   }
   oQueueCount = 0;
   printf("%d send buffers cleared in %d output lists\n", count, listcount);

   count = 0;
   listcount = 0;
   for (i=0; i < NUMBOARDS; i++) {
      boards[i].latestTime = 0;
      if (boards[i].newENode) {
         freeListFree(eNodeList, boards[i].newENode);
         boards[i].newENode = 0;
      }
      delp = boards[i].rawList;
      listcount++;
      while (delp) {
         count++;
         temp = delp;
         delp = temp->next;
         rawEvtFree(temp->delPtr, temp);
      }
      boards[i].rawList = 0;
      boards[i].rawEnd = 0;
      for (j = 0; j < NUM_DIG_CHANS; j++) {
         boards[i].chanPrevious[j] = 0;
         boards[i].chanPrevTS[j] = 0;
         for (k = 0; k < CHAN_RATE_POINTS; k++) {
            boards[i].chanPeriod[j][k] = 0.0;
         }
         boards[i].chanIndex[j]=0;
         boards[i].chanSamples[j]=0;
      }
      boards[i].colen = 0; 
      if (boards[i].carryover) {
         free(boards[i].carryover);
         boards[i].carryover = 0;
      }
   }
   printf("%d event buffers cleared in %d input lists\n", count, listcount);
   return 0;
}

int resetCEBCounters() {
   int i,j;

   boarderrs = 0;
   numevents = 0;
   numinp = 0;
   g_time_nonmono = 0;
   g_low_time = 0;
   g_truncs = 0;

   for (i=0; i < NUMBOARDS; i++) {
      for (j = 0; j < NCOUNTERS; j++) {
         boards[i].counters[j] = 0;
      }
      for (j = 0; j < NUM_DIG_CHANS; j++) {
         boards[i].chanEvents[j] = 0;
      }
   }
   return 0;
}
   

int initCEB() {
   int i;

   freeListInitPvt(&eNodeList, sizeof(struct eNode), 100);
   freeListInitPvt(&outBufList, OUTBUFSIZE, 20);
   
   eRootSem = epicsMutexCreate();
   if (!eRootSem) return -1;

   eRoot = 0;
   eRootEnd = 0;

   oQueue = epicsMessageQueueCreate(1000, sizeof(void *));
   oQueueCount = 0;
   if (!oQueue) return -1;
   
   for (i=0; i < NUMBOARDS; i++) {
      boards[i].rawList = 0;
      boards[i].rawEnd = 0;
   }

   return 0;
}

/*
 * If you call this at a 1 hz rate, you will get the event rate in the
 * channel.  Designed to be called by device support, from a 1 hz update
 * rate record.
 */
int getChanDiff(short card, short channo) {
   int retval;
   struct boardInfo *bd = 0;

   bd = &boards[card];
   if (bd == 0) return -1;
   retval = bd->chanEvents[channo] - bd->chanPrevious[channo];
   bd->chanPrevious[channo] = bd->chanEvents[channo];
   if (retval < 0) return -2;
   return retval;
}

/* 
 * Gets rate by reciprocal of period.  Period is average of several values
 * kept in an array and updated from elsewhere.  There is no mutual exclusion
 * between reading and writing this array.  Meant to be called by device 
 * support.  CHAN_RATE_POINTS must be 10...
 */
int getChanRate(short card, short channo) {

   int i;
   float avgper = 0;
   struct boardInfo *bd;
 
   bd = &boards[card];
   if (bd == 0) return -1;
   if (bd->chanSamples[channo] == 0) return 0;
   for (i = 0; i < CHAN_RATE_POINTS; i++) {
      avgper += bd->chanPeriod[channo][i];
   }
   bd->chanSamples[channo] = 0;
   if (avgper <= 0) return -1;
   return (int)(1.0e9 / avgper);
}
   


void printBoardError(char *desc, enum errtypes which) {
   int i;

   printf("%s\n", desc);
   for(i = 0; i <= maxGDigCard; i++) {
      printf("%u\t",boards[i].counters[which]); 
   }
   printf("\n");
}

void printChannelEvents(int bdnum) {
   int i;

   printf("board %d: ", bdnum);
   for(i = 0; i < NUM_DIG_CHANS; i++) {
      printf("%u\t",boards[bdnum].chanEvents[i]); 
   }
   printf("\n");
}

void printCrystalCounts(char *desc, int err) {

   printf("%s: ", desc);
   printf("\t%d ", err);
   printf("\n");
}

void printCounters() {
   int i;
   printf("\n");
   printCrystalCounts("Events Handled", numevents);
   printCrystalCounts("Hard board id errors", boarderrs);
   printCrystalCounts("Number events input", numinp);
   printCrystalCounts("Time not monotonic", g_time_nonmono);
   printCrystalCounts("Root sort inserts", g_low_time);
   printCrystalCounts("Sort list truncations", g_truncs);
   printf("board addresses: \n");
   for(i = 0; i <= maxGDigCard; i++) {
      printf("0x%x\t",boards[i].boardaddr); 
   }
   printf("\n");
   printBoardError("Searches... ",searches);
   printBoardError("breakout drop ",drops);
   printBoardError("Carryovers requested... ",coset);
   printBoardError("Carryovers retrieved... ",coget);
   printBoardError("Carryovers successful... ",cosuc);
   printBoardError("Length errors:",length);
   printBoardError("Negative time found:",neg_time);
   printBoardError("Timestamp out of order:",time_nonmono);
   printBoardError("Low timestamp:", low_time);
   printBoardError("Segment Events",events);
   printBoardError("buffers input",bufs);
   printf("\n");
   printf("Channel events by board: \n");
   for(i = 0; i <= maxGDigCard; i++) {
      printChannelEvents(i);
   }
   printf("\n");
}

int checkEvent(struct eNode *where, char *info) {

     static int lastlen = 0; /* all lengths should be the same */
     static int lenErrs = 0;

     if (lastlen == 0 && lenErrs == 0) {
        if (where->len > MAXRECLEN || where->len < 6) {
           printf("%s initial length out of range: %d\n", info, where->len);
           return -1;
        }
        lastlen = where->len;
        printf("%s Initial len %d\n", info, where->len);
     }
     if (lastlen != where->len) {
        lenErrs++;
        /*
        printf("%s delPtr, lastlen, len, lenerrs: 0x%lx %d %d %d\n", 
                         info, (unsigned long)where->delPtr, lastlen, 
                         where->len, lenErrs);
        */
        if (lenErrs > 3) {
           printf("checkEvent Resychronizing with from %d to len %d\n", 
                                   lastlen, where->len);
           lastlen = where->len;
           lenErrs = 0;
        }
     } else {
        lenErrs = 0;
     }
     
     if (*where->bptr != 0xaaaaaaaa) {
        printf("%s Event beginning not separator: 0x%lx 0x%x\n", 
                        info, (unsigned long)where->delPtr, *where->bptr);
        return -1;
     }

     return 0;
}

#define GETPERIOD						\
   if (bd->chanSamples[channo] <= CHAN_RATE_POINTS + 1) {	\
      if (bd->chanSamples[channo] > 0) {			\
         bd->chanPeriod[channo][bd->chanIndex[channo]] = 	\
            timestamp - bd->chanPrevTS[channo];			\
         bd->chanIndex[channo] = 				\
            (bd->chanIndex[channo] + 1) % CHAN_RATE_POINTS;	\
      }								\
      bd->chanSamples[channo]++;				\
      bd->chanPrevTS[channo] = timestamp;			\
   }								

struct eNode *getENode(rawEvt *delPtr, unsigned int *where, 
                        struct boardInfo *bd) {

     struct eNode *retval = 0;
     unsigned int temp, ltemp;
     long long timestamp;
     int channo;
     static int lengthRevealed = 0;

     if (!where) {
        /* this call is a flush. */
        retval = bd->newENode;
        bd->newENode = 0;
        return retval;
     }
     /* get record length and do sanity check before calloc */
     ltemp = rightCol(where + 1) & 0x7ff;
     if  ( ltemp != recLenGDig ) {
        printf("board 0x%x temp and recLenGDig: %d %d\n", bd->boardaddr, ltemp, 
                recLenGDig);
        /* discard bad input and return what we have */
        bd->counters[length]++;
        retval = bd->newENode;
        bd->newENode = 0;
        return retval;
     }

#if 0
     if (bd->newENode && ((bd->newENode->bptr + 
         bd->newENode->recs * bd->newENode->len) != where)) {
        /* where is not in the same buffer as what we have already.  */
        retval = bd->newENode;	/* this will be returned */
        bd->newENode = 0;	/* cause new buffer to be allocated */
     }
#endif

     if (!bd->newENode) 
        bd->newENode = (struct eNode *)freeListCalloc(eNodeList);

     if (!bd->newENode) return 0;


     /* break out timestamp */
     temp = rightCol(where +2);
     temp <<= 16;
     temp += leftCol(where +2);
     timestamp = leftCol(where + 3);
     timestamp <<= 32;
     timestamp |= temp;
     if (timestamp == 0) {
        /* this could happen, if imperative sync is held. It would conflict
	   with the test for a new buffer below, and is an invalid condition
           anyway, so we see it we just bail.
        */
        return retval;
     }
     if (bd->newENode->timestamp == timestamp) {
         /* This is the case where we build a longer buffer up */
         bd->newENode->recs++;
     } else {
         if (bd->newENode->timestamp != 0) {
            /* new timestamp.  We will return the buffer we have been 
             * accumulating and start a new one.
             */
            retval = bd->newENode;
            bd->newENode = (struct eNode *)freeListCalloc(eNodeList);
         }
         /* Start accumulating new buffer */
         bd->newENode->bptr = where;
         bd->newENode->timestamp = timestamp;
         bd->newENode->len = ltemp + 1;
         if (!lengthRevealed) {
            lengthRevealed = 1;
            printf("Length = %d\n", ltemp+1);
         }
         bd->newENode->recs = 1;
         bd->newENode->delPtr = delPtr;
         if (delPtr) rawEvtReserve(delPtr, timestamp, bd->newENode);
     }
     channo = getChanNo((int *)where);
     if (channo > -1 && channo < NUM_DIG_CHANS) {
        bd->chanEvents[channo]++;
        bd->counters[events]++;
        GETPERIOD
     }

     /* checkevent() call left out to simplify things.... */
     /* sanity check call for initial testing */
/*
     if (retval) printf("returning retval with %d events\n", retval->recs);
*/
     return retval;
}

struct eNode *getCarryover(rawEvt *rawBuf,  struct boardInfo *bd) {
     unsigned int *outp;
     struct eNode *retval;

     bd->counters[coget]++;
     /* copy the rest of the record into the carryover buffer */
     outp = bd->carryover + bd->colen;
     while ((rawBuf->curptr - rawBuf->bptr) <= rawBuf->len && 
                  *rawBuf->curptr != 0xaaaaaaaa &&
                  bd->colen < MAXRECLEN) {
         *outp = *rawBuf->curptr;
         bd->colen++; outp++; rawBuf->curptr++;
     }
     /* This call puts it in.  It should be empty and return nothing */
     retval = getENode(0, bd->carryover, bd);
     if (retval) {
        printf("Programming error: data discarded in getCarryover()\n");
     }
     /* This call gets it out */
     retval = getENode(0,0,bd);
     bd->carryover = 0;
     bd->colen = 0;
     bd->counters[cosuc]++;
     return retval;     
}

int addToCarryover(rawEvt *rawBuf,  struct boardInfo *bd) {
         unsigned int *outp;
   
         rawBuf->curptr = rawBuf->bptr;
         outp = bd->carryover + bd->colen;
         while ((rawBuf->curptr - rawBuf->bptr) < rawBuf->len && 
                (outp - bd->carryover < MAXRECLEN)) {
            bd->colen++;
            *outp = *rawBuf->curptr;
             outp++; rawBuf->curptr++;
         }
         return 0;
}


int setCarryover(rawEvt *rawBuf,  struct boardInfo *bd) {
         unsigned int *outp;
   
         bd->counters[coset]++;
         if (bd->carryover) { 
            printf("carryover phase error\n");
         }
         bd->carryover = (unsigned int *)calloc(MAXRECLEN,4);

         outp = bd->carryover;
         bd->colen = 0;
         while ((rawBuf->curptr - rawBuf->bptr) < rawBuf->len && 
                ((outp - bd->carryover) < MAXRECLEN)) {
            bd->colen++;
            *outp = *rawBuf->curptr;
             outp++; rawBuf->curptr++;
         }
         return 0;
}

int getBoardno(unsigned short boardaddr) {
   int i;

   for (i = 0; i <= maxGDigCard; i++)  {
      if (boards[i].boardaddr == boardaddr) {
          return i;
      }
   }
   return -1;
}

int setCESortInfo(int bindex, int boardaddr) {
   
   if (boards[bindex].boardaddr && boards[bindex].boardaddr != boardaddr) {
      return -1;
   }
   if (bindex > maxGDigCard) maxGDigCard = bindex;
   boards[bindex].boardaddr = boardaddr;
   return 0;
}

int setBoardEnable(int bindex, int enab) {
   boards[bindex].enabled = enab;
   return 0;
}

int getBoardEnable(int bindex) {
   return boards[bindex].enabled;
}



long long getLatestTime() {
         long long currentTime;
         int i;

         currentTime = boards[0].latestTime;
         for (i=0; i <= maxGDigCard; i++) {
            if (boards[i].latestTime > currentTime) {
               currentTime = boards[i].latestTime;
            }
         }
         return currentTime;
}


#define INSERTROOT bd->rawList=nextNode;bd->rawEnd=nextNode;return 1;


int addToList(struct eNode *nextNode, struct boardInfo *bd) {
   struct eNode *tptr;

   /* check negative time here */
   if (nextNode->timestamp < 0) {
      bd->counters[neg_time]++;
      rawEvtFree(nextNode->delPtr, nextNode);
      return 0;
   }
   if (!bd->rawEnd) {
      INSERTROOT
   }
   tptr = bd->rawEnd;
   while (tptr && tptr->timestamp > nextNode->timestamp) {
       tptr = tptr->prev;
   }
   if (!tptr) {
      bd->rawList->prev = nextNode;
      nextNode->next = bd->rawList;
      bd->rawList = nextNode;
   } else {
      nextNode->next = tptr->next;
      if (nextNode->next) {
         bd->counters[time_nonmono]++;
         nextNode->next->prev = nextNode;
      } else {
         bd->rawEnd = nextNode;
      }
      tptr->next = nextNode;
      nextNode->prev = tptr;
   }
   return 1;
}
int appendToList(struct eNode *nextNode, struct boardInfo *bd) {

   if (!bd->rawEnd) {
      INSERTROOT
   }
   bd->rawEnd->next = nextNode;
   nextNode->prev = bd->rawEnd;
   bd->rawEnd = nextNode;
   return 1;
}

int dumpSearch = 0;

enum listRawStates  { entry, done, colen_test, wrong_len, equivalent_0, 
                      equivalent_1, test_newbuf, search_loop };
/*
 * get a raw buffer and convert to list format
 */

int listRawEvents(int sort) {

   enum listRawStates state = entry;
   int boardno;
   unsigned int *endptr, *testptr;
   unsigned short boardaddr;
   struct eNode *nextNode, *latestNode = 0;
   rawEvt *rawBuf;
   int RecLen;
   struct boardInfo *bd;
   int noEventsReserved = 1;

   /*
    * getRawBuf() is supplied either by readout code or test program.
    */
   rawBuf = getRawBuf(&boardaddr);

   /* In the test environment, this will break at end of file. 
    * In the IOC, it will happen whenever the raw Q is empty
    */
   if (boardaddr == 255) {
      /* end of file, used in readFile.c */
      return -3;
   }
   if (boardaddr == 127) {
      /* read errors from readFile.c */
      return -4;
   }
   if (rawBuf == 0) return -2;

   if (boardaddr == 0) {
      printf("error boardaddr == 0\n");
      boarderrs++;
      rawEvtFree(rawBuf, 0);
      return -1;
   }

   boardno = getBoardno(boardaddr);

   if (boardno < 0 || boardno > maxGDigCard) {
      printf("error boardno %d\n", boardno);
      boarderrs++;
      rawEvtFree(rawBuf, 0);
      return -1;
   }

   bd = &boards[boardno];

   if (rawBuf->len == 0) {
      bd->latestTime = getLatestTime();
      rawEvtFree(rawBuf, 0);
/*
      printf("%lld: empty board %d \n", bd->latestTime, boardno);
*/
      return -1;
   }

   bd->counters[bufs]++;

   /* traverse the raw buffer extracting events */

   endptr = rawBuf->bptr + rawBuf->len - 1;
   testptr = rawBuf->bptr;
   RecLen = recLenGDig;

   while (state != done) {
      switch (state) {
         case entry:
            /* 2 */
            if (rawBuf->curptr > endptr) {
               bd->colen = 0;
               state = done;
               break;
            } 
            /* 3 */
            /* 6 */
            if (testptr > endptr) {
                state = colen_test;
                break;
            } 
            /* 5 */
            /* 11 */
            if (*testptr != 0xaaaaaaaa) {
               state = test_newbuf;
               break;
            }
            /* 7 */
            /* 8 */
            if ((testptr - rawBuf->curptr) != (RecLen + 1)) {
               state = wrong_len;
               break;
            }
            /* 27 */
            nextNode = getENode(rawBuf, rawBuf->curptr, bd);
            /* 15 */
            if (nextNode) {
               noEventsReserved = 0;
               if (sort) {
                  addToList(nextNode, bd);
               } else {
                  appendToList(nextNode, bd);
               }
               if (latestNode == 0)
                  bd->latestTime = nextNode->timestamp;
               latestNode = nextNode;
            }
            state = equivalent_0;
            /* fall-through */
         case equivalent_0:
            /* 16 */
            bd->colen = 0;	/* sometimes already true. */
            /* 10 */
            rawBuf->curptr = testptr;	/* ditto */
            testptr = rawBuf->curptr + RecLen + 1;
            state = entry;
            break;
         case colen_test:
            /* 25 */
            if (bd->colen == 0) {
               setCarryover(rawBuf, bd);
            } else {
               /* 26 */
               addToCarryover(rawBuf, bd);
            }
            /* 25 */
            /* 26 */
            state = done;
            break;
         case wrong_len:
            /* 9 */
            if (bd->colen == 0) {
               bd->counters[drops]++;
               state = equivalent_0;
               break;
            } else {
               state = equivalent_1;
            }
            /* 13 */
            /* fall-through */
         case equivalent_1:
            /* 14 */
            nextNode = getCarryover(rawBuf, bd);
            /* 15 */
            if (nextNode) {
               if (sort) {
                  addToList(nextNode, bd);
               } else {
                  appendToList(nextNode, bd);
               }
               if (latestNode == 0)
                  bd->latestTime = nextNode->timestamp;
               latestNode = nextNode;
            }
            /* 16 */
            bd->colen = 0;
            /* 10 */
            testptr = rawBuf->curptr + RecLen + 1;
            state = entry;
            break;
         case test_newbuf:
            /* 12, 17 */
            if (rawBuf->curptr == testptr && bd->colen != 0) {
               state = equivalent_1;
               break;
            }
            /* 19 */
            bd->counters[searches]++;
            testptr = rawBuf->curptr + 1;
            state = search_loop;
         case search_loop:
            /* 23 */
            if (testptr > endptr) {
               state = done;
               break;
            }
            /* 20 */
            /* 21 */
            if (*testptr != 0xaaaaaaaa) {
               testptr++;
               state = search_loop;
            } else {
               /* 22 */
               state = equivalent_0;
            }
            break;
         default:
            printf("This is not possible....\n");
            state = done;
            break;
      }
   }

   /* traversal complete. Flush and clean up */

   nextNode = getENode(0, 0, bd);
   if (nextNode) {
      noEventsReserved = 0;
      if (sort) {
         addToList(nextNode, bd);
      } else {
         appendToList(nextNode, bd);
      }
      if (latestNode == 0)
         bd->latestTime = nextNode->timestamp;
      latestNode = nextNode;
   }

   if (noEventsReserved) {
      rawEvtFree(rawBuf, 0);
      return -1;
   }

   return boardno;
}

struct eNode *getOutput() {
   struct eNode *retptr = 0;

   if (-1 == epicsMessageQueueTryReceive(oQueue, &retptr, 
                                          sizeof(void *)))
   { 
       return (0);
   } 
   oQueueCount--;
   return retptr;
}



#define EVENTGAP 10


int harvestOutput() {
   struct eNode *tptr = 0, *prptr = 0, *gptr = 0;
   int stat;
   struct eNode *retptr, *cretptr, *cretptrend;
   unsigned int *outptr;
   long long currentTime, latestTime;
   int evtlen;

   epicsMutexLock(eRootSem);

   currentTime = getCurrentTime();
   latestTime = getLatestTime();
   currentTime = currentTime - 100000 * (g_low_time * g_low_time);
   tptr = eRoot;
   if (!tptr) {
      epicsMutexUnlock(eRootSem);
      return 0;
   }
   retptr = tptr;

   while (tptr) { 
      if (tptr->timestamp >= currentTime) {
         break;
      }
      /* look for crystal event time gap */
      if (prptr && tptr->timestamp > (prptr->timestamp + EVENTGAP))
      {
         gptr = prptr;
      }
      numevents += tptr->recs;
      prptr = tptr;
      tptr = tptr->next;
   }
   if (gptr == 0) {	/* no gap found. */
      epicsMutexUnlock(eRootSem);
      return 0;
   }
   eRoot = gptr->next;
   if (eRoot) { eRoot->prev = 0; } else { eRootEnd = 0; }
   gptr->next = 0;
   epicsMutexUnlock(eRootSem);
   /* here is where we consolidate segment events into output buffers */
   cretptr = (struct eNode *)freeListCalloc(eNodeList);
   if (!cretptr) {
      printf("unable to alloc memory for data in harvestOutput() \n");
      return 0;
   }
   cretptr->bptr = freeListMalloc(outBufList);
   outptr = cretptr->bptr;
   tptr = retptr;
   evtlen = retptr->len * 4;
   cretptr->recs = 0;
   cretptr->len = retptr->len;
   cretptrend = cretptr;
   while (tptr) {
      while ((outptr + (evtlen * tptr->recs)/4) < 
             (cretptrend->bptr + OUTBUFSIZE/sizeof(int))) {
         bcopy((char *)tptr->bptr, (char *)outptr, 
                                   evtlen * tptr->recs);
         outptr += (evtlen * tptr->recs)/sizeof(int);
         cretptrend->recs += tptr->recs;
         gptr = tptr->next;
         rawEvtFree(tptr->delPtr, tptr);
         tptr = gptr;
         if (!tptr) break;	/* end of data */
      }
      if (tptr) {
         cretptrend->next = (struct eNode *)freeListCalloc(eNodeList);
         if (!cretptrend->next) {
            printf("unable alloc memory for data in harvestOutput() \n");
            return 0;
         }
         cretptrend = cretptrend->next;
         cretptrend->recs = 0;
         cretptrend->len = tptr->len;
         cretptrend->bptr = freeListCalloc(outBufList);
         outptr = cretptrend->bptr;
      }
   }
   stat = epicsMessageQueueSend(oQueue, &cretptr, sizeof(void *));
   if (stat == 0) oQueueCount++;
   return 0;
}

int purgeOutput() {
   struct eNode *tptr = 0, *prptr = 0;
   struct eNode *retptr;
   long long currentTime, latestTime;

   epicsMutexLock(eRootSem);

   currentTime = getCurrentTime();
   latestTime = getLatestTime();
   currentTime = currentTime - 100000 * (g_low_time * g_low_time);
   tptr = eRoot;
   if (!tptr) {
      epicsMutexUnlock(eRootSem);
      return 0;
   }
   retptr = tptr;

   while (tptr) { 
      if (tptr->timestamp >= currentTime) {
         break;
      }
      numevents += tptr->recs;
      prptr = tptr;
      tptr = tptr->next;
      rawEvtFree(prptr->delPtr, prptr);
   }
   eRoot = tptr;
   if (eRoot) { eRoot->prev = 0; } else { eRootEnd = 0; }
   epicsMutexUnlock(eRootSem);
   return 0;
}

int grabOutput() {
   struct eNode *tptr, *gptr;
   struct eNode *retptr,  *cretptr, *cretptrend;
   unsigned int *outptr;
   int evtlen;
   int stat;

   epicsMutexLock(eRootSem);

   retptr = eRoot;
   eRoot = 0;
   eRootEnd = 0;

   epicsMutexUnlock(eRootSem);
   /* here is where we "compress" the list */
   cretptr = (struct eNode *)freeListCalloc(eNodeList);
   if (!cretptr) {
      printf("unable to alloc memory for data in grabOutput() \n");
      return 0;
   }
   cretptr->bptr = freeListCalloc(outBufList);
   outptr = cretptr->bptr;
   tptr = retptr;
   evtlen = retptr->len * 4;
   cretptr->recs = 0;
   cretptr->len = retptr->len;
   cretptrend = cretptr;
   while (tptr) {
      while ((outptr + (evtlen * tptr->recs)/4) < 
             (cretptrend->bptr + OUTBUFSIZE/sizeof(int))) {
         bcopy((char *)tptr->bptr, (char *)outptr, 
                                   evtlen * tptr->recs);
         numevents += tptr->recs;
         outptr += (evtlen * tptr->recs)/sizeof(int);
         cretptrend->recs += tptr->recs;
         gptr = tptr->next;
         rawEvtFree(tptr->delPtr, tptr);
         tptr = gptr;
         if (!tptr) break;	/* end of data */
      }
      if (tptr) {
         cretptrend->next = (struct eNode *)freeListCalloc(eNodeList);
         if (!cretptrend->next) {
            printf("unable alloc memory for data in harvestOutput() \n");
            return 0;
         }
         cretptrend = cretptrend->next;
         cretptrend->recs = 0;
         cretptrend->len = tptr->len;
         cretptrend->bptr = freeListCalloc(outBufList);
         outptr = cretptrend->bptr;
      }
   }
   stat = epicsMessageQueueSend(oQueue, &cretptr, sizeof(void *));
   if (stat == 0) oQueueCount++;

   return 0;
}


int printTSError = 0;
static int g_high_time;
   
int sortToOutput(int boardno) {

   struct boardInfo *bd;
   long long testtime = 0;
   struct eNode *focus, *testpoint;

   if (boardno < 0) return -1;

   bd = &boards[boardno];

   if (!bd->rawEnd) return -1;

   epicsMutexLock(eRootSem);
   if (!eRoot) {
      eRoot = bd->rawList;
      eRootEnd = bd->rawEnd;
      epicsMutexUnlock(eRootSem);
      bd->rawList = 0;
      bd->rawEnd = 0;
      return 0;
   }
   focus = bd->rawEnd;
   testpoint = eRootEnd;
 
   /* This is a defense against spurious high time stamps. These can tie up
      raw data buffers indefinitely, so it is a severe case, but may never 
      actually occur any more.
      Possibly unnecessary code.
    */
   if (focus->timestamp < testpoint->timestamp) {
      g_high_time++;
   } else {
      g_high_time = 0;
   }
   if (g_high_time > 200) {
       testtime = testpoint->timestamp;
       g_truncs++;
       g_high_time = 0;
       while (testpoint && testpoint->timestamp == testtime) {
          focus = testpoint;
          testpoint = testpoint->prev;
          testpoint->next = 0;
          eRootEnd = testpoint;
          rawEvtFree(focus->delPtr, focus); 
      }
      if (!testpoint) {
         eRoot = 0;
      }
      focus = bd->rawEnd;
      testpoint = eRootEnd;
   }
   /* end possibly unnecessary code */

   while (focus) {
      while (testpoint && focus->timestamp < testpoint->timestamp) {
         if (testpoint->prev && 
             testpoint->prev->timestamp > testpoint->timestamp) {
            /* This is an indicator, not an accurate count */
            g_time_nonmono++;
         }
         testpoint = testpoint->prev;
      }
      bd->rawEnd = focus->prev;
      focus->prev = testpoint;
      if (!testpoint) {
         focus->next = eRoot;
         if (eRoot) eRoot->prev = focus;
         eRoot = focus;
      } else {
         focus->next = testpoint->next;
         focus->prev = testpoint;
         if (focus->next) focus->next->prev = focus;
         testpoint->next = focus;
      }
      if (0 == focus->next) {
         eRootEnd = focus;
      }
      focus = bd->rawEnd;
   }
   bd->rawList = 0;
   epicsMutexUnlock(eRootSem);

   return 0;
}
        

int listToOutput(int boardno) {

   struct boardInfo *bd;

   if (boardno < 0) return -1;

   bd = &boards[boardno];

   epicsMutexLock(eRootSem);
   if (!eRoot) {
      eRoot = bd->rawList;
   } else {
      eRootEnd->next = bd->rawList;
      bd->rawList->prev = eRootEnd;
   }
   eRootEnd = bd->rawEnd;
   epicsMutexUnlock(eRootSem);
   bd->rawEnd = 0;
   bd->rawList = 0;
   return 0;
}


int getNumEvents() { return numevents; }
   
/*
 * Next several functions are all to interface the Output list from this
 * code to the consumer.
 */


void printNextTS() {
   if (eRoot) {
      printf("next time: 0x%lld \n", eRoot->timestamp);
   }
}

/*
 * This is to help the sender look for gaps by checking the time of the next
 * event.
 */
long long getNextTime() {
   if (eRoot) 
      return eRoot->timestamp;
   return 0;
}

/* Timestamps only 48 bits */
#define EXCESSIVE_TIME  0x1000000000000ll 

long long getCurrentTime() {
   long long currentTime = EXCESSIVE_TIME;
   int i;

   for (i=0; i <= maxGDigCard; i++) {
      if (boards[i].enabled &&  boards[i].latestTime < currentTime) {
         currentTime = boards[i].latestTime;
      }
   }
   /* the following will happen if all channels are disabled */
   if (currentTime == EXCESSIVE_TIME) {
      currentTime = 0;
   }
   return currentTime;
}
int getLastBoard() {
   long long currentTime = EXCESSIVE_TIME;
   int retval = -2;
   int i;
   int num_enabled = 0;
   
   epicsMutexLock(eRootSem);
   for (i=0; i <= maxGDigCard; i++) {
      if (boards[i].enabled) {
         num_enabled++;
         if (boards[i].latestTime < currentTime) {
            currentTime = boards[i].latestTime;
            retval = i;
         }
      }
   }
   epicsMutexUnlock(eRootSem);
   if (num_enabled == 1) retval = -1;
   return retval;
}
int printLatestTimes() {
   int i;

   for (i=0; i <= maxGDigCard; i++) {
      printf("boardno %2d addr 0x%4hx currentTime 0x%lld\n", i, 
                               boards[i].boardaddr, boards[i].latestTime);
   }
   return 0;
}

long long getCardLatest(short cardno) {
   return boards[cardno].latestTime;
}


/* 
 * Clear events and rates at end of run 
 */
int clearEvents() {
   struct eNode *temp, *tptr;
   int i;

   epicsMutexLock(eRootSem);
   temp = eRoot;
   eRoot = 0;
   eRootEnd = 0;
   epicsMutexUnlock(eRootSem);
   i = 0;
   while ((tptr = temp)) {
      temp = tptr->next;
      numevents += tptr->recs;
      i+= tptr->recs;
      rawEvtFree(tptr->delPtr, tptr);
   }
   return i;
}

int printSome() {

      int i;
      long long times[100];
      struct eNode *temp;

      epicsMutexLock(eRootSem);

      temp = eRoot;
      
      for (i = 0; i < 100; i++) {
         if (temp) {
            times[i] = temp->timestamp;
            temp = temp->next;
         } else {
            times[i] = 0;
         }
      }
      epicsMutexUnlock(eRootSem);

      for (i = 0; i < 100; i++) {
         printf("0x%llx\n", times[i]);
         if (!times[i]) break;
      }
      return 0;
}

static const iocshFuncDef printCountersFuncDef = {"printCounters",0,0};
static void printCountersCallFunc(const iocshArgBuf *args)
{
    printCounters();
}

void PCRegistrar(void)
{
   iocshRegister(&printCountersFuncDef, printCountersCallFunc);
}
epicsExportRegistrar(PCRegistrar);
