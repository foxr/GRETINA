/* 
 * GRETINA event server code  
 *
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#ifdef vxWorks
#include <hostLib.h>
#include <sockLib.h>
#else
#include <strings.h>
#include <sys/socket.h>
#endif



#include "sender.h"

#define EVENT_MARKER 0xaaaaaaaa

#include <epicsMessageQueue.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <freeList.h>

#include "eventSource.h"
#include "psNet.h"

/* previously in senderNet.h */
#define CLIENT_REQUEST_EVENTS 1
#define SERVER_NORMAL_RETURN 2
#define SERVER_SENDER_OFF 3
#define SERVER_SUMMARY 4

#ifndef FIFO_SPEED_TEST
static
#endif
long long bytesSent;


static long long eventsSent;

/* #include "readFIFO.h" */
#include "cesort.h"

int senderRunning;

epicsMessageQueueId reqQ;

void *outbufList;

int senderInit() {

  initSenderNet();
  freeListInitPvt(&outbufList, TARGSIZE, 10);
  return 0;
}
/*
   call this at one hz to get event rate
*/
int getEventsSentDiff() {
   static long long oldEvents = 0;
   int retval;

   if (eventsSent < 0)	{ /* overflow */
      eventsSent = 0;
      oldEvents = 0;
      return 0;
   }
   retval = (eventsSent - oldEvents);
   oldEvents = eventsSent;
   return (retval);
}

static double KB = 1024;	

/*
   call this at one hz to get readout rate in KB/S
*/
long long bytesRead;

float getKBReadDiff() {
   static long long oldBS = 0;
   double retval;

   retval = (bytesRead - oldBS)/KB;
   oldBS = bytesRead;
   return (retval);
}
/*
   call this at one hz to get sending rate in KB/S
*/
float getKBSentDiff() {
   static long long oldBS = 0;
   double retval;

   retval = (bytesSent - oldBS)/KB;
   oldBS = bytesSent;
   return (retval);
}

int generateRetStruct(struct incoming *req) {

   evtServerRetStruct *retstruct;
   struct eNode *tptr;

   req->request[req->len-1] = 0;   /* null terminate request string area */
   retstruct = (evtServerRetStruct *)&req->request[0];
   retstruct->type = htonl(SERVER_SUMMARY);
   retstruct->status = htonl(0);
/*
   retstruct->recLen = htonl(tptr->len);
*/
   retstruct->recLen = htonl(recLenGDig);
   req->outlist = getOutput();
   tptr = req->outlist;
   retstruct->recs = 0;
   while (tptr) {
      retstruct->recs += tptr->recs;
      tptr = tptr->next;
   }
   if (retstruct->recs) {
      retstruct->status = htonl(0);
      retstruct->recs = htonl(retstruct->recs);
      return 0;
   } else {
      retstruct->status = htonl(5);
      return -1;
   }
}

int printNextTime() {

    printf("next time = %lld\n", getNextTime());
    return 0;
} 

extern void *eNodeList;
extern void *outBufList;

int  sendData(unsigned long requlong ) {
   struct incoming *req;
   struct eNode *outp, *temp;
   evtServerRetStruct *retstruct;
   int sentno = 0;
   int evtlen;
   int retval = 0;
   int msgsent = 0;

   req = (struct incoming *)requlong;
   retstruct = (evtServerRetStruct *)req->request;
   evtlen = (ntohl(retstruct->recLen) + 1) * 4;

   outp = req->outlist;

   while (outp) {
      sentno = send(req->clnt, (char *)outp->bptr, evtlen * outp->recs, 0);
      if (sentno != evtlen * outp->recs) {
         /* this error is not well handled.  Does it ever happen? */
         if (msgsent < 5) {
            printf("send returned %d not %d %s\n", sentno, 
                 evtlen * outp->recs, strerror(errno));
            msgsent++;
         }
         retval = -1;
      } else { 
         bytesSent += sentno;
         eventsSent += outp->recs;
      }
      temp = outp->next;
      freeListFree(outBufList, outp->bptr);
      freeListFree(eNodeList, outp);
      outp = temp;
   }

   /* begin inline sendLastPacket */

   if (-1 == retval) {
           printf("final sendEvents failed!\n");
   }
   /* other side originally closed at this point but no longer is expected 
      to do so */
   return retval;
}

int sendReply(unsigned long requlong, int err, int eventGap) {
   evtServerRetStruct *retstruct;
   struct incoming *req;
   int retval;

   req = (struct incoming *)requlong;
   retstruct = (evtServerRetStruct *)req->request;

   if (!err) {
     retval = generateRetStruct(req);
     if (retval != 0) {
       retstruct->type = htonl(INSUFF_DATA);
       retval = -1;
     }
   } else {
      retstruct->type = htonl(err);
      if (err != 3) {
         printf("Return error from server %d\n", err);
      }
      retval = -1;
   }
   if (sendRetStruct(req)) {
       perror("sendReply: sendRetStruct failed");
       retval = -2;
   }

   return retval;
}

void closeRequest(unsigned long requlong) {
   struct incoming *req;

   req = (struct incoming *)requlong;

   close(req->clnt);
   free(req);
   req = 0;

   return;
}
void serverThread(void *args) {
   unsigned long *largs, requlong;
   int gap;
   int retstat;
   int datstat = 0;
   int tries = 0;

   largs = (unsigned long *)args;

   requlong = largs[0];
   gap = largs[1];

   free(args);

   printf("server thread start\n");

   /* first reply to initial request */
   retstat = sendReply(requlong, 0, gap);
   if (!retstat) {
      sendData(requlong);
   } else if (retstat == -2) {
      closeRequest(requlong);
      return;
   }
   
   while (senderRunning) {
       /* wantsMoreData no longer required to be non-blocking */
       datstat = wantsMoreData(requlong);
       if (datstat == 0) break;		/* eof */
       if (datstat == -1) {		/* error */
          epicsThreadSleep(0.1);
          continue;
       }
       retstat = sendReply(requlong, 0, gap);
       if (retstat) {
          if (retstat == -2) {
             closeRequest(requlong);
             return;
          }
       } else { 
          sendData(requlong);
       } 
   }
   /* wait for other side to close */
   while (datstat && tries < 5) {
      datstat = wantsMoreData(requlong);
      if (datstat == -1) epicsThreadSleep(1.0);
      tries++;
   }
   if (datstat) printf("Data client did not close connection\n");
   closeRequest(requlong);
}

void runServerInstance(unsigned long requlong, int gap) {

   unsigned long  *args;

   if (!senderRunning) {
      sendReply(requlong, 3, 0);
      closeRequest(requlong);
      return;
   }
   args = calloc(2, sizeof(unsigned long));
   args[0] = requlong;
   args[1] = gap;

   if (0 == epicsThreadCreate("dataServerT", 5, 20000, 
                      serverThread, (void *)args)) {
      printf("server thread create failed\n");
      closeRequest(requlong);
   }

}

void senderControl(int cont) {

   senderRunning = cont;
}

/* previously in senderNet.c */

unsigned int bytesent;

#ifdef GARBAGE_COLLECT
long long lastTimeSent = 0;
#endif

int printPackets() {
   printf("Number of bytes sent: %ud\n", bytesent);
   return 0;
}


static  int s;

int initSenderNet() {
  struct sockaddr_in our_addr;
  char hname[80];
#ifndef vxWorks
  struct hostent *hp;
#endif

  if (s > 0) close(s);

  s = socket(AF_INET, SOCK_STREAM, 0);
  if (s == -1) {
     printf("socket call failed in initSenderNet\n");
     return -1;
  }
 
  bzero((char *)&our_addr,sizeof(struct sockaddr_in));
  our_addr.sin_family = AF_INET;
  our_addr.sin_port = htons(SERVER_PORT);
 
  if  (gethostname(hname,79)) {
     printf("our host name unavailable in request grabber\n");
     return -1;
  }

#ifndef vxWorks

  printf("HostName = %s\n", hname);

  hp = gethostbyname(hname);

  if (!hp) {
      printf("could not get host entry for %s\n", hname);
      return -1;
  }

  if (hp->h_addrtype == AF_INET) {
     our_addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)
                                (hp->h_addr_list[0]))));
  } else { 
     printf("host not on network??\n");
     return -1;
  }
#else
  our_addr.sin_addr.s_addr = hostGetByName(hname);
#endif

  if (our_addr.sin_addr.s_addr == -1) {
     printf("Error setting up our receiving address in request grabber\n");
     return -1;
  }

  if (-1 == bind(s, (struct sockaddr *)&our_addr,
                       sizeof(struct sockaddr_in)))  {
     printf("request grabber unable to bind our address\n");
     return -1;
  }

  if (-1 == listen(s, 10)) {
     printf("request grabber unable to listen our address\n");
     return -1;
  }

  printf("hname and port: %s %d\n", hname, SERVER_PORT);

  return 0;
}

unsigned long getRequest() {

     struct incoming *buf;
     int insize;

     buf = (struct incoming *)malloc(sizeof(struct incoming));
     if (!buf) {
         printf("out of memory in getRequest\n");
         return(0);
     }

     buf->clnt = accept(s, 0, 0);
     if (buf->clnt == -1) {
        printf("getRequest accept failed %s\n", strerror(errno));
        close(buf->clnt);
        free(buf);
        return(0);
     }

     insize = read(buf->clnt, buf->request, sizeof(struct reqPacket));
     if (insize <= 0) {
        printf("requestgrabber failed to read request\n");
        close(buf->clnt);
        free(buf);
        return(0);
     }
     buf->len = insize;
     while (buf->len < sizeof(struct reqPacket)) {
        insize = read(buf->clnt, buf->request+buf->len, 
                         sizeof(evtServerRetStruct) - buf->len);
        if (insize <= 0) {
           printf("requestgrabber failed to read request\n");
           close (buf->clnt);
           free(buf);
           return(0);
        }
        buf->len += insize;
     }
     return (unsigned long)buf;
}
int wantsMoreData(unsigned long int requlong) {

     struct incoming *buf;
     int insize;

     buf = (struct incoming *)requlong;

     insize = recv(buf->clnt, buf->request, sizeof(struct reqPacket), 0);
     /* -1 indicates no request present.  0 indicates eof */
     if (insize <= 0) return insize;
     buf->len = insize;
     while (buf->len < sizeof(struct reqPacket)) {
        insize = recv(buf->clnt, buf->request+buf->len, 
                         sizeof(evtServerRetStruct) - buf->len, 0);
        if (insize <= 0) {
           printf("wantsMoreData failed to read request\n");
           return(-1);
        }
        buf->len += insize;
     }
     return buf->len;
}


int  sendRetStruct(struct incoming *req) {

   int sentno = 0;
   int totalout = 0;

   while (totalout < sizeof(evtServerRetStruct) && sentno > -1) {
      sentno = write(req->clnt, req->request, 
                     sizeof(evtServerRetStruct) - totalout);
      totalout += sentno;
   }


   if (sentno == -1) {
       return -1;
   }
   return 0;
}

