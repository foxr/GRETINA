int senderInit();
int sendReply(unsigned long requlong, int err, int eventGap);
void closeRequest(unsigned long requlong);
int  sendData(unsigned long requlong);
int  wantsMoreData(unsigned long requlong);
unsigned long getRequest();
void senderControl(int really);
float getKBSentDiff();
float getKBReadDiff();
int getEventsSentDiff();
void runServerInstance(unsigned long requlong, int gap);


