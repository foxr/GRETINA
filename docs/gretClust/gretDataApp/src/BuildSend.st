program  BuildSend ("P=A") 

option +d;
option +r;

%%#include "readFIFO.h"
%%#include "sender.h"
%%#include "cesort.h"
%%#include "OnMonSender.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

DECLARRAY(short, traceData, DAQC{CN}_CV_Trace,1024)
DECLMON(int, traceBoard, DAQC{CN}_CS_TraceBd)
DECLMON(int, traceChan, DAQC{CN}_CS_TraceChan)
DECL(int, traceLen, DAQC{CN}_CV_TraceLen)

DECL(short,senderRunning,DAQC{CN}_CV_SenderRunning)
DECLEVENT(short,senderWrite,DAQC{CN}_CS_SenderWrite)
DECLEVENT(short,AcqRun,Online_CS_StartStop)
DECLMON(short,AcqRunRO,DAQC{CN}_CS_AcqRunRO)
DECLMON(int,eventGap,DAQG_CS_EventGap)   /* timestamp units, ie, 10 ns */
DECLMON(short,buildEnable,DAQG_CS_BuildEnable)
DECLMON(short,saveData,Online_CS_SaveData)

DECLEVENT(string, OnMonRcvr, OnMon{CN}_CS_RcvrIP)
DECLMON(int, OnMonNumToSend, OnMon{CN}_CS_NumToSend)
DECL(int, OnMonAttStat, OnMon{CN}_CV_AttStat)
DECL(int, OnMonCount, OnMon{CN}_CV_Count)

int OnMonStat;

evflag requestAvail;
evflag senderWaiting;
evflag senderRan;
evflag buildInit;
evflag buildInit2;
evflag requestInit;
evflag senderInitE;
evflag rawFlag;
evflag dataAvail;
evflag builtFlag;

%%extern int readFIFOBufs;
int datstat;
int gc;
int refreshTrace;
int outputList;

unsigned long clientReq;	/* actually struct incoming * */
unsigned long eventBd;	        /* board number */

ss eventBuild {
   state init {
      when() {
         initCEB();
         OnMonAttStat = OnMonAttach(OnMonRcvr);
         pvPut(OnMonAttStat);
         efSet(buildInit);
         AcqRunRO = 0;
         pvPut(AcqRunRO);
         efClear(OnMonRcvrEvent);
      } state stopped
   }
   state stopped {
      when (AcqRun) {
         resetCEBCounters();
         AcqRunRO = 1;		/* turn on readout */
         pvPut(AcqRunRO);
      }  state testSender
      when (efTestAndClear(OnMonRcvrEvent)) {
           OnMonAttStat = OnMonAttach(OnMonRcvr);
           pvPut(OnMonAttStat);
      } state stopped
   }
   state testSender {
      when (saveData) {
         senderWrite = 1;	/* turn on main sender */
         pvPut(senderWrite);
      } state saveTestBuild
      when () {
      } state noSaveTestBuild
   }
   state saveTestBuild {
      when (buildEnable) {
      } state runSaveBuild
      when () {
      } state runSaveNoBuild
   }
   state noSaveTestBuild {
      when (buildEnable) {
      } state runNoSaveBuild
      when () {
      } state runNoSaveNoBuild
   }
   state runNoSaveNoBuild {
      when(efTestAndClear(rawFlag)) {
        if (OnMonAttStat == 0) {
           OnMonStat = OnMonSend(eventBd, OnMonNumToSend);
           if (!OnMonStat) {
              OnMonCount++;
           } else if (OnMonStat == -1) {
              OnMonAttStat = -1; pvPut(OnMonAttStat); 
           }
        }
        if (refreshTrace && traceBoard == eventBd) {
           traceLen = getRawTrace(traceBoard, traceChan, traceData);
           pvPut(traceLen);
           if (traceLen > 0)  {
              pvPut(traceData);
              refreshTrace = 0;
           }
        }
        listToOutput(eventBd);
        if (outputList) {
            outputList = 0;
            clearEvents();
        }
        efSet(builtFlag);
      } state runNoSaveNoBuild
      when (efTestAndClear(OnMonRcvrEvent)) {
           OnMonAttStat = OnMonAttach(OnMonRcvr);
           pvPut(OnMonAttStat);
      } state runNoSaveNoBuild
      when (delay(1) && !AcqRun) { 	/* allow data to trickle in */
         resetCEB();
         OnMonCount = 0;
      } state stopped
   }
   state runNoSaveBuild {
      when(efTestAndClear(rawFlag)) {
        if (OnMonAttStat == 0) {
           OnMonStat = OnMonSend(eventBd, OnMonNumToSend);
           if (!OnMonStat) {
              OnMonCount++;
           } else if (OnMonStat == -1) {
              OnMonAttStat = -1; pvPut(OnMonAttStat); 
           }
        }
        if (refreshTrace && eventBd == traceBoard) {
           traceLen = getRawTrace(traceBoard, traceChan, traceData);
           pvPut(traceLen);
           if (traceLen > 0) {
              pvPut(traceData);
              refreshTrace = 0;
           }
        }
        sortToOutput(eventBd);
        if (outputList) {
            outputList = 0;
            purgeOutput();
        }
        efSet(builtFlag);
      } state runNoSaveBuild
      when (efTestAndClear(OnMonRcvrEvent)) {
           OnMonAttStat = OnMonAttach(OnMonRcvr);
           pvPut(OnMonAttStat);
      } state runNoSaveBuild
      when (delay(1) && !AcqRun) { 	/* allow data to trickle in */
         resetCEB();
         OnMonCount = 0;
      } state stopped
   }
   state runSaveNoBuild {
      when(efTestAndClear(rawFlag)) {
        if (OnMonAttStat == 0) {
           OnMonStat = OnMonSend(eventBd, OnMonNumToSend);
           if (!OnMonStat) {
              OnMonCount++;
           } else if (OnMonStat == -1) {
              OnMonAttStat = -1; pvPut(OnMonAttStat); 
           }
        }
        if (refreshTrace && eventBd == traceBoard) {
           traceLen = getRawTrace(traceBoard, traceChan, traceData);
           pvPut(traceLen);
           if (traceLen > 0) {
              pvPut(traceData);
              refreshTrace = 0;
           }
        }
        listToOutput(eventBd);
        if (outputList) {
           outputList = 0;
            if (getOQueueCount() < 20) {
               grabOutput();
            }
        }
        efSet(builtFlag);
      } state runSaveNoBuild
      when (efTestAndClear(OnMonRcvrEvent)) {
           OnMonAttStat = OnMonAttach(OnMonRcvr);
           pvPut(OnMonAttStat);
      } state runSaveNoBuild
      when (delay(1) && !AcqRun) { 	/* allow data to trickle in */
         resetCEB();
         OnMonCount = 0;
      } state stopped
   }
   state runSaveBuild {
      when(efTestAndClear(rawFlag)) {
        if (OnMonAttStat == 0) {
           OnMonStat = OnMonSend(eventBd, OnMonNumToSend);
           if (!OnMonStat) {
              OnMonCount++;
           } else if (OnMonStat == -1) {
              OnMonAttStat = -1; pvPut(OnMonAttStat); 
           }
        }
        if (refreshTrace && eventBd == traceBoard) {
           traceLen = getRawTrace(traceBoard, traceChan, traceData);
           if (traceLen > 0) { 
              pvPut(traceLen);
              refreshTrace = 0;
              pvPut(traceData);
           }
        }
        sortToOutput(eventBd);
        if (outputList) {
            outputList = 0;
            if (getOQueueCount() < 20) {
                harvestOutput();
            }
        }
        efSet(builtFlag);
      } state runSaveBuild
      when (efTestAndClear(OnMonRcvrEvent)) {
           OnMonAttStat = OnMonAttach(OnMonRcvr);
           pvPut(OnMonAttStat);
      } state runSaveBuild
      when (delay(1) && !AcqRun) { 	/* allow data to trickle in */
         resetCEB();
         OnMonCount = 0;
      } state stopped
   }
}

/* state set newTrace just sets the flag periodically */
ss newTrace {
   state run {
      when (delay(0.5)) {
         refreshTrace = 1;
         pvPut(OnMonCount);
      } state run
   }
}

ss newList {
   state run {
      when (delay(0.1)) {
         outputList = 1;
      } state run
   }    
}       

/*
  state set getEvents wraps listRawEvents(), which can block waiting for raw
  input. getEvents is allowed to be unresponsive.
*/
ss getEvents {
   state init {
      when (efTestAndClear(buildInit) ) {
         efSet(buildInit2);
      } state getRaw
   }
   state getRaw {
      when () {
         /* this blocks until a buffer is input */
         eventBd = listRawEvents(buildEnable);  
         efSet(rawFlag);
      } state waitfordone
   }
   state waitfordone {
      when (efTestAndClear(builtFlag)) {
      } state getRaw
   }
}

ss requestTest {
   state init {
      when (efTestAndClear(buildInit2)) {
         efSet (requestInit);
      } state waitfordone
   }
   state waitforrequest {
      when() {
         clientReq = getRequest();
      } state testclient
   }
   state testclient {
      when(clientReq) {
         efClear(senderWaiting);
         printf("request obtained..\n");
         efSet(requestAvail);
      } state waitfordone
      when(delay(1.0)) {
         /* something went wrong.  slow down. */
      }  state waitforrequest
   }
   state waitfordone {
      when (efTestAndClear(senderWaiting)) {
      } state waitforrequest
   } 
}

ss senderControl {
   state init {
       when (efTestAndClear(senderInitE)) {
       } state senderNotRunning
   }
   state senderRunning {
       when(efTestAndClear(senderWriteEvent)) {
       } state senderSwitch
       when (efTestAndClear(senderRan)) {
          senderRunning = 1; pvPut(senderRunning);
       }  state senderRunning
       when (delay(5)) {
          senderRunning = 0; pvPut(senderRunning);
       } state senderRunning
   }
   state senderNotRunning {
       when(efTestAndClear(senderWriteEvent)) {
       } state senderSwitch
       when (delay(0.1)) {
          /* discard output data */
       } state senderNotRunning
   }
   state senderSwitch {
       when(senderWrite) {
          /* enable server to actually run */
          senderControl(1);
       }  state senderRunning
       when() {
          /* Make senderControl stop all running server instances */
          senderControl(0);
          senderRunning = 0; pvPut(senderRunning);
       }  state senderNotRunning
   }
}


ss sender {
   state init {
       when (efTestAndClear(requestInit)) {
          senderInit();
          efSet(senderInitE);
          efSet(senderWaiting);
       } state acqTest
   }
   state acqTest {
      when (!AcqRun && AcqRunRO) {
         AcqRunRO = 0;
         pvPut(AcqRunRO);
         senderWrite = 0;
         pvPut(senderWrite);
      } state acqTest
      when () {
      } state senderWait
   }
   state senderWait {
       when(efTestAndClear(requestAvail)) {
         /* return with error if sending not enabled */
         /* otherwise run as separate task until sending is turned off */
         printf("starting runServerInstance\n");
         runServerInstance(clientReq, eventGap);
         efSet(senderWaiting);
       } state acqTest
       when (delay(1)) {
       } state acqTest
   }
}

