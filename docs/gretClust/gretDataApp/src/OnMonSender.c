
#include <sockLib.h>
#include <inetLib.h>

#include <iosLib.h>
#include <stdioLib.h>
#include <stdlib.h>
#include <hostLib.h>

#include "cesort.h"

#define ACQ_CMD_OK      0
#define ACQ_UNKNOWN     1
#define ACQ_DNS_ERROR   2
#define ACQ_OPEN_ERROR  4

#define SENDER_PORT       1101

int OnMonSenderPort = SENDER_PORT;


struct netCntl {
  int initialized;
  unsigned int seqnum;
  int sockfd;
  struct sockaddr_in sockaddr;
  int numSentBufs;
};

static struct netCntl netCookie;
static struct netCntl *n = &netCookie;

/* new effort to skim data off at the end.  See notebook page 21b */
#if 0
static struct eNode *onMonData, *onMonDataLast;
static int skimfrac = 5;

int OnMonSetFrac(int divisor) {

   if (divisor <= 0) return -1;
   skimfrac = divisor;
   return 0;
}

int OnMonSkim(rawEvt *rep, struct eNode *that) {

   static int trashit;

   trashit++;
   trashit %= skimfrac;

   if (trashit) {
      rawEvtFree(rep, that);
   } else {
      epicsMutexLock(onMonLock);
      if (!onMonDataLast) {
         onMonData = that;
   
}
#endif

int OnMonInit() {
return 0;
}

int OnMonDetach() {

  close(n->sockfd);
  bzero((char *) &(n->sockaddr), sizeof(n->sockaddr));

  n->initialized = 0;   /* mark network as not initalized */

  return ACQ_CMD_OK;
}

int OnMonAttach(char *serverName) {

  int tval;
  /* initialize connection if not already done so */

  if (n->initialized == 0) {            /* networking not initialized */
    printf("initializing OnMon sender\n");
    n->sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (n->sockfd == -1) {
      return ACQ_OPEN_ERROR;
    }
    n->initialized = 1;
  }
  if (0 == serverName) return -1;
  
  /* initialize net.sockaddr */
  bzero((char *) &(n->sockaddr), sizeof(n->sockaddr));
  n->sockaddr.sin_family = AF_INET;
  n->sockaddr.sin_port = OnMonSenderPort;

  /* look up dns info */
/*
  n->sockaddr.sin_addr.s_addr = hostGetByName(serverName);
*/
  inet_aton(serverName, &(n->sockaddr.sin_addr));  /* MC */
  if (n->sockaddr.sin_addr.s_addr == 0) {
    return ACQ_DNS_ERROR;
  }

  return ACQ_CMD_OK;
}

int printSender = 0;

int OnMonSend(int boardno, int numtosend) {
   int size, stat;
   static char *outbuf = 0;
   struct eNode *where = 0;
   char *outp;
   int i;

   if (numtosend <= 0) return 0;

   if (boardno < 0) return -2;

   if (outbuf == 0) outbuf = calloc(ONLINE_PACKET_SIZE, 1);
   if (outbuf == 0) return -2;


   outp = outbuf;

   for (i = 0; i < numtosend; i++) {
     /* Sequence number is written in the first 4 bytes of each sent
      * buffer. 
      */
     *(unsigned int *)(outp) = n->seqnum++;
     outp  += 4;

     stat = getRawData(boardno, outp, &where);
   
     if (stat == -1) return -2;

     outp = outbuf;

     size = sendto(n->sockfd, outp,
                   ONLINE_PACKET_SIZE,
                   0, (struct sockaddr *)&(n->sockaddr),
                   sizeof(struct sockaddr_in));
      if (size < 0) {
        printf("sendto failed - errno %d\n", errno);
        return -1;
      }
      else {
        n->numSentBufs++;
        if (printSender) {
           if (n->numSentBufs % 16 == 0) {
              printf("OnMonSender sent %d\n", size);
           }
        }
      }
      if (stat == -2) break;
   }
   return 0;
}



  


