program  TrigCon ("P=A") 

option +d;

%%#include "readFIFO.h"
%%#include "sender.h"
%%#include "cesort.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

DECL(float,buffersAvail,DAQC{CN}_CV_BuffersAvail)
DECL(short,TrigInhL,DAQC{CN}_CS_TrigInh) /* local copy */
DECL(short,TrigInhD,DAQC{CN}_CV_TrigInhD) /* local diagnostic */

%%extern int readFIFOBufs;
int gc;

ss bufferMaint {
   state init {
      /* allow eventBuild init to run */
      when () {
      } state bufferWait
   }
   state bufferTest {
       when (buffersAvail < RAW_Q_SIZE/3) {
          TrigInhL = 1;
          pvPut(TrigInhL);	
          TrigInhD = 1;
          pvPut(TrigInhD);	
       }  state bufferWait
       when (buffersAvail < RAW_Q_SIZE/2) {  
          gc = rawEvtGC();
          if (gc) printf("%d stray raw buffers reclaimed.\n", gc);
       }  state bufferWait
       when() {
       }  state bufferWait
   }
   state bufferWait {
       when (delay(0.25)) {
          buffersAvail = readFIFOBufs;
          pvPut(buffersAvail);
       }  state bufferTest
   }
}


