program RunRcvr ("instance=001,cryid=01")

option +d; 	/* debug (verbose) output */
option +m;
option -c;

%%#include <string.h>
%%#include "queues.h"
%%#include "rcvrIF.h"
%%#include "receiver.h"
%%#include "logSend.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYMON(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;	

#define DECLARRAYEVT(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		\
        evflag n##Event;        \
        sync n n##Event;        \

#define SENDSTATE(s) 		\
	strcpy(statepv, #s);	\
	pvPut(statepv);		\
	pvFlush();

DECLARRAYEVT(char, runname, Data_CS_RunDir, 100)

/* data directory of which run directories are subdirectories */
DECLARRAYEVT(char, dirname, Data_CS_Dir, 100)

DECL(string, sender, Det{cryid}_CS_Sender)
DECL(string, statepv, Decomp{instance}_CV_State)
DECL(int, eventrate, Decomp{instance}_CV_Rate)
DECL(int, Status, Decomp{instance}_CV_Status)

DECLMON(short, startStop, Online_CS_StartStop)

DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECLMON(short,saveData,Online_CS_SaveData)

DECLMON(string, logger, Log_CV_Addr)
DECLMON(int, logport, Log_CV_Port)

evflag startInput;
int stopInput;

evflag startProcess;
evflag stopProcess;
evflag stopOutput;
evflag startOutput;
evflag doneFlag;
evflag initLogger;

int initstat;
char *receiver;
int writestat;
int writeerrs;
int runproc;
int prev_events;
int iqstat;
int oqstat;

char *input1;
char *sendInput;
char *rawout;

int tcstat;
string ourname;
string logbuffer;

ss setupAndRun {
  state init {
     when(pvChannelCount() == pvConnectCount()) {
        initstat = rcvrIFInit();
        efSet(initLogger);
        sprintf(ourname, "runRcvr%s", macValueGet("instance"));
     }  state initCheck
  }
  state initCheck {
     when(initstat == 1) {
        printf("Receiver initialization failed\n");
        SENDSTATE(Failed)
        rcvrIFShutdown();
        exit(-1);
     } state restart
     when() { 
     } state restart
  }
  state restart {
    /* go through at startup and in case of various failures in run mode */
    when(startStop != 0 ) {
    } state check
    when() {
    } state setup
  }
  state check {
    /* We stay here until system goes to stop */
    entry {
       SENDSTATE(Check)
    }
    when (startStop == 0) {
    } state restart
  }
  state run {
    when (startStop == 0 || saveData == 0) {
       stopInput = 1;
    } state waitfordone
  }
  state setup {
    when (startStop == 1 && saveData) {
       printf("Start Command\n");
    } state checkrun
    when (efTestAndClear(runnameEvent) ||
          efTestAndClear(dirnameEvent) ) {
       setOutputFile3(dirname, runname, macValueGet("cryid"));
    } state setup
  }
  state checkrun {
    when (openOutputFile()) {
       efSet(startInput);
    } state run
    when() {
       printf("unable to open file\n");
    } state restart 
  }
  state waitfordone {
    when(delay(2)) {
        clearList(0);
    } state setup
  }
}

ss sendCounters {
   state send {
      when (delay(1.0)) {
         eventrate = getEvents(0) - prev_events;
         prev_events += eventrate;
         pvPut(eventrate);
      }  state send
   }
}

ss input {
   state init {
      when() {
      }   state stopped
   }
   state offline {
      entry {
         SENDSTATE(Offline)
      }
      when (stopInput) {
         stopInput = 0;
         efSet(stopProcess);
      } state offline
      when (efTestAndClear(startInput)) {
         stopInput = 0;
         efSet(startProcess);
      } state offline
      when (enable) {
      } state stopped
   }
   state run {
      when (!enable) {
      } state offline
      when() {
         while (!stopInput) {
           if (receiver && !getReceiverData(receiver, 1.0, &input1)) {
              printf("Data received\n"); 
              iqstat = queueSend(0, input1);
              while (!stopInput && iqstat) {
                 iqstat = queueSend(0, input1);
              }
           } else {
             Status = 1;
             pvPut(Status);
             epicsThreadSleep(0.2);
           }
         }
         stopReceiver(receiver);
         stopInput = 0;
         efSet(stopProcess);
      }  state stopped
   }
   state stopped {
      entry {
         SENDSTATE(Setup)
      }
      when (!enable) {
      } state offline
      when (efTestAndClear(startInput)) {
         pvGet(sender);
         receiver = initReceiver(sender);
         stopInput = 0;
         efSet(startProcess);
         snprintf(logbuffer, MAX_STRING_SIZE,
                           "begin acquisition crystal id %s", macValueGet("cryid"));
         sendLogMsg(ourname, logbuffer);
         sendToLog(0);
         SENDSTATE(Run)
      } state run
      when (delay(0.5)) {
         Status = 1;
         pvPut(Status);
      } state stopped
   }
}

ss runLogger {
   state preinit {
      when (efTestAndClear(initLogger)) {
      } state init
   }
   state init {
      option -e;
      entry {
         tcstat = checkLogger();
      }
      when (tcstat == 0) {
         printf("Logger not connected\n");
         setLogger(logger, logport);
         sendLogMsg(ourname, "log opened");
         sendToLog(0);
      } state wait
      when (delay(1.0)) {
      } state init
   }
   state wait {
     when (delay(1.0)) {
     } state init
   }
}

ss process {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when (efTestAndClear(startProcess)) {
          startSort();
          efClear(stopProcess);
          efSet(startOutput);
      } state run
   }
   state run {
      when (0 == queueReceive(0, &rawout)) {
         /* processing would occur here */
         oqstat = queueSend(1, rawout);
      } state testqs
      when (efTestAndClear(stopProcess)) {
         efSet(stopOutput);
         runproc = 0;
      } state stopped
      when(delay(0.2)) {
      } state run
   }
   state testqs {
      when (oqstat == 0) {
      } state run
      when (delay(0.1)) {
         oqstat = queueSend(1, rawout);
      } state testqs
   }
}

ss output {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when(efTestAndClear(startOutput)) {
         efClear(stopOutput);
         writeerrs = 0;
         /* open output connection ? */
      } state run
   }
   state run {
      when (0 == queueReceive(1, &sendInput)) {
         writestat = writeEvents(sendInput);
      } state testwrite
      when (efTestAndClear(stopOutput)) {
         /* close output? */
         closeOutputFile();
         rcvrIFShutdown();
         clearList(1);
      } state stopped
      when (delay (0.2)) {
      } state run
   }
   state testwrite {
         when (writestat == -2) {
           sendLogMsg(ourname, "writeEvents Failed");
           sendToLog(0);
           writeerrs++;
         } state hold
         when (writestat == -1) {
           sendLogMsg(ourname, "Null output file; stopping");
           sendToLog(0);
           efSet(stopProcess);
         } state stopped
         when() {
           if (writeerrs) {
              sendLogMsg(ourname, "writeEvents recovered ");
              sendToLog(0);
              writeerrs = 0;
           }
         } state run
      }
   state hold {
      when (writeerrs > 10) {
           sendLogMsg(ourname, "Stopping due to write errors");
           sendToLog(0);
           efSet(stopProcess);
      } state stopped
      when (delay(0.1)) {
      } state run
   }
}

