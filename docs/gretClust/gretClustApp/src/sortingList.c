#include <stdlib.h>
#include <stdio.h>
#include <epicsMutex.h>
#include "sortingList.h"

/*
 * This is for sorting data by timestamp that pretty much but not completely 
 * comes in in order by inserting it into the list, just using a linear test, 
 * but starting at the recent end of the list.  It keeps an indicator of how 
 * close to the beginning of the list it has inserted recently as well as 
 * being able to provide the oldest timestamp, so the program can get an idea
 * of whether to get the next thing out.
 */

sortingList *sortingListCreate() {
   sortingList *retval;
   
   retval = calloc(1, sizeof(sortingList));
   if (!retval) return 0;

   retval->mutex = epicsMutexCreate();
   if (!retval->mutex) return 0;

   return retval;
}

void sortingListDestroy(sortingList *id) {
  
   if (!id) return;
   /* empty the list.  We don't free the contents */
   while (sortingListGet(id));
   epicsMutexDestroy(id->mutex);
   free(id);
}

int sortingListWaterline(sortingList *id) {
   int retval;

   if (!id) return -1;

   epicsMutexLock(id->mutex);
   retval = id->waterline;
   epicsMutexUnlock(id->mutex);
   return retval;
}

int sortingListLength(sortingList *id) {
   int retval;

   if (!id) return -1;

   epicsMutexLock(id->mutex);
   retval = id->listlen;
   epicsMutexUnlock(id->mutex);
   return retval;
}


void *sortingListGet(sortingList *id) {
   
   void *retval;
   sortingListNode *temp;

   if (!id) return 0;
   
   epicsMutexLock(id->mutex);
   temp = id->listFront;
   if (!temp) {
      epicsMutexUnlock(id->mutex);
      return 0;
   }
   id->listFront = temp->next;
   if (id->listFront) {
      id->listFront->prev = 0;
   } else {
      id->listEnd = 0;
   }
   id->listlen--;
   epicsMutexUnlock(id->mutex);
   retval = temp->data;
   free(temp);
   return retval;
}

long long sortingListFrontTime(sortingList *id) {

   long long retval;

   if (!id) return -1;
 
   epicsMutexLock(id->mutex);
   if (!id->listFront) {
      epicsMutexUnlock(id->mutex);
      return -1;
   }
   retval = id->listFront->timestamp;
   epicsMutexUnlock(id->mutex);

   return retval;
}

/*
 * Add one node to list.  Starts at end (most recent times) and looks 
 * backward to insert in time order.  If timestamp is always set to 0,
 * it always appends.
 */
int sortingListAdd(sortingList *id, void *datum, long long timestamp) {

   sortingListNode *newnode, *temp;
   int moveup = 0;

   if (!id || !datum || timestamp < 0) return -1;
   
   newnode = calloc(1, sizeof(sortingListNode));
   if (!newnode) return -1;

   newnode->data = datum;
   newnode->timestamp = timestamp;

   epicsMutexLock(id->mutex);
   if (!id->listFront) {
      id->listFront = newnode;
   } else {
      temp = id->listEnd;
      while (temp && temp->timestamp > newnode->timestamp) {
         temp = temp->prev;
         moveup++;
      }
      if (temp) {
         newnode->next = temp->next;
         if (temp->next) temp->next->prev = newnode;
         temp->next = newnode;
         newnode->prev = temp;
      } else {
         newnode->next = id->listFront;
         id->listFront->prev = newnode;
         id->listFront = newnode;
      }
   }
   if (newnode->next == 0) {
      id->listEnd = newnode;
   }
   id->listlen++;
   id->waterline++;
   if ((id->listlen - moveup) < id->waterline) {
      id->waterline = id->listlen - moveup;
   }
   epicsMutexUnlock(id->mutex);
   return 0;
}

/* 
 * merge two sortingLists.  This is done from the end for each list, so
 * the first comparisons are the most recent in each list and both go back
 * towards the front from there.
 */
int sortingListMerge(sortingList *dest, sortingList *src) {

   sortingListNode *focus, *testpoint;

   epicsMutexLock(dest->mutex);
   epicsMutexLock(src->mutex);

   focus = src->listEnd;
   testpoint = dest->listEnd;

   while (focus) {
      while (testpoint && focus->timestamp < testpoint->timestamp) {
         testpoint = testpoint->prev;
      }
      src->listEnd = focus->prev;
      focus->prev = testpoint;
      if (!testpoint) {
         focus->next = dest->listFront;
         if (dest->listFront) dest->listFront->prev = focus;
         dest->listFront = focus;
      } else {
         focus->next = testpoint->next; 
         if (focus->next) focus->next->prev = focus;
         testpoint->next = focus;
      }
      if (0 == focus->next) {
         dest->listEnd = focus;
      }
      dest->listlen++;
      focus = src->listEnd;
   }
   src->listFront = 0;
   src->listlen = 0;
   epicsMutexUnlock(src->mutex);
   epicsMutexUnlock(dest->mutex);

   return 0;
}
