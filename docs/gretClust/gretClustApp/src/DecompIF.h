
int decompInit(char *comp, char *cryid);
void setCrystalId(int cryid);
int setBasisName(char *fname);
int setDetMapName(char *fname);
int setFilterName(char *fname);
int setTrGainName(char *fname);
int setXtalkParsName(char *fname);
void setRecLen(int reclen);
void decompShutdown();
void setVerbose(int verbose);

int startPreProcess();
int preProcess(char *input, char **output);
int runPreProcess(char *input, int *keeprunning);

int startDecomp(char *ourname);
int stopDecomp();
int sendPosition(char *i);
int setGEB(char *addr, int port);
void closeGEB();
int checkGEB();

int getDecomps();
int getBuiltChans();
int getDecompErrs(int which);
