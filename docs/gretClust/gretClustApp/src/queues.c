#include <epicsMessageQueue.h>

epicsMessageQueueId queues[2]; 
static int ptrsize;

int queueInit(int n0, int n1) {

   ptrsize = sizeof(void *);
   queues[0] = epicsMessageQueueCreate(n0, ptrsize);
   queues[1] = epicsMessageQueueCreate(n1, ptrsize);
   return (queues[0] == 0 || queues[1] == 0);
}

int queueSend(int qnum, char *outp) {
  return  epicsMessageQueueSendWithTimeout(queues[qnum], &outp, ptrsize, 0.1);
}

void queueFlush(int qnum) {
   char *devnull = 0;
   while (ptrsize == epicsMessageQueueTryReceive(queues[qnum], &devnull, ptrsize));
}

int queueReceive(int qnum, char **bp) {
   return (ptrsize == epicsMessageQueueReceiveWithTimeout(queues[qnum],
                     (void *)bp, ptrsize, 0.1))?0:1;
}
