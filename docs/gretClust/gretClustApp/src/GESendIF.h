
int GEBInit(char *, int *);
void GEBShutdown();
int setGEBDelay(int);
int getGEBTimeNM();

int getRequests();
int nextPosition(char **in);
int nextPositionDummy(char **in);
int startGEB();

int setTracker(char * geb_addr, int port);
void closeTracker();
int checkTracker();
int sendPosition(char *pos);

