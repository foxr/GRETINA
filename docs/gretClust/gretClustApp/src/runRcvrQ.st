program RunRcvr ("C1=1,C2=2,C3=3,C4=4")

option +d; 	/* debug (verbose) output */
option +m;

%%#include <string.h>
%%#include "queues.h"
%%#include "rcvrIF.h"
%%#include "receiver.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYEVT(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		\
        evflag n##Event;        \
        sync n n##Event;        \

#define SENDSTATE(s) 		\
	strcpy(statepv1, #s);	\
	strcpy(statepv2, #s);	\
	strcpy(statepv3, #s);	\
	strcpy(statepv4, #s);	\
	pvPut(statepv1);		\
	pvPut(statepv2);		\
	pvPut(statepv3);		\
	pvPut(statepv4);		\
	pvFlush();

/* output file */
DECLARRAY(char, filename, Data_CS_FileName, 100)
DECLARRAYEVT(char, dirname, Data_CS_Dir, 100)

DECLMON(string, sender1, Decomp001_CS_Sender)
DECLMON(string, sender2, Decomp002_CS_Sender)
DECLMON(string, sender3, Decomp003_CS_Sender)
DECLMON(string, sender4, Decomp004_CS_Sender)
DECLEVENT(int, reclen, CryG_CS_RDLen.RVAL)
DECL(string, statepv1, Decomp001_CV_State)
DECL(string, statepv2, Decomp002_CV_State)
DECL(string, statepv3, Decomp003_CV_State)
DECL(string, statepv4, Decomp004_CV_State)
DECL(int, eventrate1, Decomp001_CV_Rate)
DECL(int, eventrate2, Decomp002_CV_Rate)
DECL(int, eventrate3, Decomp003_CV_Rate)
DECL(int, eventrate4, Decomp004_CV_Rate)
DECL(int, Status1, Decomp001_CV_Status)
DECL(int, Status2, Decomp002_CV_Status)
DECL(int, Status3, Decomp003_CV_Status)
DECL(int, Status4, Decomp004_CV_Status)

DECLMON(short, startStop, Online_CS_StartStop)

DECLMON(short, enable1, Decomp001_CS_Enable)
DECLMON(short, enable2, Decomp002_CS_Enable)
DECLMON(short, enable3, Decomp003_CS_Enable)
DECLMON(short, enable4, Decomp004_CS_Enable)

DECLMON(short,saveData,Online_CS_SaveData)

evflag startInput1;
int stopInput1;
evflag startInput2;
int stopInput2;
evflag startInput3;
int stopInput3;
evflag startInput4;
int stopInput4;

evflag startInputProc;
int stopInputProc;
evflag stopOutput;
evflag startOutput;
evflag trackerInited;

int initstat;
int oqstat;
int iqstat1;
int iqstat2;
int iqstat3;
int iqstat4;
char *receiver1;
char *receiver2;
char *receiver3;
char *receiver4;
int writestat;
int writeerrs;
int prev_events1;
int prev_events2;
int prev_events3;
int prev_events4;

char *input1;
char *input2;
char *input3;
char *input4;
char *sendInput;
char *rawout;

#define INPUTSS(TSS,NSS) 			\
ss input ## TSS {					\
   state init {			\
      when (enable ## TSS) {			\
      } state stopped			\
      when() {			\
      } state offline			\
   }			\
   state offline {			\
      entry {			\
	strcpy(statepv ## TSS, "Offline");				\
        pvPut(statepv ## TSS);			\
      }			\
      when (stopInput ## TSS) {			\
         stopInput ## TSS = 0;			\
         stopInput ## NSS = 1;			\
      } state offline			\
      when (efTestAndClear(startInput ## TSS)) {			\
         stopInput ## TSS = 0;			\
         efSet(startInput ## NSS);			\
      } state offline			\
      when (enable ## TSS) {			\
      } state stopped			\
      when (delay(0.5)) {			\
         Status1 = 1;			\
         pvPut(Status ## TSS);			\
      } state offline			\
   }			\
   state run {			\
      when (!enable ## TSS) {		\
      } state offline			\
      when() {	\
         while (!stopInput ## TSS) {	\
           if (receiver ## TSS && !getReceiverData(receiver ## TSS, 	\
              1.0,  &input ## TSS)) {		\
              iqstat ## TSS = queueSend(0, input ## TSS);	\
              while (!stopInput ## TSS && iqstat ## TSS) {	\
                 iqstat ## TSS = queueSend(0, input ## TSS);	\
              }		\
           } else {	\
             Status ## TSS = 1;			\
             pvPut(Status ## TSS);		\
             epicsThreadSleep(0.2);		\
           }	\
         }	\
         stopInput ## TSS = 0;	\
         stopInput ## NSS = 1;	\
      }  state stopped 	\
   }	\
   state stopped {			\
      entry {			\
	strcpy(statepv ## TSS, "Setup");				\
        pvPut(statepv ## TSS);			\
      }			\
      when (!enable ## TSS) {			\
      } state offline			\
      when (efTestAndClear(startInput ## TSS)) {			\
         receiver ## TSS = initReceiver(sender ## TSS);			\
         stopInput ## TSS = 0;			\
         efSet(startInput ## NSS);			\
	 strcpy(statepv ## TSS, "Run");				\
         pvPut(statepv ## TSS);			\
      } state run			\
      when (delay(0.5)) {			\
         Status ## TSS = 1;			\
         pvPut(Status ## TSS);			\
      } state stopped			\
   }			\
}			

ss setupAndRun {
  state init {
     when((pvChannelCount() - 4) < pvConnectCount()) {
        /* allow it to run with only one acquisition crate */
        initstat = rcvrIFInit();
        /* one-time initializations */
     } state initCheck
  }
  state initCheck {
     when(initstat == 1) {
        printf("Receiver initialization failed\n");
        SENDSTATE(Failed)
        rcvrIFShutdown();
        exit(-1);
     } state restart
     when() { 
     } state restart
  }
  state restart {
    /* go through at startup and in case of various failures in run mode */
    when(startStop != 0 ) {
    } state check
    when() {
    } state setup
  }
  state check {
    /* We stay here until system goes to stop */
    entry {
       SENDSTATE(Check)
    }
    when (startStop == 0) {
    } state restart
  }
  state run {
    when (startStop == 0 || saveData == 0) {
       stopInput1 = 1;
    } state waitfordone
  }
  state setup {
    when (startStop == 1 && saveData) {
       printf("Start Command\n");
       fflush(stdout);
    } state checkrun
    when (efTestAndClear(dirnameEvent)) {
        strcpy(filename, setOutputFile(dirname));
        pvPut(filename);
    } state setup
    when (efTestAndClear(reclenEvent)) {
        setRecLen(reclen);
    } state setup
  }
  state checkrun {
    when (openOutputFile()) {
       efSet(startInput1);
    } state run
    when() {
       printf("unable to open file\n");
    } state restart 
  }
  state waitfordone {
    when(delay(2)) {
        clearList(0);		
    } state setup
  }
}

ss sendCounters {
   state send {
      when (delay(1.0)) {
         eventrate1 = getEvents(1) - prev_events1;
         prev_events1 += eventrate1;
         pvPut(eventrate1);
         eventrate2 = getEvents(2) - prev_events2;
         prev_events2 += eventrate2;
         pvPut(eventrate2);
         eventrate3 = getEvents(3) - prev_events3;
         prev_events3 += eventrate3;
         pvPut(eventrate3);
         eventrate4 = getEvents(4) - prev_events4;
         prev_events4 += eventrate4;
         pvPut(eventrate4);
      }  state send
   }
}

INPUTSS(1, 2)
INPUTSS(2, 3)
INPUTSS(3, 4)
INPUTSS(4, Proc)

ss process {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when (efTestAndClear(startInputProc)) {
          startSort();
          stopInputProc = 0;
      } state gotorun
   }
   state gotorun {
      when() {
         efSet(startOutput);
      } state run
   }
   state run {
      when (0 == queueReceive(0, &rawout)) {
         /* processing would occur here */
         oqstat = queueSend(1, rawout);
      } state testqs
      when() {
      } state teststop
   }
   state teststop {
      when (stopInputProc) {
         efSet(stopOutput);
      } state cleanup
      when(delay(0.01)) {
      } state run
   }
   state testqs {
      when (oqstat == 0) {
      } state run
      when (delay(0.1)) {
         oqstat = queueSend(1, rawout);
      } state testqs
   }
   state cleanup {
      when() {
      } state stopped
   } 
}

ss output {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when(efTestAndClear(startOutput)) {
         efClear(stopOutput);
         writeerrs = 0;
         /* open output connection ? */
      } state run
   }
   state run {
      when (0 == queueReceive(1, &sendInput)) {
         writestat = writeEvents(sendInput);
      } state testwrite
      when() {
      } state teststop
   }
   state teststop {
      when (efTestAndClear(stopOutput)) {
         /* close output? */
         closeOutputFile();
         rcvrIFShutdown();
         strcpy(filename, setOutputFile(dirname));
         pvPut(filename);
         clearList(1);
      } state stopped
      when (delay (0.1)) {
      } state run
   }
   state testwrite {
         when (writestat == -2) {
           printf("writeEvents failed\n");
           writeerrs++;
         } state hold
         when (writestat == -1) {
           printf("null output file: stopping \n");
           stopInput1 = 1;
         } state stopped
         when() {
           if (writeerrs) {
              printf("writeEvents succeeded after %d failures\n", writeerrs);
              writeerrs = 0;
           }
         } state run
      }
   state hold {
      when (writeerrs > 10) {
           printf("too many write errors: stopping \n");
           stopInput1 = 1;
      } state stopped
      when (delay(0.1)) {
      } state run
   }
}
