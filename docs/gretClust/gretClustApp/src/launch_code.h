
int startProg(char *progString, int component, int cryid, int telnetport);
int killProg(int progId);
char *makePVName(char *suffix);
int makeInstance(int slot);
int waitProg(int progId);
