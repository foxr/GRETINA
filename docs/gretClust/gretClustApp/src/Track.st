program Track ("instance=001")

option +d; 	/* debug (verbose) output */

%%#include <string.h>
%%#include "queues.h"
%%#include "TrackIF.h"
%%#include "logSend.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \
        t n##Save;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYEVT(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		\
        evflag n##Event;        \
        sync n n##Event;        

#define SENDSTATE(s) 		\
	strcpy(statepv, #s);	\
	pvPut(statepv);		\
	pvFlush();

DECL(int, port, Track_CV_Port)
DECL(string, tracker, Track_CV_Addr)
/* Data directory */
DECLARRAYEVT(char, dirname, Data_CS_Dir, 100)
/* Run Directory, a subdirectory of the above */
DECLARRAYEVT(char, runname, Data_CS_RunDir, 100)
DECLMON(short, startStop, Online_CS_StartStop)
DECLMON(string, logger, Log_CV_Addr)
DECLMON(int, logport, Log_CV_Port)

DECL(int, Status, Decomp{instance}_CV_Status)
DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECL(string, statepv, Decomp{instance}_CV_State)


evflag startInput;
evflag stopInput;
evflag startProcess;
evflag stopProcess;
evflag startOutput;
evflag doneFlag;
evflag trackInited;
evflag loggerInited;

int stopOutput;
int initstat;
int insock;
int instat;
int tcstat;

ss setupAndRun {
  state init {
     when() {
        pvGet(port);
        printf("port = %d\n", port);
        initstat = trackInit(tracker, &port);
        pvPut(tracker);
        pvPut(port);
        /* one-time initializations */
     } state initCheck
  }
  state initCheck {
     when(initstat !=0) {
        printf("Track initialization failed\n");
        SENDSTATE(Failed)
        trackShutdown();
        exit(-1);
     } state restart
     when() { 
        efSet(trackInited);
     } state restart
  }
  state offline {
    entry {
       SENDSTATE(Offline)
    }
    when(enable) {
    } state restart
  }
  state restart {
    when(!enable) {
    } state offline
    when(startStop != 0 ) {
    } state check
    when() {
    } state setup
  }
  state check {
    entry {
       SENDSTATE(Check)
    }
    when (!enable) {
    } state offline
    /* If we restart while things are running, do nothing. */
    when (startStop == 0) {
    } state restart
    when () {
    } state setup
  }
  state run {
    entry {
       SENDSTATE(Run)
    }
    when (!enable || startStop == 0) {
       efSet(stopProcess);
    } state waitfordone
  }
  state checkrun {
     when (openOutputFile()) {
       efSet(loggerInited);
       efSet(startProcess);
       efClear(doneFlag);
     } state run
     when () {
       printf("failed to open output file\n");
     } state restart
  }
  state setup {
    entry {
       SENDSTATE(Setup)
    }
    when (!enable) {
    } state offline
    when (startStop == 1) {
       printf("Start Command\n");
    } state checkrun
    when (efTestAndClear(runnameEvent) ||
          efTestAndClear(dirnameEvent) ) {
       setOutputFile(dirname, runname);
    } state setup
    when (delay(0.5)) {
       Status = 1;
       pvPut(Status);
    } state setup
  }
  state waitfordone {
    entry {
       SENDSTATE(Wait)
    }
    when(efTestAndClear(doneFlag)) {
    } state setup
  }
}

ss checkLogger {

   state preinit {
      when (efTestAndClear(loggerInited)) {
      } state init
   }
   state init {
      option -e;
      entry {
         tcstat = checkLogger();
      }
      when (tcstat == 0) {
         printf("Logger not connected\n");
         setLogger(logger, logport);
         sendLogMsg("Track", "log opened");
         sendToLog(0);
      } state wait
      when (delay(1.0)) {
      } state init
   }
   state wait {
     when (delay(1.0)) {
     } state init
   }
}
ss getData {
   state init {
       when (efTest(trackInited)) {
       } state testrun
   }
   state testrun {
      when (!startStop) {
      } state standby
   }
   state standby {
       when (startStop) {
       } state run
   }
   state run {
       when () {
           insock = getRequest();
       } state teststat 
   }
   state teststat {
      when (insock > 0) {
          while (1) { 
             instat = gebReader(insock);
             if (instat != 0) break;
          }
      } state standby
      when (delay(1.0)) {
      } state standby
   }
}

ss process {
   state init {
     entry {
        printf("entering process:init\n");
     }
      when() {
      } state stopped
   }
   state stopped {
     entry {
        printf("entering process:stopped\n");
     }
      when (efTestAndClear(startProcess)) {
          startTrack();
          efClear(stopProcess);
          efSet(startOutput);
      } state run
   }
   state run {
      when (efTestAndClear(stopProcess)) {
         stopTrack();
      } state cleanup
   }

   state cleanup {
     entry {
        printf("entering process:cleanup\n");
     }
      when(delay(6)) {
         stopOutput = 1;
      } state stopped
   } 
}

ss output {
   state stopped {
      when (efTestAndClear(startOutput)) {
      } state run
   }
   state run {
      when () {
         while (!stopOutput) {
            sortAndWrite();
         }
         closeOutputFile();
         efSet(doneFlag);
      } state stopped
   }
}
         

