
int GEBInit(char *, int *);
char *setOutputFile(char *dirname, char *runname);
int validOutput();
void GEBShutdown();
int setGEBDelay(int);
int getGEBTimeNM();

int getRequests();
int nextPosition(char **in);
int nextPositionDummy(char **in);
int startGEB();
int openOutput();
int closeOutput();
int writeOutput(char *out);

