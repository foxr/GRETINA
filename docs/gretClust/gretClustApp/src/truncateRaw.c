#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>

/* truncateRaw.c is for making shorter test files */
int main(int argc, char *argv[]) {

    unsigned int inbuf, outbuf[1024];
    FILE *inf, *outf;
    struct stat sbuf;
    int wstat, megabytes, wordt, wordc = 0, outc = 0;
    char *eptr;
    
    
    if (argc != 4) {
       printf("Usage: %s <input file> <output file> size (MB)\n", argv[0]);
       exit(1);
    } 

    megabytes = strtol(argv[3], &eptr, 10);
    if (megabytes < 1 || eptr == argv[3]) {
       printf("size of %s not allowed\n", argv[3]);
       exit(3);
    }
    wordt = megabytes * 256 * 1024;
       
    if (!stat(argv[2],&sbuf)) {
       printf("Output file already exists; exiting\n");
       exit(3);
    }

    inf = fopen(argv[1], "r"); 
    if (!inf) {
       printf("Cannot open %s for reading\n", argv[1]);
       exit(2);
    }

    outf = fopen(argv[2], "w");
    if (!outf) {
       printf("Cannot open %s for writing\n", argv[2]);
       fclose(inf);
       exit(2);
    }

    while (wordc < wordt && fread(&inbuf, sizeof(int), 1, inf)) {
       wordc++;
       if (outc < 256) {
          outbuf[outc] = inbuf;
          outc++;
       } else {
          printf("Input event too long at word %d\n", wordc);
          fclose(inf);
          fclose(outf);
          exit(3);
       }
       if (inbuf == 0xaaaaaaaa) {
          wstat = fwrite(outbuf, sizeof(int), outc, outf);
          if (wstat != outc) {
             printf("fwrite returned %d not %d\n", wstat, outc);
             fclose(inf);
             fclose(outf);
             exit(3);
          }
          outc = 0;
       }
    }
    fclose(inf);
    fclose(outf);
    exit(3);
}
