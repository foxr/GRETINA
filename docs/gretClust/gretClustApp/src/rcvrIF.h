#define MAX_SERVERS 4

int rcvrIFInit();
void rcvrIFShutdown();
void setRecLen(int);

void startSort();
void clearList(int);
int writeEvents(char *);
int getEvents(int);

int setOutputFile3(char *datadir, char *rundir, char *crystal);
char *setOutputFile(char *);
int openOutputFile();
int closeOutputFile();
int validOutputFile();


