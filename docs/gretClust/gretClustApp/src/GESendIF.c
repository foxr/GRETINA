#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsMutex.h>
#include <epicsThread.h>
#include <epicsMessageQueue.h>
#include <iocsh.h>

#include "queues.h"
#include <gdecomp.h>
#include "GEBLink.h"
#include "logSend.h"

epicsMutexId connectionMutex;
static int outSock = 0;                         /* Socket */
int numWritten;

/* geb_addr is a network name, like node0005-cluster */
int setTracker(char * geb_addr, int port) {

   struct sockaddr_in adr_srvr;  /* AF_INET */
   struct hostent *hp;

   numWritten = 0;
   epicsMutexLock(connectionMutex);

   if (outSock != 0) {
      close(outSock);
   }
   memset(&adr_srvr,0,sizeof adr_srvr);
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(port);

   hp = gethostbyname(geb_addr);
   if (!hp) {
      printf("hostname %s not resolved\n", geb_addr);
      return 4;
   }
   adr_srvr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *) 
                                (hp->h_addr_list[0]))));

   if ( adr_srvr.sin_addr.s_addr == INADDR_NONE ) {
      printf("bad geb address %s port %d\n", geb_addr, port);
      epicsMutexUnlock(connectionMutex);
      return 1;
   }

   outSock = socket(AF_INET, SOCK_STREAM, 0);
   if (outSock == -1) {
      printf("Unable to open output socket.\n");
      epicsMutexUnlock(connectionMutex);
      return 2;
   }

   if (connect(outSock, (struct sockaddr *)&adr_srvr, sizeof(adr_srvr)) < 0) {
      printf("Connect to Tracker failed\n");
      close(outSock);
      outSock = 0;
      epicsMutexUnlock(connectionMutex);
      return 3;
   }

   epicsMutexUnlock(connectionMutex);

   return 0;

}

void closeTracker() {

   if (outSock) {
      epicsMutexLock(connectionMutex);
      close(outSock);
      outSock=0;
      epicsMutexUnlock(connectionMutex);
   }
}

int checkTracker() {

   int status = 1;
   int data;

   if (connectionMutex == 0) {
      return 0;
   }

   epicsMutexLock(connectionMutex);
   if (outSock == 0) {
      epicsMutexUnlock(connectionMutex);
      return 0;
   }
   epicsMutexUnlock(connectionMutex);
      
   status = read(outSock, &data, sizeof(int));

   if (status == 0) {
      epicsMutexLock(connectionMutex);
         close(outSock);
         outSock = 0;
         printf("Tracker disconnected\n");
      epicsMutexUnlock(connectionMutex);
   }

   return status;
}

int sendPosition(char *pos) {

   static struct gebData zeromsg;
   struct gebData *outmsg;

   if (pos) {
      outmsg = (struct gebData *)pos;
   } else {
     outmsg = &zeromsg;
     printf("Sending keepalive message to tracker\n");
   }

   epicsMutexLock(connectionMutex);
   if (!outSock) {
      epicsMutexUnlock(connectionMutex);
      if (pos) {
         free(outmsg->payload);
         free(outmsg);
      }
      return 3;
   }

  if ( write(outSock, outmsg, GEB_HEADER_BYTES) != GEB_HEADER_BYTES) {
     printf("output to Tracker failed 1 %s\n", strerror(errno));
     close(outSock);
     outSock = 0;
     printf("Tracker disconnected\n");
     epicsMutexUnlock(connectionMutex);
     if (pos) {
        free(outmsg->payload);
        free(outmsg);
     }
     return 1;
   }
   /* and here we send the payload itself */
   if ( write(outSock, outmsg->payload, outmsg->length) != outmsg->length) {
     printf("output to Tracker failed 2 %s\n", strerror(errno));
     close(outSock);
     outSock = 0;
     printf("Tracker disconnected\n");
     epicsMutexUnlock(connectionMutex);
     if (pos) {
        free(outmsg->payload);
        free(outmsg);
     }
     return 1;
   }
   epicsMutexUnlock(connectionMutex);
   if (pos) {
      free(outmsg->payload);
      free(outmsg);
   }
   return 0;
}

extern struct seqProgram GESend;

int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    seqRegisterSequencerCommands();
    threadId = seq((void *)&GESend, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        epicsThreadExitMain();
    }
    return(0);
}
