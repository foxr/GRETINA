
#include "GEBLink.h"

struct gebClient {
   epicsMutexId connectionMutex;
   int outSock;
   int bigendian;
   struct gebData zeromsg;
};

struct gebClient *GEBClientInit();

int sendGEBData(struct gebClient *, struct gebData *);
int setGEBClient(struct gebClient *, char *addr, int port);
void closeGEBClient(struct gebClient *);
int checkGEBClient(struct gebClient * );

