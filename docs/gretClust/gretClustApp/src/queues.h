/* 
 * fixed char buffer size for any decomp output, to simplifiy 
 * transport to tracker.
 */

int queueInit(int num0, int num1);
int queueSend(int qnum, char *outp);
void queueFlush(int qnum);
int queueReceive(int qnum, char **bp);
