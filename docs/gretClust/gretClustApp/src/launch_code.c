#include <sys/types.h>	/* for fork and kill */
#include <unistd.h>	/* for fork */
#include <signal.h>	/* for kill */
#include <wait.h>	/* for waitpid */
#include <errno.h>
#include <ctype.h>	/* for isdigit */
#include <stdlib.h>	/* for getenv and malloc */
#include <string.h>	/* strcat, strcpy, strlen */
#include <stdio.h>	/* printf, perror */


#include <string.h>
#include "epicsThread.h"
#include "iocsh.h"

#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>

#include <seqCom.h>

extern struct seqProgram launcher;

int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    threadId = seq((void *)&launcher, macro_def, 0);
    while (1) sleep(1);
    return(0);
}

extern struct seqProgram launcher;
					
/* main program taken from launcher.c built with -m option, then the 
 * daemonize call added .
 */

/*
int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;

    threadId = seq((void *)&launcher, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        int status;
        while (1) 
        {
            sleep(1);
        }
    }
    return(0);
}
*/

/*
   a small library for starting and stopping programs for use by the launcher
   sequencer program.  Program ID is kept by the sequencer in a PV.
 */


/*
  Sequencer calls this with "IdPV = startProg(progPV, instancePV)"
 */
   
int startProg(char *progString, int instance, int cryid, int tnport) {

   int lpid;

   lpid = fork();

   if (lpid == 0) {
      if (progString && progString[0] != 0) {
         char instbuffer[30];
         char portbuffer[20];
         char progbuffer[20];
   
         snprintf(instbuffer, 30, "instance=%03d,cryid=%02d", instance, cryid);
         instbuffer[29] = 0;
         snprintf(portbuffer, 20, "%d", tnport);
         portbuffer[19] = 0;
         snprintf(progbuffer, 20, "./%s", progString);
         progbuffer[19] = 0;

         execlp("procServ", "procServ", "--noautorestart",  
              portbuffer, progbuffer, "-s", instbuffer, (char *)0);
         /* this assumes we have been dropped into the bin dir we want to run */
/*
         execl(progbuffer, progbuffer, "-s", instbuffer, (char *)0);
*/
         printf("Exec of %s %s failed\n", progbuffer, instbuffer);
      }
      execlp("sleep", "sleep", "10", (char *)0);
      exit(127);
   } else {
      return lpid;
   }
   return 0;
}

/* 
   kill program.  Sequencer uses "killProg(idPV);"
 */
int killProg(int progId) {
   if (progId) {
      kill(progId, SIGKILL);
      return 0;
   }
   return -1;
}

int waitProg(int progId) {
   int status;

   if (progId) {
      status = waitpid(progId, 0, WNOHANG);
      if (status > 0 && status != progId) return 0;
      if (status > 0) return 1;
      if (status == 0) return 0;
      return -1;
   }
   return 1;
}
/*
   Put hostname on PV names 
 */
char *makePVName(char *suffix) {

   char *retval;
   char *hn;

   hn = getenv("HOSTNAME");
   hn++;	/* hostnmes now have form n0000 */
   retval = (char *)malloc(strlen(suffix) + strlen(hn) + 5);
   strcpy (retval, "node");
   strcat(retval, hn);
   strcat (retval, suffix);
   return retval;
}
#define MAXSLOTS 2

/* 
   makeInstance() returns an instance number which is the slot added to the 
   node number after the latter has been shifted left according to the number 
   of bits required by MAXSLOTS. 

   slot is 0-based.  Returns -1 on error.
*/
int makeInstance(int slot) {
   char *hn, *ep;
   int retval = 0;
   int slots, slotbits = 0, slotmask = 0;

   hn = getenv("HOSTNAME");
   hn++;        /* hostnmes now have form n0000 */
   retval = strtol(hn,&ep,10);
   if (ep == hn) return -1;
   if (MAXSLOTS > 1) {
      slots = MAXSLOTS;
      while (slots > 1) {
        slotmask = (slotmask << 1) + 1;
        slotbits++;
        slots /= 2;
      }
      if (slot > slotmask ) return -1;
      retval <<= slotbits;
      retval += slot;
   } 
   retval++;	/* nodes start at 0; instances start at 1 */
   return retval;
}
     


