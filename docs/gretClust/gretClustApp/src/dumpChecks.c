#include <stdio.h>
#include <stdlib.h>

#include <gdecomp.h>

int main(int argc, char *argv[]) {

    struct crys_intpts inbuf;
    FILE *inf;
    
    if (argc != 2) {
       printf("Usage: %s <input file>", argv[0]);
       exit(1);
    } 

    inf = fopen(argv[1], "r"); 

    if (!inf) {
       printf("Cannot open %s for reading\n", argv[1]);
       exit(2);
    }

    while (fread(&inbuf, sizeof(struct crys_intpts), 1, inf)) {
       printf("%24lld 0x%x\n", inbuf.timestamp, (unsigned int)inbuf.num);
    }
    return 0;
    
}
