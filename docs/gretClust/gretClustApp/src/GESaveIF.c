#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsMutex.h>
#include <epicsThread.h>
#include <epicsMessageQueue.h>
#include <iocsh.h>

#include "queues.h"
#include <gdecomp.h>
#include "GEBLink.h"
#include "logSend.h"

static char *outputfile;
int numWritten;

   
/*
 * Generate output file name based on data directory, run directory, and
 * crystal id. 
 */
int setOutputFile(char *dirname, char *runname) {
   int pathlen;
   char *namebuf;

   if (!dirname || *dirname == 0) return -1;
   if (!runname || *runname == 0) return -1;

   pathlen = strlen(dirname) + strlen(runname) + 20;
   namebuf = (char *)calloc(pathlen, 1);
   snprintf(namebuf, pathlen, "%s/%s/GlobalPos.dat", dirname, runname);
   printf("new filename %s\n", namebuf);
   if (outputfile) free(outputfile);
   outputfile = namebuf;
   return 0;
}

int validOutput() {
   /* so far, just don't let it exist */
   struct stat retstat;

   return (stat(outputfile, &retstat));
}

FILE *outfile;

int openOutput()  {

    if (!validOutput()) return 0;

    numWritten = 0;
    outfile = fopen(outputfile, "w");
    return (outfile?1:0);
}

int closeOutput() {
   fclose(outfile);
   return 0;
}

/* here we write the decomp positions but ignore aux data */
int writeOutput(char *input) {
   int numwrit = 0;
   struct gebData *indat;
   struct crys_intpts *inpts;

   if (!input) 
      return 1;

   indat = (struct gebData *)input;

   switch (indat->type) {
      case GEB_TYPE_DECOMP:
        inpts = (struct crys_intpts *)indat->payload;
        numwrit = fwrite(inpts, sizeof(struct crys_intpts), 1, outfile);
        if (numwrit) numWritten++;
        break;
      default:
        break;
   }
   free(indat->payload);
   free(input);
   return (numwrit?0:1);
}

extern struct seqProgram GESave;

int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    seqRegisterSequencerCommands();
    threadId = seq((void *)&GESave, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        epicsThreadExitMain();
    }
    return(0);
}
