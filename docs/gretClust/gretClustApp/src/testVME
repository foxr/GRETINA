#!/bin/bash -l
#
# A script to help set up the test vme crate environment
#
# This script must be invoked using the full path to the copy that has 
# been installed in the bin/architecture directory. It assumes that 
# the tcDet (location of vme code) is located at the same level. 
#
# Each element in the system runs in an xterm that is running on the local 
# machine where the script is first invoked.  For software not apropriate
# to run on that machine, the command is run over ssh on the remove machine
# by invoking this script there.
#
# Because the xterms all are running locally it is easy to locate and kill 
# them.  Because things are started remotely by calling the same script the
# code is localized to a single file
#
KillByWord() {
   word=$1
   PID=`ps -ef |
       grep "$word" |
       grep -v "grep" |
       awk '{print $2}'`
   if [ "$PID" != "" ]; then
       kill $PID
   fi
}
vmeConsole() {
   export TERM=vt100
   cu -l /dev/ttyS0
}
runEdm() {
   export EDMDATAFILES=.:$gretTop/gretClust/gretClustApp/Opi:$gretTop/tcDig/gretDigApp/Opi:$gretTop/gretTrig/gretTrigApp/Opi:$gretTop/gretVME/gretVMEApp/Opi:/global/devel/supTop/31410/vxStats/opi:$gretTop/CAENPS/CAENPSApp/Opi:$gretTop/gretClust/gretDataApp/Opi
   edm -x gretEngTS.edl
}
runRunCon() {
# Saves data to 1 file/run. Use only with runRcvrQ
    $gretTop/gretClust/gretClustApp/src/testControl.py
# new run control that makes directories for run data
#   $gretTop/gretClust/gretClustApp/src/runControl.py
}
runSoftIOC() {
   cd $gretTop/gretClust/iocBoot/iocGretClustGT
   export LD_LIBRARY_PATH=$baseLib
   ./st.cmd
}
runCAENPS() {
   cd $gretTop/CAENPS/iocBoot/iocCAENPS
   export LD_LIBRARY_PATH=$baseLib:$gretTop/CAENPS/CAENPSApp/src
   ./st.cmd
}
runReceiver() {
   cd $clustBin
   export LD_LIBRARY_PATH=$baseLib
# 4-channel receiver that writes to single file
   ./runRcvrQ -s "C1=41,C2=42,C3=43,C4=44"
#  single-channel receiver that uses directory
#   ./runRcvr -s "instance=001" 
}
runLogger() {
   cd $clustBin
   export LD_LIBRARY_PATH=$baseLib
   ./Log -s 
}
   
KILL() {
   KillByWord VMECPU
   KillByWord DISPLAY
   KillByWord RUNCONTROL
   KillByWord SOFTIOC
   KillByWord CAENPS
   KillByWord RECEIVER
   KillByWord LOGGER
}
EXIT() {
   exit 0
}

#
# Inline portion begins here
#

fullname=$0
FC=`expr "$fullname" : '\(.\).*'`
if [ $FC != "/" ]
then
   echo "Must invoke $fullname with full path"
   exit
fi
export clustBin=`dirname $0`
echo "clustBin $clustBin"
gretTop=`dirname $clustBin`
gretTop=`dirname $gretTop`
gretTop=`dirname $gretTop`
echo "gretTop $gretTop"
HN=`hostname`
PREFMACH=op1.gam
baseLib=/global/devel/base/R3.14.10/lib/linux-x86
# engineering test stand
export EPICS_CA_SERVER_PORT=5072
export EPICS_CA_REPEATER_PORT=5073
export EPICS_CA_CONN_TMO=4
export EPICS_CA_BEACON_PERIOD=2

#
# This part of the script runs the commands that will appear in the xterm 
# windows, whether run remotely or locally.  This is the only case
# in which the script is invoked with a parameter. The parameter is the same
# number that was entered in the menu by the operator in the interactive copy. 
#
if [ $# -eq 1 ]
then
    cmd=$1
    case $cmd in
       1)	vmeConsole ;;
       2)	runEdm ;;
       3)	runRunCon ;;
       4)	runSoftIOC ;;
       5)	runCAENPS ;;
       6)	runReceiver ;;
       7)	runLogger ;;
    esac
    exit
fi

#
# This is the interactive part of the script. Loop on printing menu and
# acting on input 
#
# For each requested software item to start, the script starts an xterm on the
# local machine. The xterm invokes the script again with the menu item number
# as a parameter, either locally or remotely using ssh. This way the software
# is always started on the appropriate machine using data from within this 
# script. Assumes script is available on all machines in the same location.
#
# The kill command will kill the xterms and thus kill the software on all 
# machines.
#
# The Exit command will exit the script but not kill the xterms. To exit the
# script and kill the xterms, use control-C.  
#

# first check we are running on the console, on the right machine
if [ ${DISPLAY=""} != ":0" -a $DISPLAY != ":0.0"  ]; then
   echo "Must run script from main console."
   exit
fi
if [ $HN != $PREFMACH ]; then
   echo "Must run on " $PREFMACH
   exit
fi

while :
do
   echo "------ MENU ------"
   echo "   VME		1"
   echo "   Display	2"
   echo "   Run Con	3"
   echo "   Soft IOC	4"
   echo "   CAEN PS	5"
   echo "   Receiver	6"
   echo "   Logger	7"
   echo "   EXIT		E"
   echo "   KILL ALL	K"
   echo "------------------"

   echo -n "Command> "; read cmd

   case $cmd in
      1)	xterm -T VMECPU -sb -sl 1000 -e "$fullname $cmd" & ;;
      2)	xterm -T DISPLAY -sb -sl 1000 -e "$fullname $cmd" & ;;
      3)	xterm -T RUNCONTROL -sb -sl 1000 -e "$fullname $cmd" & ;;
      4)	xterm -T SOFTIOC -sb -sl 1000 -e "$fullname $cmd" & ;;
      5)	xterm -T CAENPS -sb -sl 1000 -e "ssh d0.gam $fullname $cmd" & ;;
      6)	xterm -T RECEIVER -sb -sl 1000 -e "ssh op2.gam $fullname $cmd" & ;;
      7)	xterm -T LOGGER -sb -sl 1000 -e "$fullname $cmd" & ;;
      [eE])	EXIT ;;
      [kK])	KILL ;;
   esac

done
