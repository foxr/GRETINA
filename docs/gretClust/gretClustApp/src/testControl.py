#!/usr/bin/python
import wx
#import wx.lib.dialogs
import os
import time
import os.path
import shutil
from CaMonitor import *
import time
import sys

#
# Root window
#

class runconFrame(wx.Frame):
   def  __init__(self, parent,ID,title):
      wx.Frame.__init__(self, parent, ID, title, wx.DefaultPosition, wx.Size(300,150))
      #
      # status bar at bottom
      #
      self.CreateStatusBar()
      self.SetStatusText("GRETINA Run Control Root Window")
      
      #
      #  menubar at top
      #
      menu = wx.Menu()
      self.menuAppend(menu, 'Exit', 'Exit Program', self.OnQuit)

      menuBar = wx.MenuBar()
      menuBar.Append(menu, "&File")

      menu = wx.Menu()
      self.menuAppend(menu, 'Start EDM', 'Start EPICS Displays', self.OnEDM)

      menuBar.Append(menu, "&Tools")

      self.SetMenuBar(menuBar)

      tophsizer = wx.BoxSizer(wx.HORIZONTAL)
      sizer = wx.BoxSizer(wx.VERTICAL)
     
      # data dir
      self.dirArea = DirArea(self)
      sizer.Add(self.dirArea, 0, wx.GROW|wx.ALL, 5)
      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      # file name
      self.filenameArea = FilenameArea(self)
      sizer.Add(self.filenameArea, 0, wx.GROW|wx.ALL, 5)
      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      self.filename = CaMonitor('Data_CS_FileName', ca.DBF_CHAR, 
           updateFilename(self.filenameArea), ConnEvent)
      self.dirname = CaMonitor('Data_CS_Dir', ca.DBF_CHAR, 
           updateDir(self.dirArea), ConnEvent)

      # ca connection status
      self.connectArea = showConnect(self)
      sizer.Add(self.connectArea, 0, wx.GROW|wx.ALL|wx.ALIGN_CENTER, 5)
      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      # start button
      self.bigButton = ButtonArea(self)
      sizer.Add(self.bigButton, 0, wx.GROW|wx.ALL|wx.ALIGN_CENTER, 5)

      tophsizer.Add(sizer, 0, wx.GROW|wx.ALL|wx.ALIGN_LEFT, 5)

      # chi sqr max
      sizer = wx.BoxSizer(wx.VERTICAL)
      self.chiSqrMax = ChiSqrMax(self)
      sizer.Add(self.chiSqrMax, 0, wx.GROW|wx.ALL|wx.ALIGN_TOP, 5)
      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      # output control (TestMode and Verbose)
      self.testMode = TestMode(self)
      sizer.Add(self.testMode, 0, wx.GROW|wx.ALL|wx.ALIGN_TOP, 5)
      self.verbose = Verbose(self)
      sizer.Add(self.verbose, 0, wx.GROW|wx.ALL|wx.ALIGN_TOP, 5)

      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      # run timer
      self.runTimeArea = RunTimeArea(self)
      sizer.Add(self.runTimeArea, 0, wx.GROW|wx.ALL|wx.ALIGN_TOP, 5)
      sizer.Add(wx.StaticLine(self), 0, wx.GROW|wx.ALL, 5)

      tophsizer.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.GROW|wx.ALL, 5)

      tophsizer.Add(sizer,  0, wx.GROW|wx.ALL|wx.ALIGN_RIGHT, 5)

      self.SetSizer(tophsizer)
      self.SetAutoLayout(True)
      tophsizer.Fit(self)


   def OnEDM(self, event): 
      print 'EDM start'
      
   #
   # Handle exit selected from file menu
   #
   def OnQuit(self, event):
      print 'Bailing..'
      self.Close(True)

   def menuAppend(self, menu, text, help, func):
      mi = wx.MenuItem(menu,wx.NewId(), text, help)
      self.Bind(wx.EVT_MENU, func, mi)
      menu.AppendItem(mi)

class connEvent(baseEvent):
   def callback(self):
         app.frame.connectArea.setConnection(CaConnect.caConnected())

ConnEvent = connEvent()

class showConnect(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.VERTICAL)
      self.CaStatusText = wx.TextCtrl(parent, -1,
                          "CA Not Connected",size=(80,-1),
                          style=wx.TE_READONLY)
      self.Add(self.CaStatusText, 0, wx.GROW|wx.ALL, 5)

   def setConnection(self,conned):
      if conned:
         self.CaStatusText.Clear()
         self.CaStatusText.AppendText('CA Connected')
      else:
         self.CaStatusText.Clear()
         self.CaStatusText.AppendText('CA Not Connected')

class RunTimeArea(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.VERTICAL)
      self.winda = parent
      hbox = wx.BoxSizer(wx.HORIZONTAL)
      self.runtime = 0
      self.timing = 0
      self.starttime = 0
      label = wx.StaticText(self.winda, -1, "Run Time (s) : ")
      hbox.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.TimeCtrl = wx.TextCtrl(self.winda, -1, "", size=(80,-1),
                                       style = wx.TE_PROCESS_ENTER)
      self.winda.Bind(wx.EVT_TEXT_ENTER, self.SetRunTime, self.TimeCtrl)
      hbox.Add(self.TimeCtrl, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.Add(hbox, 0, wx.GROW|wx.ALIGN_CENTRE|wx.ALL, 5)
      hbox = wx.BoxSizer(wx.HORIZONTAL)
      label = wx.StaticText(self.winda, -1, "Remaining: ")
      hbox.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.TextUpdate = wx.TextCtrl(self.winda,-1,"",size=(80,-1),
                                       style = wx.TE_READONLY)
      hbox.Add(self.TextUpdate, 0, wx.GROW|wx.ALL, 5)
      self.sec = str(0)
      self.TextUpdate.AppendText(self.sec)
      self.timetick = CaMonitor('DAQG_CV_TS', ca.DBF_STRING,
                          updateRunTime(), ConnEvent)
      self.Add(hbox, 0, wx.GROW|wx.ALIGN_CENTRE|wx.ALL, 5)

   def starttiming(self):
      if self.runtime == 0:
         app.frame.bigButton.onlineStartStop.putw("Start",ca.DBR_STRING)
      else:
         self.timing = 1

   def SetRunTime(self, evt): 
      self.runtime = int(self.TimeCtrl.GetValue())
      self.TextUpdate.Clear()
      self.TextUpdate.AppendText(str(self.runtime))

class updateRunTime(baseEvent):
   def callback(self):
      try:
         timetick = int(app.frame.runTimeArea.timetick.getCache())
         if app.frame.runTimeArea.timing == 0:
            disptime = app.frame.runTimeArea.runtime
         elif app.frame.runTimeArea.timing == 1:
            app.frame.runTimeArea.starttime = timetick
            app.frame.bigButton.onlineStartStop.array_put("Start",ca.DBR_STRING)
            disptime = app.frame.runTimeArea.runtime
            app.frame.runTimeArea.timing = 2
         else:
            disptime = app.frame.runTimeArea.starttime + \
                       app.frame.runTimeArea.runtime - timetick
            if disptime <= 0:
               app.frame.bigButton.StartStop(None)
               app.frame.runTimeArea.timing = 0

         app.frame.runTimeArea.TextUpdate.Clear()
         app.frame.runTimeArea.TextUpdate.AppendText(str(disptime))
      except CaMonitor.NoValueYetException:
         print "Run Timer timetick invalid"


class DirArea(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.VERTICAL)
      self.winda = parent
      hbox = wx.BoxSizer(wx.HORIZONTAL)
      label = wx.StaticText(self.winda, -1, "Data Directory")
      hbox.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      btn = wx.Button(self.winda, -1, " Browse ")
      self.winda.Bind(wx.EVT_BUTTON, self.GetDataDir, btn)
      hbox.Add(btn, 0, wx.ALL, 5)
      self.Add(hbox, 0, wx.GROW|wx.ALIGN_CENTRE|wx.ALL, 5)
      self.DataDirText = wx.TextCtrl(self.winda,-1,"",size=(80,-1),
                          style=wx.TE_READONLY)
      self.Add(self.DataDirText, 0, wx.GROW|wx.ALL, 5)
      self.path = os.getcwd();
      self.DataDirText.AppendText(self.path)

   def GetDataDir(self,evt):
      browser = wx.DirDialog(self.winda, defaultPath = self.path, 
                         message="Run Directory",
                         style = wx.DD_NEW_DIR_BUTTON)
      if browser.ShowModal() == wx.ID_OK:
          try:
             self.winda.dirname.putw(browser.GetPath(), ca.DBR_CHAR)
             self.path = browser.GetPath()
             self.DataDirText.Clear()
             self.DataDirText.AppendText(self.path)
          except CaChannelException, status: 
             print ca.message(status)
      browser.Destroy()

   def updatePath(self, newPath):
      self.path = newPath
      self.DataDirText.Clear()
      self.DataDirText.AppendText(self.path)

class FilenameArea(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.VERTICAL)
      self.winda = parent
      hbox = wx.BoxSizer(wx.HORIZONTAL)
      self.label = wx.StaticText(self.winda, -1, "Next FileName: ")
      hbox.Add(self.label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.FilenameText = wx.StaticText(self.winda,-1,"")
      hbox.Add(self.FilenameText, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.Add(hbox, 0, wx.GROW|wx.ALL, 5)

   def SetFilename(self, nfn):
      self.filename = nfn;
      self.FilenameText.SetLabel(self.filename)

class updateFilename(baseEvent):
   def __init__(self, fnobject):
      self.fnobject = fnobject

   def callback(self):
      try:
         filename = app.frame.filename.getStrCache()
         app.frame.filenameArea.SetFilename(filename)
      except CaMonitor.NoValueYetException: pass
   

class updateDir(baseEvent):
   def __init__(self, pathobject):
      self.pathobject = pathobject

   def callback(self):
      try:
         path = app.frame.dirname.getStrCache()
         self.pathobject.updatePath(path)
      except CaMonitor.NoValueYetException: pass

class updateStart(baseEvent):
   def callback(self):
      try:
         stop = app.frame.bigButton.onlineStartStop.getCache()
         if stop == "Start":
            app.frame.bigButton.startButton.SetLabel("Stop")
            app.frame.filenameArea.label.SetLabel("Using Filename: ")
         else:
            app.frame.bigButton.startButton.SetLabel("Start")
            app.frame.filenameArea.label.SetLabel("Next Filename: ")
      except CaMonitor.NoValueYetException: pass
  
class TestMode(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.HORIZONTAL)
      self.winda = parent
      self.testModeCheck = wx.CheckBox(self.winda, -1, "Test Mode")
      self.winda.Bind(wx.EVT_CHECKBOX, self.SetTestMode, self.testModeCheck)
      self.Add(self.testModeCheck, 0, wx.GROW|wx.ALL, 5)
      self.testmode = CaMonitor('Decomp_CS_TestMode', ca.DBF_STRING,
                                                             updateTestMode(), 
                                                             ConnEvent)

   def SetTestMode(self, evt):
      self.mode = self.testModeCheck.GetValue()
      if (self.mode):
         output = 'Test'
      else:
         output = 'Normal'
      try:
         self.testmode.putw(output, ca.DBR_STRING)
      except CaChannelException, status: 
         print ca.message(status)


class updateTestMode(baseEvent):
   def callback(self):
      try:
         input = app.frame.testMode.testmode.getCache()
         if (input == 'Test'):
            app.frame.testMode.mode = True
         else:
            app.frame.testMode.mode = False
         app.frame.testMode.testModeCheck.SetValue(app.frame.testMode.mode)
      except CaMonitor.NoValueYetException: 
         print "EPICS value not valid", app.frame.testMode.testmode

class Verbose(wx.BoxSizer):
   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.HORIZONTAL)
      self.winda = parent
      self.verboseCheck = wx.CheckBox(self.winda, -1, "Verbose ")
      self.winda.Bind(wx.EVT_CHECKBOX, self.SetVerbose, self.verboseCheck)
      self.Add(self.verboseCheck, 0, wx.GROW|wx.ALL, 5)
      self.verbose = CaMonitor('Decomp_CS_Verbose', ca.DBF_STRING,
                                                             updateVerbose(), 
                                                             ConnEvent)

   def SetVerbose(self, evt):
      self.verb = self.verboseCheck.GetValue()
      if (self.verb):
         output = 'Verbose'
      else:
         output = 'Quiet'
      try:
         self.verbose.putw(output, ca.DBR_STRING)
      except CaChannelException, status: 
         print ca.message(status)


class updateVerbose(baseEvent):
   def callback(self):
      try:
         input = app.frame.verbose.verbose.getCache()
         if (input == 'Verbose'):
            app.frame.verbose.verb = True
         else:
            app.frame.verbose.verb = False
         app.frame.verbose.verboseCheck.SetValue(app.frame.verbose.verb)
      except CaMonitor.NoValueYetException: 
         print "EPICS value not valid", app.frame.verbose.verbose
        

class ChiSqrMax(wx.BoxSizer):

   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.HORIZONTAL)
      self.winda = parent
      label = wx.StaticText(self.winda, -1, "Max ChiSqr:")
      self.Add(label, 0, wx.ALL|wx.ALIGN_CENTER, 5)
      self.TextUpdate = wx.TextCtrl(self.winda,-1,"",size=(80,-1),
                                       style = wx.TE_PROCESS_ENTER)
      self.winda.Bind(wx.EVT_TEXT_ENTER, self.SetChiMax, self.TextUpdate)
      self.Add(self.TextUpdate, 0, wx.GROW|wx.ALL, 5)
      self.max = str(99.0)
      self.TextUpdate.AppendText(self.max)
      self.chisqrmax = CaMonitor('Decomp_CS_ChiSqMax', ca.DBF_STRING,
                                                             updateChiMax(), 
                                                             ConnEvent)
   def SetChiMax(self, evt): 
      self.max = self.TextUpdate.GetValue()
      try:
         self.chisqrmax.putw(self.max, ca.DBR_STRING)
      except CaChannelException, status: 
         print ca.message(status)

class updateChiMax(baseEvent):
   def callback(self):
      try:
         newmax = app.frame.chiSqrMax.chisqrmax.getCache()
         app.frame.chiSqrMax.TextUpdate.Clear()
         app.frame.chiSqrMax.max = str(newmax)
         app.frame.chiSqrMax.TextUpdate.AppendText(app.frame.chiSqrMax.max)
      except CaMonitor.NoValueYetException: 
         print "EPICS value not valid", app.frame.chiSqrMax.chisqrmax
        
       

class ButtonArea(wx.BoxSizer):

   def __init__(self, parent):
      wx.BoxSizer.__init__(self,wx.VERTICAL)
      self.winda = parent
      self.startButton = wx.Button(parent, -1, "Init")
      self.winda.Bind(wx.EVT_BUTTON, self.StartStop, self.startButton)
      self.Add(self.startButton, 0, wx.ALL | wx.ALIGN_CENTER, 5)
      self.onlineStartStop = CaMonitor('Online_CS_StartStop', ca.DBF_STRING,
                                                             updateStart(), 
                                                             ConnEvent)
      self.onlineStartStop.pend_io()

   def StartStop(self,evt):
      if not CaConnect.caConnected():
         return
      if self.startButton.GetLabel() == "Initial":
         app.frame.filenameArea.label.SetLabel("Next Filename: ")
         return
      if self.startButton.GetLabel() == 'Start':
         app.frame.runTimeArea.starttiming()
      elif self.startButton.GetLabel() == 'Stop':
         self.onlineStartStop.array_put("Stop",ca.DBR_STRING)
      else:
         print 'Start Button: ', self.startButton.GetLabel()
         
      print 'Start Button'

#
# Establish and run application
#
class runconApp(wx.App):
   def OnInit(self):
      self.frame = runconFrame(None,-1,"GRETINA Run Control")

      # overall window setup
      self.frame.Show(True)
      self.SetTopWindow(self.frame)

      # start channel access polling
      self.polltimer = wx.PyTimer(self.pollCa)
      self.polltimer.Start(200)

      return True

   def pollCa(self):
      ca.poll()
      self.polltimer.Start(200)




app = runconApp(0)


app.MainLoop()      

