#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <psNet.h>
#include "receiver.h"

struct rcvrInstance {

   struct sockaddr_in adr_srvr;  /* AF_INET */
   int len_inet;                /* length  */
   int recSock;
   int packetsreceived;
   int packetssent;
   int seqerrs;
   int bytesrec;
};



char *initReceiver(char *srvr_addr) {

   struct rcvrInstance *retval;

   retval = (struct rcvrInstance *)calloc(sizeof(struct rcvrInstance), 1);
   if (!retval) 
      return (0);


   /* create server addressed socket */
   memset(&retval->adr_srvr,0,sizeof retval->adr_srvr);
   retval->adr_srvr.sin_family = AF_INET;
   retval->adr_srvr.sin_port = htons(SERVER_PORT);
   retval->adr_srvr.sin_addr.s_addr =
          inet_addr(srvr_addr);

   if ( retval->adr_srvr.sin_addr.s_addr == INADDR_NONE ) {
      printf("bad server address %s port %d\n", srvr_addr, SERVER_PORT);
      return (0);
   }

   retval->len_inet = sizeof(retval->adr_srvr);
   retval->recSock = -1;

   printf("Server addr and port: %s %d\n", srvr_addr, SERVER_PORT);

   return (char *)retval;
}

int stopReceiver(char *instancechar) {
   struct rcvrInstance *instance;

   instance = (struct rcvrInstance *)instancechar;
   if (!instance) {
      printf("Null receiver instance in stopReceiver\n");
      return -1;
   }
   if (instance->recSock == -1) {
      return 0;
   }
   close(instance->recSock);
   instance->recSock = -1;
   return 0;
}

#define INBUFSIZE (64 * 1024)

int getReceiverData(char *instancechar, float len, char **retptr) {
  
   struct rcvrInstance *instance;
   struct reqPacket request;
   struct inbuf *datRoot = 0;
   struct inbuf *datEnd = 0;
   evtServerRetStruct firstreply;
   int bytesret = 0;
   struct inbuf *thisbuf;
   int temptype;
   int recsize;
   int numrecs = 0;
   int numret = 0;
   int recstoread, recsperbuf;

   instance = (struct rcvrInstance *)instancechar;
   if (!instance) {
      printf("Null receiver instance in getReceiverData\n");
      return -1;
   }
          
   if (instance->recSock == -1) {
   
      instance->recSock = socket(AF_INET,SOCK_STREAM,0);
      if (instance->recSock == -1) {
         printf("Unable to open socket.\n");
         return -1;
      }
      
      if ( connect(instance->recSock, (struct sockaddr *)&instance->adr_srvr, 
                                              instance->len_inet) < 0) {
         printf("connect failed %s\n", strerror(errno));
         close(instance->recSock);
         instance->recSock = -1;
         return -1;
      }
      request.type = htonl(CLIENT_REQUEST_EVENTS);
      if ( write(instance->recSock, &request, sizeof(struct reqPacket)) < 0) {
        printf("request send failed\n");
        close(instance->recSock);
        instance->recSock = -1;
        return -1;
      }
   }

   datRoot = 0;
   datEnd =0;
   

   while (bytesret < (sizeof(evtServerRetStruct))) {
      numret = read(instance->recSock, ((char *)&firstreply.type) + bytesret, 
                               sizeof(evtServerRetStruct) - bytesret);
      if (numret <= 0) { break; } else {bytesret += numret;}
   }
   if (numret <= 0) { 
      printf("read returned %d\n", numret); 
      return -1;
   }
   temptype = ntohl(firstreply.type);

   if (temptype == SERVER_SUMMARY) {
      recsize = ntohl(firstreply.recLen) + 1;
      numrecs = ntohl(firstreply.recs);
      /* ask for the next data */
      request.type = htonl(CLIENT_REQUEST_EVENTS);
      if ( write(instance->recSock, &request, sizeof(struct reqPacket)) < 0) {
        printf("request send failed\n");
        close(instance->recSock);
        instance->recSock = -1;
        return -1;
      }
   } else {
      if (temptype == INSUFF_DATA) {
         printf("Sender: Data not available\n");
         /* go ahead and ask again */
         request.type = htonl(CLIENT_REQUEST_EVENTS);
         if ( write(instance->recSock, &request, 
               sizeof(struct reqPacket)) < 0) {
           printf("request send failed\n");
           close(instance->recSock);
           instance->recSock = -1;
           return -1;
         }
         return -1;
      } else {
         /* No point in asking for more; we are bailing out */
         if (temptype == SERVER_SENDER_OFF) {
/*
            printf("Sender not enabled\n");
*/
         } else {
            printf("Illegal first packet type %d\n", temptype);
         }
         close(instance->recSock);
         instance->recSock = -1;
         return -1;
      }
   }

   /* if you got here, there is data to be read */

   recsperbuf = INBUFSIZE/(recsize*4);
 
   while (numrecs != 0) {
      
      if (numrecs > recsperbuf) {
         recstoread = recsperbuf;
         numrecs -= recsperbuf;
      } else {
         recstoread = numrecs;
         numrecs = 0;
      }
      thisbuf = (struct inbuf *)calloc(1,sizeof(struct inbuf));
      if (!thisbuf) {
         printf ("malloc failed\n");
         close(instance->recSock);
         instance->recSock = -1;
         return -1;
      }
      thisbuf->pdata = (unsigned int *)calloc(INBUFSIZE/4,4);
      if (!thisbuf->pdata) {
         printf ("pdata malloc failed\n");
         free(thisbuf);
         close(instance->recSock);
         instance->recSock = -1;
         return -1;
      }
      thisbuf->type = SERVER_NORMAL_RETURN;
      thisbuf->evtlen = recsize * 4;
      thisbuf->recs = recstoread;

      bytesret = 0;
      while (bytesret < thisbuf->evtlen * recstoread) {
         numret = read(instance->recSock, ((char *)thisbuf->pdata) + bytesret, 
                                  thisbuf->evtlen * recstoread - bytesret);
         if (numret <= 0) { break; } else {bytesret += numret;}
     }

     if (numret == 0) {
          printf(" End of file! \n");
          free(thisbuf->pdata);
          free(thisbuf);
          close(instance->recSock);
          instance->recSock = -1;
          return -1;
      }
      if (numret < 0) { 
         printf("read returned %d\n", numret); 
         return -1;
      }
      instance->bytesrec += bytesret;
      instance->packetsreceived++;
      if (datRoot == 0) {
         datRoot = thisbuf;
      } else {
         datEnd->next = thisbuf;
      }
      datEnd = thisbuf;
   }  /* end while */
   *retptr = (char *)datRoot;
   return 0;
}


int printPackets(char *instancechar) {

   struct rcvrInstance *instance;

   instance = (struct rcvrInstance *)instancechar;

   printf("Packets received, sent, diff, seqerrs, bytesrec  = %d %d %d %d %d\n",
                           instance->packetsreceived, instance->packetssent,
                           instance->packetsreceived - instance->packetssent, 
                           instance->seqerrs, instance->bytesrec);
   return 0;
}

