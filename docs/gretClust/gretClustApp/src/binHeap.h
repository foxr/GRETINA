
typedef struct binHeap {
   epicsMutexId mutex;
   struct iheap heap;
   struct iheap *root;
} binHeap;

binHeap *binHeapCreate(); 
void *binHeapGet(binHeap *id);
long long binHeapFrontTime(binHeap *id);
int binHeapAdd(binHeap *id, void *datum, long long timestamp);
int binHeapMerge(binHeap *dest, binHeap *inp);
