#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsMutex.h>
#include <epicsThread.h>
#include <epicsMessageQueue.h>
#include <iocsh.h>

#include "queues.h"
#include "iheapstruct.h"
#include "binHeap.h"
#include <gdecomp.h>
#include "logSend.h"
#include "GEBLink.h"

#define GEB_PORT 9005
#define MAX_DATA_CLIENTS 100 

static binHeap *sortedList;
static binHeap *inputList;

static long fds[MAX_DATA_CLIENTS];
static epicsMutexId fdMutexes[MAX_DATA_CLIENTS];
static binHeap *inLists[MAX_DATA_CLIENTS];
static long long exlowtime;
static int recSock;
static long long oldtime;

/* takingData can be reset to 0 from anywhere to stop accepting data */
int takingData = 1;

int GEBInit(char *id, int *port) {

   int i;

   char hname[80];
   struct sockaddr_in our_addr;
   struct hostent *hp;

   for (i = 0; i < MAX_DATA_CLIENTS; i++) {
      fdMutexes[i] = epicsMutexCreate();
      if (!fdMutexes[i]) {
         strcpy(id, "mutex create error");
         return 6;
      }
      inLists[i] = binHeapCreate();
      if (!inLists[i]) {
         strcpy(id, "list create error");
         return 6;
      }
   }


   recSock = socket(AF_INET, SOCK_STREAM, 0);
   if (recSock == -1) {
      strcpy(id, "socket error");
      return 1;
   }

   bzero((char *)&our_addr,sizeof(struct sockaddr_in));
   our_addr.sin_family = AF_INET;

   if  (gethostname(hname,79)) {
      strcpy(id, "host name error");
      return 2;
   }

   hp = gethostbyname(hname);
   if (!hp) {
      printf("hostname %s not resolved\n", hname);
      return 4;
   }

   our_addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)
                                (hp->h_addr_list[0]))));

   if (our_addr.sin_addr.s_addr == -1) {
      strcpy(id, "address error");
      return 3;
   }



   (*port)++;
   if (*port > (GEB_PORT + 99))
      *port = GEB_PORT;
   our_addr.sin_port = htons(*port);
   if (-1 == bind(recSock, (struct sockaddr *)&our_addr,
                        sizeof(struct sockaddr_in)))  {

      strcpy(id, "bind failed");
      return 4;
   }

   if (-1 == listen(recSock, 10)) {
      strcpy(id, "listen failed");
      return 5;
   }

   strcpy(id, hname);

   queueInit(100,100);

   sortedList = binHeapCreate();
   if (!sortedList) return 6;
   inputList = binHeapCreate();
   if (!inputList) return 6;

   return 0;
}

void dataReader(void *inSockstar) {
   long inSock;
   int numret, bytesret;
   char *inbuffer;
   struct gebData *outstruct;
   int ournode;

   inSock = (long)inSockstar;

   if (!takingData) {
      printf("Rejecting connection attempt; not taking data.\n");
      close(inSock);
      return;
   }

   printf("GEB data reader thread started\n");

   for (ournode=0; ournode < MAX_DATA_CLIENTS; ournode++) {
      epicsMutexLock(fdMutexes[ournode]);
      if (fds[ournode] != 0) { /* check if it has been closed */
         numret = recv(fds[ournode], &inbuffer, sizeof(void *),
                       MSG_PEEK | MSG_DONTWAIT);
         if (!numret) {
            close(fds[ournode]);
            fds[ournode] = inSock;
            epicsMutexUnlock(fdMutexes[ournode]);
            break;
         }
      } else {
          fds[ournode] = inSock;
          epicsMutexUnlock(fdMutexes[ournode]);
          break;
      }
      epicsMutexUnlock(fdMutexes[ournode]);
   }

   if (ournode >= MAX_DATA_CLIENTS) {
      printf("Node input slots depleted\n");
      close(inSock);
      return;
   }

   printf("Slot %d allocated in GESave\n", ournode);

   while (1) {
      
      if (!takingData) {
         printf("GEB data reader closed: not taking data.\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
      outstruct = calloc(1, sizeof(struct gebData));
      if (!outstruct) {
         printf("GEB data reader closed: malloc failed\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
      numret = 0; bytesret = 0;
      while (bytesret < GEB_HEADER_BYTES) {
         numret = read(inSock, ((char *)outstruct) + bytesret, 
                         GEB_HEADER_BYTES - bytesret);
         if (numret == 0) {
             printf("GEB data reader closed: End of file\n");
             free(outstruct);
             epicsMutexLock(fdMutexes[ournode]);
             close(inSock);
             fds[ournode] = 0;
             epicsMutexUnlock(fdMutexes[ournode]);
             return;
         }
         if (numret < 0) { 
            printf("read returned %d\n", numret); 
            numret = 0;
         }
         bytesret += numret;
      }
      if (outstruct->type == GEB_TYPE_KEEPALIVE) { 
         /* keepalive message */
         free(outstruct);
         continue; 	
      }
          
      numret = 0; bytesret = 0;
      inbuffer = calloc(1, outstruct->length);
      if (!inbuffer) {
         free(outstruct);
         printf("GEB data reader closed: malloc failed\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
      bytesret = 0;
      while (bytesret < outstruct->length) {
         numret = read(inSock, inbuffer + bytesret, 
                       outstruct->length - bytesret);
         if (numret == 0) {
             printf("GEB data reader closed: End of file\n");
             free(outstruct);
             free(inbuffer);
             epicsMutexLock(fdMutexes[ournode]);
             close(inSock);
             fds[ournode] = 0;
             epicsMutexUnlock(fdMutexes[ournode]);
             return;
         }
         if (numret < 0) { 
            printf("read returned %d\n", numret); 
            numret = 0;
         }
         bytesret += numret;
      }
      outstruct->payload = inbuffer;
      if (binHeapAdd(inLists[ournode], outstruct, outstruct->timestamp)) {
         free(outstruct);
         free(inbuffer);
         printf("Fatal sorting error , GEB Acceptor closed\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
      /* Usually this just means "if we just got read out, put us back in the
         hopper", but sometimes it could mean "We just got much earlier data
         that finally came through"
       */
      if (outstruct->timestamp == binHeapFrontTime(inLists[ournode])) {
         if (binHeapAdd(inputList, inLists[ournode], outstruct->timestamp)){
            printf("Fatal input sorting error , GEB Acceptor closed\n");
            epicsMutexLock(fdMutexes[ournode]);
            close(inSock);
            fds[ournode] = 0;
            epicsMutexUnlock(fdMutexes[ournode]);
            return;
         }
      }
/*
      printf("position obtained %x\n",ournode);
*/
   }
   printf("GEB data reader closed: Programming Error.\n");
}


int getRequests() {

     int inSock;

     while (1) {
        inSock = accept(recSock, 0, 0);
        if (inSock == -1) {
           printf("getRequest accept failed %s\n", strerror(errno));
           continue;
        }
        epicsThreadCreate("dataReader", epicsThreadPriorityLow, 
                          16384, dataReader, 
                          (void *)((long)inSock));
     }
     return 0;    
}

static int delay_seconds = 20;

int setGEBDelay(int d) {
   if (d < 0) return -1;
   delay_seconds = d;
   return 0;
}

long long getLowTime() {

   long long lowtime = 0, ttime;

   ttime = binHeapFrontTime(inputList);
   if (ttime > 0) {
       lowtime = ttime -  100000000 * delay_seconds; 
   }

   return lowtime;
}

static int time_nonmono;

int getGEBTimeNM() {
   return time_nonmono;
}

int nextPosition(char **out) {

   long long availabletime;
   binHeap *inp = 0;

   availabletime = binHeapFrontTime(sortedList);
   if (availabletime < 0) {
      inp = binHeapGet(inputList);
      if (inp) {
         printf("initializing main GEB list\n");
         binHeapMerge(sortedList, inp); 
         availabletime = binHeapFrontTime(sortedList);
      }
      if (availabletime < 0) return 1;
   }

   while (availabletime >= exlowtime && (inp = binHeapGet(inputList))) {
      binHeapMerge(sortedList, inp); 
      exlowtime = getLowTime();
      availabletime = binHeapFrontTime(sortedList);
   }
   if (availabletime >= exlowtime) {
      return 1;
   }
   if (availabletime < oldtime) {
      printf("output not monotonic %24lld %24lld\n", availabletime, oldtime);
      time_nonmono++;
   }
   oldtime = availabletime;
   *out = (char *)binHeapGet(sortedList);
   if (!*out) return -1;

   return 0;
}

void GEBShutdown() {
   close(recSock);
   printf("shutting down GEB\n");
}

int startGEB() {
   int i;
   void *temp;

   oldtime = 0;
   time_nonmono = 0;
  
   while ((temp = binHeapGet(sortedList))) free(temp);
   while ((temp = binHeapGet(inputList)));
   
   for (i = 0; i < MAX_DATA_CLIENTS; i++) {
      while ((temp = binHeapGet(inLists[i]))) free(temp);
      
   }
   exlowtime = 0;

   queueFlush(0);
   queueFlush(1);
   
   return 0;
}
