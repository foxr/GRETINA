#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsMutex.h>
#include <epicsThread.h>
#include <epicsMessageQueue.h>
#include <iocsh.h>

#include "queues.h"
#include <gdecomp.h>
#include "LogLink.h"
#include "sortingList.h"

#define MAX_LOG_NODES 120

static char outputfile[256];

static epicsThreadId ids[MAX_LOG_NODES];
static long fds[MAX_LOG_NODES];
static epicsMutexId fdMutexes[MAX_LOG_NODES];

static int recSock;

extern struct seqProgram Log;

int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    seqRegisterSequencerCommands();
    threadId = seq((void *)&Log, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        epicsThreadExitMain();
    }
    return(0);
}

static sortingList *logList;

int logInit(char *id, int *port) {

   int i;

   char hname[80];
   struct sockaddr_in our_addr;

   struct hostent *hp;
   
   logList = sortingListCreate();
   if (!logList) return 6;

   for (i = 0; i < MAX_LOG_NODES; i++) {
      fdMutexes[i] = epicsMutexCreate();
      if (!fdMutexes[i]) {
         strcpy(id, "mutex create error");
         return 6;
      }
   }


   recSock = socket(AF_INET, SOCK_STREAM, 0);
   if (recSock == -1) {
      strcpy(id, "socket error");
      return 1;
   }

   bzero((char *)&our_addr,sizeof(struct sockaddr_in));
   our_addr.sin_family = AF_INET;

   if  (gethostname(hname,79)) {
      strcpy(id, "host name error");
      return 2;
   }


   hp = gethostbyname(hname);

   if (!hp) {
      printf("bad hostname %s \n", hname);
      return 4;
   }

   our_addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)
                                (hp->h_addr_list[0]))));

   if (our_addr.sin_addr.s_addr == -1) {
      strcpy(id, "address error");
      return 3;
   }

   (*port)++;
   our_addr.sin_port = htons(*port);
   if (*port > (LOG_PORT + 99))
      *port = LOG_PORT;
   if (-1 == bind(recSock, (struct sockaddr *)&our_addr,
                        sizeof(struct sockaddr_in)))  {

      strcpy(id, "bind failed");
      return 4;
   }

   if (-1 == listen(recSock, 10)) {
      strcpy(id, "listen failed");
      return 5;
   }

   strcpy(id, hname);

   queueInit(100,100);

   return 0;
}


void logReader(void *inSockstar) {
   long inSock;
   int numret, bytesret;
   char *inbuffer;
   int ournode;

   inSock = (long)inSockstar;

   printf("Log accepter thread started\n");

   for (ournode=0; ournode < MAX_LOG_NODES; ournode++) {
      epicsMutexLock(fdMutexes[ournode]);
      if (fds[ournode] != 0) { /* check if it has been closed */
         numret = recv(fds[ournode], &inbuffer, sizeof(void *),
                    MSG_PEEK | MSG_DONTWAIT);
         if (!numret) {
            close(fds[ournode]);
            fds[ournode] = inSock;
            epicsMutexUnlock(fdMutexes[ournode]);
            break;
         }
      } else {
          fds[ournode] = inSock;
          ids[ournode] = epicsThreadGetIdSelf();
          epicsMutexUnlock(fdMutexes[ournode]);
          break;
      }
      epicsMutexUnlock(fdMutexes[ournode]);
   }

   if (ournode >= MAX_LOG_NODES) {
      printf("Node input slots depleted\n");
      close(inSock);
      return;
   }

   printf("Slot %d allocated in logger\n", ournode);
   while (1) {
      
      numret = 0; bytesret = 0;
      inbuffer = calloc(LOG_OUT_SIZE, 1);
      if (!inbuffer) {
         printf("Log acceptor closed: malloc failed\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
      bytesret = 0;
      while (bytesret < LOG_OUT_SIZE) {
         numret = read(inSock, inbuffer + bytesret, LOG_OUT_SIZE - bytesret);
         if (numret <= 0) { printf("read returned %d\n", numret); }

         if (numret == 0) {
             printf("Log acceptor closed: End of File\n");
             free(inbuffer);
             epicsMutexLock(fdMutexes[ournode]);
             close(inSock);
             fds[ournode] = 0;
             epicsMutexUnlock(fdMutexes[ournode]);
             return;
         }
         bytesret += numret;
      }
      if (inbuffer[0] == 0) {
         /* keepalive message */
         free(inbuffer);
         continue;
      }

      if (sortingListAdd(logList, inbuffer, 0)) {
         free(inbuffer);
         printf("Fatal logging error , Log Acceptor closed\n");
         epicsMutexLock(fdMutexes[ournode]);
         close(inSock);
         fds[ournode] = 0;
         epicsMutexUnlock(fdMutexes[ournode]);
         return;
      }
   }
   printf("Log acceptor closed: unknown error\n");
}


int getRequests() {

     int inSock;

     while (1) {
        inSock = accept(recSock, 0, 0);
        if (inSock == -1) {
           printf("getRequest accept failed %s\n", strerror(errno));
           continue;
        }
        epicsThreadCreate("logReader", epicsThreadPriorityLow, 
                          16384, logReader, 
                          (void *)((long)inSock));
     }
     return 0;    
}


int nextEntry(char **out) {

   *out = (char *)sortingListGet(logList);
   if (!*out) return 1;
   
   return 0;
}

int validOutputFile(char *dirname, char *rname) {
   /* so far, just don't let it exist */
   struct stat retstat;

   sprintf(outputfile, "%s/%s/Log", dirname, rname);

   return (stat(outputfile, &retstat));
}

FILE *outfile;

int openOutputFile(char *dirname, char *rname)  {

    time_t now;
    char *tc;

    if (!validOutputFile(dirname, rname)) return 1;

    now = time(0);
    tc = ctime(&now);
    tc[strlen(tc)-1] = 0;
    outfile = fopen(outputfile, "w");
    if (outfile) {
       fprintf(outfile, "%s Log file opened\n", tc); 
       fflush(outfile);
       fsync(fileno(outfile));
    } else {
       printf("Unable to open log file\n");
       fflush(stdout);
    }
    return (outfile?1:0);
}

int closeOutputFile() {
    time_t now;
    char *tc;

    now = time(0);
    tc = ctime(&now);
    tc[strlen(tc)-1] = 0;
    if (outfile) {
       fprintf(outfile, "%s Log file closed\n", tc); 
   }
   fclose(outfile);
   outfile = 0;
   return 0;
}

int writeToDisk(char *input) {
   int status = 0;
   int i;
   char *optr;

   if (!input) 
      return 1;


   optr = input;

   for (i = 0; i < 50; i++, optr += 80) {
      if (*optr == 0) break;
      if (outfile) {
          status = fprintf(outfile, "%s\n", optr); 
          fflush(outfile);
          fsync(fileno(outfile));
      } else {
          status = printf("%s\n", optr); 
          fflush(stdout);
      }
      if (status < 1) break;
   }
   free(input);
   if (status < 1) return 1;
   return 0;
}

void logShutdown() {
   close(recSock);
   printf("shutting down Log\n");
}

int startLog() {
   char *inbuffer;

   do {
      inbuffer = sortingListGet(logList);
      if (inbuffer) free(inbuffer);
   } while (inbuffer); 
   
   queueFlush(0);
   queueFlush(1);
   
   return 0;
}

