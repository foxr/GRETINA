program GESend ("instance=001")

option +d; 	/* debug (verbose) output */

%%#include <string.h>
%%#include "queues.h"
%%#include "GESendIF.h"
%%#include "logSend.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \
        t n##B;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYEVT(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		\
        evflag n##Event;        \
        sync n n##Event;        

#define SENDSTATE(s) 		\
	strcpy(statepv, #s);	\
	pvPut(statepv);		\
	pvFlush();

DECL(int, gebport, GEB_CV_Port)
DECL(string, gebaddr, GEB_CV_Addr)
DECLMON(short, startStop, Online_CS_StartStop)

DECL(int, timeerrs, GEB_CV_TimeErr)
DECLEVENT(int, gebdelay, GEB_CS_Delay)

DECL(int, Status, Decomp{instance}_CV_Status)
DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECL(string, statepv, Decomp{instance}_CV_State)

DECL(int, numwritten, Decomp{instance}_CV_Rate)

DECLMON(string, trackaddr, Tracker_CV_Addr)
DECLMON(int, trackport, Tracker_CV_Port)

DECLMON(string, logger, Log_CV_Addr)
DECLMON(int, logport, Log_CV_Port)


evflag startInput;
evflag stopInput;
evflag startProcess;
evflag stopProcess;
evflag stopOutput;
evflag startOutput;
evflag doneFlag;
evflag gebInited;
evflag loggerInited;
evflag trackInited;

int initstat;
int reqstat;
int count;
int tcstat;
int keepalive_counter;
int sendstat;
string logbuf1;

char *procInput;


ss setupAndRun {
  state init {
     when() {
        pvGet(gebport);
        printf("GEB port = %d\n", gebport);
        initstat = GEBInit(gebaddr, &gebport);
        pvPut(gebaddr);
        pvPut(gebport);
        /* one-time initializations */
     } state initCheck
  }
  state initCheck {
     when(initstat !=0) {
        printf("GEB initialization failed\n");
        SENDSTATE(Failed)
        GEBShutdown();
        exit(-1);
     } state restart
     when() { 
        efSet(gebInited);
     } state restart
  }
  state offline {
    entry {
       SENDSTATE(Offline)
    }
    when(enable) {
    } state restart
  }
  state restart {
    when(!enable) {
    } state offline
    when(startStop != 0 ) {
    } state check
    when() {
    } state setup
  }
  state check {
    entry {
       SENDSTATE(Check)
    }
    when (!enable) {
    } state offline
    /* If we restart while things are running, do nothing. */
    when (startStop == 0) {
    } state restart
    when () {
    } state setup
  }
  state run {
    entry {
       SENDSTATE(Run)
    }
    when (!enable || startStop == 0) {
       efSet(stopProcess);
    } state waitfordone
  }
  state checkrun {
     when (!setTracker(trackaddr, trackport)) {
       efSet(loggerInited);
       efSet(startProcess);
       efSet(trackInited);
       efClear(doneFlag);
     } state run
     when () {
       printf("failed to connect to tracker\n");
     } state restart
  }
  state setup {
    entry {
       SENDSTATE(Setup)
    }
    when (!enable) {
    } state offline
    when (startStop == 1) {
       printf("Start Command\n");
    } state checkrun
    when (delay(0.5)) {
       Status = 1;
       pvPut(Status);
    } state setup
  }
  state waitfordone {
    entry {
       SENDSTATE(Wait)
    }
    when(efTestAndClear(doneFlag)) {
    } state setup
  }
}

ss counters {
   state run {
      when(efTestAndClear(gebdelayEvent)) {
         gebdelayB = gebdelay;
         setGEBDelay(gebdelay);
      } state run
      when(delay(1)) {
         timeerrs = getGEBTimeNM();
         pvPut(timeerrs);
      }  state run
   }
}


ss checkOutSock {
   state preinit {
      when (efTestAndClear(trackInited)) {
      } state init
   }
   state init {
      option -e;
      entry {
         tcstat = checkTracker();
      }
      when (tcstat == 0) {
         sendLogMsg("GEB", "Tracker not connected");
         setTracker(trackaddr, trackport);
         sendLogMsg("GEB", "Tracker connection opened");
         sendToLog(0);
      } state wait
      when (delay(1.0)) {
      } state init
   }
   state wait {
     when (delay(1.0)) {
     } state init
   }
}
ss checkLogger {

   state preinit {
      when (efTestAndClear(loggerInited)) {
      } state init
   }
   state init {
      option -e;
      entry {
         tcstat = checkLogger();
      }
      when (tcstat == 0) {
         printf("Logger not connected\n");
         setLogger(logger, logport);
         sendLogMsg("GEB", "log opened");
         sendToLog(0);
      } state wait
      when (delay(1.0)) {
      } state init
   }
   state wait {
     when (delay(1.0)) {
     } state init
   }
}
ss getData {
   state run {
       when (efTest(gebInited)) {
           reqstat = getRequests();	/* never returns */
       } state teststat 
   }
   state teststat {
      when (delay(0.1)) {
      } state run
   }
}

ss process {
   state init {
     entry {
        printf("entering process:init\n");
     }
      when() {
      } state stopped
   }
   state stopped {
     entry {
        printf("entering process:stopped\n");
     }
      when (efTestAndClear(startProcess)) {
          numwritten = 0;
          pvPut(numwritten);
          startGEB();
          efClear(stopProcess);
      } state run
   }
   state run {
      when (efTestAndClear(stopProcess)) {
      } state cleanup
      when (keepalive_counter > 3600) {
         keepalive_counter = 0;
         sendPosition(0);
      } state run
      when() {
         count = 0;
         while (count < 100) {
            if ( 0 == nextPosition(&procInput)) {
               keepalive_counter = 0;
               sendstat = sendPosition(procInput);
/*
               sendLogMsg("GEB", "global position sent");
*/
               if (sendstat) {
                 sprintf(logbuf1, "global sendPosition returned %d\n", sendstat);
                 sendLogMsg("GEB", logbuf1);
               } else {
                  keepalive_counter = 0;
               }
            } else {
              printf(" no global data ");
              epicsThreadSleep(1.0);
              keepalive_counter++;
              count += 100;
            }
            count++;
         }
         Status = 1;
         pvPut(Status);
      } state run
   }
   state cleanup {
     entry {
        printf("entering process:cleanup\n");
     }
      when() {
         queueFlush(1);
         closeTracker();
         efSet(doneFlag);
      } state stopped
   } 
}

