#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef vxWorks
#include <sockLib.h>
#include <hostLib.h>
#else
#include <sys/socket.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <time.h>

#include <seqCom.h>
#include <epicsThread.h>
#include <iocsh.h>
#include <epicsMutex.h>

#include "LogLink.h"

epicsMutexId logConMutex;

int loggerSet = 0;

int validLogger() {

   return loggerSet;
}

static int logSock = 0;                         /* Socket */

/* log_addr is a network name, like node0005-cluster */
int setLogger(char * log_addr, int port) {

   struct sockaddr_in adr_srvr;  /* AF_INET */
#ifndef vxWorks
   struct hostent *hp;
#endif

   if (logConMutex == 0) {
      logConMutex =  epicsMutexCreate();
      if (logConMutex == 0) {
         return 4;
      }
   }


   epicsMutexLock(logConMutex);

   if (logSock != 0) {
      close(logSock);
   }
   memset(&adr_srvr,0,sizeof adr_srvr);
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(port);

#ifndef vxWorks
   hp = gethostbyname(log_addr);
   if (!hp) {
      printf("bad logger address %s \n", log_addr);
      epicsMutexUnlock(logConMutex);
      return 4;
   }

   adr_srvr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *) 
                                (hp->h_addr_list[0]))));
#else
   adr_srvr.sin_addr.s_addr = hostGetByName(log_addr);
#endif
   loggerSet = 0;

   if ( adr_srvr.sin_addr.s_addr == -1 ) {
      printf("bad logger address %s port %d\n", log_addr, LOG_PORT);
      epicsMutexUnlock(logConMutex);
      return 1;
   }

   logSock = socket(AF_INET, SOCK_STREAM, 0);
   if (logSock == -1) {
      printf("Unable to open output socket.\n");
      epicsMutexUnlock(logConMutex);
      return 2;
   }

   if (connect(logSock, (struct sockaddr *)&adr_srvr, sizeof(adr_srvr)) < 0) {
      printf("Connect to Logger failed\n");
      close(logSock);
      logSock = 0;
      epicsMutexUnlock(logConMutex);
      return 3;
   }

   loggerSet = 1;
   epicsMutexUnlock(logConMutex);
   printf("logger connected\n");

   return 0;

}

int checkLogger() {

   int status = 1;
   int data;

   if (logConMutex == 0) {
      return 0;
   }

   epicsMutexLock(logConMutex);
   if (logSock == 0) {
      epicsMutexUnlock(logConMutex);
      return 0;
   }
   epicsMutexUnlock(logConMutex);
      
   status = read(logSock, (char *)&data, sizeof(int));

   if (status == 0) {
      epicsMutexLock(logConMutex);
         close(logSock);
         logSock = 0;
         printf("Logger disconnected\n");
      epicsMutexUnlock(logConMutex);
   }

   return status;
}

int sendToLog(char *msg) {

   static char *logChunk = 0, *offset;

   if (!logConMutex) {
      if (msg) printf("%s\n",msg);
      return 2;
   }

   epicsMutexLock(logConMutex);

   if (!logSock) {
      if (msg) printf("%s\n",msg);
      epicsMutexUnlock(logConMutex);
      return 3;
   }

   if (logChunk == 0) {
      logChunk = calloc(LOG_OUT_SIZE, 1);
      offset = logChunk;
   }
   if (logChunk == 0) {
      if (msg) printf("%s\n",msg);
      epicsMutexUnlock(logConMutex);
      return 1;
   }


   if (msg) {
      strncpy(offset, msg, LOG_LINE_LEN);
      offset[LOG_LINE_LEN - 1] = 0;
      offset+= LOG_LINE_LEN;
   } 

   if (offset < (logChunk + LOG_OUT_SIZE) && msg != 0) {
      epicsMutexUnlock(logConMutex);
      return 0;
   } 

   /* return now if buffer still empty */
   if (offset == logChunk) {
      epicsMutexUnlock(logConMutex);
      return 0;
   }

  if ( write(logSock, logChunk, LOG_OUT_SIZE) < 0) {
     int i;
     char *optr;
     printf("output to Logger failed %s\n", strerror(errno));
     close(logSock);
     logSock = 0;
     printf("Following messages not sent to Logger:\n");
     optr = logChunk;
     /* print out messages that couldn't be sent */
     for (i = 0; i < 50; i++, optr += 80) {
        if (*optr == 0) break;
        printf("%s\n", optr);
     }
     free(logChunk);
     logChunk = 0;
     epicsMutexUnlock(logConMutex);
     return 4;
   }
   free(logChunk);
   logChunk = 0;
   epicsMutexUnlock(logConMutex);
   return 0;
}

int sendLogMsg(char *where, char *what) {

   time_t now;
   char *tc;
   char logbuf[160];
   int len;
   
   now = time(0);
   tc = ctime(&now);
   tc[strlen(tc) - 1] = 0;
   strcpy(logbuf, tc);
   strcat(logbuf, " ");
   len = 79 - strlen(logbuf);
   if (len > 0) {
      strncat(logbuf, where, len);
   }
   len = 79 - strlen(logbuf);
   if (len > 0) {
      strcat(logbuf, ":");
   }
   if (len > 1) {
      strncat(logbuf, what, len - 1);
   }
   
   logbuf[79] = 0;
   sendToLog(logbuf);
   return 0;
}

