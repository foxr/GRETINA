#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <epicsMutex.h>

#include "GEBClient.h"


struct gebClient *GEBClientInit() {
   struct gebClient *retval;
   int testvar = 1;

   retval = calloc(1, sizeof(struct gebClient));
   if (!retval) return 0;

   retval->connectionMutex = epicsMutexCreate();
   if (!retval->connectionMutex) {
      free(retval);
      return 0;
   }

   if (htonl(testvar) == 1) retval->bigendian = 1;

   return retval;
}


/* geb_addr is a network name, like node0005-cluster */
int setGEBClient(struct gebClient *i, char * geb_addr, int port) {

   struct sockaddr_in adr_srvr;  /* AF_INET */
   struct hostent *hp;

   epicsMutexLock(i->connectionMutex);

   if (i->outSock != 0) {
      close(i->outSock);
   }
   memset(&adr_srvr,0,sizeof adr_srvr);
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(port);

   hp = gethostbyname(geb_addr);
   if (!hp) {
      printf("hostname %s not resolved\n", geb_addr);
      epicsMutexUnlock(i->connectionMutex);
      return 4;
   }
   adr_srvr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *) 
                                (hp->h_addr_list[0]))));

   if ( adr_srvr.sin_addr.s_addr == INADDR_NONE ) {
      printf("bad geb address %s port %d\n", geb_addr, port);
      epicsMutexUnlock(i->connectionMutex);
      return 1;
   }

   i->outSock = socket(AF_INET, SOCK_STREAM, 0);
   if (i->outSock == -1) {
      printf("Unable to open output socket.\n");
      epicsMutexUnlock(i->connectionMutex);
      return 2;
   }

   if (connect(i->outSock, 
        (struct sockaddr *)&adr_srvr, sizeof(adr_srvr)) < 0) {
      printf("Connect to GEB failed\n");
      close(i->outSock);
      i->outSock = 0;
      epicsMutexUnlock(i->connectionMutex);
      return 3;
   }

   epicsMutexUnlock(i->connectionMutex);

   return 0;

}

void closeGEBClient(struct gebClient *i) {

   if (i->outSock) {
      epicsMutexLock(i->connectionMutex);
      close(i->outSock);
      i->outSock=0;
      epicsMutexUnlock(i->connectionMutex);
   }
}

int checkGEBClient(struct gebClient *i) {

   int status = 1;
   int data;

   if (i->connectionMutex == 0) {
      return 0;
   }

   epicsMutexLock(i->connectionMutex);
   if (i->outSock == 0) {
      epicsMutexUnlock(i->connectionMutex);
      return 0;
   }
   epicsMutexUnlock(i->connectionMutex);
      
   status = read(i->outSock, &data, sizeof(int));

   if (status == 0) {
      epicsMutexLock(i->connectionMutex);
         close(i->outSock);
         i->outSock = 0;
         printf("GEB disconnected\n");
      epicsMutexUnlock(i->connectionMutex);
   }

   return status;
}

/* this next does a bunch of messing with the timestamp and length of the 
   outmsg.  It assumes long long is 64 bits and int is 32, but not the 
   endieness of the machine. It sends the auxData payload as a string of 
   bytes.
 */
int sendGEBData(struct gebClient *i, struct gebData *outmsg) {

   unsigned int *outdata;
   int outsize, sendsize;
   unsigned long long temp;

   if (0 == outmsg) {
     outmsg = &i->zeromsg;
     outdata = 0;
     printf("Sending keepalive message to geb\n");
     sendsize = GEB_HEADER_BYTES;
   } else {
     if (!outmsg->payload || outmsg->length < 1) {
        free (outmsg);
        return 2;
     }
     sendsize = GEB_HEADER_BYTES + outmsg->length;
     outsize = sendsize/4 + 1;
     outdata = calloc(outsize, 4);

     if (!(i->bigendian)) {
        bcopy(outmsg, &outdata[0], GEB_HEADER_BYTES);
        bcopy(outmsg->payload, &outdata[4], outmsg->length);
     } else {
        swab(outmsg, &outdata[0], GEB_HEADER_BYTES);
        swab(outmsg->payload, &outdata[4], outmsg->length);
     }
   }

   epicsMutexLock(i->connectionMutex);
   if (!i->outSock) {
      epicsMutexUnlock(i->connectionMutex);
      if (outmsg != &i->zeromsg) {
          free(outmsg->payload);
          free(outmsg);
          free(outdata);
      }
      return 3;
  }

  if ( write(i->outSock, outdata, sendsize) < 0) {
     printf("output to GEB failed %s\n", strerror(errno));
     close(i->outSock);
     i->outSock = 0;
     printf("GEB disconnected\n");
     epicsMutexUnlock(i->connectionMutex);
     if (outmsg != &i->zeromsg) {
         free(outmsg->payload);
         free(outmsg);
         free(outdata);
     }
     return 1;
   }
   epicsMutexUnlock(i->connectionMutex);
   if (outmsg != &i->zeromsg) {
      free(outmsg->payload);
      free(outmsg);
      free(outdata);
   }
   return 0;
}


