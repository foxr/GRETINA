
typedef struct sortingListNode {
   void *data;
   struct sortingListNode *prev;
   struct sortingListNode *next;
   long long timestamp;
} sortingListNode;

typedef struct sortingList {
   epicsMutexId mutex;
   sortingListNode *listFront;
   sortingListNode *listEnd;
   int waterline;
   int listlen;
} sortingList;

sortingList *sortingListCreate(); 
void sortingListDestroy(sortingList *id);
int sortingListWaterline(sortingList *id);
int sortingListLength(sortingList *id);
void *sortingListGet(sortingList *id);
long long sortingListFrontTime(sortingList *id);
int sortingListAdd(sortingList *id, void *datum, long long timestamp);
int sortingListMerge(sortingList *dest, sortingList *inp);
