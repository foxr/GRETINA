#include <stdlib.h>
#include <epicsMutex.h>
#include <limits.h>
#include "iheap.h"
#include "binHeap.h"

/*

typedef struct binHeap {
   epicsMutexId mutex;
   struct iheap heap;
   struct iheap *root;
} binHeap;

*/
int binHeapMerge(binHeap *dest, binHeap *inp) {
   epicsMutexLock(dest->mutex);
   epicsMutexLock(inp->mutex);
   iheap_union(dest->root, inp->root);
   epicsMutexUnlock(inp->mutex);
   epicsMutexUnlock(dest->mutex);
   return 0;
}

int binHeapAdd(binHeap *id, void *datum, long long timestamp) {
   struct iheap_node *hn;

   hn = calloc(1, sizeof(struct iheap_node));
   if (!hn) return -1;
   iheap_node_init(hn, timestamp, datum);
   epicsMutexLock(id->mutex);
   iheap_insert(id->root, hn);
   epicsMutexUnlock(id->mutex);
   return 0;
}

long long binHeapFrontTime(binHeap *id) {
   struct iheap_node *hn;

   epicsMutexLock(id->mutex);
   hn = iheap_peek(id->root);
   epicsMutexUnlock(id->mutex);
   if (!hn) return -1;
   return hn->key;
}

void *binHeapGet(binHeap *id) {
   struct iheap_node *hn;
   void *retval;

   if (!id) return 0;
   epicsMutexLock(id->mutex);
   hn = iheap_take(id->root);
   epicsMutexUnlock(id->mutex);
   if (!hn) return 0;
   retval = (void *)hn->value;
   free(hn);
   return retval;
}

binHeap *binHeapCreate()  {

   binHeap *retval;

   retval = calloc(1, sizeof(binHeap));
   if (!retval) return 0;
   retval->mutex = epicsMutexCreate();
   if (!retval->mutex) {
     free(retval);
     return 0;
   }
   retval->root = &(retval->heap);
   iheap_init(retval->root);
   return retval;
}




