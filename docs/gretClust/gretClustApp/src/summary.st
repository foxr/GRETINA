program summary

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \
        t n##Save;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

DECL(short, Summary, Cluster_CV_State)

short ComponentStates[13];

assign ComponentStates[0] to "Decomp001_CV_State";
assign ComponentStates[1] to "Decomp002_CV_State";
assign ComponentStates[2] to "Decomp003_CV_State";
assign ComponentStates[3] to "Decomp004_CV_State";
assign ComponentStates[4] to "Decomp005_CV_State";
assign ComponentStates[5] to "Decomp006_CV_State";
assign ComponentStates[6] to "Decomp007_CV_State";
assign ComponentStates[7] to "Decomp008_CV_State";
assign ComponentStates[8] to "Decomp009_CV_State";
assign ComponentStates[9] to "Decomp010_CV_State";
assign ComponentStates[10] to "Decomp011_CV_State";
assign ComponentStates[11] to "Decomp012_CV_State";
monitor ComponentStates[0];
monitor ComponentStates[1];
monitor ComponentStates[2];
monitor ComponentStates[3];
monitor ComponentStates[4];
monitor ComponentStates[5];
monitor ComponentStates[6];
monitor ComponentStates[7];
monitor ComponentStates[8];
monitor ComponentStates[9];
monitor ComponentStates[10];
monitor ComponentStates[11];

short ComponentEnables[13];

assign ComponentEnables[0] to "Decomp001_CS_Enable";
monitor ComponentEnables[0];
assign ComponentEnables[1] to "Decomp002_CS_Enable";
monitor ComponentEnables[1];
assign ComponentEnables[2] to "Decomp003_CS_Enable";
monitor ComponentEnables[2];
assign ComponentEnables[3] to "Decomp004_CS_Enable";
monitor ComponentEnables[3];
assign ComponentEnables[4] to "Decomp005_CS_Enable";
monitor ComponentEnables[4];
assign ComponentEnables[5] to "Decomp006_CS_Enable";
monitor ComponentEnables[5];
assign ComponentEnables[6] to "Decomp007_CS_Enable";
monitor ComponentEnables[6];
assign ComponentEnables[7] to "Decomp008_CS_Enable";
monitor ComponentEnables[7];
assign ComponentEnables[8] to "Decomp009_CS_Enable";
monitor ComponentEnables[8];
assign ComponentEnables[9] to "Decomp010_CS_Enable";
monitor ComponentEnables[9];
assign ComponentEnables[10] to "Decomp011_CS_Enable";
monitor ComponentEnables[10];
assign ComponentEnables[11] to "Decomp012_CS_Enable";
monitor ComponentEnables[11];

int i;
short testState;

ss summarize {
   state init {
      when() {
         Summary = 0;
         pvPut(Summary);
      } state test
   } 
   state test {
      when () {
         for (i=0; i<13; i++) {
            if (ComponentEnables[i]) {
               testState = ComponentStates[i];
               Summary = testState;
               break;  
            }
         }    
         for (i++; i<13; i++) {
            if (ComponentEnables[i] &&
                ComponentStates[i] != testState) {
                Summary = 7;
                break;
            }
         }
         pvPut(Summary);
      } state wait
   }
   state wait {
      when (delay(0.5)) {
      } state test
   }
}

