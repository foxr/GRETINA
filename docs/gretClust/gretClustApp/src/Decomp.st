program Decomp ("instance=001,cryid=01")

option +d; 	/* debug (verbose) output */

%%#include <string.h>
%%#include "queues.h"
%%#include "DecompIF.h"
%%#include "logSend.h"
%%#include "receiver.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYEVT(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		\
        evflag n##Event;        \
        sync n n##Event;        \

#define SENDSTATE(s) 		\
	strcpy(statepv, #s);	\
	pvPut(statepv);		\
	pvFlush();

/* these are file names.  They are defined after crystal id is available */
DECLARRAYEVT(char, basis, Det{cryid}_CS_Basis, 100)
char basisCopy[100];
DECLARRAYEVT(char, detmap, Det{cryid}_CS_DetMap, 100)
char detmapCopy[100];
DECLARRAYEVT(char, tr_gain, Det{cryid}_CS_TrGain, 100)
char tr_gainCopy[100];
DECLARRAYEVT(char, xtalk_pars, Det{cryid}_CS_XtkPars, 100)
char xtalk_parsCopy[100];
DECLARRAYEVT(char, filter, Det{cryid}_CS_Filter, 100)
char filterCopy[100];

DECLMON(string, sender, Det{cryid}_CS_Sender)

DECLEVENT(double, chisqrmax, Decomp_CS_ChiSqMax)
DECLMON(short, startStop, Online_CS_StartStop)
/* 0 = stop, 1 = run */
DECLMON(string, gebaddr, GEB_CV_Addr)
DECLMON(int, port, GEB_CV_Port)
DECLMON(string, logger, Log_CV_Addr)
DECLMON(int, logport, Log_CV_Port)

DECLEVENT(short, verbose, Decomp_CS_Verbose)
DECLEVENT(short, reload, Decomp{instance}_CS_Reload)
DECL(int, Status, Decomp{instance}_CV_Status)
DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECL(string, statepv, Decomp{instance}_CV_State)
DECL(int, decomprate, Decomp{instance}_CV_Rate)
DECL(int, nonetrate, Decomp{instance}_CV_NoNet)
DECL(int, tmnetrate, Decomp{instance}_CV_TMNet)
DECL(int, sumenerrate, Decomp{instance}_CV_SumEner)
DECL(int, badchisqrate, Decomp{instance}_CV_BadChisq)
DECL(int, builtchans, Decomp{instance}_CV_BuiltChans)
DECL(short, bstat, Decomp{instance}_CV_BStat)
DECL(short, tstat, Decomp{instance}_CV_TStat)
DECL(short, dstat, Decomp{instance}_CV_DStat)
DECL(short, xstat, Decomp{instance}_CV_XStat)
DECL(short, fstat, Decomp{instance}_CV_FStat)

evflag startInput;
evflag stopInput;
evflag startProcess;
evflag stopProcess;
evflag stopOutput;
evflag startOutput;
evflag doneFlag;
evflag startedFlag;
evflag gebInited;
evflag loggerInited;
evflag startupfailureE;

int tcstat;
int gebstat;
char *receiver;
int sendstat;
int keepalive_counter;
int decomp_started;
int prev_decomps;
int prev_nonet;
int prev_tmnet;
int prev_sumener;
int prev_badchisq;
string ourname;
string logbuf1;
int ppstat;
int onefailuremessage;

char *input;
char *ppoutput;
char *sendInput;

ss setupAndRun {
   state init {
      when () {
        SENDSTATE(Initial)
        decompInit(macValueGet("instance"), macValueGet("cryid"));
        strcpy(basisCopy, basis);
        bstat = setBasisName(basis);
        pvPut(bstat);
        strcpy(filterCopy, filter);
        fstat = setFilterName(filter);
        pvPut(fstat);
        strcpy(xtalk_parsCopy, xtalk_pars);
        xstat = setXtalkParsName(xtalk_pars);
        pvPut(xstat);
        strcpy(tr_gainCopy, tr_gain);
        tstat = setTrGainName(tr_gain);
        pvPut(tstat);
        strcpy(detmapCopy, detmap);
        dstat = setDetMapName(detmap);
        pvPut(dstat);
        sprintf(ourname, "Decomp%s", macValueGet("instance"));
     } state restart
  }
  state offline {
    entry {
       SENDSTATE(Offline)
    }
    when(enable) {
    } state restart
  }
  state restart {
    when(!enable) {
    } state offline
    when() {
    } state setup
  }
  state run {
    entry {
       SENDSTATE(Run)
    }
    when (efTestAndClear(verboseEvent)) {
 	setVerbose(verbose);      
    } state run
    when (!enable || startStop == 0) {
       efSet(stopInput);
    } state waitfordone
  }
  state setup {
    entry {
       SENDSTATE(Setup)
    }
    when (!enable) {
    } state offline
    when (startStop == 1) {
       efSet(loggerInited);
    } state waitForLog
    when (efTestAndClear(verboseEvent)) {
 	setVerbose(verbose);      
    } state setup
    when (reload) {
       bstat = setBasisName(basis);      
       pvPut(bstat);
       printf("basis reloaded\n");
       fstat = setFilterName(filter);      
       pvPut(fstat);
       printf("filter reset\n");
       xstat = setXtalkParsName(xtalk_pars);      
       pvPut(xstat);
       printf("xtalk_pars reset\n");
       dstat = setDetMapName(detmap);      
       pvPut(dstat);
       printf("detmap reset\n");
       tstat = setTrGainName(tr_gain);      
       pvPut(tstat);
       printf("tr_gain reset\n");
       reload = 0;
       pvPut(reload);
    } state setup
    when (efTestAndClear(basisEvent)) {
        if (strcmp(basis, basisCopy)) {
 	   bstat = setBasisName(basis);      
           pvPut(bstat);
           printf("basis loaded\n");
           strcpy(basisCopy, basis);
        }
    } state setup
    when (efTestAndClear(filterEvent)) {
        if (strcmp(filter, filterCopy)) {
           fstat = setFilterName(filter);      
           pvPut(fstat);
           printf("filter set\n");
           strcpy(filterCopy, filter);
        }
    } state setup
    when (efTestAndClear(xtalk_parsEvent)) {
       if (strcmp(xtalk_pars, xtalk_parsCopy)) {
 	   xstat = setXtalkParsName(xtalk_pars);      
           pvPut(xstat);
           printf("xtalk_pars set\n");
           strcpy(xtalk_parsCopy, xtalk_pars);
       }
    } state setup
    when (efTestAndClear(detmapEvent)) {
       if (strcmp(detmap, detmapCopy)) {
 	  dstat = setDetMapName(detmap);      
          pvPut(dstat);
          printf("detmap set\n");
          strcpy(detmapCopy, detmap);
       }
    } state setup
    when (efTestAndClear(tr_gainEvent)) {
       if (strcmp(tr_gain, tr_gainCopy)) {
 	  tstat = setTrGainName(tr_gain);      
          pvPut(tstat);
          printf("tr_gain set\n");
          strcpy(tr_gainCopy, tr_gain);
       }
    } state setup
  }
  state waitForLog {
     when (delay(1)) {
       sendLogMsg(ourname, "Start Command");
     } state checkValidState
  }
  state checkValidState {
     when (bstat == 0 && fstat == 0 && dstat == 0 && xstat == 0 && tstat == 0) {
       sendToLog(0);
       setVerbose(verbose);      
       efSet(startInput);
       efSet(gebInited);
     } state waitforstarted
     when () {
       sendLogMsg(ourname, "Input files invalid; not going to Run");
       sendToLog(0);
     }  state setup
  }
  state waitforstarted {
    entry {
       SENDSTATE(Wait) 
    }
    when(efTestAndClear(startedFlag)) {
    } state run
  }
  state waitfordone {
    entry {
       SENDSTATE(Wait)
    }
    when(efTestAndClear(doneFlag)) {
        decompShutdown();
    } state setup
  }
}

ss sendCounters {
   state send {
      when (delay(1.0)) {
         decomprate = getDecomps() - prev_decomps;
         prev_decomps += decomprate;
         nonetrate = getDecompErrs(0) - prev_nonet;
         prev_nonet += nonetrate;
         tmnetrate = getDecompErrs(1) - prev_tmnet;
         prev_tmnet += tmnetrate;
         sumenerrate = getDecompErrs(2) - prev_sumener;
         prev_sumener += sumenerrate;
         badchisqrate = getDecompErrs(3) - prev_badchisq;
         prev_badchisq += badchisqrate;
         builtchans = getBuiltChans();
         pvPut(decomprate);
         pvPut(nonetrate);
         pvPut(sumenerrate);
         pvPut(tmnetrate);
         pvPut(badchisqrate);
         pvPut(builtchans);
      }  state send
   }
}

ss checkLogger {
   state preinit {
      when (efTestAndClear(loggerInited)) {
      } state init
   }
   state init {
      option -e;
      entry {
         tcstat = checkLogger();
      }
      when (tcstat == 0) {
         printf("Logger not connected\n");
         setLogger(logger, logport);
         sendLogMsg(ourname, "log opened");
         sendToLog(0);
      } state wait
      when (delay(1.0)) {
      } state init
   }
   state wait {
     when (delay(1.0)) {
     } state init
   }
}

ss setStatus {
   state run {
       when(delay(0.5)) {
         Status = 1;
         pvPut(Status);
       } state run
   }
}

ss input {
   state init {
      when() {
      }   state stopped
   }
   state run {
      when (efTestAndClear(stopInput)) {
         stopReceiver(receiver);
         efSet(stopProcess);
      } state cleanup
      when(0 == getReceiverData(receiver, 1.0, &input)) {
/*
         sendLogMsg(ourname, "getReceiverData succeeded");
         sendToLog(0);
*/
         onefailuremessage = 0;
         if (0 == preProcess(input, 0)) {
            printf("input sent to preProcess\n");
            while (0 == preProcess(0, &ppoutput)) {
/*
               printf("crystal event built\n");
*/
               while(queueSend(0, ppoutput)) {
                  if (efTest(stopInput)) break;
               }
               if (efTest(stopInput)) break;
            }
         } else {    
/*
            sendLogMsg(ourname, "runPreProcess failed");
            sendToLog(0);
*/
         }
      } state run
      when (delay(0.5)) {
         if (!onefailuremessage) {
            sendLogMsg(ourname, "getReceiverData failed");
            sendToLog(0);
            onefailuremessage = 1;
         }
      } state run
   }
   state cleanup {
      when(delay(10)) {
         queueFlush(0);
      } state stopped
   }
   state stopped {
      when (efTestAndClear(startInput)) {
         receiver = initReceiver(sender);
         ppstat = startPreProcess();
         efClear(startupfailureE);
      } state testStartup
   }
   state testStartup {
      when(receiver && 0 == ppstat) {
         efClear(stopInput);
         efSet(startProcess);
      } state run
      when () {
         sendLogMsg(ourname, "Preprocess init failed");
         sendToLog(0);
         efSet(startupfailureE);
      } state stopped
   }
}

ss process {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when (efTestAndClear(startProcess)) {
          decomp_started = startDecomp(ourname);
      } state check_run
   }
   state check_run {
      when (decomp_started == 0) {
          efClear(stopProcess);
          efSet(startOutput);
      } state run
      when () {
         sendLogMsg(ourname, "Decomp init failed");
         sendToLog(0);
      } state stopped
   }
   state run {
      when (efTestAndClear(stopProcess)) {
         stopDecomp();
         efSet(stopOutput);
      } state cleanup
   }
   state cleanup {
      when(delay(10)) {
         queueFlush(1);
      } state stopped
   } 
}

ss output {
   state init {
      when() {
      } state stopped
   }
   state stopped {
      when(efTestAndClear(startOutput)) {
         efClear(stopOutput);
         gebstat = setGEB(gebaddr, port);
         /* open output connection ? */
      } state checkgeb
   }
   state checkgeb {
      when (gebstat == 0) {
         sendLogMsg(ourname, "GEB connect succeeded");
         sendToLog(0);
         efSet(startedFlag);
      } state run
      when (delay(1)) {
         sendLogMsg(ourname, "GEB connect failed");
         sendToLog(0);
         gebstat = setGEB(gebaddr, port);
      } state checkgeb
   }
   state run {
      when (efTestAndClear(stopOutput)) {
         closeGEB();
         sendLogMsg(ourname, "GEB connection closed");
         sendToLog(0);
         efSet(doneFlag);
      } state stopped
      when (0 == queueReceive(1, &sendInput)) {
         sendstat = sendPosition(sendInput);
/*
         sendLogMsg(ourname, "position sent");
*/
         if (sendstat) {
           sprintf(logbuf1, "sendPosition returned %d\n", sendstat);
           sendLogMsg(ourname, logbuf1);
         } else {
           keepalive_counter = 0;
         }
      } state run
      when (keepalive_counter > 3600) {
         keepalive_counter = 0;
         sendPosition(0);
      } state run
      when () {
         keepalive_counter++;
      } state run
   }
}
