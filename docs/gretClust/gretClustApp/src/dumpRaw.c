#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

int main(int argc, char *argv[]) {

    unsigned int inbuf;
    FILE *inf;
    int state = 0;
    unsigned int timebuf[2] = {0,0};
    long long time = 0;
    unsigned int ebuf = 0, flags;
    int energy;
    
    if (argc != 2) {
       printf("Usage: %s <input file>", argv[0]);
       exit(1);
    } 

    inf = fopen(argv[1], "r"); 

    if (!inf) {
       printf("Cannot open %s for reading\n", argv[1]);
       exit(2);
    }

    while (fread(&inbuf, sizeof(int), 1, inf)) {
        switch(state) {
            case 0:	/* find beginning of packet */
               if (inbuf == 0xaaaaaaaa) {
                  state = 1;
               }
               break;
            case 1:	/* dump vme slot and channel */
               inbuf = ntohl(inbuf);
               printf("%d %d ", inbuf >> 27, inbuf & 0xf);
               state = 2;
               break;
            case 2:	/* least significant word of LED timestamp */
               timebuf[1] = ntohl(inbuf);
               state = 3;
               break;
            case 3:	/* dump LED timestamp */
               timebuf[0] = ntohl(inbuf) & 0xffff;
               ebuf = ntohl(inbuf) & 0xffff0000;
               time = timebuf[0];
               time <<= 32;
               time += timebuf[1];
               ebuf >>= 16;
               printf("%lld\n",time); 
/*
               printf("%u%u\n",*((unsigned int *)(&timebuf[0])), 
                               *((unsigned int *)(&timebuf[1])));
*/
               state = 4;
               break;
            case 4:
               energy = ntohl(inbuf) & 0x1ff;
               energy <<= 16;
               energy += ebuf;
               energy = energy << 7;
               energy >>= 7;
               flags = ntohl(inbuf) & 0xf800;
               printf("energy: %d flags: 0x%x\n", energy, flags);
               state = 0;
               break;
            default:
               printf("state %d coerced to 0.\n", state);
               state = 0;
        }
               
    }
    return 0;
    
}
