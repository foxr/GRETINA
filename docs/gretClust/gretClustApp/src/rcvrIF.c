#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include <netinet/in.h>

#include <seqCom.h>
#include "receiver.h"
#include "rcvrIF.h"
#include "queues.h"

/* globals to be used by all instances */
int recLenGDig;

int events[MAX_SERVERS];
static char *outputfile;

int rcvrIFInit() { 
   seqRegisterSequencerCommands();
   queueInit(1000,1000);
   return 0; 
}

void startSort() {
   return;
}

void clearList(int which) {
   struct inbuf *inlist = 0, *tmp;

   while (0 == queueReceive(which, (char **)&inlist)) {
      while (inlist) {
         if (inlist->pdata) free(inlist->pdata);
         tmp = inlist;
         inlist = inlist->next;
         free(tmp);
      }
   }
   return;
}   

void rcvrIFShutdown() {
   printf("shutting down receivers\n");
}

void setRecLen(int rl) {
   recLenGDig = rl / 2;
}  

int getEvents(int which) { 

   return events[which]; 
}

int outfile;

int writeEvents(char *inptr) {
   
   struct inbuf *inlist = 0;
   char *insigptr;
   void *ftmp;
   int wstat = 0, towrite;
   int retval = 0;
   int goodctr= 0, badctr = 0;

   if (!inptr) return -1;

   inlist = (struct inbuf *)inptr;
   
   while (inlist) {
      events[0]++;
      if (outfile) {
         towrite = inlist->evtlen * inlist->recs;
         insigptr = (char *)inlist->pdata;
         wstat = write(outfile,  insigptr, towrite);
         while (wstat != -1 && wstat != towrite) {
            printf("Write to disk returned %d not %d\n", wstat, towrite);
            towrite = towrite - wstat;
            insigptr += wstat;
            usleep(100000);	/* 0.1 second */
            wstat = write(outfile,  insigptr, towrite);
         }
         if (wstat == -1) {
            badctr++;
            retval = -2;
            printf("write to disk returned -1\n");
         } else {
            goodctr++;
         }
      } else {
         printf("attempt to write to NULL file\n");
      }
      free(inlist->pdata);
      ftmp = inlist;
      inlist = inlist->next;
      free(ftmp);
   }
   if (badctr) printf("%d write failures out of %d packets\n", badctr, 
                      badctr + goodctr);
   return retval;
}

/*
 * Generate output file name based on data directory, run directory, and
 * crystal id. 
 */
int setOutputFile3(char *dirname, char *runname, char *cryid) {
   int pathlen;
   char *namebuf;

   if (!dirname || *dirname == 0) return -1;
   if (!runname || *runname == 0) return -1;
   if (!cryid || *cryid == 0) return -1;

   pathlen = strlen(dirname) + strlen(runname) + 20;
   namebuf = (char *)calloc(pathlen, 1);
   snprintf(namebuf, pathlen, "%s/%s/Raw%s.dat", dirname, runname, cryid);
   printf("new filename %s\n", namebuf);
   fflush(stdout);
   if (outputfile) free(outputfile);
   outputfile = namebuf;
   events[0] = 0;
   return 0;
}

/*
 * version that finds largest file number 
 */
char *setOutputFile(char *oname) {
   char *baseindex;
   int pathlen, fnum = 0;
   char *namebuf, *numptr, *retptr=0;
   static char retval = 0;

   struct dirent **namelist;
   int n;

   if (!oname || *oname == 0) return &retval;

   n = scandir(oname, &namelist, 0, alphasort);
   if (n < 0) return &retval;
   while (n--) {
      numptr = rindex(namelist[n]->d_name, '.');
      if (!numptr) {
         free(namelist[n]);
         continue;
      } 
      numptr -= 3;
      fnum = strtol(numptr, &retptr, 10);
      if (retptr != (numptr + 3)) {
         free(namelist[n]);
         continue;
      }
      break;	/* fnum should be good at this point */
   }
   if (n > -1) {
      while (n--) {
         free(namelist[n]);
      }
   }
   free(namelist);
   if (fnum < 0 || fnum > 998) return &retval;
   fnum++;

   baseindex = rindex(oname, '/');

   if (!baseindex) return &retval;

   /* delete trailing slash and repeat */
   if (*(baseindex + 1) == 0) {
      *baseindex = 0;
      baseindex = rindex(oname, '/');
      if (!baseindex) return &retval;
   }
   baseindex++;
   pathlen = strlen(oname) + strlen(baseindex) + 10;
   namebuf = (char *)calloc(pathlen, 1);
   snprintf(namebuf, pathlen, "%s/%s%03d.dat", oname, baseindex, fnum);
   printf("new filename %s\n", namebuf);
   fflush(stdout);
   if (outputfile) free(outputfile);
   outputfile = namebuf;
   baseindex = rindex(namebuf, '/');
   baseindex++;
   events[0] = 0;
   return baseindex;
}

int validOutputFile() {
   /* so far, just don't let it exist */
   struct stat retstat;

   return (stat(outputfile, &retstat));
}

int openOutputFile()  {

    if (!validOutputFile()) return 1;

    outfile = open(outputfile, 
                   O_WRONLY | O_CREAT | O_EXCL | O_APPEND, 
                   S_IRWXU | S_IRWXG | S_IROTH);
    if (outfile == -1) outfile = 0;
    return (outfile?1:0);
}

int closeOutputFile() {
   if (outfile) {
      close(outfile);
      outfile = 0;
   } else {
      printf("attempt to close NULL file\n");
   }
   return 0;
}


