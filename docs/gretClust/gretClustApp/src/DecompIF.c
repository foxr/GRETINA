#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsThread.h>
#include <iocsh.h>
#include <epicsMutex.h>

#define ALLOC_STORAGE
#include "preproc.h"
#include "gdecomp.h"
#include "receiver.h"
#include "queues.h"
#include "logSend.h"

#include "GEBClient.h"

struct gebClient *geb;

#define DECOMP_PATHLEN 100
#define MAX_THRDS 8 

static char basisfile[DECOMP_PATHLEN];
static char xtalkparsfile[DECOMP_PATHLEN];


/* filenames stored in decompLib to make library build simpler */
extern char filterfile[];
extern char detmapfile[];
extern char trgainfile[];

static int decompNum;
static int crystalId;

char threadnames[MAX_THRDS][20];
int threadDone[MAX_THRDS];

void setVerbose(int verbose) {
   quiet = verbose?0:1;
   return; 
}

int decompInit(char *instance, char *cryid) {
   char *endptr;
   int i;

   geb = GEBClientInit();
   if (!geb) return 1;

   if (queueInit(200,200)) return 2;

   decompNum = strtol(instance, &endptr, 10);

   if (endptr == instance) return 3;

   crystalId = strtol(cryid, &endptr, 10);
   if (endptr == cryid) return 4;

   for (i = 0; i < MAX_THRDS; i++) threadDone[i] = 1;

   return 0;
}


void setCrystalId(int c) {
   crystalId = c;
}

int setBasisName(char *bname) {
   if (bname && strlen(bname) < DECOMP_PATHLEN) {
       if (strcmp(basisfile, bname)) {
           if (0 == dl_decomp_init_global(bname, quiet)) {
              strcpy(basisfile, bname);
              return 0;
           } else {
              strcpy(basisfile, "Invalid");
              return 1;
           }
       } else {
         return 0;
       }
   } 
   return 1;
}

int setDetMapName(char *fname) {
   if (fname && strlen(fname) < DECOMP_PATHLEN) {
       strcpy(detmapfile, fname);
       return 0;
   }
   return 1;
}

int setFilterName(char *fname) {
   if (fname && strlen(fname) < DECOMP_PATHLEN) {
       strcpy(filterfile, fname);
      return 0;
   }
   return 1;
}

int setTrGainName(char *fname) {
   if (fname && strlen(fname) < DECOMP_PATHLEN) {
       strcpy(trgainfile, fname);
      return 0;
   }
   return 1;
}

int setXtalkParsName(char *fname) {
   if (fname && strlen(fname) < DECOMP_PATHLEN) {
       strcpy(xtalkparsfile, fname);
      return 0;
   }
   return 1;
}

/* geb_addr is a network name, like node0005-cluster */
int setGEB(char * geb_addr, int port) {
   return setGEBClient(geb, geb_addr, port);
}

void closeGEB() {
    closeGEBClient(geb);
}

int checkGEB() {
   return checkGEBClient(geb);
}

int sendPosition(char *pos) {

   struct crys_intpts *outdata;
   struct gebData *outmsg = 0;

   if (pos) {
      outdata = (struct crys_intpts *)pos;
      outdata->crystal_id = crystalId;
      outmsg = calloc(1, sizeof(struct gebData));
      if (!outmsg) return 4;
      outmsg->type = GEB_TYPE_DECOMP;
      outmsg->length = sizeof(struct crys_intpts);
      outmsg->payload = outdata;
      outmsg->timestamp = outdata->timestamp;
   }
   return sendGEBData(geb, outmsg);
}

static int tdecomps[MAX_THRDS];

int getDecomps() { 
   int i, decomps = 0;
  
   for (i=0; i<MAX_THRDS; i++) decomps += tdecomps[i];

   return decomps; 
}


void decompShutdown() {
   printf("%d signal decompositions performed\n", getDecomps());
   printf("shutting down Decomp\n");
}

/* 
 * Preprocess takes a list of raw single-channel events from the receiver
 * and returns a list of 37-channel Event_Signals.  Items of both types
 * are passed as strings.
 */ 


static pair *map;
static float a0[37], a1[37], delay0[37], delay1[37];
static int gap = 5;  
static struct filter *fltr;
static float tr_gain[37];
static int evt_len, tr_len;
static int num_seg_evts, num_xtal_evts;

static int *cur_tr;
static unsigned short *ebuf;

int preProcess(char *, char **); 

int startPreProcess() {

   int num;
   float avg = 0.0;
   FILE *f_tr_gain;
   int i;

   map = read_mapfile(detmapfile, a0, a1);

   if(map == 0)  return -1;

  fltr = read_filterfile(filterfile);
  if (0 == fltr) return -1;
  f_tr_gain = fopen(trgainfile, "r");
  if (f_tr_gain == 0) return -1;
  for (i = 0; i < 37; i++) {
    num = fscanf(f_tr_gain, "%f", tr_gain + i);
    if (num != 1) return -1;
  }
  num = read_param(xtalkparsfile, "delay0", delay0, TOT_SEGS);
  if (num <  0) {
    fprintf(stderr, "error: cannot read delay0 from %s\n", xtalkparsfile);
    return -1;
  }
  /* subtract mean value of delay0 from individual values */
  for (i = 0; i < 36; i++) {
    avg += delay0[i];
  }
  avg /= 36.0;
  for (i = 0; i < 36; i++) {
    delay0[i] -= avg;
  }

  num = read_param(xtalkparsfile, "delay1", delay1, TOT_SEGS);
  if (num <  0) {
    fprintf(stderr, "error: cannot read delay1 from %s\n", xtalkparsfile);
    return -1;
  }
  delay0[36] = delay1[36] = 0.0;

  preProcess(0,0);	/* reset internals */
  queueFlush(0);

  return 0;
}
/*
   ebuf = calloc(37 * evt_len, sizeof(short));
   evt_len to be passed as length in shorts
   data already byte-swapped
*/
void dumpBuffer(unsigned short *ebuf, int evt_len) {
   int len;	
   int curr = 0, state = 0;

   len = 37 * evt_len;
 
   while (curr  < len) {
        switch(state) {
            case 0:	/* find beginning of packet */
               if (ebuf[curr] == 0xaaaa && ebuf[curr + 1] == 0xaaaa) {
                  state = 1;
               }
               curr += 2;
               break;
            case 1:	/* dump vme slot channel and time */
               printf("%d %d ", get.vec(ebuf + curr), get.ch(ebuf + curr));
               printf("%lld\n", get.time(ebuf + curr)); 
               state = 0;
               break;
            default:
               printf("state %d coerced to 0.\n", state);
               state = 0;
        }
    }

}

int preProcess(char *inptr, char **outptr) {
  
   struct inbuf *inlist=0, *gone;
   long long t = 0;
   Event_Signal *event = 0;
   int  x, v, e0, e1, t0, i, j, k;
   int ener_cc;
   int num_net, segs[36], net2[36][36];
   int vec, ch, id, slot, *a;
   float cal_ener_cc, fact, cc_avg;
   char *bbptr,*lbptr;
   int ie;
   int recordlen;
   unsigned long long mask;

   static int *ener_sp;
   static int inlentotal;
   static int num_aligned = 0, num_net1=0, net1_cnt=0;
   static int curr;
   static unsigned long long int full, tlast;
   static unsigned short *sbuf = 0;
   static int oldrecordlen = 0;


   if (!outptr && !inptr) {
      /* restart */
      num_xtal_evts = 0;
      num_seg_evts = 0;
      full = 0;
      tlast = 0;
      if (sbuf) {
        free(sbuf);
        sbuf = 0;
      }
      return 0;
   }

   if (inptr) {
      curr = 0;
      full = 0;
      tlast = 0;
      inlist = (struct inbuf *)inptr;
      recordlen = inlist->evtlen;	/* length in bytes */
      
       /*
         this code removed from startPreProcess and put here so that the event
         length from the file may be used.  I'm not sure the sizes make 
         sense.
         evt_len = 2 * (recLenGDig + 2);	length in shorts
         tr_len = evt_len - HDR_LEN;
       
         cur_tr =  calloc(TOT_SEGS * tr_len, sizeof(int));
         ebuf = calloc(TOT_SEGS * evt_len, sizeof(short));
       */
      if (!oldrecordlen || oldrecordlen != recordlen) {
         oldrecordlen = recordlen;
         evt_len = recordlen/2;
         tr_len = evt_len - HDR_LEN;
         if (cur_tr) free(cur_tr); 
         cur_tr =  calloc(37 * tr_len, sizeof(int));
         if (ebuf) free(ebuf);
         ebuf = calloc(37 * evt_len, sizeof(short));
      }
      if (!ener_sp) {
         ener_sp = calloc(37 * 4096, sizeof(int));
      }
      inlentotal = 0;
      while (inlist) {
         inlentotal += evt_len * inlist->recs;
         inlist = inlist->next;
      }
      if (inlentotal <= 0) {
         return -3;
      }
      if (sbuf) {
         printf("preProcess calling phase error 1\n");
         return -4;
      }
      sbuf = (unsigned short *)calloc(inlentotal, 2);
      inlist = (struct inbuf *)inptr;
      bbptr = (char *)sbuf;
      while (inlist) {
         lbptr = (char *)inlist->pdata;
         swab(lbptr, bbptr, recordlen * inlist->recs);
         bbptr += recordlen * inlist->recs;
         free (inlist->pdata);
         gone = inlist;
         inlist = inlist->next;
         free(gone);
      }
      return 0;
   }
   /* comments refer to the behavior tree diagram in preprocessing.isf */
   /* pp2 */
   if (0 == sbuf) {
      printf("preProcess calling phase error 2 \n");
      return -1;
   }
   while (curr + evt_len <= inlentotal) {
      /* pp23 */
      if (sbuf[curr] != 0xaaaa || sbuf[curr + 1] != 0xaaaa) {
         curr++;
/*
         printf("!");
*/
         continue;	/* up to pp2 */
      }
      /* pp10 */
      vec = get.vec(sbuf + curr + 2);
      ch = get.ch(sbuf + curr + 2);
      id = (vec << 4) + ch;
/*
      if (curr == 0) {
         char logbuf[80];
         t=get.time(sbuf + curr + 2);
         sprintf(logbuf, " %24lld first event in buf", t);
         sendLogMsg("Prep", logbuf);
      }
*/
      /* pp11 */
      if (id > 4095) {
         /* pp12 */
         curr += evt_len;
/*
         printf("illegal id %d\n", id);
*/
         continue; /* up to pp2 */
      }
      /* pp14 */
      slot = map[id].a;
      if (slot == -1) {
         /* pp12 equivalent */
         curr += evt_len;
/*
         printf("illegal slot with id %d\n", id);
*/
         continue; /* up to pp2 */
      }
      num_seg_evts++;
      t=get.time(sbuf + curr + 2);
      tlast =  tlast?tlast:t;
      /* pp22 */
      if (abs(t - tlast) <= gap) {
         /* pp21 */
         memcpy(ebuf + slot * evt_len, sbuf+ curr, evt_len * 2);
         mask = 1;
         mask <<= slot;
         full |= mask;
         tlast = t;
         curr += evt_len;
         if (curr < inlentotal)
            continue;	/* up to pp2 */
      }
      /* pp27 */
      if (full != FULL) {
/*
         char logbuf[80];
         sprintf(logbuf, " %24lld full is 0x%llx", t, full);
         sendLogMsg("Prep", logbuf);
*/
         /* pp28 */
         full = 0;
         /* pp21 equivalent */
         memcpy(ebuf + slot * evt_len, sbuf+ curr, evt_len * 2);
         mask = 1;
         mask <<= slot;
         full |= mask;
         tlast = t;
         curr += evt_len;
         continue;	/* up to pp2 */
      } 
      /* pp19 */
      if (num_xtal_evts == 0) {
         char logbuf[80];
         sprintf(logbuf, "first built time: %llu", tlast);
         sendLogMsg("Prep", logbuf);
         sendToLog(0);
      }
      num_xtal_evts++;
      full = 0;
      /* pp31 seems redundant so omitted */
      /* pp32 */
      ener_cc = segevt_ener(ebuf + evt_len * 36, 7);
      cal_ener_cc = a0[36] + a1[36] * 
                    ((float) segevt_ener(ebuf + evt_len * 36, 7));
      /* pp34 ignored in code */
      /* pp36 */
      for (i = 0, num_net = 0; i < TOT_SEGS; i++) {
         /* pp43   */
         a = cur_tr + i *tr_len;
         get.tr(a, tr_len, 1, ebuf + evt_len *i +2);
         adjoff(a, tr_len);
         /* pp45 */
        if (!net(a, tr_len)) {
            continue;	/* up to pp36 */
        }
        /* pp46 */
        segs[num_net] = i;
        num_net++;
      }
      /* pp42 */ 
      a = cur_tr + i * tr_len;
      get.tr(a, tr_len, 0, ebuf + evt_len *i +2);
      adjoff(a, tr_len);
      /* pp47 */
      x = ener_cc;
      if (x >= fltr->xmin && x <= fltr->xmax && num_net == 2) {
         e0 = segevt_ener(ebuf + evt_len * segs[0], 7);
         e1 = segevt_ener(ebuf + evt_len * segs[1], 7);
         if (e0 >= e1) {
            net2[segs[0]][segs[1]]++;
         } else {
            net2[segs[1]][segs[0]]++;
         }
      }
      /* pp58 */
      if ( ((cal_ener_cc < fltr->emin || 
             cal_ener_cc > fltr->emax)) || 
           ((x < fltr->xmin || x > fltr->xmax)) || 
            (num_net != 1  && num_net != 2) ||
           segs[0] < fltr->segmin || segs[0] > fltr->segmax) {
         continue; /* up to pp2 */
      } 
      /* pp59 */
      num_net1++;
      t0 = align_cfd_1(cur_tr, tr_len, delay1, delay0);
      v = t_cfd(cur_tr + 36 * tr_len, tr_len);
      /* pp61 */
      if (t0 < 0) {
         net1_cnt++;
         continue; /* up to pp2 */
      }
      num_aligned++;
      fact = ((float)x)/3.3639;
      event = calloc(1, sizeof(Event_Signal));
      if (!event) return -1;
      /* pp89 */
      for (i = 0;  i < TOT_SEGS; i++) {
         /* pp71 */
         for (v=45,cc_avg=0; v < 50; v++) {
            /* pp70 */
            cc_avg += (float) (*(cur_tr + 36 * tr_len + v));
         }
         /* pp72 */
         event->seg_energy[i] = a0[i] + a1[i] * 
              ((float) segevt_ener(ebuf + evt_len * i, 7));
         ie  = (int) (event->seg_energy[i] - 0.5 + drand48());
         /* pp73 */
         if (ie >= 0 && ie < 4096) {
           ener_sp[i * 4096 + ie]++;
         }
         /* pp76 */
         for (j = 0; j < TIME_STEPS; j++) {
            event->signal[i][j] = (1.0 / 
            (tr_gain[i] * fact)) * ((float) (*(cur_tr + i * tr_len + j)));
         }
         /* pp88 */
     }
     /* pp82 */
     event->total_energy = a0[36] + 
                          a1[36] * ((float) segevt_ener(ebuf + evt_len * 36, 7));
     ie  = (int) (event->total_energy - 0.5 + drand48());
     if (ie >= 0 && ie < 4096) {
       ener_sp[36 * 4096 + ie]++;
     }
     /* pp84 */
     for (k = 0; k < TIME_STEPS; k++) {
        event->signal[36][k] = - (1.0 / fact) * 
                              ((float) (*(cur_tr + 36 * tr_len + k)));
     }
     /* pp87 */
     event->time = tlast;
     *outptr = (char *)event;
     return 0;
   }
/*
   {
         char logbuf[80];
         sprintf(logbuf, " %24lld last event in buf", t);
         sendLogMsg("Prep", logbuf);
         sprintf(logbuf, "full 0x%llx of 0x%llx ", full, FULL);
         sendLogMsg("Prep", logbuf);
   }
*/
   free(sbuf);
   sbuf = 0;
   return -1;
}

static int builtchans;

/* note that nothing sets builtchans */
int getBuiltChans() { return builtchans; }

int stop_flag = 0;

static struct decomp_errcnt *dcerrs[MAX_THRDS];

int getDecompErrs(int which) { 
   int i, errs = 0;
  
   switch (which) {
      case 0:
         for (i=0; i<MAX_THRDS; i++) {
            if (dcerrs[i]) errs += dcerrs[i]->nonet;
         }
         break;
      case 1:
         for (i=0; i<MAX_THRDS; i++){
            if (dcerrs[i]) errs += dcerrs[i]->toomanynet;
         }
         break;
      case 2:
         for (i=0; i<MAX_THRDS; i++) {
            if (dcerrs[i]) errs += dcerrs[i]->sumener;
         }
         break;
      case 3:
         for (i=0; i<MAX_THRDS; i++) {
            if (dcerrs[i]) errs += dcerrs[i]->badchisq;
         }
         break;
   }

   return errs; 
}

struct crys_intpts *dl_checksum(struct decomp_state *di, Event_Signal *asig) {

  struct crys_intpts *ci;
  unsigned char *cptr;
  int i, csum=0;

  ci = (struct crys_intpts*) calloc(1, sizeof(struct crys_intpts));

  cptr = (unsigned char *)asig;
  for (i = 0; i < sizeof(Event_Signal); i++) {
     csum += *cptr;
     cptr++;
  }
  ci->num = csum;
  ci->timestamp = asig->time;
  return ci;
}

  


/* This is taken from the benchmark and is meant to run as a thread. There
 * were some thread control functions that have been removed and new ones not
 * added (eg, stop_flag).
 */
void eachThread(void  *tnum) {

   Event_Signal *evt;
   char *evtchar;
   struct crys_intpts *x;
   struct decomp_state *di;
   int outstat, instat;

   di = dl_decomp_init_thread();
   if (!di) {
      printf("Thread %ld init failed\n", (long)tnum);
      threadDone[(long)tnum] = 1;
      return;
   }

   dcerrs[(long)tnum] = di->err;

   while (1)  {
      /* get an Event_Signal from input queue.  */

      instat = 1;
      while (instat) {
         instat = queueReceive(0, &evtchar);
         if (instat && stop_flag) {
            char logbuf[80];
            sprintf(logbuf, "nonet = %d", di->err->nonet);
            sendLogMsg(threadnames[(long int)tnum], logbuf);
            sprintf(logbuf, "bad chisq = %d", di->err->badchisq);
            sendLogMsg(threadnames[(long int)tnum], logbuf);
            sprintf(logbuf, "# decomp = %d", di->cnt);
            sendLogMsg(threadnames[(long int)tnum], logbuf);
            sendToLog(0);
            threadDone[(long)tnum] = 1;
            return;
         }
      }
      evt = (Event_Signal *)evtchar;

      /* do the decomposition */
      if (tdecomps[(long)tnum] == 0) {
         char logbuf[80];
         sprintf(logbuf, "first time into decomp: %llu", evt->time);
         sendLogMsg("Decomp", logbuf);
         sendToLog(0);
      }
      x = dl_decomp(di, evt);
/*
      x = dl_checksum(di, evt);
*/

      /* write the result to the output queue */
      if (x != 0) {
         if (tdecomps[(long)tnum] == 0) {
            char logbuf[80];

            sprintf(logbuf, "first decomposed time: %llu", x->timestamp);
            sendLogMsg("Decomp", logbuf);
            sendToLog(0);
         }
         tdecomps[(long)tnum]++;
         outstat = 1;
         while (outstat) {
            outstat = queueSend(1, (char *)x);
         }   
      }
      free(evt);
   }
}


int startDecomp(char *ourname) {

   long i;

   stop_flag = 0;

   for (i = 0; i < MAX_THRDS; i++) {
      sprintf(threadnames[i],"%s %ld", ourname, i);
      if (threadDone[i]) {
         threadDone[i] = 0;
         epicsThreadCreate(threadnames[i], epicsThreadPriorityMedium, 
                           1024 * 1024, eachThread, (void *)i);   
      } else {
        sprintf(threadnames[i], "%s %ld bad", ourname, i);
        printf("Previous thread did not finish...\n");
      }
      tdecomps[i] = 0;
   }
   queueFlush(1);
   return 0;
}

int stopDecomp() {
   int i, j;

   stop_flag = 1;
   for (j = 0; j < 25; j++) {
      for (i = 0; i < MAX_THRDS; i++) {
         if (!threadDone[i]) break;
      }
      if (i >= MAX_THRDS) break;
      epicsThreadSleep(1.0);
   }
   return 0;
}

   
      


