program launcher 

option -c;

%%#include "launch_code.h"
%%#include <stdio.h>
%%#include <string.h>

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \
        t n##Save;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

string Program1;
assign Program1 to "";
string Program1Save;

string Program2;
assign Program2 to "";
string Program2Save;

int Instance1;

int Instance2;

int PID1;
assign PID1 to "";

int PID2;
assign PID2 to "";

int cryid1;
assign cryid1 to "";
int cryid1Save;
string cryid1buf;

int cryid2;
assign cryid2 to "";
int cryid2Save;
string cryid2buf;

int Status;
assign Status to "";

evflag launcherStarted1;
evflag launcherStarted2;

int waitstatus1;
int waitstatus2;




ss launcher {
   state init {
      when() {
          Instance1 = makeInstance(0);
          Instance2 = makeInstance(1);
          pvAssign(Program1, makePVName("_CS_Program1"));
          pvMonitor(Program1);
          pvAssign(Program2, makePVName("_CS_Program2"));
          pvMonitor(Program2);
          pvAssign(PID1, makePVName("_CV_PID1"));
          pvAssign(PID2, makePVName("_CV_PID2"));
          pvGet(PID1);
          pvGet(PID2);
          pvAssign(Status, makePVName("_CV_Status"));
          sprintf(cryid1buf, "Decomp%03d_CS_CryId", Instance1);
          pvAssign(cryid1, cryid1buf);
          pvMonitor(cryid1);
          sprintf(cryid2buf, "Decomp%03d_CS_CryId", Instance2);
          pvAssign(cryid2, cryid2buf);
          pvMonitor(cryid2);
      } state startup
   }
   state startup {
      when (pvChannelCount() == pvConnectCount()) {
         strcpy(Program1Save, Program1);
         strcpy(Program2Save, Program2);
         cryid1Save = cryid1;
         cryid2Save = cryid2;
      } state connected
      when (delay(1.0)) {
      } state startup
   }
   state connected {
      when (strcmp(Program1, Program1Save)) {
              strcpy(Program1Save, Program1);
              killProg(PID1);
      } state connected
      when (cryid1Save != cryid1) {
              cryid1Save = cryid1;
              killProg(PID1);
      } state connected
      when (strcmp(Program2, Program2Save)) {
              strcpy(Program2Save, Program2);
              killProg(PID2);
      } state connected
      when (cryid2Save != cryid2) {
              cryid2Save = cryid2;
              killProg(PID2);
      } state connected
      when (delay(1.0)) {
          efSet(launcherStarted1);
          efSet(launcherStarted2);
      } state connected
   }
}


ss watchPrograma {
    state init {
       when (efTest(launcherStarted1)) {
       } state waitprog
    }
    state startprog {
       when(PID1) {
       } state waitprog
       when() {
          PID1 = startProg(Program1,Instance1, cryid1, 20001);
          pvPut(PID1);
       } state waitprog
    }
    state waitprog {
       option -e;
       entry {
          waitstatus1 = waitProg(PID1);
       }
       when(waitstatus1 == 1) {
          PID1 = 0;
          pvPut(PID1);
       } state startprog
       when(waitstatus1 == -1 ) {
          killProg(PID1);
          PID1 = 0;
          pvPut(PID1);
       } state startprog
       when (delay(0.1)) {
          Status = 1;
          pvPut(Status);
       } state waitprog
    }
}

ss watchProgramb {
    state init {
       when (efTest(launcherStarted2)) {
       } state waitprog
    }
    state startprog {
       when(PID2) {
       } state waitprog
       when() {
          PID2 = startProg(Program2, Instance2, cryid2, 20002);
          pvPut(PID2);
       } state waitprog
    }
    state waitprog {
       option -e;
       entry {
          waitstatus2 = waitProg(PID2);
       }
       when(waitstatus2 == 1) {
          PID2 = 0;
          pvPut(PID2);
       } state startprog
       when(waitstatus2 == -1) {
          killProg(PID2);
          PID2 = 0;
          pvPut(PID2);
       } state startprog
       when (delay(1.0)) {
       } state waitprog
    }
}

