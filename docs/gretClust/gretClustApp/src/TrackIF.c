#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include <seqCom.h>
#include <epicsMutex.h>
#include <epicsThread.h>
#include <epicsMessageQueue.h>
#include <iocsh.h>

#include "queues.h"
#include "sortingList.h"
#include "GEBLink.h"
#include <ctk.h>

#define CHATFILE "placeholder"
int readChatFile(char *);
int setupTrack(void);
int trackEvent(TRACK_STRUCT *);

sortingList *tracks;

struct indata {
   struct gebData *data;
   struct indata *next;
};

struct indata *dataRoot, *dataEnd;

#define TRACK_PORT 9205

#define OUTPUT_SORT_LENGTH 20

static char *outputfile;

static epicsMutexId dataMutex;

static int recSock;

extern struct seqProgram Track;

int main(int argc,char *argv[]) {
    char * macro_def;
    epicsThreadId threadId;
    int callIocsh = 0;
    if(argc>1 && strcmp(argv[1],"-s")==0) {
        callIocsh=1;
        --argc; ++argv;
    }
    macro_def = (argc>1)?argv[1]:NULL;
    seqRegisterSequencerCommands();
    threadId = seq((void *)&Track, macro_def, 0);
    if(callIocsh) {
        iocsh(0);
    } else {
        epicsThreadExitMain();
    }
    return(0);
}


int trackInit(char *id, int *port) {

   char hname[80];
   struct sockaddr_in our_addr;
   struct hostent *hp;
   
   dataMutex = epicsMutexCreate(); 
   if (!dataMutex) {
      strcpy(id, "mutex create error");
      return 6;
   }

   recSock = socket(AF_INET, SOCK_STREAM, 0);
   if (recSock == -1) {
      strcpy(id, "socket error");
      return 1;
   }

   bzero((char *)&our_addr,sizeof(struct sockaddr_in));
   our_addr.sin_family = AF_INET;

   if  (gethostname(hname,79)) {
      strcpy(id, "host name error");
      return 2;
   }

   hp = gethostbyname(hname);
   if (!hp) {
      printf("hostname %s not resolved\n", hname);
      return 6;
   }
   our_addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)
                                (hp->h_addr_list[0]))));

   if (our_addr.sin_addr.s_addr == -1) {
      strcpy(id, "address error");
      return 3;
   }

   (*port)++;
   if (*port > (TRACK_PORT + 10))
      *port = TRACK_PORT;
   our_addr.sin_port = htons(*port);
   if (-1 == bind(recSock, (struct sockaddr *)&our_addr,
                        sizeof(struct sockaddr_in)))  {

      strcpy(id, "bind failed");
      return 4;
   }

   if (-1 == listen(recSock, 10)) {
      strcpy(id, "listen failed");
      return 5;
   }

   strcpy(id, hname);

   queueInit(100,100);
   tracks = sortingListCreate();
   if (!tracks) return 6;

   /* now do actual global (non-per-thread) tracking initialization */
   readChatFile(CHATFILE);
   setupTrack();

   return 0;
}

int getRequest() {

   int inSock;
   struct indata *outp;

   inSock = accept(recSock, 0, 0);
   if (inSock == -1) {
       printf("getRequest accept failed %s\n", strerror(errno));
       return -1;
   }
   epicsMutexLock(dataMutex);
   while (dataRoot) {
      outp = dataRoot;
      dataRoot = dataRoot->next;
      free(outp->data->payload);
      free(outp->data);
      free(outp);
   }
   dataEnd = 0;
   epicsMutexUnlock(dataMutex);

   return inSock;
}


int gebReader(int inSock) {
      
   int numret, bytesret;
   char *inbuffer;
   struct gebData *outp;
   struct indata *listout;

      outp = calloc(1, sizeof(struct gebData));
      if (!outp) {
         printf("Track acceptr reader closed: malloc failed\n");
         close(inSock);
         return -1;
      }

      numret = 0; bytesret = 0;
      while (bytesret < GEB_HEADER_BYTES) {
         numret = read(inSock, ((char *)outp) + bytesret, 
                                     GEB_HEADER_BYTES - bytesret);
         if (numret < 0) { 
            printf("read returned %d\n", numret); 
            numret = 0;
         }
         if (numret == 0) {
             printf("Track acceptor closed: End of File\n");
             free(outp);
             close(inSock);
             return -1;
         }
         bytesret += numret;
      }
      if (outp->type == GEB_TYPE_KEEPALIVE) {
         /* keepalive message */
         free(outp);
         return 0;
      }
      inbuffer = calloc(1, outp->length);
      if (!inbuffer) {
         printf("Track acceptor closed: malloc failed\n");
         free(outp);
         close(inSock);
         return -1;
      }
      bytesret = 0;
      while (bytesret < outp->length) {
         numret = read(inSock, inbuffer + bytesret, 
                                     outp->length - bytesret);
         if (numret < 0) { 
            printf("read 2 returned %d\n", numret); 
            numret = 0;
         }
         if (numret == 0) {
             printf("Track acceptor closed: End of File\n");
             free(outp);
             free(inbuffer);
             close(inSock);
             return -1;
         }
         bytesret += numret;
      }
      outp->payload = inbuffer;

      listout = calloc(1, sizeof(struct indata));
      if (listout == 0) {
          printf("Track acceptor closed: malloc failed\n");
          free(outp);
          free(inbuffer);
          close(inSock);
          return -1;
      }
      listout->data = outp;
      epicsMutexLock(dataMutex);

      if (dataRoot == 0) {
         dataRoot = listout;
      } else {
         dataEnd->next = listout;
      }
      dataEnd = listout;
      epicsMutexUnlock(dataMutex);

      return 0;
}

struct gebData *getACluster(long long timediff) {
/* note that TRACK_STRUCT has no room for aux data packets... */

   struct indata *temp, *prev;
   struct indata *retval;
   TRACK_STRUCT *returnval;
   int len = 0, i;

   epicsMutexLock(dataMutex);

   if (!dataRoot) {
      epicsMutexUnlock(dataMutex);
      return 0;
   }

   temp = dataRoot;

   while (temp->next && (temp->data->timestamp - 
                         temp->next->data->timestamp) < timediff) {
      temp = temp->next;
      len++;
   }
   /* if you reach end of list, check again later */
   if (!temp->next) {
      epicsMutexUnlock(dataMutex);
      return 0;
   }
   /* if dataroot exists and you don't reach end of list you will return 
    * at least one.
    */
   retval = dataRoot;
   dataRoot = temp->next;
   temp->next = 0;

   epicsMutexUnlock(dataMutex);

   /* now that you have a list, convert to TRACK_STRUCT */

   if (0 == len) return 0;

   returnval = calloc(1, sizeof(TRACK_STRUCT));
   if (!returnval) return 0;
   returnval->n = len;
   returnval->inp = calloc(len, sizeof(struct gebData));
   if (!returnval->inp) return 0;
   for (i=0; i<len; i++) {
      bcopy(retval->data, &(returnval->inp[i]), sizeof(struct gebData));
      temp = retval;
      retval = retval->next;
      free(temp->data);
      free(temp);
   }
   return returnval;
}

int setOutputFile(char *dirname, char *runname) {
   int pathlen;
   char *namebuf;

   if (!dirname || *dirname == 0) return -1;
   if (!runname || *runname == 0) return -1;

   pathlen = strlen(dirname) + strlen(runname) + 20;
   namebuf = (char *)calloc(pathlen, 1);
   snprintf(namebuf, pathlen, "%s/%s/Tracks.dat", dirname, runname);
   printf("new filename %s\n", namebuf);
   if (outputfile) free(outputfile);
   outputfile = namebuf;
   return 0;
}


int validOutputFile() {
   /* so far, just don't let it exist */
   struct stat retstat;

   return (stat(outputfile, &retstat));
}

int outfile;

/* returns 1 on success */
int openOutputFile()  {

    if (!validOutputFile()) return 0;

    outfile = open(outputfile, O_WRONLY);
    return ((outfile > 0)?1:0);
}



void trackShutdown() {
   close(recSock);
   printf("shutting down Track\n");
}

int trackRunning;


int gap = 5;

/* The following group of globals would need to be a separate copy for each
   thread to have multiple tracking threads.
 */

static CLUSTER Clstr[MAXCLUSTERHITS];
static int nClusters;
/* end global group */

void eachThread(void *tnum) {
   int qstat;
   int tstat;
   TRACK_STRUCT *track;
   int i;

   /* do any per-thread init */
  for (i=0;i<MAXCLUSTERHITS;i++)
  {
    Clstr[i].v = (CLUSTER_INTPTS *) malloc(MAX_CLUSTER_inp);
    printf("allocated %li bytes for track.v; ", MAX_CLUSTER_inp);
    printf("sizeof(CLUSTER_INTPTS)=%li\n", sizeof(CLUSTER_INTPTS));
  };

   while (trackRunning) {
      /* get a cluster */
      track = getACluster(gap);
      tstat = trackEvent(track);

      if (tstat == 0) {
         /* here we need to do something analogous to writeTrack(track); */
         /* put result on queue */
         qstat = 1;
         while (qstat && trackRunning) {
            /* raw data but with global coordinates */
            qstat = queueSend(0, (char *)track);
         }
      }
  }
}

/* Currently the trackLib code only supports one thread. */
#define TRACK_THREADS 1

int startTrack() {
   long i;
   void *temp;

   queueFlush(0);
   queueFlush(1);
   while ((temp = sortingListGet(tracks))) free(temp);
   trackRunning = 1;

   for (i=0; i < TRACK_THREADS; i++) {
      epicsThreadCreate("TrackThread", epicsThreadPriorityMedium, 16 * 1024,
                                       eachThread, (void *) i);
   }
   return 0;
}

void stopTrack() {
    trackRunning = 0;
    return;
}
/* Major problem here.  Used "TRACK_STRUCT" in the output functions below
 * but that is the input struct to tracking. It gets modified in TrackEvent,
 * and that is what is here, but there is additonal data currently stored 
 * elsewhere. 
 */

int writeToDisk(TRACK_STRUCT *input) {
   int numwrit = 0, outsize = 0;
   char *output;

   if (!input) 
      return -1;

   output = (char *)input;

   while (outsize < sizeof(TRACK_STRUCT)) {
      numwrit = write(outfile, output + outsize, 
                       sizeof(TRACK_STRUCT) - outsize);
      if (numwrit < 0) {
         printf("write error to disk %s\n", strerror(errno));
         return -1;
      }
      outsize += numwrit;
   }
   return 0;
}

int sortAndWrite() {
   int listtime = 0, istat = 1;
   TRACK_STRUCT *trackin, *trackout;
   char *inptr;

   while (istat && listtime < 10) {
      istat = queueReceive(0, &inptr);
      listtime++;
   }
   if (istat) {
      /* 1 seconds no data, return */
      return 0;
   }
   trackin = (TRACK_STRUCT *)inptr;
   sortingListAdd(tracks, trackin, trackin->inp->timestamp);
   while (sortingListWaterline(tracks) > OUTPUT_SORT_LENGTH) {
      trackout = sortingListGet(tracks);
      if (writeToDisk(trackout)) return -1; 
   }
   return 0;
}
         
int closeOutputFile() {
   TRACK_STRUCT *trackout;

   while ((trackout = sortingListGet(tracks))) {
      if (writeToDisk(trackout)) break; 
   }
   close(outfile);
   return 0;
}
