program Log ("instance=001")

option +d; 	/* debug (verbose) output */

%%#include <string.h>
%%#include "queues.h"
%%#include "LogIF.h"

#define DECLEVENT(t,n,s)        \
        t n;                    \
        assign n to #s;         \
        monitor n;              \
        evflag n##Event;        \
        sync n n##Event;        \
        t n##Save;

#define DECLMON(t,n,s)          \
        t n;                    \
        assign n to #s;         \
        monitor n;

#define DECL(t,n,s)             \
        t n;                    \
        assign n to #s;

#define DECLARRAY(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;

#define DECLARRAYMON(t,n,s,l)      \
        t n[l];                 \
        assign n to #s;		\
        monitor n;		

#define SENDSTATE(s) 		\
	strcpy(statepv, #s);	\
	pvPut(statepv);		\
	pvFlush();

DECL(int, port, Log_CV_Port)
DECL(string, logger, Log_CV_Addr)
DECLARRAYMON(char, runname, Data_CS_RunDir, 100)
DECLARRAYMON(char, dirname, Data_CS_Dir, 100)
DECLMON(short, startStop, Online_CS_StartStop)

DECL(int, Status, Decomp{instance}_CV_Status)
DECLMON(short, enable, Decomp{instance}_CS_Enable)
DECL(string, statepv, Decomp{instance}_CV_State)

evflag startInput;
evflag stopInput;
evflag startProcess;
evflag stopProcess;
evflag stopOutput;
evflag startOutput;
evflag doneFlag;
evflag logInited;

int initstat;
int reqstat;
int count;

char *procInput;


ss setupAndRun {
  state init {
     when() {
        pvGet(port);
        printf("port = %d\n", port);
        initstat = logInit(&logger[0], &port);
        pvPut(logger);
        pvPut(port);
        /* one-time initializations */
     } state initCheck
  }
  state initCheck {
     when(initstat !=0) {
        printf("Log initialization failed\n");
        SENDSTATE(Failed)
        logShutdown();
        exit(-1);
     } state restart
     when() { 
        efSet(logInited);
     } state restart
  }
  state offline {
    entry {
       SENDSTATE(Offline)
    }
    when(enable) {
    } state restart
  }
  state restart {
    when(!enable) {
    } state offline
    when(startStop != 0 ) {
    } state check
    when() {
    } state setup
  }
  state check {
    entry {
       SENDSTATE(Check)
    }
    when (!enable) {
    } state offline
    /* If we restart while things are running, do nothing. */
    when (startStop == 0) {
    } state restart
    when () {
    } state setup
  }
  state run {
    entry {
       SENDSTATE(Run)
    }
    when (!enable || startStop == 0) {
       efSet(stopProcess);
    } state waitfordone
  }
  state checkrun {
     when (openOutputFile(dirname, runname)) {
       efSet(startProcess);
       efClear(doneFlag);
    } state run
    when () {
    } state restart
  }
  state setup {
    entry {
       SENDSTATE(Setup)
    }
    when (!enable) {
    } state offline
    when (startStop == 1) { 
       printf("Start Command\n");
    } state checkrun
    when (delay(0.5)) {
       Status = 1;
       pvPut(Status);
    } state setup
  }
  state waitfordone {
    entry {
       SENDSTATE(Wait)
    }
    when(efTestAndClear(doneFlag)) {
    } state setup
  }
}

ss getData {
   state run {
       when (efTest(logInited)) {
           reqstat = getRequests();	/* never returns */
       } state teststat 
   }
   state teststat {
      when (delay(0.1)) {
      } state run
   }
}

ss process {
   state init {
     entry {
        printf("entering process:init\n");
     }
      when() {
      } state stopped
   }
   state stopped {
     entry {
        printf("entering process:stopped\n");
     }
      when (efTestAndClear(startProcess)) {
          startLog();
          efClear(stopProcess);
      } state run
   }
   state run {
      when (efTestAndClear(stopProcess)) {
         efSet(stopProcess);
      } state cleanup
      when() {
         count = 0;
         while (count < 100) {
            if ( 0 == nextEntry(&procInput)) {
              writeToDisk(procInput);
              printf("log message written to disk\n");
            } else {
              epicsThreadSleep(1.0);
              count += 100;
            }
            count++;
         }
         Status = 1;
         pvPut(Status);
      } state run
   }

   state cleanup {
     entry {
        printf("entering process:cleanup\n");
     }
      when() {
         queueFlush(1);
         closeOutputFile();
         efSet(doneFlag);
      } state stopped
   } 
}

