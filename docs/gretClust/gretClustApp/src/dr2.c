#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define LINELEN 12

int main(int argc, char *argv[]) {

    unsigned int inbuf[LINELEN/4];
    unsigned int outbuf[LINELEN/4];
    FILE *inf;
    int i;
    
    if (argc != 2) {
       printf("Usage: %s <input file>", argv[0]);
       exit(1);
    } 

    inf = fopen(argv[1], "r"); 

    if (!inf) {
       printf("Cannot open %s for reading\n", argv[1]);
       exit(2);
    }

    while (fread(&inbuf, LINELEN, 1, inf)) {
        swab(inbuf, outbuf, LINELEN);
        for (i=0; i < LINELEN/4; i++) {
          printf("0x%08x %08d\t", outbuf[i], (int)outbuf[i]);
        }
        printf("\n");
    }
    return 0;
}
