#define TRACK_PORT 9505

int trackInit(char *, int *);
char *setOutputFile(char *dirname, char *runname);
int validOutputFile();
void trackShutdown();

int getRequest();
int gebReader(int inSock);
int startTrack();
int stopTrack();
int sortAndWrite();
int openOutputFile();
int closeOutputFile();

