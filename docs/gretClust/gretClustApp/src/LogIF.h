int logInit(char *, int *);
int validOutputFile(char *dir, char *base);
void logShutdown();

int getRequests();
int nextEntry(char **in);
int startLog();
int openOutputFile(char *dir, char *base);
int closeOutputFile();
int writeToDisk(char *out);

