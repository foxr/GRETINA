#!/usr/bin/python
from wxPython.wx import *
import wx.lib.dialogs
import os
import time
import os.path
import shutil
from CaMonitor import *
import time
import sys

#
# Root window
#

class secondsFrame(wxFrame):
   def  __init__(self, parent,ID,title):
      wxFrame.__init__(self, parent, ID, title, wxDefaultPosition, wxSize(300,150))
      #
      # status bar at bottom
      #
      self.CreateStatusBar()
      self.SetStatusText("GRETINA DAQ timestamp seconds")
      
      #
      #  menubar at top
      #
      menu = wxMenu()
      self.menuAppend(menu, 'Exit', 'Exit Program', self.OnQuit)

      menuBar = wxMenuBar()
      menuBar.Append(menu, "&File")

      self.SetMenuBar(menuBar)

      sizer = wxBoxSizer(wxHORIZONTAL)

      self.secArea = SecArea(self)
      sizer.Add(self.secArea, 0, wxGROW|wxALL, 5)
     
      self.SetSizer(sizer)
      self.SetAutoLayout(True)
      sizer.Fit(self)


   #
   # Handle exit selected from file menu
   #
   def OnQuit(self, event):
      print 'Bailing...'
      self.Close(true)

   def menuAppend(self, menu, text, help, func):
      mi = wxMenuItem(menu,wxNewId(), text, help)
      self.Bind(wx.EVT_MENU, func, mi)
      menu.AppendItem(mi)

class connEvent(baseEvent):
   def callback(self):
         app.frame.connectArea.setConnection(CaConnect.caConnected())

ConnEvent = connEvent()

class SecArea(wxBoxSizer):

   def __init__(self, parent):
      wxBoxSizer.__init__(self,wxHORIZONTAL)
      self.winda = parent
      label = wxStaticText(self.winda, -1, "Second: ")
      self.Add(label, 0, wxALL|wxALIGN_CENTER, 5)
      self.TextUpdate = wxTextCtrl(self.winda,-1,"",size=(80,-1),
                                       style = wxTE_READONLY)
      self.Add(self.TextUpdate, 0, wxGROW|wxALL, 5)
      self.sec = str(0)
      self.TextUpdate.AppendText(self.sec)
      self.timestampe = CaMonitor('Dig1_CV_EBTS', ca.DBF_STRING,
                                                             updateSec(), 
                                                             ConnEvent)

class updateSec(baseEvent):
   def callback(self):
      try:
         timestamp = app.frame.secArea.timestamp.getCache()
         timestamp = timestamp / 100000000
         app.frame.secArea.TextUpdate.Clear()
         app.frame.secArea.sec = str(timestamp)
         app.frame.secArea.TextUpdate.AppendText(app.frame.secArea.sec)
      except CaMonitor.NoValueYetException: 
         print "EPICS value not valid", app.frame.secArea.sec
        
       
#
# Establish and run application
#
class secondsApp(wxApp):
   def OnInit(self):
      self.frame = secondsFrame(NULL,-1,"GRETINA Run Control")

      # overall window setup
      self.frame.Show(true)
      self.SetTopWindow(self.frame)

      # start channel access polling
      self.polltimer = wx.PyTimer(self.pollCa)
      self.polltimer.Start(200)

      return true

   def pollCa(self):
      ca.poll()
      self.polltimer.Start(200)




app = secondsApp(0)


app.MainLoop()      

