struct gebData {
int type;
int length;     /* of payload in bytes */
long long timestamp;
void *payload;
};
#define GEB_TYPE_KEEPALIVE 	0
#define GEB_TYPE_DECOMP 	1

#define GEB_PORT 9005
#define GEB_HEADER_BYTES 16

#include "GEBLink.h"

struct gebClient {
   epicsMutexId connectionMutex;
   int outSock;
   int bigendian;
   struct gebData zeromsg;
};

struct gebClient *GEBClientInit();

int sendGEBData(struct gebClient *, struct gebData *);
int setGEBClient(struct gebClient *, char *addr, int port);
void closeGEBClient(struct gebClient *);
int checkGEBClient(struct gebClient * );

