<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
                      "file:///usr/share/xml/docbook/schema/dtd/4.3/docbookx.dtd
"
>
<book>
    <bookinfo>
      <title>Gretina Global Event Builder</title>
      <author><firstname>Ron</firstname><surname>Fox</surname></author>
        <copyright>
            <year>2012</year>
            <holder>Board of Trustess of Michigan State University</holder>
        </copyright>
 
      <revhistory>
          <revision>
             <revnumber>1.0</revnumber>
             <date>March 20, 2012</date>
             <authorinitials>RF</authorinitials>
             <revremark>Original Release</revremark>
          </revision>
      </revhistory>
    </bookinfo>
    <chapter>
        <title>Introduction</title>
        <para>
            The Gretina global event builder accepts event fragments from
            multiple sources and produces a totally time ordered output
            stream of fragments.  Software that receives these fragments
            can then make time slices to build events.
        </para>
        <para>
            This document describes the event builder software interface.
        </para>
        <section>
            <title>Basic concepts</title>
            <para>
                Using the GEB is a cyclical process.  After using
                <function>GEBClientInit</function> to initialize the
                software, the client should wait until a run starts. Once the run
                starts, <function>setGEBClient</function> can be called
                to form a connection with the GEB at a specified
                DNS hostname and port.  This connection is only
                valid for the length of the run.  Alternatively, client code
                can do one of the following:
                <orderedlist>
                    <listitem>
                        <para>Retry indefinitely until a connection is established</para>
                    </listitem>
                    <listitem>
                        
                        <para>
                            Use EPICS to monitor the state of the
                            <varname>Cluster_CV_State</varname> process variable.
                            Gretina will start taking data when this channel
                            transitions to the <literal>Run</literal> state.
                            At that time, clients can connect to the GEB.
                        </para>
                    </listitem>     
                </orderedlist>
            </para>
            <para>
                Once connected, <function>sendGEBData</function> is used
                to transmit event fragments to the GEB and
                <function>checkGEBClient</function> to check if the GEB has
                dropped the connection to the client.
            </para>
            <para>
                For more information, see the reference material.
            </para>
        </section>
        <section>
            <title>Software Prerequisistes</title>
            <para>
                The following software is a prerequisite to installing
                the GEB interface library.
            </para>
            <itemizedlist>
                <listitem>
                    <para>The EPICS control system</para>
                </listitem>
                <listitem>
                    <para>An  ANSI compliant C compiler</para>
                </listitem>
                <listitem>
                    <para>gnuMake</para>
                </listitem>
            </itemizedlist>
        </section>
        <section>
            <title>Software installation</title>
            <para>
                Yet to be written.
            </para>    
        </section>
    </chapter>
    <chapter>
        <title>Reference material</title>
        <refentry id="geb3_GEBClientInit">
          <refmeta>
             <refentrytitle>GEBClientInit</refentrytitle>
             <manvolnum>3Gretina</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>GETBClientInit</refname>
             <refpurpose>Initialize GEB clients.</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <synopsis>
#include &lt;errno.h&gt;
#include &lt;GEBClient.h&gt;
            </synopsis>
            <funcsynopsis>
            <funcprototype><funcdef>struct getbClient* <function>GEBClientInit</function></funcdef>
            <void />
            </funcprototype>
            </funcsynopsis>
          </refsynopsisdiv>
          <refsect1>
             <title>PARAMETERS</title>
             <para>
                None
             </para>
          </refsect1>
          <refsect1>
             <title>
            DESCRIPTION
             </title>
             <para>
                Initializes the client software library.  Note that this does not
                connect to a GEB.  To do that, <function>setGEBClient</function>
                must be called when the run is active.
             </para>
          </refsect1>
          <refsect1>
            <title>RETURNS</title>
            <para>
                If not null, a pointer to a <literal>gebClient</literal>
                struct.  This pointer should be treated as an opaque pointer by
                the client software.
            </para>
            <para>
                If null, an initialization error occured and the reason for
                the error can be found in <varname>errno</varname>
            </para>
          </refsect1>
          <refsect1>
            <title>EXAMPLES</title>
            <informalexample>
                <programlisting>
#include &lt;errno.h&gt;
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

#include &lt;GEBClient.h&gt;

...
struct gebClient* pClient;
..
pClient = GEBClientInit();
if (!pClient) {
  perror("Could not initialize Gretina GEB client software");
  exit(-1);
}
                </programlisting>
            </informalexample>
          </refsect1>
        </refentry>
        <refentry id="geb3_setclient">
          <refmeta>
             <refentrytitle>setGEBClient</refentrytitle>
             <manvolnum>3GEB</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>setGEBClient</refname>
             <refpurpose>Select and connect to GEB</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <synopsis>

#include &lt;GEBClient.h&gt;
            </synopsis>
            <funcsynopsis>
            <funcprototype><funcdef>int <function>setGEBClient</function></funcdef>
            <paramdef>
             struct gebClient*
              <parameter>pClient</parameter>
            </paramdef>
            <paramdef>
                char*<parameter>host</parameter>
            </paramdef>
            <paramdef>
                int<parameter>port</parameter>
            </paramdef>
            </funcprototype>
            </funcsynopsis>            
          </refsynopsisdiv>
          <refsect1>
             <title>PARAMETERS</title>
             <variablelist>
                <varlistentry>
                    <term><type>struct gebClient*</type> <parameter>pClient</parameter></term>
                    <listitem>
                        <para>
                            The client opaque pointer returned by a successful
                            call to <function>GEBClientInit</function>
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><type>char*</type><parameter>host</parameter></term>
                    <listitem>
                        <para>
                            The DNS name of the host that is running the
                            GEB.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><type>int</type><parameter>port</parameter></term>
                    <listitem>
                        <para>
                            The port on which the GEB is listening for connections.
                            The port is provided in host byte ordering,
                            <function>htons</function> is called within
                            this function to convert it to network byte ordering.
                        </para>
                        <para>
                            Normally the value of this parameter is
                            <literal>GEB_PORT</literal>.
                        </para>
                    </listitem>
                </varlistentry>
             </variablelist>
          </refsect1>
          <refsect1>
             <title>
            DESCRIPTION
             </title>
             <para>
                Selects the host and port on which to connect to an event builder
                and forms the connection.
             </para>
          </refsect1>
          <refsect1>
            <title>RETURNS</title>
            <para>
                On success <literal>0</literal> is returned. On error a message
                is emitted to stdout describing the error and a small positive
                number is returned.  Unfortunately the error details (e.g.
                <varname>errno</varname>) are not preserved and therefore cannot
                be analyzed nor presented to the user.
            </para>
            <para>
                The possible return error codes include:
            </para>
            <orderedlist>
                <listitem>
                    <para>Unable to convert the IP address that corresponds
                        to the host name to internal format
                        (<function>inet_addr</function>(3) failed).
                    </para>
                </listitem>
                <listitem>
                    <para><function>socket</function>(2) call failed.</para>
                </listitem>
                <listitem>
                    <para>Connection with GEB failed</para>
                </listitem>
                <listitem>
                    <para>
                        Unable to resolve <parameter>pHost</parameter> to an
                        IP address via DNS.
                    </para>
                </listitem>
            </orderedlist>
          </refsect1>
          <refsect1>
            <title>EXAMPLE</title>
            <informalexample>
                <programlisting>
#include &lt;GEBClient.h&gt;
...
struct gebClient* pClient;
const char*       pHost="geb.gretina.cluster"; /* This name is made up */
int status;
...
pClient = GEBClientInit();
...
status = setGEBClient(pclient, (char*)pHost, GEB_PORT);
if (status) {
  exit(-1);                /* Error message already emitted. */
}
                </programlisting>
            </informalexample>
          </refsect1>
        </refentry>
        <refentry id="geb3_sendGEBData">
          <refmeta>
             <refentrytitle>sendGEBData</refentrytitle>
             <manvolnum>3geb</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>sendGEBData</refname>
             <refpurpose>Send event fragments to the GEB.</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <synopsis>
#include &lt;GEBClient.h&gt;
            </synopsis>
            <funcsynopsis>
            <funcprototype><funcdef>int <function>sendGEBData</function></funcdef>
            <paramdef>
              struct gebClient*
              <parameter>pClient</parameter>
            </paramdef>
            <paramdef>
                struct gebData*
                <parameter>pData</parameter>
            </paramdef>
            </funcprototype>
            </funcsynopsis>            
          </refsynopsisdiv>
          <refsect1>
             <title>PARAMETERS</title>
             <variablelist>
                <varlistentry>
                    <term><type>struct gebClient*</type><parameter>pClient</parameter></term>
                    <listitem>
                        <para>
                            <type>struct gebClient*</type> returned from a successful
                            call to <function>GEBClientInit</function>.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><type>struct gebData*</type><parameter>pData</parameter></term>
                    <listitem>
                        <para>
                            Describes the event fragments to send to the GEB.
                            See DESCRIPTION below for more information.
                        </para>
                    </listitem>
                </varlistentry>
             </variablelist>
          </refsect1>
          <refsect1>
             <title>
            DESCRIPTION
             </title>
             <para>
                Sends fragments to the GEB.  The fragments to send are
                defined by the <parameter>pData</parameter> pointer which
                points to a linked list of fragments to send.
                <parameter>pData</parameter> is a pointer to a
                <type>struct gebData</type> which has the following fields:
             </para>
             <fieldsynopsis>
                <type>int</type><varname>type</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>int</type><varname>length</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>long long</type><varname>timestamp</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>void*</type><varname>payload</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>short</type><varname>refCount</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>short</type><varname>refIndex</varname>
             </fieldsynopsis>
             <fieldsynopsis>
                <type>struct gebData*</type><varname>next</varname>
             </fieldsynopsis>
             <variablelist>
                <varlistentry>
                    <term><varname>type</varname></term>
                    <listitem>
                        <para>
                            Describes the type of data in the fragment. Gretina
                            defines several types and ancillary detectors may
                            define other types.
                        </para>
                        <table><title>Gretina fragment types</title>
                            <tgroup cols='2' colsep='1' rowsep='1'>
                                <thead>
                                    <row>
                                        <entry>data type</entry>
                                        <entry>Explanation</entry>
                                    </row>
                                </thead>
                                <tbody>
                                    <row>
                                        <entry><literal>GEB_TYPE_KEEPALIVE</literal></entry>
                                        <entry>Keep alive message. Echoed back
                                            without processing by the GEB.
                                        </entry>
                                    </row>
                                    <row>
                                        <entry><literal>GEB_TYPE_DECOMP</literal></entry>
                                        <entry>Decomposed data(?)</entry>
                                    </row>
                                    <row>
                                        <entry><literal>GEB_TYPE_RAW</literal></entry>
                                        <entry>Raw digitizer data (waveforms).</entry>
                                    </row>
                                    <row>
                                        <entry><literal>GEB_TYPE_TRACK</literal></entry>
                                        <entry>Output of the tracker</entry>
                                    </row>
                                    <row>
                                        <entry><literal>GEB_TYPE_BGS</literal></entry>
                                        <entry>Data from the BGS (ancillary detector).
                                        </entry>
                                    </row>
                                </tbody>
                            </tgroup>
                        </table>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>length</varname></term>
                    <listitem>
                        <para>
                            The number of bytes in the fragment data (see
                            <varname>payload</varname> below).
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>timesstamp</varname></term>
                    <listitem>
                        <para>
                            Timestamp.  This is the value of the timestamp'
                            clock at the time the event was taken.  It is used
                            to provide the total time ordering of event fragments
                            that is the output of the GEB.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>payload</varname></term>
                    <listitem>
                        <para>
                            The fragment data itself.  This should be a pointer
                            to <varname>length</varname> bytes of data that
                            will be carried along with this fragment without
                            interpretation.
                        </para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>refCount</varname></term>
                    <listitem>
                        <para>??</para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>refIndex</varname></term>
                    <listitem>
                        <para>???</para>
                    </listitem>
                </varlistentry>
                <varlistentry>
                    <term><varname>next</varname></term>
                    <listitem>
                        <para>
                            Link to the next fragment to send to the
                            GEB (or is this only used in tap outputs and
                            internally to Gretina)?  That is can I actually send
                            a chain of fragments or do I need to send them one at
                            a time.
                        </para>
                    </listitem>
                </varlistentry>
             </variablelist>
          </refsect1>
          <refsect1>
            <title>RETURNS</title>
            <para>
                <literal>0 </literal> is returned on success.  Otherwise a
                small integer is returned, and an error message is printed to
                <literal>stdout</literal>.  Due to this error message, information
                in <varname>errno</varname> is no longer relevant to the
                reason for the error at the time the error ocured.
            </para>
            <para>
                Legal integera values include:
            </para>
            <table><title>Gretina fragment types</title>
                 <tgroup cols='2' colsep='1' rowsep='1'>
                     <thead>
                         <row>
                             <entry>Return value</entry>
                             <entry>Explanation</entry>
                         </row>
                     </thead>
                     <tbody>
                         <row>
                            <entry>1</entry>
                            <entry>Write to socket failed</entry>
                         </row>
                         <row>
                            <entry>2</entry>
                            <entry><varname>length</varname> field is
                            &lt;= 1, or the <varname>payload</varname>
                            field is null.
                            </entry>
                         </row>
                         <row>
                            <entry>3</entry>
                            <entry>Socket link is closed</entry>
                         </row>
                     </tbody>
                 </tgroup>
             </table>
          </refsect1>
          <refsect1>
            <title>EXAMPLE</title>
            <para>
                This example assumes the existence of <varname>pGeb</varname>
                 a pointer returned from <function>GEBClientInit</function>.
                 It also assumes the existence of <function>getFragment</function>
                 which returns the next fragment to send, <function>freeFragment</function>
                 which frees the storage allocated by <function>getFragment</function>,
                 and finally <function>connectGEB</function> which invokes
                 <function>setGEBClient</function> repeatedly until the connection
                 is established.
            </para>
            <informalexample>
                <programlisting>
#include &lt;GEBClient.h%gt;

...

struct gebData* pData;
int    status;
while (1) {
    pDate = getFragment();
    status = sendGEBData(pGeb, pData)
    freeFragment(pData);
    if (status == 3) {  /* GEB Dropped link. fragment is orhpaned from last run*/
        connectGEB(pGeb);     
   } else if (status != 0) {
        exit(-1);             // Serious problem.
   }
}
                </programlisting>
            </informalexample>
          </refsect1>
        </refentry>
        <refentry id="geb3_closeGEBClient">
          <refmeta>
             <refentrytitle>closeGEBClient</refentrytitle>
             <manvolnum>3geb</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>closeGEBClient</refname>
             <refpurpose>Close connection to a GEB</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <synopsis>
#include &lt;GEBClient.h&gt;
            </synopsis>
            <funcsynopsis>
            <funcprototype><funcdef>void <function>closeGEBClient</function></funcdef>
            <paramdef>
                struct gebClient*
              <parameter>pClient</parameter>
            </paramdef>
            </funcprototype>
            </funcsynopsis>            
          </refsynopsisdiv>
          <refsect1>
             <title>PARAMETERS</title>
             <variablelist>
                <varlistentry>
                    <term><type>struct gebClient*</type><varname>pClient</varname></term>
                    <listitem>
                        <para>
                            A pointer returned from a successful call to
                            <function>GEBClientInit</function>
                        </para>
                    </listitem>
                </varlistentry>
             </variablelist>
          </refsect1>
          <refsect1>
             <title>
            DESCRIPTION
             </title>
             <para>
                Closes the current connection to the GEB.  This closes the
                socket and releases any resources associated with the
                connection.  The <varname>pClient</varname> remains
                valid and can be used in another call to
                <function>setGEBClient</function> to open a new connection
                later in the life of the program.
             </para>
          </refsect1>
        </refentry>
        <refentry id="geb3_checkGEBClient">
          <refmeta>
             <refentrytitle>checkGEBClient</refentrytitle>
             <manvolnum>3geb</manvolnum>
          </refmeta>
          <refnamediv>
             <refname>checkGEBClient</refname>
             <refpurpose>Check link liveness.</refpurpose>
          </refnamediv>
          
          <refsynopsisdiv>
            <synopsis>
#include &lt;GEBClient.h&gt;
            </synopsis>
            <funcsynopsis>
            <funcprototype><funcdef>int <function>checkGEBClient</function></funcdef>
            <paramdef>
                struct gebClient*
              <parameter>pClient</parameter>
            </paramdef>
            </funcprototype>
            </funcsynopsis>            
          </refsynopsisdiv>
          <refsect1>
             <title>PARAMETERS</title>
             <variablelist>
                <varlistentry>
                    <term><type>struct gebClient*</type><varname>pClient</varname></term>
                    <listitem>
                        <para>
                            Pointer returned from a successful call to
                            <function>GEBClientInit</function>.
                        </para>
                    </listitem>
                </varlistentry>
             </variablelist>
          </refsect1>
          <refsect1>
             <title>
            DESCRIPTION
             </title>
             <para>
                Checks the liveness of thelink with the current GEB
                client.  This is done by attempting to send and
                receive a keepalive packet to the GEB.
             </para>
          </refsect1>
          <refsect1>
            <title>RETURNS</title>
            <para>
                <literal>1</literal> if the link is still alive.
                <literal>0</literal> if not, in which case the
                function also prints an error message to
                <literal>stdout</literal>
            </para>
            <note>
                <title>NOTE</title>
                <para>
                    If the link is no longer alive, the function also performs
                    the actions of <function>closeGEBClient</function>
                </para>
            </note>
          </refsect1>
        </refentry>


    </chapter>
    
</book>
